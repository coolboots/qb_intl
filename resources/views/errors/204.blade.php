@extends('layouts/frontend')
@section('content')

 <div class="main-content">
    <section class="inner-header divider">
      <div class="container pt-5 pb-5">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row"> 
            <div class="col-md-12 xs-text-center">
              <h3 class="font-28">Error Code:204 - No Relevant Search Found!</h2>
              
            </div>
          </div>
        </div>
      </div>
    </section>
</div>

@endsection
