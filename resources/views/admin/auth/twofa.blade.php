<!-- Main Content --> <!DOCTYPE html> 
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- CSRF Token --> 
      <meta name="csrf-token" content="kNoHyPJfF47uKRDGaXicdER1FmCJXshvrZZCHzG2">
      <title>2FA Authentication :: MGL</title>
      <!-- Styles --> 
      <link href="{{ asset('/css/app.css')}}" rel="stylesheet">
      <!-- Scripts --> <script> window.Laravel = {"csrfToken":"kNoHyPJfF47uKRDGaXicdER1FmCJXshvrZZCHzG2"} </script> 
      <style type="text/css"> .navbar-text, .navbar-brand, .navbar-default .navbar-nav>li>a, .navbar-default .navbar-text{color:#fff !important;} </style>
   </head>
   <body>
      <nav class="navbar navbar-default navbar-static-top" style="background: #3d4c5a !important">
         <div class="container">
            <div class="navbar-header">
               <!-- Collapsed Hamburger --> <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse"> <span class="sr-only">Toggle Navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button> <!-- Branding Image --> <a class="navbar-brand" href="#"> 2FA Authentication - MGL Connect </a> 
            </div>
            <div class="collapse navbar-collapse" id="app-navbar-collapse">
               <!-- Left Side Of Navbar --> 
               <ul class="nav navbar-nav"> &nbsp; </ul>
              
            </div>
         </div>
      </nav>
      <div class="container">
         <div class="row">
            <div class="col-md-8 col-md-offset-2">
               <div class="panel panel-default">
                  <div class="panel-heading">OTP Verification</div>
                  <div class="panel-body">
                  	<?php 
                  	echo $TWOFA['message'];?> 
                     <form class="form-horizontal" role="form" method="POST" action="">
                        {{ csrf_field() }}
                        <div class="form-group">
                           <label for="email" class="col-md-4 control-label">OTP Code</label> 
                           <div class="col-md-6"> <input id="otp_code" type="text" class="form-control" name="otp_code" value=""> </div>
                        </div>
                        <div class="form-group">
                           <div class="col-md-6 col-md-offset-4"> <button type="submit" class="btn btn-primary"> Submit</button> </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Scripts --> <script src="{{ asset('/js/app.js')}}"></script> 
   </body>
</html>
