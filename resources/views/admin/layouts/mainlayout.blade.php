<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Admin Panel</title>
<link rel="icon" type="image/ico" href="{{asset('/ba/assets/images/favicon.ico')}}" />
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Stylesheets  -->
<!-- vendor css files -->
<link rel="stylesheet" href="{{asset('/ba/assets/css/vendor/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('/ba/assets/css/vendor/animate.css')}}">
<link rel="stylesheet" href="{{asset('/ba/assets/css/vendor/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{asset('/ba/assets/js/vendor/chosen/chosen.css')}}">
<link rel="stylesheet" href="{{asset('/ba/assets/js/vendor/animsition/css/animsition.min.css')}}">
<link rel="stylesheet" href="{{asset('/ba/assets/js/vendor/toastr/toastr.min.css')}}">
<!-- project main css files -->
<link rel="stylesheet" href="{{asset('/ba/assets/css/main.css')}}">
<!--/ stylesheets -->

<script src="{{asset('/ba/assets/js/vendor/jquery/jquery-1.11.2.min.js')}}"></script> 

<!--Modernizr-->
<script src="{{asset('/ba/assets/js/vendor/modernizr/modernizr-2.8.3-respond-1.4.2.min.js')}}"></script>
<!--/ modernizr -->


<!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>;
        var base_url = "<?php echo URL::to('/');?>";
    </script>
</head>
<body id="pixel" class="appWrapper">
<!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]--> 
<!-- Application Content -->
<div id="wrap" class="animsition"> 
  <!-- HEADER Content -->
  <section id="header">
    <header class="clearfix"> 
      <!-- Branding -->
      <div class="branding"> <a class="brand" href="index-2.html"><span>ADMIN <strong>V1</strong></span></a> <a role="button" tabindex="0" class="offcanvas-toggle visible-xs-inline"><i class="fa fa-bars"></i></a> </div>
      <!-- Branding end --> 
      <!-- Left-side navigation -->
      <ul class="nav-left pull-left list-unstyled list-inline">
        <li class="sidebar-collapse"><a role="button" tabindex="0" class="collapse-sidebar"><i class="fa fa-outdent"></i></a></li>
        
      </ul>
      <!-- Left-side navigation end --> 
      
     
      
      <!-- Right-side navigation -->
      <ul class="nav-right pull-right list-inline">
       @if (auth()->guard('admin')->guest())
                        <li><a href="{{ url('/admin/login') }}">Login</a></li>
                        <!-- <li><a href="{{ url('/admin/register') }}">Register</a></li> -->
                    @else
        <li class="dropdown nav-profile"> <a href class="dropdown-toggle" data-toggle="dropdown"> <img src="{{asset('/ba/assets/images/profile-photo.jpg')}}"" alt="" class="br-5 size-30x30"> <span>{{ auth()->guard('admin')->user()->name }}  <i class="fa fa-angle-down"></i></span> </a>
          <ul class="dropdown-menu animated littleFadeInDown br-5" role="menu">
            <li> <a href="{{url(str_replace('/','',getPrefix()).'/settings')}}" role="button" tabindex="0"><i class="fa fa-cog"></i>Settings</a></li>
            <li class="divider"></li>
            <li><a href="{{ url('/admin/logout') }}" role="button" tabindex="0"><i class="fa fa-sign-out"></i>Logout</a></li>
          </ul>
        </li>
        @endif
        
    </ul>
      <!-- Right-side navigation end --> 
    </header>
  </section>
  <!--/ HEADER Content  --> 
  
  <!--  CONTROLS Content -->
  <div id="controls"> 
    <!--SIDEBAR Content -->
    <aside id="sidebar">
      <div id="sidebar-wrap">
        <div class="panel-group slim-scroll" role="tablist">
          <div class="panel panel-default">
            <div class="panel-heading" role="tab">
              <h4 class="panel-title"><a data-toggle="collapse" href="#sidebarNav">Navigation <i class="fa fa-angle-up"></i></a></h4>
            </div>
            <div id="sidebarNav" class="panel-collapse collapse in" role="tabpanel">
              <div class="panel-body"> 
                <!--  NAVIGATION Content -->

                {!!prepareAdminUrl()!!}
               
                <!--/ NAVIGATION Content --> 
              </div>
            </div>
          </div>
         
        </div>
      </div>
    </aside>
    <!--/ SIDEBAR Content --> 
    
  </div>
  <!--/ CONTROLS Content --> 
  
  <!-- CONTENT -->

  <section id="content">
    <div class="overlay_loader"> <div class="loader"> </div> </div>
    <div class="page">
       @yield('content')
      
    </div>
  </section>
  <!--/ CONTENT --> 
  
</div>
<!--/ Application Content --> 

<!-- Vendor JavaScripts --> 
<script src="{{asset('/ba/assets/js/vendor/jquery/jquery-1.11.2.min.js')}}"></script> 
<script src="{{asset('/ba/assets/js/vendor/bootstrap/bootstrap.min.js')}}"></script> 
<script src="{{asset('/ba/assets/js/vendor/jRespond/jRespond.min.js')}}"></script> 
<script src="{{asset('/ba/assets/js/vendor/sparkline/jquery.sparkline.min.js')}}"></script> 
<script src="{{asset('/ba/assets/js/vendor/slimscroll/jquery.slimscroll.min.js')}}"></script> 
<script src="{{asset('/ba/assets/js/vendor/animsition/js/jquery.animsition.min.js')}}"></script> 
<script src="{{asset('/ba/assets/js/vendor/daterangepicker/moment.min.js')}}"></script> 
<script src="{{asset('/ba/assets/js/vendor/screenfull/screenfull.min.js')}}"></script> 
<script src="{{asset('/ba/assets/js/vendor/chosen/chosen.jquery.min.js')}}"></script>
<!--/ vendor javascripts --> 


<!--/ custom javascripts --> 
<script type="text/javascript" src="{{asset('/ba/tools/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript" src="{{asset('/ba/tools/ckeditor/adapters/jquery.js')}}"></script>


<!--dynamic activity-->
<script src="{{asset('/ba/vue/library/axios.js')}}"></script> 
<!--popup notification -->
<script src="{{asset('/ba/assets/js/vendor/toastr/toastr.min.js')}}"></script> 

<!-- Custom JavaScripts --> 
<script src="{{asset('/ba/assets/js/main.js')}}"></script> 
<script src="{{asset('/ba/assets/js/rkcommon.js')}}"></script> 

@yield('dynamic_js')

<!-- Modal -->
<div class="modal fade" id="defaultModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">

    <div class="modal-content">
      <?php /*<div class="modal-header">
        <h3 class="modal-title custom-font"></h3>
      </div> */ ?>
      <div class="modal-header" style="border:none;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body defaultModalBodyContainer">  </div>
     
    </div>
  </div>
</div>


</html>
