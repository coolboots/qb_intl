@extends('admin.layouts.mainlayout')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Access Not Granted</div>
                <div class="panel-body">
                    <h1>You are not authorised to access this page.</h1>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
