<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" value="{{ csrf_token() }}"/>
    <link href="{{ mix('css/app.css') }}" type="text/css" rel="stylesheet"/>
    <link rel="manifest" href="/manifest.json"/>
    <meta name="robots" content="noindex" />
    <link rel="shortcut icon" href="{{asset('/assets/images/favicon.png')}}">
    <!--[if IE]><link rel="shortcut icon" href="{{asset('/assets/images/favicon.png')}}"><![endif]-->
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('/assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('/assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('/assets/css/responsive.css')}}">
    <link rel="stylesheet" href="{{asset('/assets/css/modelstyle.css')}}">
    <title>Qureka Lite</title>
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <?php
    $ga_id = getGAId();
    if(!empty($ga_id))
    {
        ?>
        <script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $ga_id;?>"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());gtag('config', "<?php echo $ga_id;?>");
        </script>
        <?php
    }
    ?>
  </head>
  <body>
    <main class="app-wrapper-container dashboard-wrapper">
     @yield('content')
    </main>

    <!-- jQuery and Bootstrap Bundle (includes Popper) -->
   
    
    <script src="{{ mix('js/app.js') }}" type="text/javascript"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js"></script>
    
    <script src="{{asset('/assets/js/custom.js')}}"></script>
   
    <!-- <script type="application/javascript" src="https://sdki.truepush.com/sdk/v2.0.2/app.js" async></script> -->
    <script>
    // var truepush = window.truepush || [];
            
    // truepush.push(function(){
    //     truepush.Init({
    //         id: "5ff8a3d69032769ba8ab8d7a"
    //     },function(error){
    //       if(error) console.error(error);
    //     })
    // })
    </script>

  </body>
</html>