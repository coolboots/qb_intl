<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" value="{{ csrf_token() }}"/>
    <link href="{{ mix('css/app.css') }}" type="text/css" rel="stylesheet"/>
    <link rel="manifest" href="/manifest.json"/>
    <link rel="shortcut icon" href="{{asset('/assets/images/favicon.png')}}">
    <!--[if IE]><link rel="shortcut icon" href="{{asset('/assets/images/favicon.png')}}"><![endif]-->
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('/assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('/assets/css/style.css')}}">
    <title>Qureka Lite</title>
    <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
    <?php
    $ga_id = getGAId();
    $current_date = date('YmdH');
    $publisher = getGAPubliser();
    if(!empty($ga_id))
    {
        ?>
        <!-- <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js" data-ad-client="<?php //echo $publisher;?>" type="text/javascript"></script>
        <script async src="https://www.googletagmanager.com/gtag/js?id=<?php //echo $ga_id;?>"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());gtag('config', "<?php //echo $ga_id;?>");
          var base_url = "<?php //echo URL::to('/');?>";
        </script> -->
        <?php
    } */
    ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js"></script>
    
    
  </head>
  <body>
    <main class="app-wrapper-container dashboard-wrapper">
     @yield('content')
    </main>

    <!-- jQuery and Bootstrap Bundle (includes Popper) -->
   
    <script src="<?php echo asset('/assets/js/3.5.1-jquery.min.js');?>"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>
    
    @yield('jsblock')
  </body>
</html>