<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="robots" content="noindex" />
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <link rel="shortcut icon" href="{{getFecdnPath('/assets/images/favicon.png')}}">  
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{getFecdnPath('/assets/css/style.css?version='.\Config::get('qureka.css_version'))}}" />
    <link rel="stylesheet" href="{{getFecdnPath('/assets/css/responsive.css?version='.\Config::get('qureka.css_version'))}}" />
    <title>Rewarded Quiz</title>


    <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
    <?php
    $ga_id = getGAId();
    $current_date = date('YmdH');
    $publisher = getGAPubliser();
    $gaBlockedCountries = \Config::get("qureka.ga_blocked_countries");
    $uzone = json_decode(Session::get('userzone'));

    if(!empty($ga_id))
    {
        ?>
        <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js" data-ad-client="<?php echo $publisher;?>" type="text/javascript"></script>

        <?php if(!in_array(@$uzone->country, $gaBlockedCountries)){ ?>
        <script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $ga_id;?>"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());gtag('config', "<?php echo $ga_id;?>", { 'anonymize_ip': true });
          var base_url = "<?php echo URL::to('/');?>";
        </script>
        <?php
      }
    }
    ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
   <!--  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js"></script> -->
  </head>
  <body>
    <main class="app-wrapper-container">
     @yield('content')
    </main>

     <!-- jQuery and Bootstrap Bundle (includes Popper) -->
   <!--  <script src="<?php //echo asset('/assets/js/3.5.1-jquery.min.js');?>"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" type="text/javascript"></script> -->

  <!-- Check for webview close button -->
    <script type="text/javascript">
       const isWebview = () => {
          if (typeof window === undefined) { return false };

         // let navigator: any = window.navigator;

           const standalone = window.navigator.standalone;
           const userAgent = window.navigator.userAgent.toLowerCase();
           const safari = /safari/.test(userAgent);
           const ios = /iphone|ipod|ipad/.test(userAgent);

           return ios ? !standalone && !safari : userAgent.includes('wv');
      }

      if (isWebview()) {
        $('.shimmer2').css('display','none');
        $('.wv-parent').css('display','block');
      } else {
        $('.shimmer2').css('display','block');
        $('.wv-parent').css('display','none');
      }
     </script>

    <script>
    
      let usermode = "{{Session::get('usermode')}}";
      if(usermode=="block" && location.pathname!="/block"){
        location.href = "/block";
      }

    </script>
    <script>
        // Do not sell my info button hide
        var optout = "{{session()->get('optout')}}";
        if(optout!='true')
        {
          $('.do-not-sell-myinfo-text').css('display','block'); 
        }
        if(optout=='true')
        {
          $('.do-not-sell-myinfo-text').hide(); 
        }
      });
      </script>
    
    @yield('jsblock')

  </body>
</html>
