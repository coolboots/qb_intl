import Home from './presignup/Home.vue';
import Score from './presignup/Score.vue';
import Dashboard from './postsignup/Dashboard.vue';
import CricketDashboard from './postsignup/CricketDashboard.vue';
import ContestDetail from './postsignup/ContestDetail.vue';
import PlayContest from './postsignup/PlayContest.vue';
import ContestScore from './postsignup/ContestScore.vue';

import CoinsWallet from './postsignup/CoinsWallet.vue';
import Signup from './presignup/Signup.vue';
import Logout from './presignup/Logout.vue';

import PageNotFound from './common/PageNotFound.vue';
import Blockpage from './common/Blockpage.vue';
import Optout from './common/Optout.vue';
import Privacy from './common/Privacy.vue';
import Terms from './common/Terms.vue';
import Quizrules from './postsignup/Quizrules.vue';
import Portcoin from './postsignup/Portcoin.vue';
import PortCoinOtp from './postsignup/PortCoinOtp.vue';
import PortCoinSuccess from './postsignup/PortCoinSuccess.vue';
import ContactUs from './postsignup/ContactUs.vue';
import PartnerUs from './postsignup/PartnerUs.vue';
import WinnersDashboard from './postsignup/WinnersDashboard.vue';
import WinnersList from './postsignup/WinnersList.vue';
import Intro from './presignup/Intro.vue';
 
export const routes = [
    {
        name: 'Intro',
        path: '/intro',
        component: Intro,
         meta: { transitionName: 'zoom',guest: true }
    },
    //score page non-logged in user
    {
        name: 'Score',
        path: '/score',
        component: Score,
        meta: { transitionName: 'zoom',guest: true }
    },
    {
        name: 'signup',
        path: '/signup',
        component: Signup,
         meta: { transitionName: 'zoom',guest: true }
    },
    {
        name: 'logout',
        path: '/logout',
        component: Logout,
         meta: { transitionName: 'zoom',requiresAuth: true }
    },
    {
          //path: 'https://frontend.local.com/api/auth/:provider/callback',
          path: '/api/auth/:provider/callback',
          component: {
            template: '<div class="auth-component"></div>'
          }
    },
    //post signup activity
    {
        name: 'Dashboard',
        path: '/',
        component: Dashboard,
        // meta: {
        //         requiresAuth: true
        //     }
        meta: { transitionName: 'zoom',requiresAuth: true }
    },
    {
        name: 'Cricket-Dashboard',
        path: '/',
        component: CricketDashboard,
        // meta: {
        //         requiresAuth: true
        //     }
        meta: { transitionName: 'zoom',requiresAuth: true }
    },
    {
        //to show contest details
        name: 'contest-details',
        path: '/contest/:id',
        component: ContestDetail,
        meta: { transitionName: 'zoom',requiresAuth: true }
    },
    {
        //play contest area
        name: 'playcontest',
        path: '/playcontest/:id',
        component: PlayContest,
        meta: { transitionName: 'zoom',requiresAuth: true }
    },
    {
        //contest scores
        name: 'contestscore',
        path: '/contestscore/:id',
        component: ContestScore,
        meta: { transitionName: 'zoom',requiresAuth: true }
    },
    {
        name: 'wallet',
        path: '/wallet',
        component: CoinsWallet,
        meta: { transitionName: 'zoom',requiresAuth: true}
    },
    // {
    //     name: 'WinnerList',
    //     path: '/winners',
    //     component: WinnerList,
    //     meta: { transitionName: 'zoom',requiresAuth: true }
    // },  
    { 
        name:'Blockpage',
        path: "/block", 
        component: Blockpage,
        meta: { transitionName: 'zoom',requiresAuth: false }
    },
    { 
        name:'Optout',
        path: "/optout", 
        component: Optout,
        meta: { transitionName: 'zoom',requiresAuth: false }
    },
    { 
        name:'Privacy',
        path: "/privacy-policy", 
        component: Privacy,
        meta: { transitionName: 'zoom',requiresAuth: false }
    },
    { 
        name:'Terms',
        path: "/terms-conditions", 
        component: Terms,
        meta: { transitionName: 'zoom',requiresAuth: false }
    },
    { 
        name:'Quizrules',
        path: "/quizrules", 
        component: Quizrules,
        meta: { transitionName: 'zoom',requiresAuth: false }
    },
    { 
        name:'Portcoin',
        path: "/portcoin", 
        component: Portcoin,
        meta: { transitionName: 'zoom',requiresAuth: false }
    },
    {
        name: 'portcoinotp',
        path: '/portcoinotp/:mobile',
        component: PortCoinOtp,
        meta: { transitionName: 'zoom',requiresAuth: true }
    },
    {
        name: 'portcoinsuccess',
        path: '/portcoinsuccess/:coin',
        component: PortCoinSuccess,
        meta: { transitionName: 'zoom',requiresAuth: true }
    },
    { 
        name:'Contactus',
        path: "/contactus", 
        component: ContactUs,
        meta: { transitionName: 'zoom',requiresAuth: false }
    },
    { 
        name:'Partnerus',
        path: "/partnerus", 
        component: PartnerUs,
        meta: { transitionName: 'zoom',requiresAuth: false }
    },
    {
        name: 'WinnersDashboard',
        path: '/winnersdashboard',
        component: WinnersDashboard,
        meta: { transitionName: 'zoom',requiresAuth: true }
    },
    {   
        name: 'WinnersList',
        path: '/winners/:id',
        component: WinnersList,
        meta: { transitionName: 'zoom',requiresAuth: true }
    }, 
    {
        name:'Samplequestions',
        path: "/sample-questions", 
        component: Home,
        meta: { transitionName: 'zoom',requiresAuth: false }
    },
    { 
        path: "*", 
        component: PageNotFound,
        meta: { transitionName: 'zoom',requiresAuth: false }
    },
     
    
];
