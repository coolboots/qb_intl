import axios from 'axios';
// import { cacheAdapterEnhancer } from 'axios-extensions';
import { throttleAdapterEnhancer } from 'axios-extensions';

// const cacheConfig = {
//     enabledByDefault: false, 
//     cacheFlag: 'useCache'
// }

const throttleConfig = {
    // threshold: 2*1000 // 10 seconds
}

//var baseURL = window.location.protocol+window.location.hostname+'/api';

let token = document.head.querySelector('meta[name="csrf-token"]').getAttribute('value');


const httpClient = axios.create({
    baseURL: '/api',
    timeout: 10000,
     headers: {
        'Content-Type':"application/json",
        'X-Requested-With':"XMLHttpRequest",
        'X-CSRF-TOKEN':token
    },
    adapter: throttleAdapterEnhancer(axios.defaults.adapter, throttleConfig)
    //adapter: cacheAdapterEnhancer(axios.defaults.adapter, cacheConfig); - for permanent caching
});

const getAuthToken = () => localStorage.getItem('token');

const authInterceptor = (config) => {
    config.headers['Authorization'] = "bearer abcyxs";//getAuthToken();
    return config;
}

const errorInterceptor = error => {

    // check if it's a server error
    if (!error.response) {
      //notify.warn('Network/Server error');
      return Promise.reject(error);
    }

    // all the error responses
    switch(error.response.status) {
        case 400:
            console.error(error.response.status, error.message);
            // notify.warn('Nothing to display','Data Not Found');
            break;

        case 401: // authentication error, logout the user
            // notify.warn( 'Please login again', 'Session Expired');
            localStorage.removeItem('token');
            router.push('/auth');
            break;

        default:
            // console.error(error.response.status, error.message);
            notify.error('Server Error');

    }
    return Promise.reject(error);
}

// Interceptor for responses
const responseInterceptor = response => {
    // console.log('response interceptors'+response);
    /*switch(response.status) {
        case 200: 
            // yay!
            return response;
            break;
        // any other cases
        default:
            // default case
    }*/

    return response;
}
httpClient.interceptors.request.use(authInterceptor);
httpClient.interceptors.response.use(responseInterceptor, errorInterceptor);




export default httpClient;