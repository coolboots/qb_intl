/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
Vue.use(require('vue-cookies'))
Vue.use(VueCookies)
import App from './App.vue';
import VueRouter from 'vue-router';
import VueAxios from 'vue-axios';
import axios from 'axios';
// import {routes} from './routes';
import VueSocialauth from 'vue-social-auth'
//import {curlservice} from './curlservice';

import httpClient from './api/httpClient';

import VueCookies from 'vue-cookies';
import router from './router.vue';
import md5 from 'md5';
// import VueGtag from "vue-gtag";

import numkeyboard from 'vue-numkeyboard';
import 'vue-numkeyboard/style.css';

Vue.use(numkeyboard);

import OtpInput from "@bachdgvn/vue-otp-input";
 
Vue.component("v-otp-input", OtpInput);

// setInterval(() => {
//    let response = httpClient.get('users');
// console.log(response);
// }, 2*1000);

$cookies.config('1d','/', '', false, 'Lax');

//$cookies.config(expireTimes[,path[, domain[, secure[, sameSite]]])

//Vue.prototype.$hostname = 'http://localhost:8000/api/';

//define global access

Vue.prototype.$md5 = md5;
Vue.prototype.$httpClient = httpClient;
Vue.prototype.$cdnUrl = process.env.MIX_CDN_URL;
var baseURL = window.location.protocol+'//'+window.location.hostname+'/api';
Vue.prototype.$pageLoadType = 'true'; //'true'=>'for regular', 'false'=>'for native' 

//please add below config in .env file if missing
/*FACEBOOK_ID=310300550407644
FACEBOOK_SECRET=8577c6c5109e8d42314a320750bda939
FACEBOOK_URL="api/auth/facebook/callback"


GOOGLE_ID=27703031747-n9o96sgkb92e9t9j3v26qvc61o7hl7f1.apps.googleusercontent.com
GOOGLE_SECRET=Z9PebTVdNcCzYwH638Qc3B4J
GOOGLE_URL="api/auth/google/callback"
*/

 // console.log('sociallogin');
 // console.log($cookies.get('sociallogin'));

 let sociallogin = atob($cookies.get('sociallogin'));
 if(sociallogin!=null)
 {
  sociallogin = JSON.parse(sociallogin);
  // console.log(sociallogin);
 
  // Vue.use(md5);
  Vue.use(VueSocialauth, {
    providers: {
      facebook: {
        clientId: sociallogin.facebook.id,
        //clientId: process.env.FACEBOOK_ID,
        redirectUri: baseURL+'/auth/facebook/callback' // Your client app URL
      },
      google: {
        clientId: sociallogin.google.id,
        //clientId: process.env.GOOGLE_ID,
        clientSecret: sociallogin.google.secret,
        //clientSecret: process.env.GOOGLE_SECRET,
        redirectUri: baseURL+'/auth/google/callback' // Your client app URL
      },
      
    }
})
}



Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(httpClient);
// Vue.use(VueCryptojs);



let api_url = process.env.MIX_API_URL;
// console.log("host name:"+window.location.hostname);
// console.log(api_url);





/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

//Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// const router = new VueRouter({
//     mode: 'history',
//     routes: routes
// });


//google analytics
/*let ga_ids = $cookies.get('WEB_GA_ID');
if(ga_ids)
{
      //google analytics
      Vue.use(VueGtag, {
        config: { 
          id: ga_ids,
          params: {
            send_page_view: true
          } 
        }
      },router);
    
}
*/




const app = new Vue({
    el: '#app',
    router: router,
    render: h => h(App),
});

