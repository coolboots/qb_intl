export default
{
  isUserAuthenticate: function()
  {
        let user_id = $cookies.get('cutk');
        let user_token = $cookies.get('ustk');
        if (user_id !== null && user_token !== null) { return true;}

  },
  adNetworkManipulation: function(data)
  {
    if(data!=null || data !=undefined)
    {
      localStorage.setItem('adnetwork', JSON.stringify(data));
    }
  },
  getImgUrl: function(path)
    {
        return this.$cdnUrl+path;
      
    },
    loopTimer: function(input,id='',prependtext='',gameBlockPeriod=0)
    {
      const self = this;
      //return Math.random();
       setInterval(function(){
         
          let timer = self.convertDateToTime(input,gameBlockPeriod);
          if(timer=="00:00:00")
          {
            $('.block_'+id).find('.play-btn').after('<a href="javascript:void(0);" class="play-btn">Play Game</a>');
            $('.block_'+id).find('.play-btn').first().remove();
            //$('.block_'+id).remove();
          }
          timer=prependtext+timer;
          $('#'+id).html(timer);

       },1000); 
    },
    numberWithCommas: function(x) {
      if(x!=undefined)
      return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    },
    convertDateToTime: function(input,gameBlockPeriod=0)
    {
      if(input!=null)
      {
        gameBlockPeriod = gameBlockPeriod*1000;
        var endTime = Date.parse(input);
        if(isNaN(endTime))
        {
          // console.log('coming inside');
          var inputDateNewformat = input.split(' ');
          var inputDate = inputDateNewformat[0].split('-');
          var inputHrs = inputDateNewformat[1].split(':');
          inputDate = inputDate.concat(inputHrs);
          var dy = inputDate;
          // console.log(inputDate.toString());
          // endTime = Date.parse(inputDate);
          // endTime = new Date(dy[0],dy[1],dy[2],dy[3],dy[4],dy[5]);
          endTime = new Date(dy[1]+'/'+dy[2]+'/'+dy[0]+' '+dy[3]+':'+dy[4]+':'+dy[5]);
          // console.log(endTime);
          endTime = Date.parse(endTime);
        }
        endTime = endTime-gameBlockPeriod;
        // console.log('end time');
        // console.log(endTime);
        //var currentTime = new Date();
        // var d = new Date();
        // var utc = d.getTime() + (d.getTimezoneOffset() * 60000);
        // var nd = new Date(utc + (3600000*+5.5));
         // var ist =  nd.toLocaleString();
        // var currentTime = new Date();
        // console.log('end time:'+endTime);
        // console.log('start time:');
        // console.log(currentTime.getTime());

        var date = new Date();
        var invdate = new Date(date.toLocaleString('en-US', {
            timeZone: "Asia/Kolkata"
          }));
        var diff = date.getTime() - invdate.getTime();
        var currentTime = new Date(date.getTime() - diff);
        
        const temp=this.timeDiffCalc(endTime,currentTime.getTime());

        // if(temp=="00:00:00")
        // {
        //   $('#play_now_'+quiz.id).removeAttr("href").removeClass("userlivebtnwin").addClass("userlivebtnwin1");
        // }
        return temp;
      }
        
    },
    timeDiffCalc: function(dateFuture, dateNow) {
              let difference = '';
              
              if( dateFuture > dateNow){
              let diffInMilliSeconds = Math.abs(dateFuture - dateNow) / 1000;

              // calculate days
              const days = Math.floor(diffInMilliSeconds / 86400);
              diffInMilliSeconds -= days * 86400;
             

              // calculate hours
              var hours = Math.floor(diffInMilliSeconds / 3600) % 24;
              diffInMilliSeconds -= hours * 3600;
             

              // calculate minutes
              var minutes = Math.floor(diffInMilliSeconds / 60) % 60;
              diffInMilliSeconds -= minutes * 60;
             

              // calculate seconds
              var seconds = Math.floor(diffInMilliSeconds) % 60;
              diffInMilliSeconds -= seconds * 60;
             
              if (hours   < 10) {hours   = "0"+hours;}
              if (minutes < 10) {minutes = "0"+minutes;}
              if (seconds < 10) {seconds = "0"+seconds;}
              
              
              if (days > 0) {
               // difference += (days === 1) ? `${days} day, ` : `${days} days, `;
                hours  = +hours + (days * 24);
              }

              difference += (hours === 0 || hours === 1) ? `${hours}:` : `${hours}:`;

              difference += (minutes === 0 || hours === 1) ? `${minutes}:` : `${minutes}:`; 

              difference += (seconds === 0 || minutes === 1) ? `${seconds}` : `${seconds}`; 
              
              }else{
                 difference +="00:00:00";
              }
              
              return difference;
    },
    timeDiffInSecond: function(input)
    {
      var difference = 0;
      // var input = '2021-01-30 14:00:13';
      var endTime = Date.parse(input);
      if(isNaN(endTime))
        {
          // console.log('coming inside');
          var inputDateNewformat = input.split(' ');
          var inputDate = inputDateNewformat[0].split('-');
          var inputHrs = inputDateNewformat[1].split(':');
          inputDate = inputDate.concat(inputHrs);
          var dy = inputDate;
          // console.log(inputDate.toString());
          // endTime = Date.parse(inputDate);
          // endTime = new Date(dy[0],dy[1],dy[2],dy[3],dy[4],dy[5]);
          endTime = new Date(dy[1]+'/'+dy[2]+'/'+dy[0]+' '+dy[3]+':'+dy[4]+':'+dy[5]);
          // console.log(endTime);
          endTime = Date.parse(endTime);
        }
        
      var currentTime = new Date().getTime();

      if( endTime > currentTime){
      let diffInMilliSeconds = Math.abs(endTime - currentTime) / 1000;

        difference = Math.floor(diffInMilliSeconds);
        //console.log('difference in second'+difference);
      }

      return difference;

    },
    showPrizeBreakup: function(game) {
           this.prize_breakup = game;
           console.log('prize breakup');
           console.log(game);
           this.popup = 'prize_breakup';
           //console.log(this.page_type);
           $('#rankbreakup').modal('show');
           },
     showErrorPopup: function(data) {
           this.error_message = data;
           // this.popup = 'prize_breakup';
           //console.log(this.page_type);
           $('#errorpopup').modal('show');
           },
     showConfirmPopup: function(data) {
           this.error_message = data;
           // this.popup = 'prize_breakup';
           //console.log(this.page_type);
           $('#confirmpopup').modal('show');
           },
     showMessagePopup: function(data) {
           this.message = data;
           // this.popup = 'prize_breakup';
           //console.log(this.page_type);
           $('#messagepopup').modal('show');
           },
     showVideo: function(data) {
           this.video_url = data.replace('watch?v=','embed/');
           // this.popup = 'prize_breakup';
           //console.log(this.page_type);
           $('#videopopup').modal('show');
           },
           switchGameDetail: function(game) {
            $('.tab-pane').each(function()
              {
                $(this).removeClass('active');
                $(this).removeClass('show');
              });
            
            $('#tab_'+game.id).addClass('active show');
           
           },
           moveToGameList: function()
           {
            const el = document.getElementById('gamesonmgl');
            el.scrollIntoView({behavior: "smooth"});
           },
           showPopup: function(name)
           {
              $('#'+name).modal('show');
           },
           goBack: function()
           {
	    var location = window.location.pathname;
            var page = location.split("/");
	    if(page[1] == 'contestscore'){
           	 this.$router.go(-3);
		}else{
		 this.$router.go(-1); 
		}

           },
           scoreUpdator: function(score)
           {
              // if(score > 0)
              // {
                this.$httpClient.get('score/'+ this.$route.params.id+'/'+score).then(function(res){
               // console.log('results:');
               // console.log(res.data);
               });
              // }
           },
           submitScore: function()
           {
                this.$httpClient.get('score/'+ this.$route.params.id+'/submit').then(function(res){

                  //inject data in localstorage and move to next screen
                   // console.log('results:');
                   // console.log(res.data);
               });
              
           },

            formatAMPM: function(timestamp) {
              var datestring = Date.parse(timestamp);
              var date = new Date(datestring);
              var hours = date.getHours();
              var minutes = date.getMinutes();
              var ampm = hours >= 12 ? 'PM' : 'AM';
              hours = hours % 12;
              hours = hours ? hours : 12; // the hour '0' should be '12'
              minutes = minutes < 10 ? '0'+minutes : minutes;
              var strTime = hours + ':' + minutes + ' ' + ampm;
              return strTime;
            },


             equalheight: function(container){

            var currentTallest = 0,
                 currentRowStart = 0,
                 rowDivs = new Array(),
                 $el,
                 topPosition = 0;
             $(container).each(function() {
            
               $el = $(this);
               $($el).height('auto')
               topPostion = $el.position().top;
            
               if (currentRowStart != topPostion) {
                 for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                   rowDivs[currentDiv].height(currentTallest);
                 }
                 rowDivs.length = 0; // empty the array
                 currentRowStart = topPostion;
                 currentTallest = $el.height();
                 rowDivs.push($el);
               } else {
                 rowDivs.push($el);
                 currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
              }
               for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                 rowDivs[currentDiv].height(currentTallest);
               }
             });
            },

      closeModal: function(id)
      {
        $('#'+id).modal('hide');
        $('#messagepopup .modal-content').removeClass('yourcoinbalance-modal');
      },

      showCountryTag: function()
      {
            let userzone = $cookies.get('userzone');
            if (userzone.country === 'IN') { return true;}

      },

      userPlayContestKey: function()
      {
            let user_name = $cookies.get('user_name');
            let user_id = $cookies.get('user_id');
            
            if (user_name !== null) {
              user_name = user_name.replace(/ /g, "-")
              user_name = user_name.toLowerCase();

              var key = user_name+'_play_list_'+user_id; 
             // console.log('user_name_key : '+key);
             return key;
           }

      },

      checkPlayNow: function(userPlaykey, contestId){
        //console.log(userPlaykey);
          let contestStatus = false; 
          let playedContest = JSON.parse($cookies.get(userPlaykey));
          //console.log(playedContest);
           if(playedContest !== null){
                 if($.inArray( contestId, playedContest) > -1){
                  contestStatus = true; 
                 }                     
            }
            return contestStatus;
      },

      simillarContest: function(type, categoryId=''){

        var simillarContest = [];
        
        let userPlaykey = this.userPlayContestKey();
        
        if(type !== null && categoryId !== null){
          let contests = localStorage.getItem(type);
          if (contests!=undefined) {
            let contestList = JSON.parse(contests);
            
            for (var i = 0; i < contestList.length; i++) {
              //if(contestList[i].category_id === categoryId){
                  let playstatus = this.checkPlayNow(userPlaykey,contestList[i].id);
                  if (playstatus === false && simillarContest.length <= 1) {
                    let timer = this.convertDateToTime(contestList[i].end_time,0);
                      if(timer!="00:00:00")
                      {
                        simillarContest.push(contestList[i]);
                      }
                  }
                 
              //}
            }
          
          }
        }
        return simillarContest;
      },

      getDeviceInfo: function()
      {
        let inApp = false;
        let standalone = window.navigator.standalone,
        userAgent = window.navigator.userAgent.toLowerCase(),
        safari = /safari/.test(userAgent),
        ios = /iphone|ipod|ipad/.test(userAgent);

        if (ios) {
          if (!standalone && safari) {
            // Safari
          } else if (!standalone && !safari) {
            inApp = true;//iphone inapp
          };
        } else {
          if (userAgent.includes('wv')) {
            inApp = true;// Android webview
          } else {
            // Chrome
          }
        }
        return inApp;
      },
      updateAdStorage: function()
            {
              var d = new Date().getTime();
              var ad_network = localStorage.getItem('ad_network');
              var hitapi = false;
              if(ad_network && ad_network.length > 100)
              {
                ad_network = atob(ad_network);
                ad_network = JSON.parse(ad_network);

                if(ad_network && (parseInt(ad_network.last_visit) < d))
                {hitapi = true;}
              }
              else if(!ad_network || ad_network.length < 100)
              {
                hitapi = true;
              }

              if(hitapi)
              {
                var response =  this.$httpClient.get('adnetwork').then(function(res){
                    if(res.data.status_code==200)
                   {
                    // console.log(res.data);
                      if(res.data.response!=undefined)
                      {
                         var s = new Date();
                          s = s.setMinutes(s.getMinutes()+5);
                          res.data.response.last_visit = s;
                          var dataset = JSON.stringify(res.data.response);
                        localStorage.setItem('ad_network',btoa(dataset));
                      }
                    }
                    });
              }
            },
            loadOnce: function () {
              // if (localStorage.getItem("reloaded")) {
              //   localStorage.removeItem("reloaded");
              // } else {
              //   localStorage.setItem("reloaded", "1");
              //   location.reload();
              // }
              /*if (localStorage.getItem("reloaded")) {
                localStorage.removeItem("reloaded");
              } else {
                localStorage.setItem("reloaded", "1");
                location.reload();
              } */
            },
}
