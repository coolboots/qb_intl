<?php

if (!defined('API_SERVER_URL')) 
{define('API_SERVER_URL',env('API_SERVER_URL','https://api2.qurekaquizbytes.co/api/v1/'));}

if (!defined('API_DOMESTIC_URL')) 
{define('API_DOMESTIC_URL',env('API_DOMESTIC_URL','https://api2.qurekaquizbytes.com/api/v1/'));}


if (!defined('API_QUREKA_URL')) 
{
define('API_QUREKA_URL',env('API_QUREKA_URL','https://api20.qureka.co/api/v1/'));}





return [

    //Layout preparation
    'API_SERVER_PUB_KEY'=>env('API_SERVER_PUB_KEY','eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiOTlkNGU3ZjU4ZWZhOTIxMzRlODZmYWE3YTAzMjE2M2UzYzcyNDU3NjRkYTRhMDJlNGJjYjBjYmNjODQ3NmYwZThmZDFjNTJmNTYxNTA1NGYiLCJpYXQiOjE2Mzg1OTc4ODMsIm5iZiI6MTYzODU5Nzg4MywiZXhwIjoxNjcwMTMzODgzLCJzdWIiOiIiLCJzY29wZXMiOlsiKiJdfQ.rD5p8q2MhbCRvGofqIQEL0jGIl0MaB2JIS-HP56InUyD5Qc35iu43jwaJD0C11WVK7yRioOdxRwvHq0Y5daitj10MEPaowrXP-Hcx1kMtYAfNsM4kZjECZGPrI0ZqxvvFpzapnU0-L4S9xFCpQBUwSMAq81BjC5hZWxJeRQEsubLeYV025ULZl-9ScPZtLzXfUwizb_3gCupCen1qz39mX5PDvhY5jAjwtA23ODm1hyBPj8ka42YoufE6V_Jx0BmmswxHr2y7qoKQOiyBElF1Rk5349ChnTVlkflYvGcnWpJLmJ2fp7JPAutdiSjItFQ9u8jZLCjj9wId9GHMz0KwpOxqA55SBtqe1aWpwnlGBpKh1IIQUeoKFmlHUxs193mCjRylJvA4_kGcPm5myqxDgIN8ju2GEUnzEqlT22lq8xxcWNzRKvGIAOio4IuqZjC6tWAs3NOTXRzfYGu4RpP3Y59pI1Gx4nDMq-TOhzjgqD6Gi4_r-FaSilvEwR62bp3I31vb205u8ppsJNTfRgRrssOSxFHQpDFreUNUJWqehZBf-5x5Y9ktcUifL7eLeZgf9ubLZAgENR67D_iYWKM71VvlVSWFOWNjbF61wj30jq7BylmV0YeOna_iilj2me_cOKR0q4UqCPsEqC_XaJvZwvu5yR3CT-nbJeljTgmxtc'),

    'API_DOMESTIC_PUB_KEY'=>env('API_DOMESTIC_PUB_KEY','eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiNjJhNTg5M2M1MDdiYjBkMjcxN2Q5M2FlM2U2YTNhZjdkYTI1Y2I0M2FmYjA2MGE2ZWQwYjM1M2Y4MGM4MDQ1MTQ1NTFmMDhjYmRjODRkMjQiLCJpYXQiOjE2NDI1MTE1MjQsIm5iZiI6MTY0MjUxMTUyNCwiZXhwIjoxNjc0MDQ3NTI0LCJzdWIiOiIiLCJzY29wZXMiOlsiKiJdfQ.ZP9w-RcOhgevjTmavPpzJH0mD41z8Tbp57dJZ8-JN9-GrBkI9pRSRsgjmeqMnWGGnhEBhUnkevCJ_xjNFPBbXfRknPHoaHfFMeBYK3SqN4wYawRj_-kRH2DVn70Bum4ILBZgBBtE9NqM_PVJHsuZA_dZ62qhnxPxoycx37w1DGhhfB1AKOaxP-F4tS0s0VZr0iozZ9y_HlLHJvV73Qsn_w-aT0TXjCeNedKaxGAW9TCn8F73fjxeFsbjdbqdUjwjSoyoqavSQm3n-R2eKYdeZFWHl8QX9JngaP420h06B4rg7QKzXCexOUn80nN6TmasjS8rPyvze4pGAespK015MnZe6CNS2YTWa32NA0v-FYU_RsFVrszLYGPHV7PZKtgu5pvZGkyexw3xCD_1XByt5y4s5OjhcppbF--Qgz4XcGF4vwkTDl5gvBvA3_MeVGObyUgwRj-xCZUb08XVtaPmrbfSHjdkZzfzizX38Kh0xntFnwGgRH_k4CoYc47ORVLdvT_6cF6sijxfetN71wz8bGxWfuOq5gtOzV7JWnsI1U9EfL86VyejY1YT18O_OiWv3onyNbagi0DNx7YCkbcUTpICZ5fTqlaDlnkPAcf_F5Ii6T4_E2ZX57O__LPfJs7UooeUYvoHQgRIGDRHkwe_3VOwNZPGJgYJTmQGtPRqgUI'),

    'API_QUREKA_PUB_KEY'=>'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiZTNhZTc3ZDQzYzJmZGIzMjQ2MDNhYmFkZWE5MTIwNTQ1NWE2Yjc4MDdhYjU5NzBjNGE5ZjFhMjQyNWE4MGRmOWFlMTZhNDRmNzQwNGJmZmEiLCJpYXQiOjE2NTAxMDE3MzAsIm5iZiI6MTY1MDEwMTczMCwiZXhwIjoxNjgxNjM3NzMwLCJzdWIiOiIiLCJzY29wZXMiOlsiKiJdfQ.flWUQ8h_VC5ix-FYiuJeXSMMahIbquXdLiTHkznDBuiMhHgkS-AEFzNLIFh0To5EYtZ-TkSxG39KsQ3aXlLDu0EV61Cd_eiMhjOxmrqCiuEO6Spzy-dcqRwA08QlNClQnQwltisAC-eM5j7x7xrKQdLDBhFhDyRrDXmcviNegiUbAi6ni5WnoLMSx0ubGpOjIiC2WeUViaY3YQVZg84U8WWSc7Q05tHy30fdR1a5EKD2FqSnc8OytFqwbpXSbVGQfr5WE21T3wQxr7Yw62PnrghLitAVhtx96pbYgMjV1Tq_aL7mNxQSLKjbWXHoZWi71QBGSnn0AaqV1_M738iz55v_dKCQOR45p6nFUCyS_5-Q1gMJj_2YnFZdsXOzWx41Lm_Ng4VLSgVwn8awM7qYKy5DC222bolmJL0nik55_IP1BVVB2QmxYjy827s5dl0QVHpPmUa94qwXabU3tS5p0Al21vHMA-2G3W0g8erZ3JVz64EAzO2vNkyYIATUJWSVw5DB42GJEVkj8xwjowZUc9gVIBhW0N6o9yWVWZyh2dv1uJDeMBLFt--b6a0iOyYO9ehh-R8jdxIZEV1UzJQtuAMSWrwKjh-tTme85PO2To8BzbzGzG4gRRQCBe7Ex6PtzyKe0cJN_e5TRYCR4oI4zLCmNuVZ2kPzV0HMd25hJdA',

    'intl_db' => 'intlmysql',

    'pre_flow'=>['modules'=>
        ['samplequestion'=>['url' => API_QUREKA_URL.'sample-question/{origin}', 'params'=>[],'method'=>'GET','render'=>'no','cache'=>false,'cache_interval'=>0],
        'funquiz'=>['url' => API_DOMESTIC_URL.'funquiz', 'params'=>[],'method'=>'POST','render'=>'no','cache'=>false,'cache_interval'=>5],
        
        ]],
  
     'general'=>['modules'=>
        ['domain'=>['url' => API_SERVER_URL.'domaininfo/{url}', 'params'=>[],'method'=>'GET','render'=>'no','cache'=>true,'cache_interval'=>15],
        
        
        ]],
     'static_ads'=> FALSE,

     'api_url' => env('API_URL', 'https://admin2.qurekaquizbytes.com/'),
     'app_url' => env('APP_URL', 'https://funrewards.local.com/'),
     
     'contest_block_period' => '180 seconds',

     'language' => array(
                            'EN'=> 'English',
                            'HN' => 'Hindi'
                        ),

     'tags' => array(
                            'IN'=> 'India',
                            'INT' => 'International',
                            'ALL' => 'All'
                        ),
     'quizByteKey'=>'SkOoLByTe',

     'compliance_countries'=>["US","CA"],
    'blocked_countries'=>["RU","CN"],
    'allowed_countries'=>[],
    'eu_countries'=>["AT","BE","BG","HR","CY","CZ","DK","EE","FI","FR","DE","GR","HU","IE","IT","LV","LT","LU","MT","NL","PL","PT","RO","SK","SI","ES","SE","IS","LI","NO"],
    'eea_countries'=>["AT","BE","BG","HR","CY","CZ","DK","EE","FI","FR","DE","GR","HU","IE","IT","LV","LT","LU","MT","NL","PL","PT","RO","SK","SI","ES","SE","IS","LI","NO","GB","CH"],
    'mena_countries'=>["DZ","BH","EG","IR","IQ","IL","JO","KW","LB","LY","MA","OM","PS","QA","SA","SY","TN","AE","YE"],
    'eu_ea_mena_countries'=>["AT","BE","BG","HR","CY","CZ","DK","EE","FI","FR","DE","GR","HU","IE","IT","LV","LT","LU","MT","NL","PL","PT","RO","SK","SI","ES","SE","IS","LI","NO","GB","CH","DZ","BH","EG","IR","IQ","IL","JO","KW","LB","LY","MA","OM","PS","QA","SA","SY","TN","AE","YE"],
    'signup_blocked_countries'=>["AT","BE","BG","HR","CY","CZ","DK","EE","FI","FR","DE","GR","HU","IE","IT","LV","LT","LU","MT","NL","PL","PT","RO","SK","SI","ES","SE","IS","LI","NO","DZ","BH","EG","IR","IQ","IL","JO","KW","LB","LY","MA","OM","PS","QA","SA","SY","TN","AE","YE","GB","CH"],//eu and mena countries
    'ga_blocked_countries'=>["FR","AT","LI","DK"],

    'css_version'=>'1.0',
    'js_version'=>'1.0', 
        
    'webview_domains'=>['11.fun.qurekaquizbytes.co','20.fun.qurekaquizbytes.co'],
];
 
