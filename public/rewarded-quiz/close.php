<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="robots" content="noindex" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/responsive.css" />
    <title>Rewarded Quiz</title>
  </head>
  <body>
    <main class="app-wrapper-container">
    <section class="intro-question position-relative pb-3 close-screen">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="row position-relative head d-flex align-items-end justify-content-end">
              <div class="logo"><img src="images/logo.png" alt=""></div>
            </div>
            <div class="content text-center">
              <h2>Hope you had </h2>
              <div class="row mb-2">
                <div class="col-6 pr-1"><h1>Fun</h1></div>
                <div class="col-6 pl-1"><img src="images/cloud-gift.png" alt="" class="cloudgift img-fluid"></div>
              </div>
              <h6>Your <span>reward</span> will be added shortly. </h6>
              <button type="button" class="close-btn" onclick="closeWindow()"><img src="images/close.png" alt=""> Close</button>
              <span>Close the quiz now to go back to the app. </span>
            </div>
            <div class="row ads"><img src="images/ads.jpg" alt="" class="img-fluid p-2"></div>
            <footer class="py-3">
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,hent andard dummy text ever since the 1500s, when</p>
            </footer>
          </div>
        </div>
      </div>
    </section>

    </main>

    <!-- jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="js/3.5.1-jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript">

      $(document).ready(function () {
        setTimeout(function(){
          $('#fullscreen').modal('show');
        },1000)  
        
          $('.question-answer .options a').on('click', function(){
            window.location.pathname = '/rewarded-quiz/intro-question2.php'
          })
        });

        function closeWindow() {  
          window.close();  
        }  

    </script>
  </body>
</html>
