<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="robots" content="noindex" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/responsive.css" />
    <title>Rewarded Quiz</title>
  </head>
  <body>
    <main class="app-wrapper-container">
    <section class="intro-question position-relative pb-3">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="row ads mb-3"><img src="images/ads.jpg" alt="" class="img-fluid p-2"></div>
            <hgroup>
              <h2>Let's get started,</h2>
              <h6>Play 2 simple Questions and Win your Reward</h6>
            </hgroup>
            <div class="question-answer position-relative mt-4">
              <h6 class="question-size">Question-<span>1</span></h6>
              <h3>Which of these is a holy book?</h3>
              <div class="options">
                <a href="javascript:void(0);" class="wrong">GOA</a>
                <a href="javascript:void(0);">DELHI</a>
                <a href="javascript:void(0);" class="correct">PUNJAB</a>
              </div>
            </div>
            <footer class="py-3">
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,hent andard dummy text ever since the 1500s, when</p>
            </footer>
          </div>
        </div>
      </div>
    </section>

      
      <!-- Modal -->
      <div class="modal fade modal-common-style" id="fullscreen" tabindex="-1" aria-labelledby="fullscreenLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body text-center">
              <h4>Tap ‘OK’ to view in full screen</h4>
              <button type="button" class="confirm" onClick="openFullscreen()">Ok</button>
            </div>
          </div>
        </div>
      </div>
    </main>

    <!-- jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="js/3.5.1-jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript">

      $(document).ready(function () {
        setTimeout(function(){
          $('#fullscreen').modal('show');
        },1000)  
        
          $('.question-answer .options a').on('click', function(){
            window.location.pathname = '/rewarded-quiz/intro-question2.php'
          })
        });

        var elem = document.documentElement;
        function openFullscreen() {
            if (elem.requestFullscreen) {
            elem.requestFullscreen();
            $('#fullscreen').modal('hide');
          } else if (elem.webkitRequestFullscreen) { /* Safari */
            elem.webkitRequestFullscreen();
            $('#fullscreen').modal('hide');
          } else if (elem.msRequestFullscreen) { /* IE11 */
            elem.msRequestFullscreen();
            $('#fullscreen').modal('hide');
          }
        }
    </script>
  </body>
</html>
