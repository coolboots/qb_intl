<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="robots" content="noindex" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/responsive.css" />
    <title>Rewarded Quiz</title>
  </head>
  <body>
    <main class="app-wrapper-container">
      <section class="intro-screen">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <div class="row position-relative head d-flex align-items-end justify-content-end">
                <div class="logo"><img src="images/logo.png" alt=""></div>
              </div>
              <div class="content mb-4">
                <h3>Qureka</h3>
                <h1>Fun Time</h1>
                <h2>Play 2 simple Questions & Win your Reward</h2>
              </div>
              <div class="featured-image row">
                <img src="images/intro-featured.png" alt="" class="img-fluid p-0">
                <div class="redirection-strip row">
                  <div class="col-8 d-flex align-items-center">
                    <p>Redirecting in <span id="seconds">5</span> seconds..</p>
                  </div>
                  <div class="col-4 text-right d-flex align-items-center justify-content-end">
                    <a href="/rewarded-quiz/intro-question.php">Skip</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
</main>

    <!-- jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="js/3.5.1-jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript">

      $(document).ready(function () { 
        var timeLeft = 5;
          function countdown() {
            timeLeft--;
            document.getElementById("seconds").innerHTML = String(timeLeft);
            if (timeLeft === 0) {
              window.location.pathname = '/rewarded-quiz/intro-question.php'
            }
            if (timeLeft > 0) {
              setTimeout(countdown, 1000);
            }
          }
          setTimeout(countdown, 1000);
          
        });
       
    </script>
  </body>
</html>
