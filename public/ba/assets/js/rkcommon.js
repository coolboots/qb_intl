 var urluri = base_url+'/ajax/';

 
$('#country_id').change(function()
{
  if($('#province_id').length==0)
    return false;
  var id = $(this).val();
  $.get(urluri+'province/'+id,function(data)
  {
    $('#province_id').html(data);
  })
})
$('#province_id').change(function()
{
  if($('#county_id').length==0)
    return false;

  var id = $(this).val();
  $.get(urluri+'county/'+id,function(data)
  {
    $('#county_id').html(data);
  })
})
$('#county_id').change(function()
{
  if($('#city_id').length==0)
    return false;
  
  var id = $(this).val();
  $.get(urluri+'city/'+id,function(data)
  {
    $('#city_id').html(data);
    $("#city_id").trigger("liszt:updated");
    $("#city_id").trigger("chosen:updated");
  })
})

$('textarea').ckeditor();

$('select').each(function()
{
  var updatedValue = $(this).attr('changedValue');
  if($(this).attr('multiple'))
  {
    if(updatedValue!=undefined)
    {var obj = updatedValue.split(",");
    $(this).val(obj);
    }
    
  }
  else
  {
    $(this).val(updatedValue);
  }
  
});

$('.title').keyup(function()
  {
    if($('#seo_title').val()=='')
    {
      var routeUrl = $('#seo_title').attr('routeUrl');
      $('#main_seo_title').val(convertToSlug($(this).val()));
      $('.seo_url').text(base_url + '/'+routeUrl+'/'+ convertToSlug($(this).val()));
    }

  })
   $('#seo_title').keyup(function()
  {
    var routeUrl = $(this).attr('routeUrl');
    $('#main_seo_title').val(convertToSlug($(this).val()));
    $('.seo_url').text(base_url + '/'+routeUrl+'/'+ convertToSlug($(this).val()));
  });

  $('#editTitleLink').click(function(){
    $('#seo_title').removeAttr('readonly');
  });

//check all functions for listing page
  function checkAll(ele) {
     var checkboxes = document.getElementsByClassName('tablecheck');
     if (ele.checked) {
         for (var i = 0; i < checkboxes.length; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
         } 
     } else {
         for (var i = 0; i < checkboxes.length; i++) {
             // console.log(i)
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
         }
     }
 } 


 $(".checkall").click(function(){
    $('input:checkbox').not(this).prop('checked', this.checked);
});


function convertToSlug(Text)
{
    return Text
        .toLowerCase()
        .replace(/ /g,'-')
        .replace(/[^\w-]+/g,'')
        ;
}  

function addErrorLabel(arr)
{
  $.each( arr, function( key,value ) {
  //alert( key + ": " + value );
  console.log(value);
  $('label[for='+value+']').append('<span class="errorlabel">*</span>');
  $('#'+value).attr('required','required');
});
}

if (typeof(arr_error_label) !== 'undefined') 
{
  addErrorLabel(arr_error_label);
} 


/*toastr configuration*/
toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
/*end of toastr configuration*/
