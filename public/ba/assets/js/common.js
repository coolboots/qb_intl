$('textarea').not('.ignoreeditor').ckeditor();
$(".chosen-select").chosen();

$('select').each(function()
{
  var updatedValue = $(this).attr('changedValue');
  if($(this).attr('multiple'))
  {
    if(updatedValue!=undefined)
    {var obj = updatedValue.split(",");
    $(this).val(obj);
    }
    
  }
  else
  {
    $(this).val(updatedValue);
  }

  // $updatedValue = $(this).attr('changedValue');
   // $(this).val(updatedValue);
   $(this).trigger("liszt:updated");
   $(this).trigger("chosen:updated");
  
});



//check all functions for listing page
  function checkAll(ele) {
     var checkboxes = document.getElementsByClassName('tablecheck');
     if (ele.checked) {
         for (var i = 0; i < checkboxes.length; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
         } 
     } else {
         for (var i = 0; i < checkboxes.length; i++) {
             // console.log(i)
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
         }
     }
 } 


function convertToSlug(Text)
{
    return Text
        .toLowerCase()
        .replace(/ /g,'-')
        .replace(/[^\w-]+/g,'')
        ;
}  



//dynamic checks directly parent child relation

$('.parentCheck').click(function()
                    {
                      if($(this).is(':checked'))
                      {
                        var childname = $(this).attr('childcheck');
                        $("input[type='checkbox']").each(function (index, obj) {
                          if($(this).attr('refer')==childname)
                          {
                            $(this).prop('checked',true);
                          // loop all checked items
                          }

                      });
                      }
                      else
                      {
                        var childname = $(this).attr('childcheck');
                        console.log(childname);
                        $("input[type='checkbox']").each(function (index, obj) {
                          if($(this).attr('refer')==childname)
                          {
                            $(this).prop('checked',false);
                          // loop all checked items
                          }

                      });
                      }
                    })

//dynamic trigger event

$(".bulk_action_trigger").on("click", function(){
  var array = [];
  var action = $('.bulk_action_type').val();
  $("input:checkbox:checked").each(function(){
    if($(this).attr('actionid')!=undefined)
    {
      array.push($(this).attr('actionid'));
    }
    
    });
  if(action != undefined && action > 0 && array.length > 0)
  {
    var url      = window.location.href; 
    if(action == '4')
    {
      url =url+'/quickupdate?action='+action+'&id='+array.toString();
      
      var storage = $('.bulk_action_type').find(':selected').attr('storage');
      if(url!=undefined)
      { 

        fetchUrlAxios(url,storage);
      } 

      return false;
    }else{
      $.get(url+'/quickupdate?action='+action+'&id='+array.toString());
    }
    alert('record updated successfully! Please reload page to see changes.');
  }
  console.log(action);
  
  console.log(array);
  //alert("The paragraph was clicked.");
});

//common selection all
$(".checkall").click(function(){
    $('input:checkbox').not(this).prop('checked', this.checked);
});


//lazy load for images
!function(window){
  var $q = function(q, res){
        if (document.querySelectorAll) {
          res = document.querySelectorAll(q);
        } else {
          var d=document
            , a=d.styleSheets[0] || d.createStyleSheet();
          a.addRule(q,'f:b');
          for(var l=d.all,b=0,c=[],f=l.length;b<f;b++)
            l[b].currentStyle.f && c.push(l[b]);

          a.removeRule(0);
          res = c;
        }
        return res;
      }
    , addEventListener = function(evt, fn){
        window.addEventListener
          ? this.addEventListener(evt, fn, false)
          : (window.attachEvent)
            ? this.attachEvent('on' + evt, fn)
            : this['on' + evt] = fn;
      }
    , _has = function(obj, key) {
        return Object.prototype.hasOwnProperty.call(obj, key);
      }
    ;

  function loadImage (el, fn) {
    var img = new Image()
      , src = el.getAttribute('data-src');
    img.onload = function() {
      if (!! el.parent)
        el.parent.replaceChild(img, el)
      else
        el.src = src;

      fn? fn() : null;
    }
    img.src = src;
  }

  function elementInViewport(el) {
    var rect = el.getBoundingClientRect()

    return (
       rect.top    >= 0
    && rect.left   >= 0
    && rect.top <= (window.innerHeight || document.documentElement.clientHeight)
    )
  }

    var images = new Array()
      , query = $q('img.lazy')
      , processScroll = function(){
          for (var i = 0; i < images.length; i++) {
            if (elementInViewport(images[i])) {
              loadImage(images[i], function () {
                images.splice(i, i);
              });
            }
          };
        }
      ;
    // Array.prototype.slice.call is not callable under our lovely IE8 
    for (var i = 0; i < query.length; i++) {
      images.push(query[i]);
    };

    processScroll();
    addEventListener('scroll',processScroll);

}(this);

/*Clone button highlight */
var buttons = document.getElementsByClassName('clone-btn')
Array.prototype.forEach.call(buttons, function(b) {
  b.addEventListener('click', function(e) {
    Array.prototype.forEach.call(buttons, function(b) {
      b.classList.remove('selected')
    })
    b.classList.toggle('selected')
  })
});
