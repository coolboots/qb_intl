/*var app = new Vue({
  el: '#content',
  data: {
      name: '',
      email: '',
      country: '',
      city: '',
      job: '',
      dataset: [],
      filters: [],
      module_url:'',
      dynamicCollection:'raj here'
  },
  mounted: function () {
    // alert(relative_url);
    console.log('Hello from Vue!')
    $('.overlay_loader').hide();
    
    this.getContacts()
  },

  methods: {
    addItem: function (event) {
      // `this` inside methods points to the Vue instance
      // alert('add ')
      this.module_url = relative_url;
      // console.log(this.module_url);

      // this.dynamicCollection = 'kamal1';

       axios.get(this.module_url+'/create')
        .then(function (response) {
            // console.log(response.data);
            app.dynamicCollection = response.data;

        })
        .catch(function (error) {
            console.log(error);
        });
      
    },
    editItem: function (event) {
      // `this` inside methods points to the Vue instance
      alert('edit ')
      
    },
    showItems: function (event) {
      // `this` inside methods points to the Vue instance
      alert('Show All ')
      
    },
    filterItems: function (event) {
      // `this` inside methods points to the Vue instance
      alert('Show All ')
      
    },
    deleteItem: function (event) {
      // `this` inside methods points to the Vue instance
      alert('delete  ')
      
    },
    deleteItems: function (event) {
      // `this` inside methods points to the Vue instance
      alert('delete All ')
      
    },
    getContacts: function(){
    	// externalfunction();
      this.module_url = relative_url;
      console.log(this.module_url);

    	 axios.get(this.module_url)
        .then(function (response) {
            console.log(response.data);
            this.dataset = response.data;

        })
        .catch(function (error) {
            console.log(error);
        });
    },
    createContact: function(){

    	 console.log("Create contact!")

        let formData = new FormData();
        console.log("name:", this.name)
        formData.append('name', this.name)
        formData.append('email', this.email)
        formData.append('city', this.city)
        formData.append('country', this.country)
        formData.append('job', this.job)

        var contact = {};
        formData.forEach(function(value, key){
            contact[key] = value;
        });

        axios({
            method: 'post',
            url: 'api/contacts.php',
            data: formData,
            config: { headers: {'Content-Type': 'multipart/form-data' }}
        })
        .then(function (response) {
            //handle success
            console.log(response)
            app.contacts.push(contact)
            app.resetForm();
        })
        .catch(function (response) {
            //handle error
            console.log(response)
        });
    },
    resetForm: function(){

    	this.name = '';
        this.email = '';
        this.country = '';
        this.city = '';
        this.job = '';
    }
  }
}) 

//add new
$('#departmentForm').submit(function(e)
{
  e.preventDefault();
  alert();
})*/


// $('#departmentForm').click(function(e)
// {
//   e.preventDefault();
//   alert();
//   return false;
// })

//crud related common actions
$(document).on('submit','#createForm,#editForm',function(e){

  // alert('Clicked');
  showLoader();
  var url = $(this).attr('action');
  // var navigateUrl = $(this).attr('action');

  var bodyFormData = new FormData(e.target);
  axios({
    method: 'post',
    url: url,
    data: bodyFormData,
    headers: {'Content-Type': 'multipart/form-data','axios_request': true }
    })
    .then(function (response) {
        //handle success
        response = response.data;
        // console.log(response);
        // console.log(response.data.message);
        if(response.status=='failed')
        {
          // if(response.status_code==422)
          // {
             var errors = response.response.errors.errors;
             $('.message').addClass('alert').addClass('alert-danger').removeClass('alert-info').html('<strong>'+response.message+'</strong><br>'+errors).show().fadeOut(10000);
             toastr["error"](errors,response.message);
         
          // }
          
        }
        else if(response.status=='success')
        {
          $('.message').addClass('alert').removeClass('alert-danger').addClass('alert-info').html('<strong>'+response.message+'</strong>').show().fadeOut(10000);
          toastr["success"](response.message);
             if(response.response[0].callback!=undefined)
             {
                fetchUrlAxios(response.response[0].callback);  
             }

        }
        hideLoader();
    })
    .catch(function (response) {
        //handle error
        // console.log(response);
    });

  return false;

});


//crud related common actions
$(document).on('submit','.innerForm',function(e){

  // alert('Clicked');
  showLoader();
  var url = $(this).attr('action');
  var container = $(this).attr('callbackContainer');
  
  // var navigateUrl = $(this).attr('action');

  var bodyFormData = new FormData(e.target);
  axios({
    method: 'post',
    url: url,
    data: bodyFormData,
    headers: {'Content-Type': 'multipart/form-data','axios_request': true }
    })
    .then(function (response) {
        //handle success
        response = response.data;
        // console.log(response);
        // console.log(response.data.message);
        if(response.status=='failed')
        {
          // if(response.status_code==422)
          // {
             var errors = response.response.errors.errors;
             if(response.message!='')
             {
              $('.message').addClass('alert').addClass('alert-danger').removeClass('alert-info').html('<strong>'+response.message+'</strong><br>'+errors).show().fadeOut(10000);
              toastr["error"](errors,response.message);
             }
             
          // }
          
        }
        else if(response.status=='success')
        {
          if(response.message!='')
          {
            $('.message').addClass('alert').removeClass('alert-danger').addClass('alert-info').html('<strong>'+response.message+'</strong>').show().fadeOut(10000);
            toastr["success"](response.message);
          }
          
            $('.'+container).html(response.response[0].output);
           
           
             if(response.response[0].callback!=undefined)
             {
                fetchUrlAxios(response.response[0].callback);  
             }

        }
        hideLoader();
    })
    .catch(function (response) {
        //handle error
        // console.log(response);
    });

  return false;

});

$(document).on('submit','#filterForm',function(){

  showLoader();

  let paramsData = $(this).serialize();

  // console.log(paramsData);
  var url = $(this).attr('action')+'?'+paramsData;

        axios.get(url,
          {
          headers: {
            'axios_request': true,
            'Access-Control-Allow-Origin': '*',
            'radxsoft': 'labs',
            }
          })
        .then(function (response) {
            // console.log(response.data);
            // this.dataset = response.data;
            $('.dynamiccontainer').html(response.data);

        })
        .catch(function (error) {
            // console.log(error);
        });

        hideLoader();

  return false;

});

$(document).on('click','.navigationlink a, .dynamiccontainer a',function(e){

  //console.log($(this).attr('ajax-ignore'));
  if($(this).attr('ajax-ignore')===undefined)
  {
  e.preventDefault();


  var url = $(this).attr('href');
  var storage = $(this).attr('storage');
  if(url!=undefined)
  { 

    fetchUrlAxios(url,storage); 
  } 

  return false;
  }

});

$(document).on('click','.delete',function(){
       var token = $(this).data('token');
       var id = $(this).attr('deleteid');
       var url = $(this).attr('url');
   
       conf = confirm('Are you Sure?');
       //alert(token);
       var tr = $(this).closest('tr');
   
       if(conf)
       {
        showLoader();
        // alert('sdsdsds');
          axios({
                method: 'delete',
                url: url+'/'+id,
                data: {_method: 'delete', _token :token},
                headers: {'Content-Type': 'multipart/form-data','axios_request': true }
                })
                .then(function (response) {
                    //handle success
                    response = response.data;
                    // console.log(response);
                    // console.log(response.data.message);
                    if(response.status=='failed')
                    {
                        var errors = response.response.errors.errors;
                         $('.message').addClass('alert').addClass('alert-danger').removeClass('alert-info').html('<strong>'+response.message+'</strong><br>'+errors).show();
                        toastr["error"](errors,response.message);
                    }
                    else if(response.status=='success')
                    {
                        $('.message').addClass('alert').removeClass('alert-danger').addClass('alert-info').html('<strong>'+response.message+'</strong>').show();
                        // fetchUrlAxios(url);
                        toastr["success"](response.message);
                        tr.remove();
                    }
                    

                })
                .catch(function (response) {
                    //handle error
                    // console.log(response);
                });

                hideLoader();

          }
           return false;
});

function fetchUrlAxios(url, storage='')
{
  console.log('rk'+storage);
  $('.message').removeClass('alert-danger').removeClass('alert-info').removeClass('alert').html('');
  showLoader();
       axios.get(url,{
         	mode: 'no-cors',
	       headers: {
		       'Access-Control-Allow-Origin': '*',
            'radxsoft': 'labs',
          }
        })
        .then(function (response) {
            // console.log(response.data);
            // this.dataset = response.data;
            if(storage!=undefined && storage !='' && storage=='modal')
            {
              $('#defaultModal').modal('show');
              $('.defaultModalBodyContainer').html(response.data);
            }
            else
            {$('.dynamiccontainer').html(response.data);}

            hideLoader();

        })
        .catch(function (error) {
            // console.log(error);
            hideLoader();
             $('.message').addClass('alert').addClass('alert-danger').removeClass('alert-info').html('<strong>Something went wrong! Try again or Contact Superadmin!</strong><br>').show();
             toastr["warning"]('<strong>Something went wrong! Try again or Contact Superadmin!</strong><br>');
        });
}


//seo related common actions

$(document).on('keyup','.title',function(){
    if($('#seo_title').val()=='')
    {
      var routeUrl = $('#seo_title').attr('routeUrl');
      $('#main_seo_title').val(convertToSlug($(this).val()));
      //$('.seo_url').text(base_url + '/'+routeUrl+'/'+ convertToSlug($(this).val()));
      $('.seo_url').text(convertToSlug($(this).val()));
    }

  })
$(document).on('keyup','#seo_title',function(){
    var routeUrl = $(this).attr('routeUrl');
    $('#main_seo_title').val(convertToSlug($(this).val()));
    //$('.seo_url').text(base_url + '/'+routeUrl+'/'+ convertToSlug($(this).val()));
    $('.seo_url').text(convertToSlug($(this).val()));
  });
$(document).on('click','#editTitleLink',function(){
    $('#seo_title').removeAttr('readonly');
  });

if(page_url!=undefined)
{fetchUrlAxios(page_url);}


//dependent data
$(document).ready(function($) {

    //changed dynamic select value
    $('select').each(function()
    {
      var updatedValue = $(this).attr('changedValue');
      if($(this).attr('multiple'))
      {
        if(updatedValue!=undefined)
        {var obj = updatedValue.split(",");
        $(this).val(obj);
        }
        
      }
      else
      {
        $(this).val(updatedValue);
      }
      
    });

    //dynamic textarea ckeditor
    $('textarea').ckeditor();

});

$(".checkall").click(function(){
    $('input:checkbox').not(this).prop('checked', this.checked);
});

function showLoader()
{
  $('.overlay_loader').show();
}
function hideLoader()
{
  $('.overlay_loader').hide();
}


