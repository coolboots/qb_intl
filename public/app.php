<script>

var standalone = window.navigator.standalone,
  userAgent = window.navigator.userAgent.toLowerCase(),
  safari = /safari/.test(userAgent),
  ios = /iphone|ipod|ipad/.test(userAgent);

if (ios) {
  if (!standalone && safari) {
	  // Safarii
	  alert('safari');
  } else if (!standalone && !safari) {
	  // iOS webview
	   alert('in app safari');
  };
} else {
  if (userAgent.includes('wv')) {
	  // Android webview
	   alert('inapp android');
  } else {
	  // Chrome
	   alert('normal browser');
  }
};

</script>
