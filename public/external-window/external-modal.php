<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <meta name="robots" content="noindex" />
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
      rel="stylesheet"
    />
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/responsive.css" />
    <title>Game Quizzes</title>
  </head>
  <body>
    <main class="app-wrapper-container">
      <section class="intro-screen customAds">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <div class="custom-modal">
                <span id="seconds">5</span>
                <iframe src="https://www.youtube.com/embed/yAoLSRbwxL8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>

    <!-- jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="js/3.5.1-jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript">
     
      $(document).ready(function () {
        
        var timeLeft = 5;
          function countdown() {
            timeLeft--;
            document.getElementById("seconds").innerHTML = String(timeLeft);
            if (timeLeft === 0) {
              $('.custom-modal #seconds').empty().append('<img src="images/close.png">');
            }
            if (timeLeft > 0) {
              setTimeout(countdown, 1000);
            }
          }
          setTimeout(countdown, 1000);
          
          $(document).on('click', '.custom-modal #seconds img', function () {
            closeWindow();
          });
        });
        function closeWindow() {  
          window.close();  
        }  
// var elem = document.documentElement;
// function openFullscreen() {

//     if (elem.requestFullscreen) {
//     elem.requestFullscreen();
//   } else if (elem.webkitRequestFullscreen) { /* Safari */
//     elem.webkitRequestFullscreen();
//   } else if (elem.msRequestFullscreen) { /* IE11 */
//     elem.msRequestFullscreen();
//   }

// }
    </script>
  </body>
</html>
