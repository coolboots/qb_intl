<?php
namespace app\library;
class cacheManager {
    private $cachekey     = '';
    private $cacheData   = array();

    function addData($key,$data,$others=array())
    {
        // echo $key;die;
        if(!isset($others['cache_interval']))
        {
            $interval = 3600;
        }
        else
        {
            $interval = $others['cache_interval'];
        }
        $key = $this->genCacheKey($key);
        setLog($key,'coming fresh data');
        \Cache::put($key, ($data), $interval);

        // p($data);
       
       return json_decode($data);
    }

    function removeData($key)
    {
        $key = $this->genCacheKey($key);
        return \Cache::forget($key);
    }

    function getData($key)
    {
        $key = $this->genCacheKey($key);
         if(\Cache::has($key))
        {
            $data = \Cache::get($key);
            // p($data);
            setLog($key,'coming from cache');
            return json_decode($data);
        }
        return false;
    }
    

function genCacheKey($keyName){
        return sha1($keyName);
    }
}