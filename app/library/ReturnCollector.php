<?php
namespace App\library;
use Carbon\Carbon;
class ReturnCollector {
    private $data     = array();
    private $tmpArr   = array();
    function addData($content, $url, $ch, $search,$headerInfo=array()){
        //echo '<pre>';print_r($headerInfo);
        setLog($url,'This is some useful information');
        $headerInfo = array_slice($headerInfo, 0, 11);

        //\Log::info($search['module'], ['request_header' => json_encode($headerInfo)]);
        setLog(['request_header' => json_encode($headerInfo)],$search['module']);

        $this->tmpArr   = json_decode($content, true);
        //print_r($this->tmpArr);
        //$this->data[] = array_merge($this->tmpArr, $this->data);
        if($headerInfo['http_code']==200)
        {
            if($search['cache']==true and $search['cache_interval'] > 0)
            {
                //$expiresAt = \Carbon::now()->addMinutes(10);
                $interval = $search['cache_interval'] * 60;
                //\Log::info('creating cache for '.$interval);
                \Cache::put(sha1($url), $content, $interval);
            }
        }    
        $this->data[$search['module']] = $this->tmpArr;
    }
    function addCacheData($content,$module)
    {
        //\Log::info('creating cache for  :returning data from cache');
        $this->tmpArr   = json_decode($content, true);
        $this->data[$module] = $this->tmpArr;

    }
    function getData(){
        return $this->data;
    }
    function outputData(){
        //echo json_encode($this->getData());
        return $this->getData();
    }
}