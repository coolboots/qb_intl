<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
class Adsconfig extends Model  {

	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'publisher_configs';
	protected $fillable = ['page_id','publisher_id','partner_id','scripts_1','scripts_2','scripts_3','scripts_4','scripts_5','status'];

	/**
	 * The table key used by the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'id';
	
	/**
	 * The table fields used by the model.
	 *
	 * @var array
	 */
	//protected $fillable = [$fields];
	//protected $timestamps = false;

	public function page()
    {
        return $this->hasOne('App\Http\Models\Page','id','page_id');
    }
    public function publisher()
    {
        return $this->hasOne('App\Http\Models\Publisher','id','publisher_id');
    }
    public function partner()
    {
        return $this->hasOne('App\Http\Models\Partner','id','partner_id');
    }
    


	
}

			