<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redis;
class Contest extends Model  {

	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'contests';

	/**
	 * The table key used by the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'id';
	
	/**
	 * The table fields used by the model.
	 *
	 * @var array
	 */
	//protected $fillable = [$fields];
	//protected $timestamps = false;
	protected $casts = [
    'end_date' => 'timestamp',
];

	public function game()
    {
        return $this->hasOne('App\Http\Models\Game','id','game_id');
    }

    //get all matrix of contest
	public function getMatrixAttribute()
    {
    	return Awardmatrix::select('winner_from as from','winner_to as to','prize_type as type','prize_amount as amount')->where('contest_id',$this->id)->get();
        //return $this->hasMany('App\Http\Models\Awardmatrix','contest_id');
    }
    //get seo url
	public function getSeoUrlAttribute()
	 {
	    return $seo = Seo::where('object_id',$this->id)->where('object_type','contest')->pluck('object_data')->first();
	}
    
//get all matrix of contest
	public function getTotalUsersAttribute()
    {
    	$total_users = Redis::get('contest:'.$this->id.':total_users');
    	//$total_users = Contestparticipanthistory::where('contest_id',$this->id)->count();
        return ($total_users)?$total_users:0;
    }
    






	
}

			