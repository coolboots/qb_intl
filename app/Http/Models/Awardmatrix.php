<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
class Awardmatrix extends Model  {

	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'contest_award_matrix';

	/**
	 * The table key used by the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'id';
	protected $fillable = ['contest_id','winner_from','winner_to','prize_type','prize_amount','created_at'];

	protected $hidden = array('created_at', 'updated_at');
	
	/**
	 * The table fields used by the model.
	 *
	 * @var array
	 */
	//protected $fillable = [$fields];
	//protected $timestamps = false;

	public function contest()
    {
        return $this->hasOne('App\Http\Models\Contest','id','contest_id');
    }


	
}

			