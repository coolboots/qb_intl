<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Models\PartnerDetail;
use App\Http\Models\PartnerPackage;

class Partner extends Model  {

	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'partners';

	/**
	 * The table key used by the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'id';
	
	/**
	 * The table fields used by the model.
	 *
	 * @var array
	 */
	//protected $fillable = [$fields];
	//protected $timestamps = false;

	public function getGoogleNetworkAttribute()
    {
    	$data = Adsconfig::select('pages.page_code as page_label','publisher_configs.scripts_1 as unit_name','publisher_configs.scripts_2 as slot','publisher_configs.scripts_3 as format','publisher_configs.scripts_4 as responsive','publisher_configs.scripts_5 as rawscript')->where('publisher_configs.partner_id',$this->id)
    			->leftJoin('pages','publisher_configs.page_id','pages.id')->where('publisher_configs.publisher_id',1)->where('publisher_configs.status','ACTIVE')->get()->toArray();
    			return objToArray($data,'page_label');
        //return $this->hasMany('App\Http\Models\Awardmatrix','contest_id');
    }

    public function getGooglePublisherAttribute()
    {
    	$publisher = Publisher::find(1);
    	return $publisher->toArray();
    }

	/**
     * Get the details record associated with the partner.
     */
    public function details()
    {
        return $this->hasOne(PartnerDetail::class);
    }

    public function packages()
    {
        return $this->hasOne(PartnerPackage::class);
    }
	
}

			