<?php

namespace App\Http\Models;
use App\Http\Models\Partner;

use Illuminate\Database\Eloquent\Model;

class PartnerDetail extends Model
{
	protected $fillable = ['max_click_per_day','blocked_hours_per_day','ads_tracker_enable','mail_to','mail_cc','mail_bcc'];

    /**
     * Get the partner that owns the partner details.
     */
    public function partner()
    {
        return $this->belongsTo(Partner::class);
    }
}
