<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
class Funquiz extends Model  {

	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'funquiz_callback';

	/**
	 * The table key used by the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'id';
	
	/**
	 * The table fields used by the model.
	 *
	 * @var array
	 */
	protected $fillable = ['domain','handshake_key'];
	//protected $timestamps = false;


	
}

			