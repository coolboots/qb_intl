<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
class Seo extends Model  {

	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'cms_seo_urls';

	/**
	 * The table key used by the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'id';
	
	protected $fillable = ['id', 'object_type', 'object_id', 'object_data'];

	 public $timestamps = false;

	
}

			