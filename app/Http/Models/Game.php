<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redis;
use Carbon\Carbon;
class Game extends Model  {

	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'games';

	/**
	 * The table key used by the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'id';
	
	/**
	 * The table fields used by the model.
	 *
	 * @var array
	 */
	//protected $fillable = [$fields];
	//protected $timestamps = false;

	 public function getHighScoreContestAttribute()
	 {
	    $contest =  Contest::where('game_id',$this->id)->select('contests.id','contests.end_time','cms_seo_urls.object_data as seo_url')
	    ->leftJoin('cms_seo_urls', function($leftJoin)
			        {
			            $leftJoin->on('cms_seo_urls.object_id', '=', 'contests.id')
			                ->where('cms_seo_urls.object_type', '=', 'contest');


			        })
	    ->where('contests.status','ACTIVE')
	    ->where('contests.end_time', '>', Carbon::now()->toDateTimeString())
	    ->where('contests.start_time', '<', Carbon::now()->toDateTimeString())
	    ->orderBy('contests.prize_money','DESC')->first();
	    if(!empty($contest))
	    {
	    	//$total_users = Contestparticipanthistory::where('contest_id',$contest->id)->count();
	    	$total_users = Redis::get('contest:'.$contest->id.':total_users');
		    $contest->total_users = ($total_users)?$total_users:0;
		    $contest->prize_breakup = Awardmatrix::select('winner_from as from','winner_to as to','prize_type as type','prize_amount as amount')->where('contest_id',$contest->id)->get();
		    return $contest;
	    } 
	    else
	    {
	    	return array();
	    }

	 }

	 public function getConsolidateHighScoreAttribute()
	 {
	 	$contestData = array();
	    $contest =  Contest::where('contests.game_id',$this->id)
	    			->where('contests.status','ACTIVE')
	    			->where('contests.end_time', '>', Carbon::now()->toDateTimeString())
	    			->where('contests.start_time', '<', Carbon::now()->toDateTimeString())
	    			->get();
	    $contestIds = objToArray($contest->toArray(),'id');
	    if(!empty($contest))
	    {
	    	//$total_users = Contestparticipanthistory::whereIn('contest_id',array_keys($contestIds))->count();
	    	$total_users = 0;
	    	if(!empty($contestIds))
	    	{
	    		foreach ($contestIds as $key => $value) {
	    			# code...
	    			$total_users += Redis::get('contest:'.$key.':total_users');
	    		}
	    	}
		    $contestData['total_users_playing'] = $total_users;
		    $contestData['total_prize_money'] = $contest->sum('prize_money');
		    $contestData['total_contest'] = $contest->count();
		    return $contestData;
	    }

	 }

	 //get seo url
	public function getSeoUrlAttribute()
	 {
	    return $seo = Seo::where('object_id',$this->id)->where('object_type','game')->pluck('object_data')->first();
	}

	 


	
}

			