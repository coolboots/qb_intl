<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
class Contestwinner extends Model  {

	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'contest_winners';

	/**
	 * The table key used by the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'id';
	
	/**
	 * The table fields used by the model.
	 *
	 * @var array
	 */
	//protected $fillable = [$fields];
	//protected $timestamps = false;

	public function game()
    {
        //return $this->hasOne('App\Http\Models\Game','id','game_id');
    }

    public function contest()
    {
        return $this->hasOne('App\Http\Models\Contest','id','contest_id');
    }
    public function user()
    {
        return $this->hasOne('App\Http\Models\User','id','user_id');
    }
    


	
}

			