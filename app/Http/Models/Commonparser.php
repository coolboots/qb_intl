<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
class Commonparser extends Model { 

	//

	public function parseRequest($request,$module='')
	{
		//$url = $request['modules'];
		//$request['modules'] = $url;
		$inputs = \Input::all();

		$url = $request['modules'];

		if(!empty($module))
		{
			$validUrl = $url[$module];
			if($module=='signup')
			 {	//p($inputs);
				$validUrl['params'] = $inputs;
			 }
			 elseif($validUrl['method']=='POST')
			 {
			 	$validUrl['params'] = $inputs;
			 }

			 //replace dynamic variable
			 foreach ($inputs as $key=>$input) {
			 	# code...
			 	$validUrl['url'] = str_replace('{'.$key.'}', $input, $validUrl['url']);
			 }
			 unset($url);

			 //to pass url
			 if(in_array($module, array('samplequestion')))
			 {
			 	$validUrl['url'] .= '?'.http_build_query($inputs);
			 }
			// p($validUrl);
			//print_r($module);die;
			$url[$module] = $validUrl;
			//print_r($url);

		}
		$request['modules'] = $url;
		return $request;
	}


	public function HomeRequest($request)
	{
		// $url = $request['modules']['list_cart']['url'];
		// //print_r($_GET); die;
		// $user_id = \Session::get('user_id');
		// if(isset($user_id) and !empty($user_id))
		// {
		// 	$url .='/'.$user_id;
		// }
		// $request['modules']['list_cart']['url'] = $url;
		$url = $request['modules'];
		if(!empty($module))
		{
			$validUrl = $url[$module];
			if($module=='signup')
			{$validUrl['params'] = \Input::all();}
			
			//print_r($module);die;
			$url[$module] = $validUrl;
			//print_r($url);

		}
		
		return $request;
	}
	public function PartnerRequest($request,$module='')
	{
		//echo '<pre>'; print_r($request);
		$id = \Input::get('cp_id');
		$url = $request['modules'];
		if(!empty($module))
		{
			$validUrl = $url[$module];
			if($module=='login')
			{
				$validUrl['url'] = $validUrl['url'];
				//print_r($validUrl);die;
			}
			elseif($module=='add' || $module=='update' || $module=='list')
			{
				$validUrl['params'] = \Input::all();
				if(isset($validUrl['params']['page']))
				{
					$validUrl['params']['offset'] = ($validUrl['params']['page']-1) * 10;
				}
				
				$validUrl['params']['cp_id'] = $id;
			}

			if($module=='update')
			{
				$validUrl['method'] = 'POST';
				//$request['params']['id'] = @$request['params']['partner_id'];
			}
			
			if($module=='list')
			{
				$search_url = $validUrl['url'].'&'.http_build_query($validUrl['params'],'', '&');
				$validUrl['url'] = $search_url;
			}
			unset($url);
			//print_r($module);die;
			$url[$module] = $validUrl;
			//print_r($url);

		}
		$request['modules'] = $url;
		//echo '<pre>after';print_r($request);die;
		return $request;
	}

	public function QuotationTrackerRequest($request,$module='')
	{
		//echo '<pre>'; print_r($request);
		$id = \Input::get('cp_id');
		$url = $request['modules'];
		if(!empty($module))
		{
			$validUrl = $url[$module];
			if($module=='quotation_time_tracker_reports')
			{
				$validUrl['url'] = $validUrl['url'].\Input::get('lead_crm_id');
				$validUrl['method'] = 'POST';
			}
			
			unset($url);
			$url[$module] = $validUrl;
			
		}
		$request['modules'] = $url;
		//echo '<pre>after';print_r($request);die;
		return $request;
	}

}
