<?php

namespace App\Http\Models;
use App\Http\Models\Partner;

use Illuminate\Database\Eloquent\Model;

class PartnerPackage extends Model
{
	protected $fillable = ['package_id'];

    /**
     * Get the partner that owns the partner details.
     */
    public function partner()
    {
        return $this->belongsTo(Partner::class);
    }
}
