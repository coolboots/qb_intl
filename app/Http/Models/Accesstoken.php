<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
class Accesstoken extends Model  {

	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'oauth_access_tokens';

	/**
	 * The table key used by the model.
	 *
	 * @var string
	 */
	//protected $primaryKey = 'id';
	

    public $timestamps = false;

	
}

			