<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
class Page extends Model  {

	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'pages';

	/**
	 * The table key used by the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'id';
	
	/**
	 * The table fields used by the model.
	 *
	 * @var array
	 */
	//protected $fillable = [$fields];
	//protected $timestamps = false;


	
}

			