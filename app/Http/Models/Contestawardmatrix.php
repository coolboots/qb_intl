<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
class Contestawardmatrix extends Model  {

	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'contest_award_matrix';

	/**
	 * The table key used by the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'id';
	
	/**
	 * The table fields used by the model.
	 *
	 * @var array
	 */
	protected $fillable = ['contest_id'];
	//protected $timestamps = false;


	
}

			