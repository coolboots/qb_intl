<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
class User extends Model  {

	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The table key used by the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'id';

	protected $hidden = array('password', 'token');
	
	/**
	 * The table fields used by the model.
	 *
	 * @var array
	 */
	//protected $fillable = [$fields];
	//protected $timestamps = false;

//get all transation history
		public function getHistoryAttribute()
		{
			return Userwallettranshistory::select('user_id','mode_type','coin_type','amount','remarks','created_at','updated_at')->where('id',$this->id)->get();
			
		}

	public function coin()
    {
        return $this->hasOne('App\Http\Models\Userwalletcoin','user_id');
    }
    public function cash()
    {
        return $this->hasOne('App\Http\Models\Userwalletcash','user_id');
    }
    
	

	


}

			