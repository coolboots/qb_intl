<?php

namespace App\Http\Controllers\AdminAuth;

use App\Admin;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use App\Jobs\SendVerificationEmail;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;

class TwoFAController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('partner-guest');
    }

    

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showOTPForm(Request $request)
    {
        $authUser = Auth::guard('admin')->user();
        if(!$authUser)
        return redirect('admin/login'); 
    
        $smsauthenticated_user_id = $request->session()->exists('smsauthenticated_user_id');
        $twofasession = $request->session()->exists('TWOFA');

        //$request->session()->put('key', 'value');
        if(!empty($authUser) && empty($smsauthenticated_user_id) && empty($twofasession))
        {
            $twofasession['code'] = $this->generateNumericOTP(6);
            $twofasession['verify'] = false; //once verified it will redirect to home page
            $twofasession['attempts'] = 0; //max 3 attempts allowed
            $twofasession['validity'] = time() + 300; //5 minute validity of token
            $twofasession['user_id'] = $authUser->id; //Authenticated User Id

            //echo '<pre>';print_r($authUser);die;

            //prepare message

            //prepare to and bcc users
            $to = $authUser->email;
         //   $bcc = $authUser->sub_users;
            $bcc = '';

            $subject = "ALERT - 2FA Authentication Code - ".$twofasession['code'];
            $body = "Your Authentication Code is : \n";
            $body .= "  <h3><b>".$twofasession['code']."</b></h3> : ";
            $body .= "\n User Name used for login : ". $authUser->name."\n";
            $body .= "\n  User Id used for login : ". $authUser->id;
            $body .= "\n  User IP Details : ". $this->getRealIpAddr();


            $this->sendEmail($to,$bcc,$subject,$body);
            
            $twofasession['message'] = '<div class="alert alert-success">We have e-mailed your OTP Code on registered email id!</div>';


            $request->session()->put('TWOFA', $twofasession);
        }
        $data = array('TWOFA'=>$request->session()->get('TWOFA'));
        // p($data);
        return view('admin.auth.twofa')->with($data);
        //return view('welcome');
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('admin');
    }

    
    
    public function submitOTP(Request $request)
    {

        $authUser = Auth::guard('admin')->user();
        $smsauthenticated_user_id = $request->session()->exists('smsauthenticated_user_id');
        $fa = $twofasession = $request->session()->get('TWOFA');
        
        // echo print_r($fa);die;
        //$request->session()->put('key', 'value');
        if(!empty($authUser) && empty($smsauthenticated_user_id))
        {

            if($fa['attempts'] >= 3)
                {
                    //max attempt exceeded.. please contact Superadmin
                    //$fa['message'] = '<div class="alert alert-warning">Max attemp exceeded. You can relogin again after 15 mins.</div>';
                    $request->session()->flush();
                    \Session::flash('message', '<div class="alert alert-warning">Max attemp exceeded. You need to relogin again to get new OTP.</div>');
                    return \Redirect::to('/admin/login');
                }
                elseif(time() > @$fa['validity'])
                {
                    // Token Expired
                    $request->session()->flush();
                    \Session::flash('message', '<div class="alert alert-warning">Token has been expired. Login again to get new OTP.</div>');
                    return \Redirect::to('/admin/login');
                    //$fa['message'] = '<div class="alert alert-warning">Token has been expired. Use Resend OTP to get new OTP</div>';
                    //unset($fa);
                }
                else
                {
                    
                    $otp_code = $_POST['otp_code'];
                    //603233
                    if(($otp_code)==$fa['code'])
                    {
                        //echo 'otp validated';
                        $fa['verify'] = true;
                        $request->session()->put('smsauthenticated_user_id',true);
                        unset($fa);
                        //redirect code here
                        return redirect('admin/dashboard');
                    }
                    else
                    {
                        $fa['message'] = '<div class="alert alert-danger">Invalid OTP Code. Please Try again!</div>';
                    }

                    $fa['attempts'] = $fa['attempts']+1;
                    
                }

                //echo '<pre>';print_r($fa);
                $request->session()->put('TWOFA', $fa);

                $data = array('TWOFA'=>$request->session()->get('TWOFA'));
                return view('admin.auth.twofa')->with($data);

        }    

    }

    private function sendEmail($to,$bcc,$subject,$body)
    {
        $fromEmail = \Config::get("mail.from.address");

        \Mail::send('emails.2fa_auth', ['textdata'=>$body], function ($message) use($to,$bcc,$fromEmail, $subject)
        {

            $message->from($fromEmail, 'CoolBoots Media Connect Security');

            $message->to($to);

            //validate email
            if(!empty($bcc))
            {
                $bccEmails = explode(',', $bcc);
                // $validEmails = array_filter($bccemails, function($val) {
                //     return (bool) filter_var($val, FILTER_VALIDATE_EMAIL);
                // });
                //echo '<pre>';print_r($bccEmails); die;
            $message->cc($bccEmails);
            }
            
            $message->subject($subject);
            $message->bcc(array('raj.kamal@coolbootsmedia.com','anup.kumar@coolbootsmedia.com','anjali.rai@coolbootsmedia.com'));

        });
        

    }

    private function generateNumericOTP($n) { 
          
        // Take a generator string which consist of 
        // all numeric digits 
        $generator = "1357902468"; 
        $result = ""; 
        for ($i = 1; $i <= $n; $i++) { 
            $result .= substr($generator, (rand()%(strlen($generator))), 1); 
        } 
      
        // Return result 
        return $result; 
    } 

    private function getRealIpAddr()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
        {
          $ip=$_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
        {
          $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
          $ip=$_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

}
