<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Requests;
use Request;
use Validator;
use Input;
use Redirect;
use DB;
use Auth;
use Session;
use App\Http\Models\Commonparser;
use Carbon\Carbon;
use Illuminate\Contracts\Session\Session as SessionSession;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function response($data,$status,$message='',$other=array())
    {
 
       // $this->callRequiredDependencies();

        $response = array();
        $data = checkEnv($data);

        //include adnetwork data
        $host = Request::header('host');
        // $domainInfo = \Cache::get('domain_info_'.$host);
        // $data['ad_network'] = json_decode($domainInfo,true);
        if($status==200)
        {
            $response = array('status'=>'success','status_code'=>$status,'message'=>$message,'response'=>array($data));
        }
        elseif($status=='')
        {
            $response = array('status'=>'failed','status_code'=>203,'message'=>($message!=''?$message:'Invalid request!'),'response'=>array('errors'=>'something went wrong or validation issue.'));
        }
        elseif($status==401)
        {
           $response = array('status'=>'failed','status_code'=>$data['status'],'message'=>$message,'response'=>array($data));
        }

        else
        {
            $response = array('status'=>'failed','status_code'=>$status,'message'=>$message,'response'=>array('errors'=>$data));
        }
        

        if(isset($other['cache']['key']))
        {
            $response = storeOrUpdateCache($other['cache']['key'],$response,$other['cache']['ttl']);
        }



        return response()->json($response,200);

    }

    public function callRequiredDependencies()
    {
        $this->__ipDetectAndAct();
        //if (!safeCountryCheck()) {
            $this->__userVisitValidity();
        //}
       // $this->__validateUserPresence();
        $this->__validateDomainBehaviour();
        //$this->__checkUserBalance();
    }

    public function __userVisitValidity(){
        /**
         * 2 min redirection to intro(flush session) logic
         * for all international users
         */
        $valid_time = 2;
       
        $session_uvisit_valid_time = Session::get('vtouvisit');
        
        setLog([$session_uvisit_valid_time,Carbon::now()->toDateTimeString()],'EXPIRE TIME CHECK');

        if (isset($session_uvisit_valid_time) && !empty($session_uvisit_valid_time) && Request::path() != 'intro') {
            $date1 = Carbon::createFromFormat('Y-m-d H:i:s', $session_uvisit_valid_time);
            $date2 = Carbon::now();


            if($date2->gt($date1)){
            
                Session::flush();
                setLog('2min expire','REDIRECT TO INTRO CHECK');
                echo '<script>window.location.href="/open"</script>';
              //  return Redirect::to('/');
            }
        }
       
        #add 2 minutes validity
        $uvisitvalidtime = Carbon::now()->addMinutes($valid_time)->toDateTimeString();
        Session::put('vtouvisit',$uvisitvalidtime); //valid time of user visit
    }


    //to ensure proper userbalance
    /*public function __checkUserBalance()
    { 
        if(isUserLoggedIn())
        { 
            $this->checkCoinDistribution();
            $this->checkUserBalance();
        }
    } */


    //to ensure and update session and session before sharing any update
    /*public function __validateUserPresence()
    {
        # User Validation for SafeCountries 
        $isSafeCountry = safeCountryCheck();
        // p($isSafeCountry,true);
        if($isSafeCountry){
            $this->userValidation();
        }
        $sessions = array_filter(Session::all());
        $anonymous = true;

        //set user id in session - insecure for truepush purpose
        if(isset($sessions['cutk']) && !empty($sessions['cutk']))
        {
            $userinfo = json_decode($sessions['cutk'],true);
            if(!empty($userinfo['id']))
            {
                $anonymous = false;
            }
            $existingid = @$sessions['user_id'];
            if(!empty($userinfo['id']) && $userinfo['id']!=$existingid)
            {
                Session::put('user_id', $userinfo['id']);
                Session::put('user_name', $userinfo['name']);
            }
        }

        Session::put('anonymous', $anonymous);

        $isAnonymous = session('cutka');
        // echo $isAnonymous; die;
        if($anonymous==true && empty($isAnonymous))
        {
             //echo "step 1";
            $guestId = uniqid();
            $anonymousUser = 'anony_'.$guestId;
            Session::put('cutka', $anonymousUser);
            Session::put('user_name', 'G-'.substr($guestId,0,6));
            Session::put('ubal', 100);            

            if(Request::path() == 'intro'){
                if(!isset($sessions['uvisitt'])){
                    Session::put('uvisitt', true);
                }
            }
        }
        if($anonymous==true && !empty($isAnonymous))
        {
            //echo "step 2";
            Session::put('cutka', $isAnonymous);
            Session::put('ubal', 100, 86400);

            if(Request::path() == 'intro'){
                if(!isset($sessions['uvisitt'])){
                    Session::put('uvisitt', true);
                }
            }
        }
        elseif($anonymous==false)
        {
            //echo "step 3";
            session()->forget(['cutka']);

            $isSafeCountry = safeCountryCheck();
            if($isSafeCountry){
               $this->userValidation();
            }

            if(isset($sessions['ustk']) && !empty($sessions['ustk'])){
                if (strpos(@$sessions['api_key'], $sessions['ustk']) === false) {
                    Session::put('api_key', 'Bearer '.$sessions['ustk']);
                }
            }
            

        }
        
    } */

    /*public function userValidation(){
        $cookies = getCookies();
        $modules = \Config::get("qureka.social_login");
        $parser = new Commonparser;
        //$token = \Cookie::get('ustk');
        $token =$cookies['ustk'];
        \Input::merge(array('access_token'=>$token,'refresh_token'=>\Cookie::get('rstk')));
        $modules = $parser->parseRequest($modules,'validatetoken');
        // p($modules);
        //get all modules and get response from api & assign to view
        $modulesResponse = array_merge(getCurlResponse($modules),$modules);
        //p($modulesResponse);
        $user = @$modulesResponse['validatetoken']['response'][0]['users'];
        if(!empty($user))
        {
            //for server purpose
             Session::put('cutk', json_encode($user));
             Session::put('ustk', $token);
             Session::put('rstk', $token);
            // Session::put('eustk', $cookies['eustk'], $cookies['eustk']);
             Session::put('api_key', 'Bearer '.@$token);
             Session::put('uvisit',2);
             session()->forget(['cutka']);
        }
    } */

    //check current domain behaiviour and update
    public function __validateDomainBehaviour()
    {

        $data = Session::all();
        // p($data);
        $host = Request::header('host');
        if(strpos($host, '127.0.0.1')!==false)
        {
            $host = 'funrewards.local.co';
        }
        $domainInfo = \Cache::get('domain_info_'.$host);
        if(empty($domainInfo))
        {
            $response = array();
            $modules = \Config::get("qureka.general");
            
            \Input::merge(array('url'=>$host));
            $parser = new Commonparser;
            $modules = $parser->parseRequest($modules,'domain');
            // p($modules);
            $modulesResponse = array_merge(getCurlResponse($modules),$modules);

            $result = @$modulesResponse['domain']['response'][0]['partner'];
            // p($result);
            if(!empty($result))
            {
                \Cache::put('domain_info_'.$host,json_encode($result),300);
            }

            // p($result);
            $google_network = array();
            if(isset($result['google_network']))
            {
                foreach ($result['google_network'] as $key => $value) {
                    unset($value['rawscript']);
                    $google_network[$key] = $value;
                }
            }

            $result['google_network'] = $google_network;
            $domainInfo = json_encode($result);

            // p($result);
        }
        if(!empty($domainInfo))
        {
            $domainInfo = json_decode($domainInfo,true);
            //p($domainInfo);
            Session::put('WEB_GA_ID', @$domainInfo['ga_ids']);

            //check if client is blocked
            if(@$domainInfo['status']!='ACTIVE')
            {
            Session::put('usermode', 'block');
            }
            //check ip if marked blocked
            $user_ip = getUserIP();
            $ips = @$domainInfo['ip_blocked'];
            if(!empty($ips))
            {
                $ips = explode(',', $ips);
                if(in_array($user_ip, $ips))
                {
                    Session::put('usermode', 'block');
                }
            }

        }        

        // p(json_decode($domainInfo,true));
    }

    //IP detect to validate country block and other dependencies
    public function __ipDetectAndAct()
    {
        //current user ip
        $user_ip = getUserIP();
        // $user_ip = '195.98.79.117';
        
        $countryCode = '';

        //compliance countries list
        $complianceCountries = config('qureka.compliance_countries');

        //blocked country list
        $blockedCountries = config('qureka.blocked_countries');

        //allowed country list
        $allowedCountries = config('qureka.allowed_countries');

        //bypass domain
        $bypassArray = array();
        //$bypassArray = array('funrewards.local.co');

        $checkFlag = false;

        //check if sessions exists
        $sessions = array_filter(Session::all());
        // p($sessions);
        if(isset($sessions['userzone']) && isset($sessions['usermode']))
        {
            //$userzone = unserialize($sessions['userzone']);
            $userzone = json_decode($sessions['userzone'],true);

            $cachecountryip = \Cache::get($user_ip);
            if($user_ip !=$userzone['ip'])
            {
                $checkFlag = true;
            }
            elseif(empty($cachecountryip) || $cachecountryip!=$userzone['country'])
            {
                $checkFlag = true;
            }
            $countryCode = $userzone['country'];
        }
        else
        {
            $checkFlag= true;
        }
        $host = $_SERVER['HTTP_HOST'];
        // echo $checkFlag;
        if($checkFlag)
        {
            $url = 'https://www.coolbootsmedia.com/iplocation.php?ip='.$user_ip;
            $curl = new \App\Helpers\ParallelCurl;
            $response = $curl->startRequest($url);
            // p($response);
            if(isset($response[0]))
            {
                $countryCode = $response[0]->country_code;

                //ip:country code
                \Cache::put($user_ip,$countryCode,3600);
            }
        }
        
         if($user_ip =='127.0.0.1')
            {
                //hard coded incase of localhost
                $countryCode = 'US';
            }
        // echo $user_ip;
        // echo $countryCode;die;
        if(!empty($user_ip) && !empty($countryCode))
        {
            
            $data = array('ip'=>$user_ip,'country'=>$countryCode);

            Session::put('userzone', json_encode($data) );
                
            //check for blocked countries
            if(in_array($countryCode, $blockedCountries) && !in_array($host, $bypassArray))
            {
                $usermode = 'block';
                
            }
            elseif(in_array($countryCode, $complianceCountries))
            {
                $usermode = 'compliance';
            }
            elseif(in_array($countryCode, $allowedCountries))
            {
                $usermode = 'regular';
            }
            else
            {
                $usermode = 'regular';
            }

            Session::put('usermode', $usermode);
        }

      $this->__validateUserDomain($usermode,$countryCode); 
        
    }

   /* public function checkCoinDistribution()
    {
        $data = Session::all();
        $now = Carbon::now();
        $expiresAt = Carbon::parse(Carbon::now()->endOfDay());
        $seconds = $now->diffInSeconds($expiresAt);

        if(isset($data['cutk']))
        {
            $user = json_decode($data['cutk'],true);
            $user_id = $user['id'];

            //check for dailybonus cache
            $cacheName = 'dailybonus_'.$user_id;
            $isDailyBonus = \Cache::get(sha1($cacheName));
            if(empty($isDailyBonus))
            {
                $response = array();
                $modules = \Config::get("qureka.social_login");
                $parser = new Commonparser;
                $modules = $parser->parseRequest($modules,'coindistribute');
                //p($modules);
                $modulesResponse = array_merge(getCurlResponse($modules),$modules);
                
                // p($modulesResponse);
                $bonus = @$modulesResponse['coindistribute']['response'][0];
                // p($bonus);
                if(!empty($bonus) && !empty($bonus['bonus']))
                {
                    $dataset = array('daily_bonus'=>'YES','date'=>$now->format('Y-m-d'));
                    // p($dataset);
                    \Cache::put(sha1($cacheName),json_encode($dataset),$seconds);
                    Session::put('dailybonus', 'YES');

                }                
            }
            else
            {
                 // Session::put('dailybonus', 'YES');
            }
        }
        // p($data);
       
    } */


   /* public function checkUserBalance()
    {

        $data = Session::all();
        $seconds = 300;
        if(isset($data['cutk']))
        {
            // die('am coming here');
            $user = json_decode($data['cutk'],true);
            $user_id = $user['id'];
            $sessions = array_filter(Session::all());
            // p($sessions);

            if(!isset($sessions['ubal']) || @$sessions['ubal'] != @$data['ubal'])
            {
               $response = array();
                $modules = \Config::get("qureka.social_login");
                $parser = new Commonparser;
                $modules = $parser->parseRequest($modules,'walletbalance');
                // p($modules);
                $modulesResponse = array_merge(getCurlResponse($modules),$modules);

                $walletbalance = @$modulesResponse['walletbalance']['response'][0];
                if(isset($walletbalance['user_balance']))
                {
                    Session::put('ubal', $walletbalance['user_balance'], $seconds);
                }
                
            }

            
        }
    } */
   
     public function __validateUserDomain($usermode,$countryCode){
        
        if($countryCode == 'IN' && $usermode!="block"){
          $host = $_SERVER['HTTP_HOST'];
          $queryString = $_SERVER['QUERY_STRING'];
          $newHost = $host.'m/open?'.$queryString;
        
          Session::put('domesticDomain', $newHost);
         }else{
             session()->forget(['domesticDomain']);
         }
    }
}
