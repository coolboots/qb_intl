<?php namespace App\Http\Middleware;

// First copy this file into your middleware directoy

use Closure;
use Auth;
use App\modules\backend\rbac\Models\Module;
use App\modules\backend\rbac\Models\Privilege;

class AdminRoleAuthenticate{

	/* *
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 https://gist.github.com/drawmyattention/8cb599ee5dc0af5f4246
	 */
	public function handle($request, Closure $next)
	{
		// Get the required roles from the route
		$admindata = auth()->guard('admin')->user();

		// p($request->route());
	    $capabilityArray = $this->getRequiredRoleForRoute($request->route());
	    // echo '<pre>';print_r($capabilityArray);die;
		$adminid = $admindata->id;
		$roleid = $admindata->usergroup_id;
		$method = $request->method();
		$access = false;
	    $capability = $capabilityArray['mapping_url'];
	    if($method=='PUT' || $method=='DELETE')
	    {
	    	$capable = explode('/', $capability);
	    	//print_r($capable);//die;
	    	if(isset($capable[1]) && !isset($capable[2]))
	    	{
	    		//echo $capable[1];
	    		 $capabilitynew = str_replace(array('{','}'), '', $capable[1]);
	    		 //echo strpos($capable[0], $capabilitynew);
	    		if(strpos($capable[0], $capabilitynew)!==false)
	    		{
	    			//echo 'am';
	    			$capability = $capable[0];
	    		}

	    	}
	    	//echo $capability; die;
	    	// if((isset($capable[2]) && $capable[2]=='edit'))
	    	// {

	    	// }
	    	// if(@$capable[1]=='{'.@$capable[0].'}' && !isset($capable[2]))
	    	// {
	    	// 	$capability = $capable[0];
	    	// }
	    }
	    elseif($method=='GET')
	    {
	    	$capable = explode('/', $capability);
	    	// print_r($capable);die;
	    	if((isset($capable[2]) && $capable[2]=='edit') || (isset($capable[1]) && @$capable[1]=='create'))
	    	{
	    		$capability = $capable[0];
	    	}
	    	if(isset($capable[1]) && !isset($capable[2]))
	    	{
	    		$capabilitynew = str_replace(array('{','}'), '', $capable[1]);
	    		if(strpos($capable[0], $capabilitynew)!==false)
	    		{
	    			$capability = $capable[0];
	    		}
	    	}
	    	
	    	
	    }
	    //elseif()
	    
		//echo $roleid;die;
		
		if (\Cache::has('user_capablities_'.$roleid) ==false) {
		    //
		    $result = objToArray(Module::select('rk_modules.*','rk_usergroup_privilege.is_access','rk_usergroup_privilege.usergroup_id','rk_usergroup_privilege.is_write','rk_menu_categories.name as menu_group','rk_menu_categories.position as menu_group_position')
			->leftJoin('rk_usergroup_privilege', 'rk_modules.module_id', '=', 'rk_usergroup_privilege.module_id')
			->leftJoin('rk_menu_categories', 'rk_modules.menu_type', '=', 'rk_menu_categories.menu_category_id')
			->where('rk_usergroup_privilege.usergroup_id',$roleid)
			->where('rk_modules.status','Active')
			->where('rk_usergroup_privilege.status','Active')
			->get()->toArray(),'mapping_url');

		//echo '<pre>raj here';print_r($result);die;

		\Cache::put('user_capablities_'.$roleid, json_encode($result), 1);
		}

		
		if($roleid!=1)
		{
			//echo $roleid;die;
			// p($capability);
			$result = objToArray(Module::select('rk_modules.*','rk_usergroup_privilege.is_access','rk_usergroup_privilege.is_write')
			->leftJoin('rk_usergroup_privilege', 'rk_modules.module_id', '=', 'rk_usergroup_privilege.module_id')
			->where('rk_usergroup_privilege.usergroup_id',$roleid)
			->where('rk_usergroup_privilege.status','Active')
			->where('rk_modules.mapping_url',$capability)
			->where('rk_modules.status','Active')
			// ->where(function($query) use ($capability){
			// 	if(!empty($capability))
			// 	{
			// 		//if(strpos($capability, needle))
			// 		$query->where('rk_modules.mapping_url', '=', $capability);
			// 	}
			
			// 	})
			->where('rk_modules.module_type',$capabilityArray['module'])
			->get()->toArray(),'mapping_url');

			// echo '<pre>';print_r($result);die;
			if(isset($result[$capability]) and !empty($result[$capability]))
			{
				// echo '<pre>';
				// echo $capability;
				// print_r($result);
				if($result[$capability]['is_access']=='Y')
				{
					if($result[$capability]['is_write']=='N' and $method!='GET')
						$access = false;
					else
						$access = true;
				}

			}
			//access unregistered module
			// $is_module = Modules::select('modules.module_name')->where('modules.module_name',$capability)->take(1)->get()->toArray();
			// if(empty($is_module))
			// {

			// 	$access = true;
			// }


			


		}
		else
		{
			$access=true;
		}
		//die;
		
		//check if login is successful and having limited access - redirect to dashboard
		if($capabilityArray['module']=='cms' && (in_array($capability, array('home','dashboard','unauthorized','admin'))))
		{
			$access = true;
		}

		
		//$access = true;
		// Check if a role is required for the route, and
		// if so, ensure that the user has that role.
		if($access===true)
		{
			return $next($request);
		}


		// return response([
		// 	'error' => [
		// 		'code' => 'INSUFFICIENT_ROLE',
		// 		'description' => 'You are not authorized to access this resource.'
		// 	]
		// ], 401);
		return redirect('admin/unauthorized');		

	}

	private function getRequiredRoleForRoute($route)
	{

		$data = array();
		$actions = $route->getAction();
		$prefix = str_replace('/', '', $actions['prefix']);
		$data['mapping_url']=str_replace(array($prefix.'/'), '', $route->uri());
		$data['module']=$actions['module'];
		// $data['controller']=@$actions['controller'];
		//echo $route->getPath();
		//  
		// echo '<pre>';print_r($actions);

		// //remove prefix
		// $prefix = $actions['prefix'];
		// //print_r(\Route::current()->uri());die;
		// $str =strtolower(str_replace($actions['namespace'].'\\','',strstr($actions['controller'], 'Controller@', true)));
		// return $actions["module"].'_'.$str;
		//echo $actions["module"].'_'.$str;die;
		//return $str;
		//return 'cms_company';
		return $data;
		
	}

}
