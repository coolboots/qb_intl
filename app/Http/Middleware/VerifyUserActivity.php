<?php 
namespace App\Http\Middleware;
use Request;
use Closure;
use Session;
class VerifyUserActivity
{
    /**
     * The following method loops through all request input and strips out all tags from
     * the request. This to ensure that users are unable to set ANY HTML within the form
     * submissions, but also cleans up input.
     *
     * @param Request $request
     * @param callable $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $isDomesticDomain = Session::get('domesticDomain');
       
       if($isDomesticDomain){
           header("Location: ".$_SERVER['REQUEST_SCHEME'].'://'.$isDomesticDomain);
           exit;
       } 

        return $next($request);
    }
}
