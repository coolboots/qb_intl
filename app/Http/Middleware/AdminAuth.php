<?php

namespace App\Http\Middleware;

use Closure;

class AdminAuth {
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next, $guard = 'admin') {

		//echo '<pre>';print_r(auth()->guard('admin')->user());die;
		if (!auth()->guard($guard)->check()) {
			//
			return redirect('admin/login');			
		}

		return $next($request);
	}
}
