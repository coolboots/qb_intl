<?php 
namespace App\Http\Middleware;
use Illuminate\Cookie\CookieValuePrefix;
use Illuminate\Contracts\Encryption\Encrypter;
use Illuminate\Cookie\Middleware\EncryptCookies;

use Closure;
use Crypt;
class PreventDirectHits
{
    /**
     * The following method loops through all request input and strips out all tags from
     * the request. This to ensure that users are unable to set ANY HTML within the form
     * submissions, but also cleans up input.
     *
     * @param Request $request
     * @param callable $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $flag = false;
        $request_type = in_array($request->method(), ['HEAD', 'GET', 'OPTIONS','PUT','POST']);
        //echo $request->url(); die;
        $full_url = $request->url();
        //echo $full_url;
        // p($request->header());

        //echo $request->header('host');

        //p($request->header());
        if(strpos($full_url, '/callback') !==false)
        {
            return $next($request);
        }

        else if(strpos($full_url, '/api/') !==false)
        {
            
            $token = @$request->input('_token') ?: @$request->header('X-CSRF-TOKEN');
            //echo $token; die;
            //echo $request->header('X-XSRF-TOKEN');
            
            if (! $token && $header = $request->header('X-XSRF-TOKEN')) {
                $token = substr(Crypt::decrypt($header, EncryptCookies::serialized('XSRF-TOKEN')),41);
                //$token = CookieValuePrefix::remove($Encrypter->decrypt($header, static::serialized()));
            }

            //echo $token."<br>"; die;

            $is_matched = is_string($request->session()->token()) &&
                   is_string($token) &&
                   hash_equals($request->session()->token(), $token);

            if($is_matched)
            {
                $flag = true;
                return $next($request); 
            }

            
            if($flag==false)
                return response(['error' => ['code' => 'INVALID_TOKEN3','description' => 'Invalid Token or Input Type.']], 401);
                
        }

        return $next($request);

        
        
    }
}