<?php

namespace App\Http\Middleware;

use Illuminate\Cookie\Middleware\EncryptCookies as Middleware;

class EncryptCookies extends Middleware
{
    /**
     * The names of the cookies that should not be encrypted.
     *
     * @var array
     */
    protected $except = [
        //ubal= to maintain user balance, dailybonus to manage popup
        //usermode - regular/block/compliance
        //userzone - user ip and country
        'loggedIn','dailybonus','ubal','usermode','userzone','WEB_GA_ID','user_id','user_name','sociallogin','contest_details'
    ];
}
