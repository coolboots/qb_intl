<?php 
namespace App\Http\Middleware;
use App\Http\Models\Accesstoken;
use Illuminate\Support\Facades\Auth;
use Lcobucci\JWT\Parser;

use Closure;
class VerifyUserToken
{
    /**
     * The following method loops through all request input and strips out all tags from
     * the request. This to ensure that users are unable to set ANY HTML within the form
     * submissions, but also cleans up input.
     *
     * @param Request $request
     * @param callable $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $flag = false;
        $value = $request->bearerToken();
        $id = (new Parser())->parse($value)->getClaim('jti');

        $token = Accesstoken::where('id',$id)->where('expires_at','>=',date('Y-m-d H:i:s'))->get();
        if(isset($token[0]) && $token[0]->user_id > 0)
        {
            \Input::merge(['token_user_id' => $token[0]->user_id]);
           $flag = true;
                return $next($request); 
        }
        
        if($flag==false)
            return response(['error' => ['code' => 'INVALID_TOKEN','description' => 'Invalid Token or Input Type.']], 401);
        
    }
}