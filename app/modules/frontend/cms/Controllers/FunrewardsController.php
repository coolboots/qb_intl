<?php 
namespace App\modules\frontend\cms\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Commonparser;
use Carbon\Carbon;
use Request;
use Validator;
use Input; 
use Redirect;
use DB;
use Auth;
use Cookie;
use Session;
use Config;
use Illuminate\Support\Facades\Redis;

class FunrewardsController extends Controller
{

    /**14583
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct()
    {
        $this->callRequiredDependencies();

    }

    public function getBase()
    {
       $inputs =  \Input::all();
       Session::put('src_domain',@$inputs['src_domain']);
       Session::put('handshakekey',@$inputs['handshakekey']);

       return redirect('/open/intro');
    }

    public function getIntro()
    {
        //die('am');
        $response = array();
        return view('cms::openrewards.Intro')->with($response);exit;
    }

    
    
    //get sample questions
    public function getSampleQuestions()
    {
        //required input  - origin, language code

        updateOrigin();
        // echo $userzone; die;
        \Input::merge(array('lang'=>'EN'));
        
        $status = 400;
        $response = array();

        $key = \Config::get('qureka.API_QUREKA_PUB_KEY');
        if(!empty($key))
        {session(['api_key'=>'Bearer '.$key]);}

        $modules = \Config::get("qureka.pre_flow");
        $parser = new Commonparser;

        $modules = $parser->parseRequest($modules,'samplequestion');
       // p($modules);
        $modulesResponse = array_merge(getCurlResponse($modules),$modules);
         //p($modulesResponse);
        $samplequestion = @$modulesResponse['samplequestion']['response'][0];
        
        if($samplequestion) 
        {
            $status=200;
            $response['samplequestion'] = $samplequestion;
        } 
        else
        {
            return Redirect::to('/open/intro');
        }
        session()->forget(['api_key']);
	 // p($response);
        //return $response =  $this->response($response,$status,'Collection');
        return view('cms::openrewards.Home')->with($response);exit;
    }

    public function getScore()
    {
        $response = array();
        $response['status'] = 400;
        $response['message'] = 'Unsuccess';

         $srcDomain = Session::get('src_domain');
         $handshakekey = Session::get('handshakekey');
         \Input::merge(array('src_domain'=>$srcDomain,'handshakekey'=>$handshakekey));

        $modules = \Config::get("qureka.pre_flow");
        $parser = new Commonparser;
        $modules = $parser->parseRequest($modules,'funquiz');
        //p($modules);
        $modulesResponse = array_merge(getCurlResponse($modules),$modules);
        //p($modulesResponse);
        $response['status'] = 200;
        $response['message'] = @$modulesResponse['funquiz']['message'];



      /*  if(!empty($srcDomain) && !empty($handshakekey)){

                $recordId = \DB::table('funquiz_callback')->where('mode','open')->where('domain',$srcDomain)->where('handshake_key',$handshakekey)->select('id')->first();

                if(empty($recordId)){
                    $playedUrl = "$_SERVER[HTTP_HOST]";

                    \DB::table('funquiz_callback')->insertGetId(
                    ['mode' => 'open','domain' => $srcDomain, 'handshake_key' => $handshakekey, 'event' => '', 'raw_data' => '', 'playedUrl'=>$playedUrl,'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
                );
                    $response['status'] = 200;
                    $response['message'] = 'Success';
                }                
        } */

       //  \Input::merge(array('src_domain'=>$srcDomain,'handshakekey'=>$handshakekey));

       //  $modules = \Config::get("qureka.pre_flow");
       //  $parser = new Commonparser;
       //  $modules = $parser->parseRequest($modules,'funquiz');
       // // p($modules);
       //  $modulesResponse = array_merge(getCurlResponse($modules),$modules);
       // // p($modulesResponse);

       //  $response['message'] = @$modulesResponse['funquiz']['message'];

        Session::put('uvisit',2);
        return view('cms::openrewards.Score')->with($response);exit;
    }

    public function PageNotFound(){
        return view('cms::openrewards.PageNotFound');
    }

    public function optout()
    {
        $response = array();
        $response['titleIcon'] = '/assets/images/opt-icon.png';
        $response['titleName'] = 'Quiz Bytes';
        $response['isBack'] = false;
        $response['backUrl'] = "/";
        return view('cms::common.Optout')->with($response);exit;
    }

     public function userOptout()
    {
        $status = 400;
        $ustk = Session::get('ustk');
        $userzone = json_decode(Session::get('userzone'));
        $country = $userzone->country;
        // p($sessions);
        $complianceCountry = config('qureka.compliance_countries');
        
        if (in_array($country,$complianceCountry)){
            Session::put('optout','true');
            $modulesResponse['optout'] = ['status_code'=>200,'status'=>'success','message'=>'Opt Out Guest Success'];
            $status = 200;
            
        }

        if ($status ==200) {
            return $modulesResponse['optout'];
        }else{
            return ["status_code"=>400,'Opt out'];
        }
    }

    public function privacyPolicy()
    {
        $response = array();
        $response['titleIcon'] = '/assets/images/opt-icon.png';
        $response['titleName'] = 'Privacy Policy';
        $response['isBack'] = true;
        $response['backUrl'] = "";
        return view('cms::common.Privacy')->with($response);exit;
    }

    public function blockPage()
    {
        $response = array();
        return view('cms::common.Blockpage')->with($response);exit;
    }

}
