<?php
use UniSharp\LaravelFilemanager\Middlewares\CreateDefaultFolder;
use UniSharp\LaravelFilemanager\Middlewares\MultiUser;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//die('amgetting loaded');
//array('xssprotection','frameguard','admin')
Route::group(['module'=>'cms','namespace' => 'App\modules\frontend\cms\Controllers','middleware'=>['web']], function() {
		
		Route::group(['prefix'=>'open'], function() {
			//enable regular pages
			Route::get('/', 'FunrewardsController@getBase');
			//Route::get('/', 'QurekaController@getIntro');
			Route::get('/intro', 'FunrewardsController@getIntro');
			Route::get('/intro/question', 'FunrewardsController@getSampleQuestions');
			Route::get('/intro/success', 'FunrewardsController@getScore');
			Route::get('/optout', 'FunrewardsController@optout');
			Route::get('/user_optout', 'FunrewardsController@userOptout')->name('user.optout');
			Route::get('/privacy-policy', 'FunrewardsController@privacyPolicy');
			Route::get('{any}', 'FunrewardsController@PageNotFound')->where('any', '^(?!api).*$');

		});
		
		// Route::get('/', 'QurekaController@getBase');
		// Route::get('/intro', 'QurekaController@getIntro');
		// Route::get('/intro/question', 'QurekaController@getSampleQuestions');
		// Route::get('/intro/success', 'QurekaController@getScore');
		// Route::get('/optout', 'QurekaController@optout');
		
		Route::get('/block', 'FunrewardsController@blockPage');
		//Route::get('{any}', 'QurekaController@PageNotFound')->where('any', '^(?!api).*$'); 
});

