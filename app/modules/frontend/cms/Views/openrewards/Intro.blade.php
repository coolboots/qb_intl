@extends('layouts.newlayout')

@section('content')

     <section class="intro-screen">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <div class="row position-relative head d-flex align-items-end justify-content-end">
                <div class="logo"><img src="{{getFecdnPath('/assets/images/logo-new.png')}}" alt=""></div>
              </div>
              <div class="content mb-4">
                <h3>Qureka</h3>
                <h1>Quiz Bytes</h1>
                <h2>Play 2 simple Questions <br>& Win Your Virtual Reward</h2>
              </div>
              <div class="featured-image row">
                <img src="{{getFecdnPath('/assets/images/intro-featured.png')}}" alt="" class="img-fluid p-0">
                <div class="redirection-strip row">
                  <div class="col-8 d-flex align-items-center">
                    <p>Redirecting in <span id="seconds">2</span> seconds..</p>
                  </div>
                  <div class="col-4 text-right d-flex align-items-center justify-content-end">
                      <!-- <a href="#" id="skipIntro">Skip</a> -->
                      <span id="skipIntro">Skip</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

 @section('jsblock')
<script type="text/javascript">
      $(document).ready(function () { 
        var timeLeft = 2;
          function countdown() {
            timeLeft--;
            document.getElementById("seconds").innerHTML = String(timeLeft);
            if (timeLeft === 0) {
      		   location.replace('/open/intro/question');
      		    //window.location.pathname = '/intro/question';
            }
            if (timeLeft > 0) {
              setTimeout(countdown, 1000);
            }
          }
          setTimeout(countdown, 1000);
          
        

      $("#skipIntro").click(function(){
        location.replace('/open/intro/question');
      });

      });
  </script>
@endsection
@endsection
