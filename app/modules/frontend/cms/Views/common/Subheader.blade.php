
   <div class="quizhead">
   <!--  <a onclick="goBack()"> -->
    @if(@$isBack)
      <a onclick="goBack()"><i class="fas fa-arrow-left"></i></a> {{@$titleName}}
    @endif
    @if(@$backUrl)
    <a href="{{ @$backUrl }}"><i class="fas fa-arrow-left"></i></a> {{@$titleName}}
    @endif
    
  </div>