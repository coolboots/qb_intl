@extends('layouts.newlayout')

@section('content')

    <section class="head-with-back py-2 ">
  <div class="container">
    <div class="row">
      <div class="col-12 d-flex align-items-center">
        <a href="javascript:void(0);" id="optut-back"><img src="{{getFecdnPath('/assets/images/back-angle-arrow.png')}}" alt=""></a>
        <h4>Qureka Quiz Bytes </h4>
      </div>
    </div>
  </div>
</section>

<section class="privacy-policy-content contactus-screen managedata-screen">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="featured-image mb-3"><img src="{{getFecdnPath('/assets/images/manage-data-feature-image.png')}}" alt=""></div>
        <div class="content optout" style="display: none;">
        <h6>Do not sell my personal information</h6>
        <p>We use device identifiers (like cookies, beacons, and IP addresses) to improve our website and customize your experience. This sharing of data may be defined as a sale under the California Consumer Privacy Act.</p>
        <p>You have the right to opt out of the sale of you personal information to third parties. <a href="https://www.qurekaquizbytes.co/privacy.php" class="text-anchor">Our Privacy Policy</a> describes the right in detail.</p>
        <a href="javascript:void(0);" onclick="doOptout()" class="btn-style">Opt Out</a> 
        </div>

              <div class="content opted" style="display: none;">
        <h6>You have successfully opted out!</h6>
        <p>We're sorry to see you go, but Qureka Quiz Bytes has placed an opt-out cookie in your browser. This cookie will prevent Qureka Quiz Bytes from tailoring the ads you see based on the anonymous information Qureka Quiz Bytes collects, as described further in our <a href="https://www.qurekaquizbytes.co/privacy.php" class="text-anchor">Our Privacy Policy</a>. This opt-out applies only to this browser and this computer, and you must repeat the process if you switch to a different browser or computer</p>
        </div>
      </div>
    </div>
  </div>
</section>
    
@section('jsblock')
<script type="text/javascript">

$(document).ready(function(){
  var optout = "{{session()->get('optout')}}";
  console.log('opt check',optout);
  // $('.optout').css('display','block'); 
  if(optout=='true')
  {
    $('.opted').css('display','block'); 
  }else{
    $('.optout').css('display','block'); 
  }

   $("#optut-back").click(function(){
      location.replace('/open/intro/question');
    });
});

function doOptout(){
  try {
    $.ajax({
      type: "GET",
      url: "{{route('user.optout')}}",
      headers: {
        'X-CSRF-Token': "{{ csrf_token() }}",
      },
      success: function (response) {
        console.log('opt status',response);
        if (response.status_code == 200) {
          location.reload();
        }
      }
    });
    
  } catch (error) {
    console.log('optout file doOptout method error',error);
  }

}
</script>
@endsection
@endsection