@extends('layouts.newlayout')

@section('content')
    <div class="blocked-page">
     <section class="py-4 mobile-blockview-page">
       <div class="container">
         <div class="row">
           <div class="col-12">
            <img src="{{getFecdnPath('/assets/images/logo.png')}}" alt="">
            <img src="{{getFecdnPath('/assets/newui/images/block.png')}}" alt="" class="block-img">
            <h1>We're Sorry</h1>
            <h2>Qureka Quiz Byte is currently not available in your country!</h2>
            <!-- <div class="row mb-2">
              <img src="/assets/images/mobil-blockview-img.png" alt="" class="img-fluid mb-3">
            </div> -->
            <p>If you believe you have received this message in error and you are using a VPN or web proxy, try disabling it so that your IP address is not blocked.</p>
           </div>
         </div>
       </div>
     </section>
    </div>
@endsection
@section('jsblock')
<script type="text/javascript">
  $(function(){
    $('.app-wrapper-container').addClass('mobile-block-view-wrapper');
  });
</script>
@endsection
