@extends('layouts.newlayout')

@section('content')

<?php
  $userCountry = (!empty(json_decode(Session::get('userzone')))) ? json_decode(Session::get('userzone'),true)['country'] : 'empty';

  //$cookies = array_filter(Session::all());
  $usermode = (!empty(session()->get('usermode'))) ? session()->get('usermode') : '';
  $isStaticAds = \Config::get("qureka.static_ads");
  $host = Request::header('host');
  $question_set = $samplequestion['question_set'];

?>

@if (in_array($userCountry,config('qureka.eu_ea_mena_countries')))
<div class="cookies-modal">
   <div class="modal-content p-3">
      <p>You may see ads that are less relevant to you. These ads use cookies, but not for personalization. <a href="https://www.qurekaquizbytes.co/cookie-policy.php">Learn more about how we use cookies.</a></p>
      <a href="javascript:void(0)" class="btn-ok" id="close-modal">Ok</a>
   </div>
</div>
@endif

<!-- <div class = "ads mt-0" style="min-height:360px !important; text-align: center;">        
  {!!GoogleAd('SQ_1')!!}  
</div> -->

<section class="intro-question position-relative pb-3">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="row ads mb-3"><img src="" alt="" class="img-fluid p-2"></div>
            <hgroup>
              <h2>Let's get started,</h2>
              <h6>Play 2 simple Questions and Win Your Virtual Reward</h6>
            </hgroup>
            @if(!empty($question_set))
              @foreach($question_set as $qk=>$qs)
                <div class="question-answer position-relative mt-4 question_box question_{{$qk+1}} {{ $qk >  '0' ? 'd-none' : ''  }}" answer="{{$qs['answer']}}" position="{{$qk+1}}">
                  <h6 class="question-size">Question-<span>{{$qk+1}}</span></h6>
                  <h3>{{$qs['question']}}</h3>
                  <div class="options">
                    <a href="javascript:void(0);" class="option option_1_A" data-txt="A">{{$qs['option_a']}}</a>
                    <a href="javascript:void(0);" class="option option_1_B" data-txt="B">{{$qs['option_b']}}</a>
                    <a href="javascript:void(0);" class="option option_1_C" data-txt="C">{{$qs['option_c']}}</a>
                  </div>
                </div>
             @endforeach            
      @endif
            <footer class="mt-3 py-3">
              <h6>How Qureka Quiz Bytes works?</h6>
              <ul>
                <li>Just play 2 simple questions</li>
                <li>Press back button of device or close the tab to exit from Qureka Quiz Bytes</li>
                <li>Go back to your original app destination & continue your game/activity</li>
                <li>You will get virtual reward* by the original app
once you go back to the app.</li>
              </ul>
              <p>*Qureka Quiz Bytes does not give you any cash prize or reward, you get only virtual in-app currency for playing quiz.</p>
            </footer>

            <div class="terms-content"><a href="https://www.qurekaquizbytes.com/privacy.php">Privacy policy</a>&<a href="https://www.qurekaquizbytes.com/terms-conditions.php">Terms & Condition</a></div>
          </div>
        </div>
      </div>

      @if(@$usermode == 'compliance')
      <div class="my-3 text-center donot-sell-myinfo-text">
        <a href="/optout">Manage data preferences</a>
      </div>
      @endif
      
    </section>

      
      <!-- Modal -->
    <!--   <div class="modal fade modal-common-style" id="fullscreen" tabindex="-1" aria-labelledby="fullscreenLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body text-center">
              <h4>Tap ‘OK’ to view in full screen</h4>
              <button type="button" class="confirm" onClick="openFullscreen()">Ok</button>
            </div>
          </div>
        </div>
      </div> -->

@endsection
 @section('jsblock')
 
<script type="text/javascript">

 
     $(document).ready(function(){

      var optout = "{{session()->get('optout')}}";
      if(optout!='true')
      {
        $('.donot-sell-myinfo-text').css('display','block'); 
      }
      if(optout=='true')
      {
        $('.donot-sell-myinfo-text').hide(); 
      }
    });

        var elem = document.documentElement;
        function openFullscreen() {
            if (elem.requestFullscreen) {
            elem.requestFullscreen();
            $('#fullscreen').modal('hide');
          } else if (elem.webkitRequestFullscreen) { /* Safari */
            elem.webkitRequestFullscreen();
            $('#fullscreen').modal('hide');
          } else if (elem.msRequestFullscreen) { /* IE11 */
            elem.msRequestFullscreen();
            $('#fullscreen').modal('hide');
          }
        }

         $('.option').click(function()
        {
          $("a").addClass("disabled");

          var _self = $(this);
          var userinput = _self.attr('data-txt');
          var question = _self.closest('.question_box')[0];
          var answer = $(question).attr('answer');
          var position = $(question).attr('position');

          var correct_answer = localStorage.getItem('intro_correct_answer');
          if(correct_answer==null || position==1)
          {correct_answer = 0;}


            if(userinput==answer)
            {
              _self.addClass('correct');
              correct_answer = parseInt(correct_answer)+1;
            }
            else
            {
              _self.addClass('wrong');
            }

        localStorage.setItem('intro_correct_answer', correct_answer);



          setTimeout(function(){ 
              var next = +position + 1;
              if(position=='1')
              {
                $('.loginuser, .intro-question hgroup').hide()
                $('.question_'+position).addClass('d-none');
                $('.question_'+next).removeClass('d-none');
                $('.signup-block').hide();
                $('.flextitle').hide();

                $("a").removeClass("disabled");
                $('.donot-sell-myinfo-text').hide(); 

                 
              }
              else if(position=='2')
	      {
		      location.replace('/intro/success');
               //  window.location.href = "/intro/success";
              }

            }, 1000);

         })

  
</script>
<script>
   setTimeout(function(){
      $('.cookies-modal').show();
   },500)
   $('#close-modal').on('click', function(){
      $('.cookies-modal').hide();
   });
</script>
@endsection
