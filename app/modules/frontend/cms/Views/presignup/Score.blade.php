@extends('layouts.newlayout')

@section('content')
<?php 
  $userzone = showCountryTag();
  $isStaticAds = \Config::get("qureka.static_ads");
  $host = Request::header('host');
  //p($message);
?>


<!-- <div class="ads mt-0 mb-3">
   {!!GoogleAd('LS')!!}
</div> -->

<section class="intro-question position-relative pb-3 close-screen">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="row position-relative head d-flex align-items-end justify-content-end">
              <div class="logo"><img src="{{getFecdnPath('/assets/images/logo-new.png')}}" alt=""></div>
            </div>
            <div class="content text-center">
              <h2>Hope you had </h2>
              <div class="row mb-2">
                <div class="col-6 pr-1"><h1>Fun</h1></div>
                <div class="col-6 pl-1"><img src="{{getFecdnPath('/assets/images/cloud-gift.png')}}" alt="" class="cloudgift img-fluid"></div>
              </div>
              <h6>Your <span>Virtual Reward</span> will be added shortly. </h6>
              <div class="shimmer2"><span class="shimmer"><strong>Press back button to exit or Close the tab</strong></span></div>
              <br>
              <!-- <span>Close the quiz now to go back to the app. </span> -->
            </div>
            <div class="row ads"><img src="" alt="" class="img-fluid p-2"></div>
            <footer class="mt-3 py-3">
              <ul>
                <li>Virtual reward* will be given to you by the
original app</li>
                <li>Go back to your original app destination & continue your game/activity</li>
                <li>Come back for more fun quizzes & get in-
app reward every time for playing</li>
              </ul>
              <p>*Qureka Quiz Bytes does not give you any cash prize or reward, you get only virtual in-app currency for playing quiz.</p>
            </footer>
          </div>
        </div>
      </div>
    </section>

  @section('jsblock')
<script type="text/javascript">
  /* $(document).ready(function () {
        setTimeout(function(){
          $('#fullscreen').modal('show');
        },1000);
        
          // $('.question-answer .options a').on('click', function(){
          //   window.location.pathname = '/rewarded-quiz/intro-question2.php'
          // })
   }); */

        function closeWindow() {  
          window.close();  
        } 

  </script>
@endsection
@endsection
