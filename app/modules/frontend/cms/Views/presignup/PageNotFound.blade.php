@extends('layouts.newlayout')

@section('content')
<?php 
$userzone = showCountryTag();
?>

      <section class="rules-screen py-3 contactus-screen">
        <div class="container">
          <div class="row">
            <div class="col-12 text-center">
              <a href="/">
              <img src="{{getFecdnPath('/assets/images/landing-screen-logo.png')}}" alt="" class="logo">
              </a>
              <img src="{{getFecdnPath('/assets/images/404-image.png')}}" alt="" class="sorry-image">
              <h1>Page Not Found</h1>
              <h3>Sorry, we couldn't find this page. The page you're looking either doesn't exist or any other error occurred.</h3>

              <a href="/open" class="btn-style" >Go to Home</a>
            </div>

            @if(@$userzone['country'] == 'IN')
            <div class="proud-madeIn-india w-100 text-center py-3 mt-3">
              <img src="{{getFecdnPath('/assets/images/single-flag.png')}}" alt="">
              <p>Qureka Lite is proudly made in India</p>
            </div>
            @endif
          </div>
        </div>
      </section>

  
 @section('jsblock')

  <script type="text/javascript">

  $(document).ready(function(){

    $('.app-wrapper-container').addClass('_404-screen');
    $('.app-wrapper-container').removeClass('dashboard-wrapper');

    });

  
</script>

@endsection
@endsection