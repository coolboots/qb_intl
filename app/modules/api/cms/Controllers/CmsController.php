<?php 
namespace App\modules\api\cms\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
//use Illuminate\Http\Request;
use App\User;
use App\Http\Models\Accesstoken;
use App\Http\Models\Partner;
use App\Http\Models\Question;
use App\Http\Models\Contest;
use App\Http\Models\Category;
use App\Http\Models\Funquiz;
use Validator;
use Input;
use Redirect;
use DB;
use Exception;
use Carbon\Carbon;
use Lcobucci\JWT\Parser;
use Request;
use Illuminate\Support\Facades\Redis;

class CmsController extends Controller
{

    /**14583
     * Display a listing of the resource.
     *
     * @return Response
     */
    var $lang = 'en';
    public function __construct()
    {
        
    }



    


    public function getGlobalConfig()
    {


                $input = Input::all();
                $input = array_filter($input);
                $version = @$input['version'];
                $packageId = @$input['package_id'];
                
                $response = array();
                $status = 400;
                $message = 'Oops, Plz try later';

                if($version == 1) {
                    # code...
                $response['sdk_base_url'] = '';
                $response['ad_play_url'] = '';
		$response['grant_type'] = 'client_credentials';
                $response['client_id'] = '';
                $response['client_secret'] = '';
                $response['scope'] = '*';
                $response['key'] = 'SkOoLByTe';

                if($packageId){
                    $partner = \DB::table('partner_packages')->select('allocated_domain')
                    ->leftJoin('partners', 'partner_packages.partner_id', '=', 'partners.id')
                    ->where('package_id',$packageId)->first();
                    if($partner){
                       $response['sdk_base_url'] = 'https://'.$partner->allocated_domain; 
                    }

                    $clienCredentials = \DB::table('oauth_clients')->select('id','secret')->first();

                    if($clienCredentials){
                       $response['client_id'] = $clienCredentials->id;
                       $response['client_secret'] = $clienCredentials->secret; 
                    }


                    
                }
                $status = 200;
                $message = 'config list';
            }
                

            return  $this->response($response,$status,$message);
            
    }

     public function getSampleQuestion($origin){

        #Get Data by cache
        $version = rand(0, 100);
        $cacheKey = 'sample_question:'.$version.'_'.$origin;
        $cacheStorage = array();
        $response = checkCacheAvailability($cacheKey);
        if($response)
        {
            return response()->json($response,200);
        }

        $response = array();
        $status = 400;
        $QuestionArray = array();
        $message = '';
        
        if(!empty($origin))
        {
            $sampleQuestions = Question::select('question_title as question','option_1 as option_a','option_2 as option_b','option_3 as option_c','correct_answer as answer')
                             ->leftJoin('questionaire_options', function($leftJoin)
                            {
                                $leftJoin->on('questionaire_options.questionaire_id', '=', 'questionaires.id');
                            })->where('questionaires.category_id','1')
                             ->where('questionaires.language_code','EN')
                             // ->where('questionaires.tags',$origin)
                             ->inRandomOrder()->limit(2)
                            ->get();
            if(!empty($sampleQuestions))
            {
                $response['question_set'] = $sampleQuestions;
                $status = 200;
                $message = 'Sample Questions';
            }
        }
        if($status==200)
        {$cacheStorage = array('key'=>$cacheKey,'ttl'=>900);}
        return  $this->response($response,$status,$message,array('cache'=>$cacheStorage));
                
    }

    public function validateApp(){
        $response = array();
        $status = 401;
        $message = '';

        $rules = array('device_id'=>'required',
                    'package_id'=>'required',
                   // 'hash_key'=>'required',
                    );

        $inputs = \Input::all();
        $inputs = array_filter($inputs);
        
        $validator = Validator::make(\Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            $status = 401;
            $response['errors'] = laravel_error_parser($validator->errors()->toArray());
            $message = 'Validation Errors';
            $response = ['status'=>422,'message'=>'Validation Errors'];
            
        } else {

        $headers = getallheaders();

        $deviceId = @$inputs['device_id'];
        $packageId = @$inputs['package_id'];
        //$hashKey = @$inputs['hash_key'];
        $hashKey = @$headers['hash-key'];
        
        if($packageId){
            $isPackageExists = verifyPackage($packageId);
            
            if($isPackageExists){
               $hash = validateHash($deviceId,$packageId,$hashKey);

                if($hash['status'] == true && !empty($hash['key'])){
                        Redis::set($deviceId, $hash['key'], 'EX', 600);
                        $message = 'Success';
                        $status = 200;
                        $response = ['hash_url_key'=>$hash['key']];
                }else{
                   $message = 'Invalid Hash Key';
                   $response = ['status'=>400,'message'=>'Invalid Hash Key'];
                }
            }else{
                 $message = 'Invalid Package Id';
                 $response = ['status'=>400,'message'=>'Invalid Package Id'];
            }
        }else{
            $message = 'Invalid Package Id';
            $response = ['status'=>400,'message'=>'Invalid Package Id'];
        }

      }
        return  $this->response($response,$status,$message);
        

    }

    public function quizbyteEventFire(){
        $response = array();
        $status = 401;
        $message = '';

        $rules = array('device_id'=>'required',
                    'package_id'=>'required',
                    //'hash_key'=>'required',
                    'event'=>'required',
                    //'hash_url_key'=>'required',
                    );

        $inputs = \Input::all();
        $inputs = array_filter($inputs);
        
        $validator = Validator::make(\Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            $status = 401;
            $response['errors'] = laravel_error_parser($validator->errors()->toArray());
            $message = 'Validation Errors';
            $response = ['status'=>422,'message'=>'Validation Errors'];

        } else {
            $headers = getallheaders();

            $deviceId = @$inputs['device_id'];
            $packageId = @$inputs['package_id'];
            $event = @$inputs['event'];
            // $hashKey = @$inputs['hash_key'];
            // $hashUrlKey = @$inputs['hash_url_key'];

            $hashKey = @$headers['hash-key'];
            $hashUrlKey = @$headers['hash-url-key'];
            
            if($packageId){
                $isPackageExists = verifyPackage($packageId);
                
                if($isPackageExists){
                    $rHashKey = (Redis::get($deviceId) == '')? 0 : Redis::get($deviceId);

                    if ($rHashKey == $hashUrlKey) {
                        $hash = validateHash($deviceId,$packageId,$hashKey);

                        if($hash['status'] == true && !empty($hash['key'])){

                                $shaencrypt = hash('sha256', md5($hash['key']));
                                //$apiUrl = \Config::get("qureka.api_url");
                                $appUrl = '?t='.$shaencrypt.'&event='.$event;

                                Redis::set($event, $packageId, 'EX', 600);

                                $message = 'Success';
                                $status = 200;
                                $response = ['url'=>$appUrl];
                        }else{
                           $message = 'Invalid Hash Key';
                           $response = ['status'=>400,'message'=>'Invalid Hash Key'];
                        }
                    }else{
                        $message = 'Invalid Hash Url Key';
                        $response = ['status'=>400,'message'=>'Invalid Hash Url Key'];
                    }
                }else{
                     $message = 'Invalid Package Id';
                     $response = ['status'=>400,'message'=>'Invalid Package Id'];
                }
            }else{
                $message = 'Package Id Required';
                $response = ['status'=>400,'message'=>'Package Id Required'];
            }
        }
        return  $this->response($response,$status,$message);
    }

    public function validateEvent(){
        $response = array();
        $status = 401;
        $message = 'Unsuccess';
        $response = ['status'=>400,'message'=>'Kindly provide valid HashKey, HashUrlKey and Handshakekey'];

        $rules = array('device_id'=>'required',
                    'package_id'=>'required',
                    //'hash_key'=>'required',
                    'event'=>'required',
                    //'hash_url_key'=>'required',
                    //'handshakekey'=>'required',
                    );

        $inputs = \Input::all();
        $inputs = array_filter($inputs);
        
        $validator = Validator::make(\Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            $status = 401;
            $response['errors'] = laravel_error_parser($validator->errors()->toArray());
            $message = 'Validation Errors';
            $response = ['status'=>422,'message'=>'Validation Errors'];
            
        } else {
            $headers = getallheaders();

            $deviceId = @$inputs['device_id'];
            $packageId = @$inputs['package_id'];
            $event = @$inputs['event'];
            // $hashKey = @$inputs['hash_key'];
            // $hashUrlKey = @$inputs['hash_url_key'];
            // $handshakekey = @$inputs['handshakekey'];
            $hashKey = @$headers['hash-key'];
            $hashUrlKey = @$headers['hash-url-key'];
            $handshakekey = @$headers['handshakekey'];

            if($packageId){
                $isPackageExists = verifyPackage($packageId);
                
                if($isPackageExists){
                    $rHashKey = (Redis::get($deviceId) == '')? 0 : Redis::get($deviceId);

                    if ($rHashKey == $hashUrlKey) {
                        $hash = validateHash($deviceId,$packageId,$hashKey);

                        if($hash['status'] === true && !empty($hash['key'])){
                            if (!empty($handshakekey) && !empty($event)) {
                               //$recordId = \DB::table('funquiz_callback')->where('mode','auth')->where('domain',$packageId)->where('handshake_key',@$handshakekey)->select('id')->where('event',$event)->first();
                               $recordId = Funquiz::where('mode','auth')->where('domain',$packageId)->where('handshake_key',@$handshakekey)->select('id','response_given')->where('event',$event)->first();
                                if(!empty($recordId) && $recordId["response_given"]==0){
                                    $update_funquiz = Funquiz::find($recordId["id"]);
                                    $update_funquiz->response_given = 1;
                                    $update_funquiz->save();
                                    $status = 200;
                                    $message = 'Success';
                                    $response = ['status'=>200,'message'=>'Success'];
                                }
                                else{
                                    $status = 401;
                                    $message = 'Unsuccess';
                                    $response = ['status'=>400,'message'=>'Record Not Found'];
                                }
                            }

                        }
                    }
                }
            }
        }
        
        return  $this->response($response,$status,$message);
    }

    public function getfunQuizData($src, $key){
        $response = array();
        $status = 401;
        $message = 'Unsuccess';
        $response = ['status'=>400,'message'=>'Record Not Found'];

        if(!empty($src) && !empty($key))
        {
            $recordId = Funquiz::where('mode','open')->where('domain',$src)->where('handshake_key',$key)->select('id','response_given')->first();
            if(!empty($recordId) && $recordId["response_given"]==0){
                $update_funquiz = Funquiz::find($recordId["id"]);
                $update_funquiz->response_given = 1;
                $update_funquiz->save();
                $status = 200;
                $message = 'Success';
                $response = ['status'=>200,'message'=>'Success'];
            }else{

                #check in international db
                $dbName = \Config::get("qureka.intl_db");
                $db_ext = \DB::connection($dbName);
                $intlRecordId = $db_ext->table('funquiz_callback')->where('mode','open')->where('domain',$src)->where('handshake_key',$key)->select('id','response_given')->first();
                if(!empty($intlRecordId) && $intlRecordId->response_given==0){
                    
                    $db_ext->table('funquiz_callback')->where('id',$intlRecordId->id)->update(['response_given'=>1]);

                    $status = 200;
                    $message = 'Success';
                    $response = ['status'=>200,'message'=>'Success'];
                }
                
            }
        }
        return  $this->response($response,$status,$message);
    }

     public function getDomainInfo($url)
    {
        $cacheKey = 'game:domaininfo:'.$url;
        $cacheStorage = array();
        $response = checkCacheAvailability($cacheKey);
        if($response)
        {
            return response()->json($response,200);
        }

        $domain = $url;
        $inputs = \Input::all();
        $response = [];
        if(isset($domain) && !empty($domain))
        {
            $partner = Partner::where('allocated_domain',$domain)->first();
            if(!empty($partner))
            {
                //p($partner->network);
                $partner->google_network = $partner->google_network;
                $partner->publisher = $partner->google_publisher;
                $response['partner'] = $partner->setHidden(['created_at','updated_at','id','partner_name','logo']);
            }
            // p($partner);
            
 
        }

         $cacheStorage = array('key'=>$cacheKey,'ttl'=>300);
         return  $this->response($response,200,'Partner Data',array('cache'=>$cacheStorage));
    }

}
        
