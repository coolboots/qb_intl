<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
 
//die('amgetting loaded');
//array('xssprotection','frameguard','admin','verifyapitoken')
Route::group(['module'=>'cms','namespace' => 'App\modules\api\cms\Controllers','prefix'=>'api/v1','middleware'=>['api']], function() {
	
			
			//restrcited api by public token
			Route::group(['middleware'=>['verifyapitoken']], function() {

				#Sample Questions api
				Route::get('/sample-question/{origin}', 'CmsController@getSampleQuestion');
				#Quizbyte Api
				Route::post('/validateapp', 'CmsController@validateApp');
				Route::post('/fireevent', 'CmsController@quizbyteEventFire');
				Route::post('/validateevent', 'CmsController@validateEvent');
				// Route::get('/validatefunquiz/{src}/{key}', 'CmsController@getfunQuizData');

				//get domain status and ads by page
				Route::get('/domaininfo/{domain}', 'CmsController@getDomainInfo');

			});

			Route::get('/validatefunquiz/{src}/{key}', 'CmsController@getfunQuizData');

			Route::post('/globalconfig', 'CmsController@getGlobalConfig');
});