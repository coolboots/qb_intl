<?php namespace App\modules\backend\cms\Controllers;

		    use App\Http\Requests;
			use App\Http\Controllers\Controller;
			use App\Http\Models\Userwallettranshistory;
			use Validator;
			use Redirect;
			use Request;

			class UserwallettranshistoryController extends Controller {

			/**
			 * Display a listing of the resource.
			 *
			 * @return Response
			 */
			public function __construct()
			{
				$this->middleware('adminauth');
			}

			public function index()
			{ 
				if(is_axios()==true)
				{
					//filter params 
					$inputs = Request::all();
					$inputs = array_filter($inputs);
				$user_id = @$inputs['user_id'];
				$mode_type = @$inputs['mode_type'];
				$coin_type = @$inputs['coin_type'];
				$amount = @$inputs['amount'];
				$remarks = @$inputs['remarks'];
				$user_wallet_trans_history = Userwallettranshistory::where(array())
				->when($user_id, function ($query) use ($user_id) {
				        	return $query->where('user_id','=' ,$user_id);})
				->when($mode_type, function ($query) use ($mode_type) {
				        	return $query->where('mode_type','LIKE' ,$mode_type.'%');})
				->when($coin_type, function ($query) use ($coin_type) {
				        	return $query->where('coin_type','LIKE' ,$coin_type.'%');})
				->when($amount, function ($query) use ($amount) {
				        	return $query->where('amount','=' ,$amount);})
				->when($remarks, function ($query) use ($remarks) {
				        	return $query->where('remarks','LIKE' ,$remarks.'%');})
				->paginate(20);
			      $user_wallet_trans_history->setPath('userwallettranshistory');
					$user_wallet_trans_history->appends(Request::except('page'));
					return view('cms::userwallettranshistory.index_view')->with('user_wallet_trans_history', $user_wallet_trans_history);

				}
				return view('cms::userwallettranshistory.index');
			}

			/**
			 * Show the form for creating a new resource.
			 *
			 * @return Response
			 */
			public function create()
			{
				//
				return view('cms::userwallettranshistory.create');
			}

			/**
			 * Store a newly created resource in storage.
			 *
			 * @return Response
			 */
			public function store()
			{
				//
				// validate
		        // read more on validation at http://laravel.com/docs/validation
		        	$status = 200;
			        $response = array();
			        $message = '';

		        
				$rules = array('user_id'=>'required',
			        'mode_type'=>'required',
			        'coin_type'=>'required',
			        'amount'=>'required',
			        'remarks'=>'required',
			        );
		        
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		        	$status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		            
		        } else {
		            // store
		            $userwallettranshistory = new Userwallettranshistory;
		            $userwallettranshistory->user_id = Request::get('user_id');
				        		$userwallettranshistory->mode_type = Request::get('mode_type');
				        		$userwallettranshistory->coin_type = Request::get('coin_type');
				        		$userwallettranshistory->amount = Request::get('amount');
				        		$userwallettranshistory->remarks = Request::get('remarks');
				        		$userwallettranshistory->save();

		           //trigger seo url
					triggerSeoUrls($userwallettranshistory->id,'userwallettranshistory',Request::get('main_seo_title'));
						$message = 'Successfully created Userwallettranshistory!';

		            
		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Display the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function show($id)
			{
				//
				if($id=='quickupdate')
				{
					return $this->quickUpdate();
				}
				 $userwallettranshistory = Userwallettranshistory::find($id);

				 if(empty($userwallettranshistory))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;
				 	
				 }

		        // show the view and pass the userwallettranshistory to it
		        return view('cms::userwallettranshistory.show')->with('userwallettranshistory', $userwallettranshistory);
			}

			public function quickUpdate()
			{
				$inputs = Request::all();
				if(isset($inputs['id']) && isset($inputs['action']))
				{
					if($inputs['action']==1){$action='ACTIVE';}
					else if($inputs['action']==2){$action='INACTIVE';}
					else if($inputs['action']==3){$action='DELETED';}
					if($action!='')
					{Userwallettranshistory::whereIn('id',explode(',', $inputs['id']))->update(['status'=>$action]);}
				}
			}

			/**
			 * Show the form for editing the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function edit($id)
			{
				//
				$userwallettranshistory = Userwallettranshistory::find($id);
				if(empty($userwallettranshistory))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;

				 }

		        // show the view and pass the userwallettranshistory to it
		        return view('cms::userwallettranshistory.edit')->with('userwallettranshistory', $userwallettranshistory);
			}

			/**
			 * Update the specified resource in storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function update($id)
			{
				// read more on validation at http://laravel.com/docs/validation
		       		$status = 200;
			        $response = array();
			        $message = '';
		       $rules = array();
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		            $status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		        } else {
		            // store
		             $userwallettranshistory = Userwallettranshistory::find($id);$userwallettranshistory->user_id = Request::get('user_id');
				        		$userwallettranshistory->mode_type = Request::get('mode_type');
				        		$userwallettranshistory->coin_type = Request::get('coin_type');
				        		$userwallettranshistory->amount = Request::get('amount');
				        		$userwallettranshistory->remarks = Request::get('remarks');
				        		$userwallettranshistory->save();

		           //trigger seo url
					triggerSeoUrls($userwallettranshistory->id,'userwallettranshistory',Request::get('main_seo_title'));
						$message = 'Successfully updated Userwallettranshistory!';
		           
		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Remove the specified resource from storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function destroy($id)
			{
				//
					$status = 200;
			        $response = array();
			        $message = '';
				$userwallettranshistory = Userwallettranshistory::find($id);
				if(empty($userwallettranshistory))
				 { 
				 	$status=421; 
				 	$response['errors']=array('No department found!');
				 	$message='No department found!';
				 }
				 else
				 {
				 	 $userwallettranshistory->delete();
				 	 $message = 'Successfully deleted Userwallettranshistory!';

				 }
				 return $this->response($response,$status,$message);
			 
			}

		}
		