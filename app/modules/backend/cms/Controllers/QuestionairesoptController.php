<?php namespace App\modules\backend\cms\Controllers;

		    use App\Http\Requests;
			use App\Http\Controllers\Controller;
			use App\Http\Models\Questionoption;
			use Validator;
			use Redirect;
			use Request;

			class QuestionairesoptController extends Controller {

			/**
			 * Display a listing of the resource.
			 *
			 * @return Response
			 */
			public function __construct()
			{
				$this->middleware('adminauth');
			}

			public function index()
			{ 
				if(is_axios()==true)
				{
					//filter params 
					$inputs = Request::all();
					$inputs = array_filter($inputs);
				$questionaire_id = @$inputs['questionaire_id'];
				$option_1 = @$inputs['option_1'];
				$option_2 = @$inputs['option_2'];
				$option_3 = @$inputs['option_3'];
				$questionaire_options = Questionoption::where(array())
				->when($questionaire_id, function ($query) use ($questionaire_id) {
				        	return $query->where('questionaire_id','=' ,$questionaire_id);})
				->when($option_1, function ($query) use ($option_1) {
				        	return $query->where('option_1','LIKE' ,$option_1.'%');})
				->when($option_2, function ($query) use ($option_2) {
				        	return $query->where('option_2','LIKE' ,$option_2.'%');})
				->when($option_3, function ($query) use ($option_3) {
				        	return $query->where('option_3','LIKE' ,$option_3.'%');})
				->paginate(20);
			      $questionaire_options->setPath('questionairesopt');
					$questionaire_options->appends(Request::except('page'));
					return view('cms::questionairesopt.index_view')->with('questionaire_options', $questionaire_options);

				}
				return view('cms::questionairesopt.index');
			}

			/**
			 * Show the form for creating a new resource.
			 *
			 * @return Response
			 */
			public function create()
			{
				//
				return view('cms::questionairesopt.create');
			}

			/**
			 * Store a newly created resource in storage.
			 *
			 * @return Response
			 */
			public function store()
			{
				//
				// validate
		        // read more on validation at http://laravel.com/docs/validation
		        	$status = 200;
			        $response = array();
			        $message = '';

		        
				$rules = array('questionaire_id'=>'required',
			        'option_1'=>'required',
			        'option_2'=>'required',
			        'option_3'=>'required',
			        );

				$inputs = Request::all();
				$inputs = array_filter($inputs);

		        
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		        	$status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		            
		        } else {
		            // store
		            $questionairesopt = new Questionoption;
		            $questionairesopt->questionaire_id = $inputs['questionaire_id'];
				        		$questionairesopt->option_1 = $inputs['option_1'];
				        		$questionairesopt->option_2 = $inputs['option_2'];
				        		$questionairesopt->option_3 = $inputs['option_3'];
				        		$questionairesopt->save();

		           //trigger seo url
					triggerSeoUrls($questionairesopt->id,'questionoption',$inputs['main_seo_title']);
						$message = 'Successfully created Questionairesopt!';

		            
		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Display the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function show($id)
			{
				//
				if($id=='quickupdate')
				{
					return $this->quickUpdate();
				}
				
				 $questionairesopt = Questionoption::find($id);

				 if(empty($questionairesopt))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;
				 	
				 }

		        // show the view and pass the questionairesopt to it
		        return view('cms::questionairesopt.show')->with('questionairesopt', $questionairesopt);
			}


			//to update status quickly

			public function quickUpdate()
			{
				$inputs = Request::all();
				if(isset($inputs['id']) && isset($inputs['action']))
				{
					if($inputs['action']==1){$action='ACTIVE';}
					else if($inputs['action']==2){$action='INACTIVE';}
					else if($inputs['action']==3){$action='DELETED';}
					if($action!='')
					{Questionoption::whereIn('id',explode(',', $inputs['id']))->update(['status'=>$action]);}
				}
			}

			/**
			 * Show the form for editing the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function edit($id)
			{
				//
				$questionairesopt = Questionoption::find($id);
				if(empty($questionairesopt))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;

				 }

		        // show the view and pass the questionairesopt to it
		        return view('cms::questionairesopt.edit')->with('questionairesopt', $questionairesopt);
			}

			/**
			 * Update the specified resource in storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function update($id)
			{
				// read more on validation at http://laravel.com/docs/validation
		       		$status = 200;
			        $response = array();
			        $message = '';
		       $rules = array();
				$inputs = Request::all();
				$inputs = array_filter($inputs);

		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		            $status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		        } else {
		            // store
		             $questionairesopt = Questionoption::find($id);$questionairesopt->questionaire_id = $inputs['questionaire_id'];
				        		$questionairesopt->option_1 = $inputs['option_1'];
				        		$questionairesopt->option_2 = $inputs['option_2'];
				        		$questionairesopt->option_3 = $inputs['option_3'];
				        		$questionairesopt->save();

		           //trigger seo url
					triggerSeoUrls($questionairesopt->id,'questionoption',$inputs['main_seo_title']);
						$message = 'Successfully updated Questionairesopt!';
		           
		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Remove the specified resource from storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function destroy($id)
			{
				//
					$status = 200;
			        $response = array();
			        $message = '';
				$questionairesopt = Questionoption::find($id);
				if(empty($questionairesopt))
				 { 
				 	$status=421; 
				 	$response['errors']=array('No Questionairesopt found!');
				 	$message='No Questionairesopt found!';
				 }
				 else
				 {
				 	 $questionairesopt->delete();
				 	 $message = 'Successfully deleted Questionairesopt!';

				 }
				 return $this->response($response,$status,$message);
			 
			}

		}
		