<?php namespace App\modules\backend\cms\Controllers;

		    use App\Http\Requests;
			use App\Http\Controllers\Controller;
			use App\Http\Models\Article;
			use Validator;
			use Redirect;
			use Request;

			class ArticlesController extends Controller {

			/**
			 * Display a listing of the resource.
			 *
			 * @return Response
			 */
			public function __construct()
			{
				$this->middleware('adminauth');
			}

			public function index()
			{ 
				if(is_axios()==true)
				{
					//filter params 
					$inputs = Request::all();
					$inputs = array_filter($inputs);
				$article_type = @$inputs['article_type'];
				$title = @$inputs['title'];
				$subtitle = @$inputs['subtitle'];
				$image = @$inputs['image'];
				$video_url = @$inputs['video_url'];
				$status = @$inputs['status'];
				if(empty($inputs)){
					$status='ACTIVE';
				}
				$articles = Article::where(array())
				->when($article_type, function ($query) use ($article_type) {
				        	return $query->where('article_type','LIKE' ,'%'.$article_type.'%');})
				->when($title, function ($query) use ($title) {
				        	return $query->where('title','LIKE' ,'%'.$title.'%');})
				->when($subtitle, function ($query) use ($subtitle) {
				        	return $query->where('subtitle','LIKE' ,$subtitle.'%');})
				->when($image, function ($query) use ($image) {
				        	return $query->where('image','LIKE' ,$image.'%');})
				->when($video_url, function ($query) use ($video_url) {
				        	return $query->where('video_url','LIKE' ,$video_url.'%');})
				->when($status, function ($query) use ($status) {
				        	return $query->where('status','LIKE' ,$status.'%');})
				->paginate(20);
			      $articles->setPath('articles');
					$articles->appends(Request::except('page'));
					return view('cms::articles.index_view')->with('articles', $articles);

				}
				return view('cms::articles.index');
			}

			/**
			 * Show the form for creating a new resource.
			 *
			 * @return Response
			 */
			public function create()
			{
				//
				return view('cms::articles.create');
			}

			/**
			 * Store a newly created resource in storage.
			 *
			 * @return Response
			 */
			public function store()
			{
				//
				// validate
		        // read more on validation at http://laravel.com/docs/validation
		        	$status = 200;
			        $response = array();
			        $message = '';

		        
				$rules = array('article_type'=>'required',
			        'title'=>'required',
			        'subtitle'=>'required',
			        'image'=>'required|max:500|mimes:jpeg,jpg,png,gif',
			        'video_url'=>['required','regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/'],
			        'status'=>'required',
			        );
		        
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		        	$status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		            
		        } else {
		            // store
		            $articles = new Article;
		            $articles->article_type = Request::get('article_type');
				        		$articles->title = Request::get('title');
				        		$articles->subtitle = Request::get('subtitle');
				        		
				        		$import = \Request::file('image');
									if(isset($import) and !empty($import))
						            {
						            	$data = uploadImage($import,'articles');
						            	$articles->image = $data;
									}
									$articles->video_url = Request::get('video_url');
				        		$articles->descriptions = Request::get('descriptions');
				        		$articles->status = Request::get('status');
				        		$articles->save();

		           //trigger seo url
					triggerSeoUrls($articles->id,'article',Request::get('main_seo_title'));
						$message = 'Successfully created Articles!';

		            
		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Display the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function show($id)
			{
				//
				if($id=='quickupdate')
				{
					return $this->quickUpdate();
				}
				 $articles = Article::find($id);

				 if(empty($articles))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;
				 	
				 }

		        // show the view and pass the articles to it
		        return view('cms::articles.show')->with('articles', $articles);
			}

			public function quickUpdate()
			{
				$inputs = Request::all();
				if(isset($inputs['id']) && isset($inputs['action']))
				{
					if($inputs['action']==1){$action='ACTIVE';}
					else if($inputs['action']==2){$action='INACTIVE';}
					else if($inputs['action']==3){$action='DELETED';}
					if($action!='')
					{Article::whereIn('id',explode(',', $inputs['id']))->update(['status'=>$action]);}
				}
			}

			/**
			 * Show the form for editing the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function edit($id)
			{
				//
				$articles = Article::find($id);
				if(empty($articles))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;

				 }

		        // show the view and pass the articles to it
		        return view('cms::articles.edit')->with('articles', $articles);
			}

			/**
			 * Update the specified resource in storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function update($id)
			{
				// read more on validation at http://laravel.com/docs/validation
		       		$status = 200;
			        $response = array();
			        $message = '';
		       $rules = array('image'=>'max:500|mimes:jpeg,jpg,png,gif',
			  			 'video_url'=> 'regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',);
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		            $status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		        } else {
		            // store
		             $articles = Article::find($id);$articles->article_type = Request::get('article_type');
				        		$articles->title = Request::get('title');
				        		$articles->subtitle = Request::get('subtitle');
				        		
				        		$import = \Request::file('image');
									if(isset($import) and !empty($import))
						            {
						            	$data = uploadImage($import,'articles');
						            	$articles->image = $data;
									}
									$articles->video_url = Request::get('video_url');
				        		$articles->descriptions = Request::get('descriptions');
				        		$articles->status = Request::get('status');
				        		$articles->save();

		           //trigger seo url
					triggerSeoUrls($articles->id,'article',Request::get('main_seo_title'));
						$message = 'Successfully updated Articles!';
		           
		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Remove the specified resource from storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function destroy($id)
			{
				//
					$status = 200;
			        $response = array();
			        $message = '';
				$articles = Article::find($id);
				if(empty($articles))
				 { 
				 	$status=421; 
				 	$response['errors']=array('No department found!');
				 	$message='No department found!';
				 }
				 else
				 {
				 	 $articles->delete();
				 	 $message = 'Successfully deleted Articles!';

				 }
				 return $this->response($response,$status,$message);
			 
			}

		}
		