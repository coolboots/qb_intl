<?php namespace App\modules\backend\cms\Controllers;

		    use App\Http\Requests;
			use App\Http\Controllers\Controller;
			use App\Http\Models\Publisher;
			use Validator;
			use Redirect;
			use Request;

			class PublishersController extends Controller {

			/**
			 * Display a listing of the resource.
			 *
			 * @return Response
			 */
			public function __construct()
			{
				$this->middleware('adminauth');
			}

			public function index()
			{ 
				if(is_axios()==true)
				{
					//filter params 
					$inputs = Request::all();
					$inputs = array_filter($inputs);
				$publisher_name = @$inputs['publisher_name'];
				$publisher_code = @$inputs['publisher_code'];
				$status = @$inputs['status'] ? @$inputs['status'] : 'ACTIVE';
				if(empty($inputs)){
					$status='ACTIVE';
				}
				$publishers = Publisher::where(array())
				->when($publisher_name, function ($query) use ($publisher_name) {
				        	return $query->where('publisher_name','LIKE' ,'%'.$publisher_name.'%');})
				->when($publisher_code, function ($query) use ($publisher_code) {
				        	return $query->where('publisher_code','LIKE' ,$publisher_code.'%');})
				->when($status, function ($query) use ($status) {
				        	return $query->where('status','LIKE' ,$status.'%');})
				->paginate(20);
			      $publishers->setPath('publishers');
					$publishers->appends(Request::except('page'));
					return view('cms::publishers.index_view')->with('publishers', $publishers);

				}
				return view('cms::publishers.index');
			}

			/**
			 * Show the form for creating a new resource.
			 *
			 * @return Response
			 */
			public function create()
			{
				//
				return view('cms::publishers.create');
			}

			/**
			 * Store a newly created resource in storage.
			 *
			 * @return Response
			 */
			public function store()
			{
				$status = 200;
				$response = array();
				$message = '';
		        
				$rules = array('publisher_name'=>'required',
			        'publisher_code'=>'required|unique:publishers,publisher_code',
			        'status'=>'required',
			        );

				$inputs = Request::all();
		        
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		        	$status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		            
		        } else {
		            $publishers = new Publisher;
		            $publishers->publisher_name = $inputs['publisher_name'];
					$publishers->publisher_code = $inputs['publisher_code'];
					$publishers->descriptions = $inputs['descriptions'];
					$publishers->scripts_1 = $inputs['scripts_1'];
					$publishers->scripts_2 = $inputs['scripts_2'];
					$publishers->scripts_3 = $inputs['scripts_3'];
					$publishers->scripts_4 = $inputs['scripts_4'];
					$publishers->scripts_5 = $inputs['scripts_5'];
					$publishers->status = $inputs['status'];
					$publishers->ads_enabled = (int)$inputs['ads_enabled'];
					$publishers->save();

		           	$message = 'Successfully created Publishers!';
		           	$response['callback'] = url('/'.getCurrentUrlPrefix());
		            
		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Display the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function show($id)
			{
				//
				if($id=='quickupdate')
				{
					return $this->quickUpdate();
				}
				 $publishers = Publisher::find($id);

				 if(empty($publishers))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;
				 	
				 }

		        // show the view and pass the publishers to it
		        return view('cms::publishers.show')->with('publishers', $publishers);
			}


			public function quickUpdate()
			{
				$inputs = Request::all();
				if(isset($inputs['id']) && isset($inputs['action']))
				{
					if($inputs['action']==1){$action='ACTIVE';}
					else if($inputs['action']==2){$action='INACTIVE';}
					else if($inputs['action']==3){$action='DELETED';}
					if($action!='')
					{Publisher::whereIn('id',explode(',', $inputs['id']))->update(['status'=>$action]);}
				}
			}

			/**
			 * Show the form for editing the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function edit($id)
			{
				//
				$publishers = Publisher::find($id);
				if(empty($publishers))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;

				 }

		        // show the view and pass the publishers to it
		        return view('cms::publishers.edit')->with('publishers', $publishers);
			}

			/**
			 * Update the specified resource in storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function update($id)
			{
				// read more on validation at http://laravel.com/docs/validation
		       		$status = 200;
			        $response = array();
			        $message = '';
					$rules = array('publisher_code'=>'unique:publishers,publisher_code,'.$id);
				$inputs = Request::all();

		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		            $status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		        } else {
		            // store
		             $publishers = Publisher::find($id);$publishers->publisher_name = $inputs['publisher_name'];
				        		$publishers->publisher_code = $inputs['publisher_code'];
				        		$publishers->descriptions = $inputs['descriptions'];

				        		$publishers->scripts_1 = @$inputs['scripts_1'];
				        		$publishers->scripts_2 = @$inputs['scripts_2'];
				        		$publishers->scripts_3 = @$inputs['scripts_3'];
				        		$publishers->scripts_4 = @$inputs['scripts_4'];
				        		$publishers->scripts_5 = @$inputs['scripts_5'];
				        		$publishers->status = @$inputs['status'];

				        		$publishers->ads_enabled = (int)$inputs['ads_enabled'];
				        		$publishers->save();

		           	$message = 'Successfully updated Publishers!';
		           	$response['callback'] = url('/'.getCurrentUrlPrefix());

		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Remove the specified resource from storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function destroy($id)
			{
				//
					$status = 200;
			        $response = array();
			        $message = '';
				$publishers = Publisher::find($id);
				if(empty($publishers))
				 { 
				 	$status=421; 
				 	$response['errors']=array('No Publishers found!');
				 	$message='No Publishers found!';
				 }
				 else
				 {
				 	// $publishers->delete();
				 	 $publishers->status = 'DELETED';
				 	 $publishers->save();
				 	 $message = 'Successfully deleted Publishers!';

				 }
				 return $this->response($response,$status,$message);
			 
			}

		}
		