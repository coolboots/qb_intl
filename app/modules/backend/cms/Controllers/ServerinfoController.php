<?php namespace App\modules\backend\cms\Controllers;

		    use App\Http\Requests;
			use App\Http\Controllers\Controller;
			use App\Http\Models\Serverinfo;
			use Validator;
			use Redirect;
			use Request;
			use Carbon\Carbon;

			class ServerinfoController extends Controller {

			/**
			 * Display a listing of the resource.
			 *
			 * @return Response
			 */
			public function __construct()
			{
				$this->middleware('adminauth');
			}

			public function index()
			{ 
				if(is_axios()==true)
				{
					//filter params 
					$inputs = Request::all();
					$inputs = array_filter($inputs);
				$domain_name = @$inputs['domain_name'];
				$domain_expiry = @$inputs['domain_expiry'];
				$ssl_expiry = @$inputs['ssl_expiry'];
				$updated_by = @$inputs['updated_by'];
				$status = @$inputs['status'];
				$servers_info = Serverinfo::where(array())
				->when($domain_name, function ($query) use ($domain_name) {
				        	return $query->where('domain_name','LIKE' ,'%'.$domain_name.'%');})
				->when($domain_expiry, function ($query) use ($domain_expiry) {
				        	return $query->where('domain_expiry','LIKE' ,$domain_expiry.'%');})
				->when($ssl_expiry, function ($query) use ($ssl_expiry) {
				        	return $query->where('ssl_expiry','LIKE' ,$ssl_expiry.'%');})
				->when($updated_by, function ($query) use ($updated_by) {
				        	return $query->where('updated_by','=' ,$updated_by);})
				->when($status, function ($query) use ($status) {
				        	return $query->where('status','LIKE' ,$status.'%');})
				->orderBy('ssl_expiry','ASC')
				->paginate(20);
			      $servers_info->setPath('serverinfo');
				  $servers_info->appends(Request::except('page'));

					//echo $today = \Carbon::today();;die;
					return view('cms::serverinfo.index_view')->with('servers_info', $servers_info);

				}
				return view('cms::serverinfo.index');
			}

			/**
			 * Show the form for creating a new resource.
			 *
			 * @return Response
			 */
			public function create()
			{
				//
				return view('cms::serverinfo.create');
			}

			/**
			 * Store a newly created resource in storage.
			 *
			 * @return Response
			 */
			public function store()
			{
				//
				// validate
		        // read more on validation at http://laravel.com/docs/validation
		        	$status = 200;
			        $response = array();
			        $message = '';

		        
				$rules = array('domain_name'=>'required',
			        //'domain_expiry'=>'required',
			        'ssl_expiry'=>'required',
			        //'updated_by'=>'required',
			        'status'=>'required',
			        );

				$inputs = Request::all();
				$inputs = array_filter($inputs);

		        
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		        	$status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		            
		        } else {
		            // store
		            $admindata = auth()->guard('admin')->user();
	    			$id = $admindata->id;
		            $serverinfo = new Serverinfo;
		            $serverinfo->domain_name = $inputs['domain_name'];
				        		$serverinfo->used_for = @$inputs['used_for'];
				        		$serverinfo->domain_expiry = @$inputs['domain_expiry'];
				        		$serverinfo->ssl_expiry = @$inputs['ssl_expiry'];
				        		$serverinfo->updated_by = @$id;
				        		$serverinfo->status = $inputs['status'];
				        		$serverinfo->save();

		           //trigger seo url
					//triggerSeoUrls($serverinfo->id,'serverinfo',$inputs['main_seo_title']);
						$message = 'Successfully created Serverinfo!';

		            
		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Display the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function show($id)
			{
				//
				if($id=='quickupdate')
				{
					return $this->quickUpdate();
				}
				
				 $serverinfo = Serverinfo::find($id);

				 if(empty($serverinfo))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;
				 	
				 }

		        // show the view and pass the serverinfo to it
		        return view('cms::serverinfo.show')->with('serverinfo', $serverinfo);
			}


			//to update status quickly

			public function quickUpdate()
			{
				$inputs = Request::all();
				if(isset($inputs['id']) && isset($inputs['action']))
				{
					if($inputs['action']==1){$action='ACTIVE';}
					else if($inputs['action']==2){$action='INACTIVE';}
					else if($inputs['action']==3){$action='DELETED';}
					if($action!='')
					{Serverinfo::whereIn('id',explode(',', $inputs['id']))->update(['status'=>$action]);}
				}
			}

			/**
			 * Show the form for editing the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function edit($id)
			{
				//
				$serverinfo = Serverinfo::find($id);
				if(empty($serverinfo))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;

				 }

		        // show the view and pass the serverinfo to it
		        return view('cms::serverinfo.edit')->with('serverinfo', $serverinfo);
			}

			/**
			 * Update the specified resource in storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function update($id)
			{
				// read more on validation at http://laravel.com/docs/validation
		       		$status = 200;
			        $response = array();
			        $message = '';
		       $rules = array();
				$inputs = Request::all();
				$inputs = array_filter($inputs);

		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		            $status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		        } else {
		            // store
		            $admindata = auth()->guard('admin')->user();
	    			$id = $admindata->id;
		             $serverinfo = Serverinfo::find($id);$serverinfo->domain_name = $inputs['domain_name'];
				        		$serverinfo->used_for = @$inputs['used_for'];
				        		$serverinfo->domain_expiry = @$inputs['domain_expiry'];
				        		$serverinfo->ssl_expiry = @$inputs['ssl_expiry'];
				        		$serverinfo->updated_by = @$id;
				        		$serverinfo->status = @$inputs['status'];
				        		$serverinfo->save();

		           //trigger seo url
					//triggerSeoUrls($serverinfo->id,'serverinfo',$inputs['main_seo_title']);
						$message = 'Successfully updated Serverinfo!';
		           
		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Remove the specified resource from storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function destroy($id)
			{
				//
					$status = 200;
			        $response = array();
			        $message = '';
				$serverinfo = Serverinfo::find($id);
				if(empty($serverinfo))
				 { 
				 	$status=421; 
				 	$response['errors']=array('No Serverinfo found!');
				 	$message='No Serverinfo found!';
				 }
				 else
				 {
				 	 $serverinfo->status = 'DELETED';//();
				 	 $serverinfo->save();
				 	 $message = 'Successfully deleted Serverinfo!';

				 }
				 return $this->response($response,$status,$message);
			 
			}

		}
		