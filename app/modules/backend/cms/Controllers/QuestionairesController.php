<?php namespace App\modules\backend\cms\Controllers;

		    use App\Http\Requests;
			use App\Http\Controllers\Controller;
			use App\Http\Models\Question;
			use App\Http\Models\Questionoption;
			use Validator;
			use Redirect;
			use Request;
			use Excel;

			class QuestionairesController extends Controller {

			/**
			 * Display a listing of the resource.
			 *
			 * @return Response
			 */
			public function __construct()
			{
				$this->middleware('adminauth');
			}

			public function index()
			{ 
				if(is_axios()==true)
				{
					//filter params 
					$inputs = Request::all();
					$inputs = array_filter($inputs);
				$category_id = @$inputs['category_id'];
				$question_title = @$inputs['question_title'];
				$language_code = @$inputs['language_code'];
				$correct_answer = @$inputs['correct_answer'];
				$tags = @$inputs['tags'];
				$status = @$inputs['status'] ? @$inputs['status'] : 'ACTIVE';
				$created_date = @$inputs['created_date'];
				$questionaires = Question::select('questionaires.*','questionaire_options.option_1','questionaire_options.option_2','questionaire_options.option_3')
				->leftJoin('questionaire_options', function($leftJoin)
                        {
                            $leftJoin->on('questionaire_options.questionaire_id', '=', 'questionaires.id');
                               
                        })->where(array())
				->when($category_id, function ($query) use ($category_id) {
				        	return $query->where('category_id','=' ,$category_id);})
				->when($question_title, function ($query) use ($question_title) {
				        	return $query->where('question_title','LIKE' ,$question_title.'%');})
				->when($language_code, function ($query) use ($language_code) {
				        	return $query->where('language_code','LIKE' ,$language_code.'%');})
				->when($correct_answer, function ($query) use ($correct_answer) {
				        	return $query->where('correct_answer','LIKE' ,$correct_answer.'%');})
				->when($tags, function ($query) use ($tags) {
				        	return $query->where('tags','LIKE' ,$tags.'%');})
				->when($status, function ($query) use ($status) {
				        	return $query->where('status','LIKE' ,$status.'%');})
				->when($created_date, function ($query) use ($created_date) {
				        	return $query->where('questionaires.created_at','LIKE' ,$created_date.'%');})
				->orderBy('questionaires.id', 'desc')
				->paginate(20);
			      $questionaires->setPath('questionaires');
					$questionaires->appends(Request::except('page'));

					$categoryList = getCategories();
					return view('cms::questionaires.index_view')->with(['questionaires'=> $questionaires, 'categoryList'=>$categoryList]);

				}
				return view('cms::questionaires.index');
			}

			/**
			 * Show the form for creating a new resource.
			 *
			 * @return Response
			 */
			public function create()
			{
				//
				return view('cms::questionaires.create');
			}

			/**
			 * Store a newly created resource in storage.
			 *
			 * @return Response
			 */
			public function store()
			{
				//
				// validate
		        // read more on validation at http://laravel.com/docs/validation
		        	$status = 200;
			        $response = array();
			        $message = '';

		        
				$rules = array('category_id'=>'required',
			        'question_title'=>'required',
			        'option_A'=>'required',
			        'option_B'=>'required',
			        'option_C'=>'required',
			        'language_code'=>'required',
			        'correct_answer'=>'required',
			        'tags'=>'required',
			        'status'=>'required'
			        );

				$inputs = Request::all();
				$inputs = array_filter($inputs, 'RemoveFalseButNotZero');

		        
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		        	$status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		            
		        } else {
		            // store
		            $questionaires = new Question;
		            $questionaires->category_id = $inputs['category_id'];
	        		$questionaires->question_title = $inputs['question_title'];
	        		$questionaires->language_code = $inputs['language_code'];
	        		$questionaires->correct_answer = $inputs['correct_answer'];
	        		$questionaires->status = $inputs['status'];
	        		if(isset($inputs['tags']) && !empty($inputs['tags'])){
						$questionaires->tags = implode(',', $inputs['tags']);
					}
	        		$questionaires->save();

	        		if($questionaires->id){
		        		$questionairesopt = new Questionoption;
			            $questionairesopt->questionaire_id = $questionaires->id;
		        		$questionairesopt->option_1 = $inputs['option_A'];
		        		$questionairesopt->option_2 = $inputs['option_B'];
		        		$questionairesopt->option_3 = $inputs['option_C'];
		        		$questionairesopt->save();
		        	}

		           //trigger seo url
					//triggerSeoUrls($questionaires->id,'question',$inputs['main_seo_title']);
						$message = 'Successfully created Questionaires!';
					$response['callback'] = url('/'.getCurrentUrlPrefix());

		            
		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Display the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function show($id)
			{
				//
				if($id=='quickupdate')
				{
					return $this->quickUpdate();
				}
				
				 $questionaires = Question::find($id);

				 if(empty($questionaires))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;
				 	
				 }else{
				 	$Questionoption = Questionoption::where('questionaire_id',$id)->first();
				 	if($Questionoption){
				 		$questionaires->option = $Questionoption;
				 	}
				 }

				 $categoryList = getCategories();
		        // show the view and pass the questionaires to it
		        return view('cms::questionaires.show')->with(['questionaires' => $questionaires,'categoryList' => $categoryList]);
			}


			//to update status quickly

			public function quickUpdate()
			{
				$inputs = Request::all();
				if(isset($inputs['id']) && isset($inputs['action']))
				{
					if($inputs['action']==1){$action='ACTIVE';}
					else if($inputs['action']==2){$action='INACTIVE';}
					else if($inputs['action']==3){$action='DELETED';}
					if($action!='')
					{Question::whereIn('id',explode(',', $inputs['id']))->update(['status'=>$action]);}
				}
			}

			/**
			 * Show the form for editing the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function edit($id)
			{
				//
				$questionaires = Question::find($id);
				if(empty($questionaires))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;

				 }else{
				 	$Questionoption = Questionoption::where('questionaire_id',$id)->first();
				 	if($Questionoption){
				 		$questionaires->option = $Questionoption;
				 	}
				 }

		        // show the view and pass the questionaires to it
		        return view('cms::questionaires.edit')->with('questionaires', $questionaires);
			}

			/**
			 * Update the specified resource in storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function update($id)
			{
				// read more on validation at http://laravel.com/docs/validation
		       		$status = 200;
			        $response = array();
			        $message = '';
		       $rules = array();
				$inputs = Request::all();
				$inputs = array_filter($inputs,'RemoveFalseButNotZero');

		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		            $status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		        } else {
		            // store
		             $questionaires = Question::find($id);
		             $questionaires->category_id = $inputs['category_id'];
	        		$questionaires->question_title = $inputs['question_title'];
	        		$questionaires->language_code = $inputs['language_code'];
	        		$questionaires->correct_answer = $inputs['correct_answer'];
	        		$questionaires->status = $inputs['status'];
	        		if(isset($inputs['tags']) && !empty($inputs['tags'])){
						$questionaires->tags = implode(',', $inputs['tags']);
					}
	        		$questionaires->save();

	        		$questionOption = Questionoption::where('questionaire_id',$id)->first();

	        		if($questionOption){
		        		$questionairesopt = Questionoption::find($questionOption->id);
		        	}else{
		        		$questionairesopt = new Questionoption;
		        	}
			            $questionairesopt->questionaire_id = $id;
		        		$questionairesopt->option_1 = $inputs['option_A'];
		        		$questionairesopt->option_2 = $inputs['option_B'];
		        		$questionairesopt->option_3 = $inputs['option_C'];
		        		$questionairesopt->save();
		        	

		           //trigger seo url
					//triggerSeoUrls($questionaires->id,'question',$inputs['main_seo_title']);
						$message = 'Successfully updated Questionaires!';
						$response['callback'] = url('/'.getCurrentUrlPrefix());
		           
		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Remove the specified resource from storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function destroy($id)
			{
				//
					$status = 200;
			        $response = array();
			        $message = '';
				$questionaires = Question::find($id);
				if(empty($questionaires))
				 { 
				 	$status=421; 
				 	$response['errors']=array('No Questionaires found!');
				 	$message='No Questionaires found!';
				 }
				 else
				 {
				 	//Questionoption::where('questionaire_id',$id)->delete();
				 	 //$questionaires->delete();
				 	$questionaires->status = 'DELETED';
				 	 $questionaires->save();
				 	 $message = 'Successfully deleted Questionaires!';

				 }
				 return $this->response($response,$status,$message);
			 
			}

			public function import()
			{
				//
				return view('cms::questionaires.import');
			}

			public function importExcel(Request $request)
			{
				//
				// validate
		        // read more on validation at http://laravel.com/docs/validation
		        	$status = 200;
			        $response = array();
			        $message = '';

		        
				$rules = array('category_id'=>'required',
			        'language_code'=>'required',
			        'tags'=>'required',
			        'excel_import'=>'required|file|max:2048|mimes:xls,xlsx',
			        'status'=>'required',
			        );

				$inputs = Request::all();
				$inputs = array_filter($inputs);

		        
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		        	$status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		            
		        } else {
		            // store

		        	$import = \Request::file('excel_import');

		        	if(isset($import) and !empty($import)){

		        		$path = \Request::file('excel_import')->getRealPath();
     					$data = \Excel::toArray('', $path, null, \Maatwebsite\Excel\Excel::XLSX);
     					unset($data[0][0]);	

     					$rawQuery = 'INSERT INTO questionaires(category_id, question_title, language_code, correct_answer, tags, status, created_at, updated_at) VALUES';

     					if(isset($data[0])){
			            foreach ($data[0] as $key => $row) {

			            	// p($row);
			            	if(!empty(@$row['0']) && !empty(@$row['1']) && !empty(@$row['2']) && !empty(@$row['3']) && !empty(@$row['4'])) {

				                if($key == 1) {
				                   $rawQuery.= "('".$inputs['category_id']."','".@addslashes($row[0])."','".$inputs['language_code']."','".@$row[4]."','".implode(',', $inputs['tags'])."', '".$inputs['status']."', '".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."')";
				                }else{
				                   $rawQuery.= ",('".$inputs['category_id']."','".@addslashes($row[0])."','".$inputs['language_code']."','".@$row[4]."','".implode(',', $inputs['tags'])."', '".$inputs['status']."', '".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."')";
				                }			       
			            	}
			            }

		                $rawQuery.= ' ON DUPLICATE KEY UPDATE question_title=question_title';
		                 \DB::statement($rawQuery);

		                $newRawQuery = 'INSERT INTO questionaire_options(questionaire_id, option_1, option_2, option_3, created_at, updated_at) VALUES';

			            foreach ($data[0] as $key => $row) {
			            	if(!empty(@$row['0']) && !empty(@$row['1']) && !empty(@$row['2']) && !empty(@$row['3'])) {

				                if($key == 1) {
				                   $newRawQuery.= "((SELECT id FROM questionaires WHERE question_title='".@addslashes($row[0])."' AND category_id='".$inputs['category_id']."'),'".@addslashes($row[1])."','".@addslashes($row[2])."','".@addslashes($row[3])."', '".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."')";
				                }else{
				                   $newRawQuery.= ",((SELECT id FROM questionaires WHERE question_title='".@addslashes($row[0])."' AND category_id='".$inputs['category_id']."'),'".@addslashes($row[1])."','".@addslashes($row[2])."','".@addslashes($row[3])."', '".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."')";
				                }			       
			            	}
			            }
			            $newRawQuery.= ' ON DUPLICATE KEY UPDATE questionaire_id=questionaire_id';
		                 \DB::statement($newRawQuery);

		                $message = 'Successfully Uploaded Questionaires!';
						$response['callback'] = url('/'.getCurrentUrlPrefix());
			            
			        }
			        }		        

		        }
		        return $this->response($response,$status,$message);
			}

		}
		