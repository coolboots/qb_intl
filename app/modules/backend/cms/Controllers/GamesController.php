<?php namespace App\modules\backend\cms\Controllers;

		    use App\Http\Requests;
			use App\Http\Controllers\Controller;
			use App\Http\Models\Game;
			use Validator;
			use Redirect;
			use Request;
use Symfony\Component\Process\InputStream;

class GamesController extends Controller {

			/**
			 * Display a listing of the resource.
			 *
			 * @return Response
			 */
			public function __construct()
			{
				$this->middleware('adminauth');
			}

			public function index()
			{ 
				if(is_axios()==true)
				{
					//filter params 
					$inputs = Request::all();
					$inputs = array_filter($inputs);
				$game_name = @$inputs['game_name'];
				$logo = @$inputs['logo'];
				$banner_image = @$inputs['banner_image'];
				$game_instruction_video = @$inputs['game_instruction_video'];
				$game_position = @$inputs['game_position'];
				$game_url = @$inputs['game_url'];
				$game_layout = @$inputs['game_layout'];
				$status = @$inputs['status'];
				 if(empty($inputs)){
					 $status='ACTIVE';
				 }
				$games = Game::where(array())
				->when($game_name, function ($query) use ($game_name) {
				        	return $query->where('game_name','LIKE' ,'%'.$game_name.'%');})
				->when($logo, function ($query) use ($logo) {
				        	return $query->where('logo','LIKE' ,$logo.'%');})
				->when($banner_image, function ($query) use ($banner_image) {
				        	return $query->where('banner_image','LIKE' ,$banner_image.'%');})
				->when($game_instruction_video, function ($query) use ($game_instruction_video) {
				        	return $query->where('game_instruction_video','LIKE' ,$game_instruction_video.'%');})
				->when($game_position, function ($query) use ($game_position) {
				        	return $query->where('game_position','=' ,$game_position);})
				->when($game_url, function ($query) use ($game_url) {
				        	return $query->where('game_url','LIKE' ,$game_url.'%');})
				->when($game_layout, function ($query) use ($game_layout) {
				        	return $query->where('game_layout','LIKE' ,$game_layout.'%');})
				->when($status, function ($query) use ($status) {
							return $query->where('status','LIKE' ,$status.'%');})
				->orderBy('game_position','ASC')
				->paginate(20);
			      $games->setPath('games');
					$games->appends(Request::except('page'));
					return view('cms::games.index_view')->with('games', $games);

				}
				
				return view('cms::games.index');
			}

			/**
			 * Show the form for creating a new resource.
			 *
			 * @return Response
			 */
			public function create()
			{
				//
				return view('cms::games.create');
			}

			/**
			 * Store a newly created resource in storage.
			 *
			 * @return Response
			 */
			public function store()
			{
				//
				// validate
		        // read more on validation at http://laravel.com/docs/validation
		        	$status = 200;
			        $response = array();
			        $message = '';

		        
				$rules = array('game_name'=>'required',
			        'logo'=>'required|max:100|mimes:jpeg,jpg,png,gif',
			        'banner_image'=>'required|max:500|mimes:jpeg,jpg,png,gif',
			        'game_instruction_video'=>'required|url',
			        'game_position'=>'required|numeric',
			        'game_url'=>'required|url',
			        'game_layout'=>'required',
			        'status'=>'required',
			        );
		        
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		        	$status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		            
		        } else {
		            // store
		            $games = new Game;
		            $games->game_name = Request::get('game_name');
				        		
				        		$import = \Request::file('logo');
									if(isset($import) and !empty($import))
						            {
						            	$data = uploadImage($import,'games');
						            	$games->logo = $data;
									}
								$import = \Request::file('banner_image');
									if(isset($import) and !empty($import))
						            {
						            	$data = uploadImage($import,'games');
						            	$games->banner_image = $data;
									}
								//	$games->banner_image = Request::get('banner_image');
				        		$games->game_long_desc = Request::get('game_long_desc');
				        		$games->game_short_desc = Request::get('game_short_desc');
				        		$games->game_instruction_video = Request::get('game_instruction_video');
				        		$games->game_position = Request::get('game_position');
				        		$games->game_url = Request::get('game_url');
				        		$games->game_layout = Request::get('game_layout');
				        		$games->status = Request::get('status');
				        		$games->save();

		           //trigger seo url
					triggerSeoUrls($games->id,'game',Request::get('main_seo_title'));
						$message = 'Successfully created Games!';
						$response['callback'] = url('/'.getCurrentUrlPrefix());
		            
		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Display the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function show($id)
			{
				//
				if($id=='quickupdate')
				{
					return $this->quickUpdate();
				}
				 $games = Game::find($id);

				 if(empty($games))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;
				 	
				 }
				 // show the view and pass the games to it
		        return view('cms::games.show')->with('games', $games);
			}


			public function quickUpdate()
			{
				$inputs = Request::all();
				if(isset($inputs['id']) && isset($inputs['action']))
				{
					if($inputs['action']==1){$action='ACTIVE';}
					else if($inputs['action']==2){$action='INACTIVE';}
					else if($inputs['action']==3){$action='DELETED';}
					if($action!='')
					{Game::whereIn('id',explode(',', $inputs['id']))->update(['status'=>$action]);}
				}
			}

			/**
			 * Show the form for editing the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function edit($id)
			{
				//
				$games = Game::find($id);
				if(empty($games))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;

				 }

		        // show the view and pass the games to it
		        return view('cms::games.edit')->with('games', $games);
			}

			/**
			 * Update the specified resource in storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function update($id)
			{
				// read more on validation at http://laravel.com/docs/validation
		       		$status = 200;
			        $response = array();
			        $message = '';
		       $rules = array('logo'=>'max:100|mimes:jpeg,jpg,png,gif',
			   'banner_image'=>'max:500|mimes:jpeg,jpg,png,gif',
			   'game_instruction_video'=> 'url',
			   'game_url'=>'url',
			   'game_position'=>'numeric',);
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		            $status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		        } else {
		            // store
		             $games = Game::find($id);$games->game_name = Request::get('game_name');
				        		
				        		$import = \Request::file('logo');
									if(isset($import) and !empty($import))
						            {
						            	$data = uploadImage($import,'games');
						            	$games->logo = $data;
									}
								$import = \Request::file('banner_image');
									if(isset($import) and !empty($import))
						            {
						            	$data = uploadImage($import,'games');
						            	$games->banner_image = $data;
									}
									//$games->banner_image = Request::get('banner_image');
				        		$games->game_long_desc = Request::get('game_long_desc');
				        		$games->game_short_desc = Request::get('game_short_desc');
				        		$games->game_instruction_video = Request::get('game_instruction_video');
				        		$games->game_position = Request::get('game_position');
				        		$games->game_url = Request::get('game_url');
				        		$games->game_layout = Request::get('game_layout');
				        		$games->status = Request::get('status');
				        		$games->save();

		           //trigger seo url
					triggerSeoUrls($games->id,'game',Request::get('main_seo_title'));
						$message = 'Successfully updated Games!';
						$response['callback'] = url('/'.getCurrentUrlPrefix());
		           
		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Remove the specified resource from storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function destroy($id)
			{
				//
					$status = 200;
			        $response = array();
			        $message = '';
				$games = Game::find($id);
				if(empty($games))
				 { 
				 	$status=421; 
				 	$response['errors']=array('No Game found!');
				 	$message='No game found!';
				 }
				 else
				 {
				 	 $games->status = 'DELETED';
				 	 $games->save();
				 	 $message = 'Successfully deleted Games!';

				 }
				 return $this->response($response,$status,$message);
			 
			}

		}
		