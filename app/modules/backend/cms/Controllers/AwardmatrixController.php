<?php namespace App\modules\backend\cms\Controllers;

		    use App\Http\Requests;
			use App\Http\Controllers\Controller;
			use App\Http\Models\Awardmatrix;
			use Validator;
			use Redirect;
			use Request;

			class AwardmatrixController extends Controller {

			/**
			 * Display a listing of the resource.
			 *
			 * @return Response
			 */
			public function __construct()
			{
				$this->middleware('adminauth');
			}

			public function index()
			{ 
				if(is_axios()==true)
				{
					//filter params 
					$inputs = Request::all();
					$inputs = array_filter($inputs);
				$contest_id = @$inputs['contest_id'];
				$winner_from = @$inputs['winner_from'];
				$winner_to = @$inputs['winner_to'];
				$prize_type = @$inputs['prize_type'];
				$prize_amount = @$inputs['prize_amount'];
				$contest_award_matrix = Awardmatrix::where(array())
				->when($contest_id, function ($query) use ($contest_id) {
				        	return $query->where('contest_id','=' ,$contest_id);})
				->when($winner_from, function ($query) use ($winner_from) {
				        	return $query->where('winner_from','=' ,$winner_from);})
				->when($winner_to, function ($query) use ($winner_to) {
				        	return $query->where('winner_to','=' ,$winner_to);})
				->when($prize_type, function ($query) use ($prize_type) {
				        	return $query->where('prize_type','LIKE' ,$prize_type.'%');})
				->when($prize_amount, function ($query) use ($prize_amount) {
				        	return $query->where('prize_amount','=' ,$prize_amount);})
				->paginate(20);
			      $contest_award_matrix->setPath('awardmatrix');
					$contest_award_matrix->appends(Request::except('page'));
					return view('cms::awardmatrix.index_view')->with('contest_award_matrix', $contest_award_matrix);

				}
				return view('cms::awardmatrix.index');
			}

			/**
			 * Show the form for creating a new resource.
			 *
			 * @return Response
			 */
			public function create()
			{
				//
				return view('cms::awardmatrix.create');
			}

			/**
			 * Store a newly created resource in storage.
			 *
			 * @return Response
			 */
			public function store()
			{
				//
				// validate
		        // read more on validation at http://laravel.com/docs/validation
		        	$status = 200;
			        $response = array();
			        $message = '';

		        
				$rules = array('contest_id'=>'required',
			        'winner_from'=>'required',
			        'winner_to'=>'required',
			        'prize_type'=>'required',
			        'prize_amount'=>'required',
			        );

				$inputs = Request::all();
				$inputs = array_filter($inputs);

		        
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		        	$status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		            
		        } else {
		            // store
		            $awardmatrix = new Awardmatrix;
		            $awardmatrix->contest_id = $inputs['contest_id'];
				        		$awardmatrix->winner_from = $inputs['winner_from'];
				        		$awardmatrix->winner_to = $inputs['winner_to'];
				        		$awardmatrix->prize_type = $inputs['prize_type'];
				        		$awardmatrix->prize_amount = $inputs['prize_amount'];
				        		$awardmatrix->save();

		           //trigger seo url
					//triggerSeoUrls($awardmatrix->id,'awardmatrix',$inputs['main_seo_title']);
						$message = 'Successfully created Awardmatrix!';

		            
		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Display the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function show($id)
			{
				//
				if($id=='quickupdate')
				{
					return $this->quickUpdate();
				}
				 $awardmatrix = Awardmatrix::find($id);

				 if(empty($awardmatrix))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;
				 	
				 }

		        // show the view and pass the awardmatrix to it
		        return view('cms::awardmatrix.show')->with('awardmatrix', $awardmatrix);
			}

			public function quickUpdate()
			{
				$inputs = Request::all();
				if(isset($inputs['id']) && isset($inputs['action']))
				{
					if($inputs['action']==1){$action='ACTIVE';}
					else if($inputs['action']==2){$action='INACTIVE';}
					else if($inputs['action']==3){$action='DELETED';}
					if($action!='')
					{Awardmatrix::whereIn('id',explode(',', $inputs['id']))->update(['status'=>$action]);}
				}
			}

			/**
			 * Show the form for editing the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function edit($id)
			{
				//
				$awardmatrix = Awardmatrix::find($id);
				if(empty($awardmatrix))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;

				 }

		        // show the view and pass the awardmatrix to it
		        return view('cms::awardmatrix.edit')->with('awardmatrix', $awardmatrix);
			}

			/**
			 * Update the specified resource in storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function update($id)
			{
				// read more on validation at http://laravel.com/docs/validation
		       		$status = 200;
			        $response = array();
			        $message = '';
		       $rules = array();
				$inputs = Request::all();
				$inputs = array_filter($inputs);

		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		            $status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		        } else {
		            // store
		             $awardmatrix = Awardmatrix::find($id);$awardmatrix->contest_id = $inputs['contest_id'];
				        		$awardmatrix->winner_from = $inputs['winner_from'];
				        		$awardmatrix->winner_to = $inputs['winner_to'];
				        		$awardmatrix->prize_type = $inputs['prize_type'];
				        		$awardmatrix->prize_amount = $inputs['prize_amount'];
				        		$awardmatrix->save();

		           //trigger seo url
					//triggerSeoUrls($awardmatrix->id,'awardmatrix',$inputs['main_seo_title']);
						$message = 'Successfully updated Awardmatrix!';
		           
		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Remove the specified resource from storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function destroy($id)
			{
				//
					$status = 200;
			        $response = array();
			        $message = '';
				$awardmatrix = Awardmatrix::find($id);
				if(empty($awardmatrix))
				 { 
				 	$status=421; 
				 	$response['errors']=array('No Awardmatrix found!');
				 	$message='No Awardmatrix found!';
				 }
				 else
				 {
				 	 $awardmatrix->delete();
				 	 $message = 'Successfully deleted Awardmatrix!';

				 }
				 return $this->response($response,$status,$message);
			 
			}

		}
		