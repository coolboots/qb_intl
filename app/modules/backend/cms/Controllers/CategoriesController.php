<?php namespace App\modules\backend\cms\Controllers;

		    use App\Http\Requests;
			use App\Http\Controllers\Controller;
			use App\Http\Models\Category;
			use Validator;
			use Redirect;
			use Request;

			class CategoriesController extends Controller {

			/**
			 * Display a listing of the resource.
			 *
			 * @return Response
			 */
			public function __construct()
			{
				$this->middleware('adminauth');
			}

			public function index()
			{ 
				if(is_axios()==true)
				{
					//filter params 
					$inputs = Request::all();
					$inputs = array_filter($inputs);
				$title = @$inputs['title'];
				$image = @$inputs['image'];
				$parent_id = @$inputs['parent_id'];
				$status = @$inputs['status'] ? @$inputs['status'] : 'ACTIVE';
				$categories = Category::where(array())
				->when($title, function ($query) use ($title) {
				        	return $query->where('title','LIKE' ,$title.'%');})
				->when($image, function ($query) use ($image) {
				        	return $query->where('image','LIKE' ,$image.'%');})
				->when($parent_id, function ($query) use ($parent_id) {
				        	return $query->where('parent_id','=' ,$parent_id);})
				->when($status, function ($query) use ($status) {
				        	return $query->where('status','LIKE' ,$status.'%');})
				// ->orderBy('id', 'desc')
				->paginate(20);
			      $categories->setPath('categories');
					$categories->appends(Request::except('page'));

				$categoryList = getCategories();
				return view('cms::categories.index_view')->with(['categories' => $categories, 'categoryList'=>$categoryList]);

				}
				return view('cms::categories.index');
			}

			/**
			 * Show the form for creating a new resource.
			 *
			 * @return Response
			 */
			public function create()
			{
				//
				return view('cms::categories.create');
			}

			/**
			 * Store a newly created resource in storage.
			 *
			 * @return Response
			 */
			public function store()
			{
				//
				// validate
		        // read more on validation at http://laravel.com/docs/validation
		        	$status = 200;
			        $response = array();
			        $message = '';

		        
				$rules = array('title'=>'required',
			        'image'=>'image|mimes:jpeg,jpg,png,gif',
			        //'parent_id'=>'required',
			        );

				$inputs = Request::all();
				$inputs = array_filter($inputs);

		        
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		        	$status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		            
		        } else {
		            // store
		            $categories = new Category;
		            $categories->title = $inputs['title'];
				        		
				        		$import = \Request::file('image');
									if(isset($import) and !empty($import))
						            {
						            	$data = uploadImage($import,'categories');
						            	$categories->image = $data;
									}
									$categories->parent_id = (isset($inputs['parent_id']))? $inputs['parent_id'] : 0;
									if(isset($inputs['tags']) && !empty($inputs['tags'])){
									   $categories->tags = implode(',', array_filter($inputs['tags']));
									}
								$categories->status = @$inputs['status'] ? @$inputs['status']: 'ACTIVE';
				        		$categories->save();

		           //trigger seo url
					triggerSeoUrls($categories->id,'category',$inputs['main_seo_title']);
						$message = 'Successfully created Categories!';
					$response['callback'] = url('/'.getCurrentUrlPrefix());

		            
		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Display the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function show($id)
			{
				//
				if($id=='quickupdate')
				{
					return $this->quickUpdate();
				}
				
				 $categories = Category::find($id);

				 if(empty($categories))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;
				 	
				 }

				 $categoryList = getCategories();
		        // show the view and pass the categories to it
		        return view('cms::categories.show')->with(['categories'=> $categories, 'categoryList'=>$categoryList]);
			}


			//to update status quickly

			public function quickUpdate()
			{
				$inputs = Request::all();
				if(isset($inputs['id']) && isset($inputs['action']))
				{
					if($inputs['action']==1){$action='ACTIVE';}
					else if($inputs['action']==2){$action='INACTIVE';}
					else if($inputs['action']==3){$action='DELETED';}
					if($action!='')
					{Category::whereIn('id',explode(',', $inputs['id']))->update(['status'=>$action]);}
				}
			}

			/**
			 * Show the form for editing the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function edit($id)
			{
				//
				$categories = Category::find($id);
				if(empty($categories))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;

				 }

		        // show the view and pass the categories to it
		        return view('cms::categories.edit')->with('categories', $categories);
			}

			/**
			 * Update the specified resource in storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function update($id)
			{
				// read more on validation at http://laravel.com/docs/validation
		       		$status = 200;
			        $response = array();
			        $message = '';
		       $rules = array('image'=>'image|mimes:jpeg,jpg,png,gif',);
				$inputs = Request::all();
				$inputs = array_filter($inputs);

		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		            $status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		        } else {

		        	//p($inputs);
		            // store
		             $categories = Category::find($id);$categories->title = $inputs['title'];
				        		
				        		$import = \Request::file('image');
									if(isset($import) and !empty($import))
						            {
						            	$data = uploadImage($import,'categories');
						            	$categories->image = $data;
									}
									$categories->parent_id = (isset($inputs['parent_id']))? $inputs['parent_id'] : 0;
									if(isset($inputs['tags']) && !empty($inputs['tags'])){
									   $categories->tags = implode(',', array_filter($inputs['tags']));
									}
								$categories->status = @$inputs['status'] ? @$inputs['status'] : 'ACTIVE';
				        		$categories->save();

		           //trigger seo url
					triggerSeoUrls($categories->id,'category',$inputs['main_seo_title']);
						$message = 'Successfully updated Categories!';
					$response['callback'] = url('/'.getCurrentUrlPrefix());
		           
		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Remove the specified resource from storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function destroy($id)
			{
				//
					$status = 200;
			        $response = array();
			        $message = '';
				$categories = Category::find($id);
				if(empty($categories))
				 { 
				 	$status=421; 
				 	$response['errors']=array('No Categories found!');
				 	$message='No Categories found!';
				 }
				 else
				 {
				 	 //$categories->delete();
				 	$categories->status = 'INACTIVE';
				 	 $categories->save();
				 	 $message = 'Successfully deleted Categories!';

				 }
				 return $this->response($response,$status,$message);
			 
			}

		}
		