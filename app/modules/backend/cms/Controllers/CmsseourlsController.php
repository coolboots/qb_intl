<?php namespace App\modules\backend\cms\Controllers;

		    use App\Http\Requests;
			use App\Http\Controllers\Controller;
			use App\Http\Models\Seourl;
			use Validator;
			use Redirect;
			use Request;

			class CmsseourlsController extends Controller {

			/**
			 * Display a listing of the resource.
			 *
			 * @return Response
			 */
			public function __construct()
			{
				$this->middleware('adminauth');
			}

			public function index()
			{ 
				if(is_axios()==true)
				{
					//filter params 
					$inputs = Request::all();
					$inputs = array_filter($inputs);
				$object_type = @$inputs['object_type'];
				$object_id = @$inputs['object_id'];
				$object_data = @$inputs['object_data'];
				$cms_seo_urls = Seourl::where(array())
				->when($object_type, function ($query) use ($object_type) {
				        	return $query->where('object_type','LIKE' ,$object_type.'%');})
				->when($object_id, function ($query) use ($object_id) {
				        	return $query->where('object_id','=' ,$object_id);})
				->when($object_data, function ($query) use ($object_data) {
				        	return $query->where('object_data','LIKE' ,$object_data.'%');})
				->paginate(20);
			      $cms_seo_urls->setPath('cmsseourls');
					$cms_seo_urls->appends(Request::except('page'));
					return view('cms::cmsseourls.index_view')->with('cms_seo_urls', $cms_seo_urls);

				}
				return view('cms::cmsseourls.index');
			}

			/**
			 * Show the form for creating a new resource.
			 *
			 * @return Response
			 */
			public function create()
			{
				//
				return view('cms::cmsseourls.create');
			}

			/**
			 * Store a newly created resource in storage.
			 *
			 * @return Response
			 */
			public function store()
			{
				//
				// validate
		        // read more on validation at http://laravel.com/docs/validation
		        	$status = 200;
			        $response = array();
			        $message = '';

		        
				$rules = array('object_type'=>'required',
			        'object_id'=>'required',
			        'object_data'=>'required',
			        );
		        
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		        	$status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		            
		        } else {
		            // store
		            $cmsseourls = new Seourl;
		            $cmsseourls->object_type = Request::get('object_type');
				        		$cmsseourls->object_id = Request::get('object_id');
				        		$cmsseourls->object_data = Request::get('object_data');
				        		$cmsseourls->save();

		           //trigger seo url
					triggerSeoUrls($cmsseourls->id,'seourl',Request::get('main_seo_title'));
						$message = 'Successfully created Cmsseourls!';

		            
		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Display the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function show($id)
			{
				//
				if($id=='quickupdate')
				{
					return $this->quickUpdate();
				}
				 $cmsseourls = Seourl::find($id);

				 if(empty($cmsseourls))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;
				 	
				 }

		        // show the view and pass the cmsseourls to it
		        return view('cms::cmsseourls.show')->with('cmsseourls', $cmsseourls);
			}


			public function quickUpdate()
			{
				$inputs = Request::all();
				if(isset($inputs['id']) && isset($inputs['action']))
				{
					if($inputs['action']==1){$action='ACTIVE';}
					else if($inputs['action']==2){$action='INACTIVE';}
					else if($inputs['action']==3){$action='DELETED';}
					if($action!='')
					{Seourl::whereIn('id',explode(',', $inputs['id']))->update(['status'=>$action]);}
				}
			}

			/**
			 * Show the form for editing the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function edit($id)
			{
				//
				$cmsseourls = Seourl::find($id);
				if(empty($cmsseourls))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;

				 }

		        // show the view and pass the cmsseourls to it
		        return view('cms::cmsseourls.edit')->with('cmsseourls', $cmsseourls);
			}

			/**
			 * Update the specified resource in storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function update($id)
			{
				// read more on validation at http://laravel.com/docs/validation
		       		$status = 200;
			        $response = array();
			        $message = '';
		       $rules = array();
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		            $status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		        } else {
		            // store
		             $cmsseourls = Seourl::find($id);$cmsseourls->object_type = Request::get('object_type');
				        		$cmsseourls->object_id = Request::get('object_id');
				        		$cmsseourls->object_data = Request::get('object_data');
				        		$cmsseourls->save();

		           //trigger seo url
					triggerSeoUrls($cmsseourls->id,'seourl',Request::get('main_seo_title'));
						$message = 'Successfully updated Cmsseourls!';
		           
		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Remove the specified resource from storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function destroy($id)
			{
				//
					$status = 200;
			        $response = array();
			        $message = '';
				$cmsseourls = Seourl::find($id);
				if(empty($cmsseourls))
				 { 
				 	$status=421; 
				 	$response['errors']=array('No department found!');
				 	$message='No department found!';
				 }
				 else
				 {
				 	 $cmsseourls->delete();
				 	 $message = 'Successfully deleted Cmsseourls!';

				 }
				 return $this->response($response,$status,$message);
			 
			}

		}
		