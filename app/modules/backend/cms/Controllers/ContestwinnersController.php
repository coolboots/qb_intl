<?php namespace App\modules\backend\cms\Controllers;

		    use App\Http\Requests;
			use App\Http\Controllers\Controller;
			use App\Http\Models\Contestwinner;
			use Validator;
			use Redirect;
			use Request;

			class ContestwinnersController extends Controller {

			/**
			 * Display a listing of the resource.
			 *
			 * @return Response
			 */
			public function __construct()
			{
				$this->middleware('adminauth');
			}

			public function index()
			{ 
				if(is_axios()==true)
				{
					//filter params 
					$inputs = Request::all();
					$inputs = array_filter($inputs);
				$contest_id = @$inputs['contest_id'];
				$game_id = @$inputs['game_id'];
				$user_id = @$inputs['user_id'];
				$position = @$inputs['position'];
				$earned_prize = @$inputs['earned_prize'];
				$contest_winners = Contestwinner::where(array())
				->when($contest_id, function ($query) use ($contest_id) {
				        	return $query->where('contest_id' ,$contest_id);})
				->when($game_id, function ($query) use ($game_id) {
				        	return $query->where('game_id' ,$game_id);})
				->when($user_id, function ($query) use ($user_id) {
				        	return $query->where('user_id','=' ,$user_id);})
				->when($position, function ($query) use ($position) {
				        	return $query->where('position','=' ,$position);})
				->when($earned_prize, function ($query) use ($earned_prize) {
				        	return $query->where('earned_prize','=' ,$earned_prize);})
				->paginate(20);
			      $contest_winners->setPath('contestwinners');
					$contest_winners->appends(Request::except('page'));
					return view('cms::contestwinners.index_view')->with('contest_winners', $contest_winners);

				}
				return view('cms::contestwinners.index');
			}

			/**
			 * Show the form for creating a new resource.
			 *
			 * @return Response
			 */
			public function create()
			{
				//
				return view('cms::contestwinners.create');
			}

			/**
			 * Store a newly created resource in storage.
			 *
			 * @return Response
			 */
			public function store()
			{
				//
				// validate
		        // read more on validation at http://laravel.com/docs/validation
		        	$status = 200;
			        $response = array();
			        $message = '';

		        
				$rules = array(
					'contest_id'=>'required',
					'game_id'=>'required',
			        'user_id'=>'required',
			        'position'=>'required',
			        'earned_prize'=>'required',
			        );
		        
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		        	$status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		            
		        } else {
		            // store
		            $contestwinners = new Contestwinner;
		            $contestwinners->game_id = Request::get('game_id');
		            $contestwinners->contest_id = Request::get('contest_id');
				        		$contestwinners->user_id = Request::get('user_id');
				        		$contestwinners->position = Request::get('position');
				        		$contestwinners->earned_prize = Request::get('earned_prize');
				        		$contestwinners->save();

		           //trigger seo url
					//triggerSeoUrls($contestwinners->id,'contestwinner',Request::get('main_seo_title'));
						$message = 'Successfully created Contestwinners!';

		            
		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Display the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function show($id)
			{
				//
				if($id=='quickupdate')
				{
					return $this->quickUpdate();
				}
				 $contestwinners = Contestwinner::find($id);

				 if(empty($contestwinners))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;
				 	
				 }

		        // show the view and pass the contestwinners to it
		        return view('cms::contestwinners.show')->with('contestwinners', $contestwinners);
			}

			public function quickUpdate()
			{
				$inputs = Request::all();
				if(isset($inputs['id']) && isset($inputs['action']))
				{
					if($inputs['action']==1){$action='ACTIVE';}
					else if($inputs['action']==2){$action='INACTIVE';}
					else if($inputs['action']==3){$action='DELETED';}
					if($action!='')
					{Contestwinner::whereIn('id',explode(',', $inputs['id']))->update(['status'=>$action]);}
				}
			}

			/**
			 * Show the form for editing the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function edit($id)
			{
				//
				$contestwinners = Contestwinner::find($id);
				if(empty($contestwinners))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;

				 }

		        // show the view and pass the contestwinners to it
		        return view('cms::contestwinners.edit')->with('contestwinners', $contestwinners);
			}

			/**
			 * Update the specified resource in storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function update($id)
			{
				// read more on validation at http://laravel.com/docs/validation
		       		$status = 200;
			        $response = array();
			        $message = '';
		       $rules = array();
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		            $status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		        } else {
		            // store
		             $contestwinners = Contestwinner::find($id);
		             $contestwinners->game_id = Request::get('game_id');
		             $contestwinners->contest_id = Request::get('contest_id');
				        		$contestwinners->user_id = Request::get('user_id');
				        		$contestwinners->position = Request::get('position');
				        		$contestwinners->earned_prize = Request::get('earned_prize');
				        		$contestwinners->save();

		           //trigger seo url
					//triggerSeoUrls($contestwinners->id,'contestwinner',Request::get('main_seo_title'));
						$message = 'Successfully updated Contestwinners!';
		           
		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Remove the specified resource from storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function destroy($id)
			{
				//
					$status = 200;
			        $response = array();
			        $message = '';
				$contestwinners = Contestwinner::find($id);
				if(empty($contestwinners))
				 { 
				 	$status=421; 
				 	$response['errors']=array('No winner found!');
				 	$message='No winner found!';
				 }
				 else
				 {
				 	 $contestwinners->delete();
				 	 $message = 'Successfully deleted Contestwinners!';

				 }
				 return $this->response($response,$status,$message);
			 
			}

		}
		