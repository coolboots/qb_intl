<?php namespace App\modules\backend\cms\Controllers;

		    use App\Http\Requests;
			use App\Http\Controllers\Controller;
			use App\Http\Models\Contestparticipanthistory;
			use App\User;
			use Carbon\Carbon;
			use Validator;
			use Redirect;
			use Request;
			use DB;

			class ReportsController extends Controller {

			/**
			 * Display a listing of the resource.
			 *
			 * @return Response
			 */
			public function __construct()
			{
				$this->middleware('adminauth');
			}

			// public function index()
			// { 
			// 	if(is_axios()==true)
			// 	{
			// 		//filter params 
			// 		$inputs = Request::all();
			// 		$inputs = array_filter($inputs);
			// 	$contest_id = @$inputs['contest_id'];
			// 	$user_id = @$inputs['user_id'];
			// 	$earned_score = @$inputs['earned_score'];
			// 	$contest_participant_history = Contestparticipanthistory::where(array())
			// 	->when($contest_id, function ($query) use ($contest_id) {
			// 	        	return $query->where('contest_id','=' ,$contest_id);})
			// 	->when($user_id, function ($query) use ($user_id) {
			// 	        	return $query->where('user_id','=' ,$user_id);})
			// 	->when($earned_score, function ($query) use ($earned_score) {
			// 	        	return $query->where('earned_score','=' ,$earned_score);})
			// 	->paginate(20);
			//       $contest_participant_history->setPath('contestparticipanthistory');
			// 		$contest_participant_history->appends(Request::except('page'));
			// 		return view('cms::contestparticipanthistory.index_view')->with('contest_participant_history', $contest_participant_history);

			// 	}
			// 	return view('cms::contestparticipanthistory.index');
			// }

			public function loginUsers()
			{ 
				if(is_axios()==true)
				{
					//filter params 
				$inputs = Request::all();
				$inputs = array_filter($inputs);
				$date_from = (@$inputs['date_from'])? @$inputs['date_from'] : Carbon::now()->subDays(30);
				$date_to = (@$inputs['date_to'])? @$inputs['date_to'] : Carbon::now();

				$data = User::select(DB::raw("count(DISTINCT users.id) as total_login"), DB::raw('DATE(users.updated_at) as date'))
				->where(array())
				->when($date_from, function ($query) use ($date_from) {
				        	return $query->where('users.updated_at','>=' ,$date_from.'%');})
				->when($date_to, function ($query) use ($date_to) {
				        	return $query->where('users.updated_at','<=' ,$date_to.'%');})
				->where('users.status','Active')
				->groupBy('date')
				->orderBy('date','desc')
				->paginate(20);
				
			      $data->setPath('loginusers');
					$data->appends(Request::except('page'));
					return view('cms::reports.loginusers')->with(['data' => $data]);

				}
				return view('cms::reports.index');
			}

			public function newLoginUsers()
			{ 
				if(is_axios()==true)
				{
					//filter params 
				$inputs = Request::all();
				$inputs = array_filter($inputs);
				$date_from = (@$inputs['date_from'])? @$inputs['date_from'] : Carbon::now()->subDays(30);
				$date_to = (@$inputs['date_to'])? @$inputs['date_to'] : Carbon::now();

				$data = User::select(DB::raw("count(DISTINCT users.id) as total_login"), DB::raw('DATE(users.created_at) as date'))
				->where(array())
				->when($date_from, function ($query) use ($date_from) {
				        	return $query->where('users.created_at','>=' ,$date_from.'%');})
				->when($date_to, function ($query) use ($date_to) {
				        	return $query->where('users.created_at','<=' ,$date_to.'%');})
				->where('users.status','Active')
				->groupBy('date')
				->orderBy('date','desc')
				->paginate(20);
				
			      $data->setPath('newloginusers');
					$data->appends(Request::except('page'));
					return view('cms::reports.newloginusers')->with(['data' => $data]);

				}
				return view('cms::reports.index');
			}

			public function gooleLogins()
			{ 
				if(is_axios()==true)
				{
					//filter params 
				$inputs = Request::all();
				$inputs = array_filter($inputs);
				$date_from = (@$inputs['date_from'])? @$inputs['date_from'] : Carbon::now()->subDays(30);
				$date_to = (@$inputs['date_to'])? @$inputs['date_to'] : Carbon::now();

				$data = User::select(DB::raw("count(DISTINCT users.id) as total_login"), DB::raw('DATE(users.updated_at) as date'))
				->where(array())
				->when($date_from, function ($query) use ($date_from) {
				        	return $query->where('users.updated_at','>=' ,$date_from.'%');})
				->when($date_to, function ($query) use ($date_to) {
				        	return $query->where('users.updated_at','<=' ,$date_to.'%');})
				->where('users.status','Active')
				->where('users.source','google')
				->groupBy('date')
				->orderBy('date','desc')
				->paginate(20);
				
			      $data->setPath('goolelogins');
					$data->appends(Request::except('page'));
					return view('cms::reports.goolelogins')->with(['data' => $data]);

				}
				return view('cms::reports.index');
			}

			public function facebookLogins()
			{ 
				if(is_axios()==true)
				{
					//filter params 
				$inputs = Request::all();
				$inputs = array_filter($inputs);
				$date_from = (@$inputs['date_from'])? @$inputs['date_from'] : Carbon::now()->subDays(30);
				$date_to = (@$inputs['date_to'])? @$inputs['date_to'] : Carbon::now();

				$data = User::select(DB::raw("count(DISTINCT users.id) as total_login"), DB::raw('DATE(users.updated_at) as date'))
				->where(array())
				->when($date_from, function ($query) use ($date_from) {
				        	return $query->where('users.updated_at','>=' ,$date_from.'%');})
				->when($date_to, function ($query) use ($date_to) {
				        	return $query->where('users.updated_at','<=' ,$date_to.'%');})
				->where('users.status','Active')
				->where('users.source','facebook')
				->groupBy('date')
				->orderBy('date','desc')
				->paginate(20);
				
			      $data->setPath('facebooklogins');
					$data->appends(Request::except('page'));
					return view('cms::reports.facebooklogins')->with(['data' => $data]);

				}
				return view('cms::reports.index');
			}

			public function quizUniqueUser()
			{ 
				if(is_axios()==true)
				{
					//filter params 
				$inputs = Request::all();
				$inputs = array_filter($inputs);
				$date_from = (@$inputs['date_from'])? @$inputs['date_from'] : Carbon::now()->subDays(30);
				$date_to = (@$inputs['date_to'])? @$inputs['date_to'] : Carbon::now();

				$data = Contestparticipanthistory::select(DB::raw("count(DISTINCT contest_participant_history.user_id) as total_user"), DB::raw('DATE(contest_participant_history.created_at) as date'))
				->where(array())
				->leftJoin('contests', function($leftJoin)
                        {
                            $leftJoin->on('contests.id', '=', 'contest_participant_history.contest_id');
                               
                        })
				->leftJoin('categories', function($leftJoin)
                        {
                            $leftJoin->on('categories.id', '=', 'contests.category_id');
                               
                        })
				->when($date_from, function ($query) use ($date_from) {
				        	return $query->where('contest_participant_history.created_at','>=' ,$date_from.'%');})
				->when($date_to, function ($query) use ($date_to) {
				        	return $query->where('contest_participant_history.created_at','<=' ,$date_to.'%');})
				->where('categories.parent_id', 22)
				->groupBy('date')
				->orderBy('date','desc')
				->paginate(20);
				
			      $data->setPath('quizuniqueuser');
					$data->appends(Request::except('page'));
					return view('cms::reports.quizuniqueuser')->with(['data' => $data]);

				}
				return view('cms::reports.index');
			}

			public function quizContestPlayed()
			{ 
				if(is_axios()==true)
				{
					//filter params 
				$inputs = Request::all();
				$inputs = array_filter($inputs);
				$date_from = (@$inputs['date_from'])? @$inputs['date_from'] : Carbon::now()->subDays(30);
				$date_to = (@$inputs['date_to'])? @$inputs['date_to'] : Carbon::now();

				$data = Contestparticipanthistory::select(DB::raw("count( contest_participant_history.contest_id) as total_contest"), DB::raw('DATE(contest_participant_history.created_at) as date'))
				->where(array())
				->leftJoin('contests', function($leftJoin)
                        {
                            $leftJoin->on('contests.id', '=', 'contest_participant_history.contest_id');
                               
                        })
				->leftJoin('categories', function($leftJoin)
                        {
                            $leftJoin->on('categories.id', '=', 'contests.category_id');
                               
                        })
				->when($date_from, function ($query) use ($date_from) {
				        	return $query->where('contest_participant_history.created_at','>=' ,$date_from.'%');})
				->when($date_to, function ($query) use ($date_to) {
				        	return $query->where('contest_participant_history.created_at','<=' ,$date_to.'%');})
				->where('categories.parent_id', 22)
				->groupBy('date')
				->orderBy('date','desc')
				->paginate(20);
				
			      $data->setPath('quizcontestplayed');
					$data->appends(Request::except('page'));
					return view('cms::reports.quizcontestplayed')->with(['data' => $data]);

				}
				return view('cms::reports.index');
			}

			public function crickContestPlayed()
			{ 
				if(is_axios()==true)
				{
					//filter params 
				$inputs = Request::all();
				$inputs = array_filter($inputs);
				$date_from = (@$inputs['date_from'])? @$inputs['date_from'] : Carbon::now()->subDays(30);
				$date_to = (@$inputs['date_to'])? @$inputs['date_to'] : Carbon::now();

				$data = Contestparticipanthistory::select(DB::raw("count( contest_participant_history.contest_id) as total_contest"), DB::raw('DATE(contest_participant_history.created_at) as date'))
				->where(array())
				->leftJoin('contests', function($leftJoin)
                        {
                            $leftJoin->on('contests.id', '=', 'contest_participant_history.contest_id');
                               
                        })
				->leftJoin('categories', function($leftJoin)
                        {
                            $leftJoin->on('categories.id', '=', 'contests.category_id');
                               
                        })
				->when($date_from, function ($query) use ($date_from) {
				        	return $query->where('contest_participant_history.created_at','>=' ,$date_from.'%');})
				->when($date_to, function ($query) use ($date_to) {
				        	return $query->where('contest_participant_history.created_at','<=' ,$date_to.'%');})
				->where('categories.parent_id', 23)
				->groupBy('date')
				->orderBy('date','desc')
				->paginate(20);
				
			      $data->setPath('crickcontestplayed');
					$data->appends(Request::except('page'));
					return view('cms::reports.crickcontestplayed')->with(['data' => $data]);

				}
				return view('cms::reports.index');
			}

			public function crickuniqueuser()
			{ 
				if(is_axios()==true)
				{
					//filter params 
				$inputs = Request::all();
				$inputs = array_filter($inputs);
				$date_from = (@$inputs['date_from'])? @$inputs['date_from'] : Carbon::now()->subDays(30);
				$date_to = (@$inputs['date_to'])? @$inputs['date_to'] : Carbon::now();

				$data = Contestparticipanthistory::select(DB::raw("count(DISTINCT contest_participant_history.user_id) as total_user"), DB::raw('DATE(contest_participant_history.created_at) as date'))
				->where(array())
				->leftJoin('contests', function($leftJoin)
                        {
                            $leftJoin->on('contests.id', '=', 'contest_participant_history.contest_id');
                               
                        })
				->leftJoin('categories', function($leftJoin)
                        {
                            $leftJoin->on('categories.id', '=', 'contests.category_id');
                               
                        })
				->when($date_from, function ($query) use ($date_from) {
				        	return $query->where('contest_participant_history.created_at','>=' ,$date_from.'%');})
				->when($date_to, function ($query) use ($date_to) {
				        	return $query->where('contest_participant_history.created_at','<=' ,$date_to.'%');})
				->where('categories.parent_id', 23)
				->groupBy('date')
				->orderBy('date','desc')
				->paginate(20);
				
			      $data->setPath('crickuniqueuser');
					$data->appends(Request::except('page'));
					return view('cms::reports.crickuniqueuser')->with('data', $data);

				}
				return view('cms::reports.index');
			}

		}
		