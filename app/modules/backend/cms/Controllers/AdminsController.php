<?php namespace App\modules\backend\cms\Controllers;

		    use App\Http\Requests;
			use App\Http\Controllers\Controller;
			use App\Http\Models\Admin;
			use Validator;
			use Redirect;
			use Request;

			class AdminsController extends Controller {

			/**
			 * Display a listing of the resource.
			 *
			 * @return Response
			 */
			public function __construct()
			{
				$this->middleware('adminauth');
			}

			public function index()
			{ 
				if(is_axios()==true)
				{
					//filter params 
					$inputs = Request::all();
					$inputs = array_filter($inputs);
				$name = @$inputs['name'];
				$email = @$inputs['email'];
				$password = @$inputs['password'];
				$usergroup_id = @$inputs['usergroup_id'];
				$remember_token = @$inputs['remember_token'];
				$admins = Admin::where(array())
				->when($name, function ($query) use ($name) {
				        	return $query->where('name','LIKE' ,'%'.$name.'%');})
				->when($email, function ($query) use ($email) {
				        	return $query->where('email','LIKE' ,$email.'%');})
				->when($password, function ($query) use ($password) {
				        	return $query->where('password','LIKE' ,$password.'%');})
				->when($usergroup_id, function ($query) use ($usergroup_id) {
				        	return $query->where('usergroup_id','=' ,$usergroup_id);})
				->when($remember_token, function ($query) use ($remember_token) {
				        	return $query->where('remember_token','LIKE' ,$remember_token.'%');})
				->paginate(20);
			      $admins->setPath('admins');
					$admins->appends(Request::except('page'));
					return view('cms::admins.index_view')->with('admins', $admins);

				}
				return view('cms::admins.index');
			}

			/**
			 * Show the form for creating a new resource.
			 *
			 * @return Response
			 */
			public function create()
			{
				//
				return view('cms::admins.create');
			}

			/**
			 * Store a newly created resource in storage.
			 *
			 * @return Response
			 */
			public function store()
			{
				//
				// validate
		        // read more on validation at http://laravel.com/docs/validation
		        	$status = 200;
			        $response = array();
			        $message = '';

		        
				$rules = array('name'=>'required',
			        'email'=>'required',
			        'password'=>'required',
			        'usergroup_id'=>'required',
			        'remember_token'=>'required',
			        );
		        
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		        	$status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		            
		        } else {
		            // store
		            $admins = new Admin;
		            $admins->name = Request::get('name');
				        		$admins->email = Request::get('email');
				        		$admins->password = Request::get('password');
				        		$admins->usergroup_id = Request::get('usergroup_id');
				        		$admins->remember_token = Request::get('remember_token');
				        		$admins->save();

		           //trigger seo url
					triggerSeoUrls($admins->id,'admin',Request::get('main_seo_title'));
						$message = 'Successfully created Admins!';

		            
		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Display the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function show($id)
			{
				//
				if($id=='quickupdate')
				{
					return $this->quickUpdate();
				}
				 $admins = Admin::find($id);

				 if(empty($admins))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;
				 	
				 }

		        // show the view and pass the admins to it
		        return view('cms::admins.show')->with('admins', $admins);
			}


			public function quickUpdate()
			{
				$inputs = Request::all();
				if(isset($inputs['id']) && isset($inputs['action']))
				{
					if($inputs['action']==1){$action='ACTIVE';}
					else if($inputs['action']==2){$action='INACTIVE';}
					else if($inputs['action']==3){$action='DELETED';}
					if($action!='')
					{Admin::whereIn('id',explode(',', $inputs['id']))->update(['status'=>$action]);}
				}
			}

			/**
			 * Show the form for editing the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function edit($id)
			{
				//
				$admins = Admin::find($id);
				if(empty($admins))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;

				 }

		        // show the view and pass the admins to it
		        return view('cms::admins.edit')->with('admins', $admins);
			}

			/**
			 * Update the specified resource in storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function update($id)
			{
				// read more on validation at http://laravel.com/docs/validation
		       		$status = 200;
			        $response = array();
			        $message = '';
		       $rules = array();
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		            $status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		        } else {
		            // store
		             $admins = Admin::find($id);$admins->name = Request::get('name');
				        		$admins->email = Request::get('email');
				        		$admins->password = Request::get('password');
				        		$admins->usergroup_id = Request::get('usergroup_id');
				        		$admins->remember_token = Request::get('remember_token');
				        		$admins->save();

		           //trigger seo url
					triggerSeoUrls($admins->id,'admin',Request::get('main_seo_title'));
						$message = 'Successfully updated Admins!';
		           
		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Remove the specified resource from storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function destroy($id)
			{
				//
					$status = 200;
			        $response = array();
			        $message = '';
				$admins = Admin::find($id);
				if(empty($admins))
				 { 
				 	$status=421; 
				 	$response['errors']=array('No department found!');
				 	$message='No department found!';
				 }
				 else
				 {
				 	 $admins->delete();
				 	 $message = 'Successfully deleted Admins!';

				 }
				 return $this->response($response,$status,$message);
			 
			}

		}
		