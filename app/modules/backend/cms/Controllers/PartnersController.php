<?php namespace App\modules\backend\cms\Controllers;

		    use App\Http\Requests;
			use App\Http\Controllers\Controller;
			use App\Http\Models\Partner;
			use Validator;
			use Redirect;
			use Request;

			class PartnersController extends Controller {

			/**
			 * Display a listing of the resource.
			 *
			 * @return Response
			 */
			public function __construct()
			{
				$this->middleware('adminauth');
			}

			public function index()
			{ 
				if(is_axios()==true)
				{
					//filter params 
					$inputs = Request::all();
					$inputs = array_filter($inputs);
				$partner_name = @$inputs['partner_name'];
				$logo = @$inputs['logo'];
				$allocated_domain = @$inputs['allocated_domain'];
				$status = @$inputs['status'];
				$adsStatus = @$inputs['ads_enabled'];
				$defaultLanguage = @$inputs['default_language'];
				if(empty($inputs)){
					$status='ACTIVE';
				}
				$partners = Partner::where(array())
				->when($partner_name, function ($query) use ($partner_name) {
				        	return $query->where('partner_name','LIKE' ,'%'.$partner_name.'%');})
				->when($logo, function ($query) use ($logo) {
				        	return $query->where('logo','LIKE' ,$logo.'%');})
				->when($allocated_domain, function ($query) use ($allocated_domain) {
				        	return $query->where('allocated_domain','LIKE' ,$allocated_domain.'%');})
				->when($status, function ($query) use ($status) {
				        	return $query->where('status','LIKE' ,$status.'%');})
				->when($adsStatus, function ($query) use ($adsStatus) {
				        	return $query->where('ads_enableds', $adsStatus);})
				->when($defaultLanguage, function ($query) use ($defaultLanguage) {
				        	return $query->where('default_language',$defaultLanguage);})
				->paginate(20);
			      $partners->setPath('partners');
					$partners->appends(Request::except('page'));
					return view('cms::partners.index_view')->with('partners', $partners);

				}
				return view('cms::partners.index');
			}

			/**
			 * Show the form for creating a new resource.
			 *
			 * @return Response
			 */
			public function create()
			{
				//
				return view('cms::partners.create');
			}

			/**
			 * Store a newly created resource in storage.
			 *
			 * @return Response
			 */
			public function store()
			{
				$status = 200;
				$response = array();
				$message = '';
		        
				$rules = array('partner_name'=>'required',
			        'logo'=>'max:100|mimes:jpeg,jpg,png,gif',
			        'allocated_domain'=>'required|unique:partners,allocated_domain',
			        'status'=>'required',
					'max_click_day'=>'integer',
					//'blocked_hours_per_day'=>'integer',
			        );

				$inputs = Request::all();

				if(@$inputs['blocked_hours_per_day'] != '') {
						$rules['blocked_hours_per_day']='integer';
					}
		        
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		        	$status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		            
		        } else {
		            $partners = new Partner;
		            $partners->partner_name = $inputs['partner_name'];
				        		
					$import = \Request::file('logo');
					if(isset($import) and !empty($import))
					{
						$data = uploadImage($import,'partners');
						$partners->logo = $data;
					}
					$imageImport = \Request::file('image');
					if(isset($imageImport) and !empty($imageImport))
					{
						$imageData = uploadImage($imageImport,'partners');
						$partners->image = $imageData;
					}
					$partners->allocated_domain = $inputs['allocated_domain'];
					$partners->view_id = $inputs['view_id'];
					$partners->ga_ids = $inputs['ga_id'];
					$partners->ga_measurement_id = $inputs['ga_measurement_id'];
					$partners->scripts_1 = $inputs['scripts_1'];
					$partners->scripts_2 = $inputs['scripts_2'];
					$partners->status = $inputs['status'];
					$partners->ads_enabled = (int)@$inputs['ads_enabled'];
					$partners->ip_blocked = $inputs['ip_blocked'];
					$partners->default_language = @$inputs['default_language'];

					if(isset($inputs['supported_languages']) && !empty($inputs['supported_languages'])){
						$supported_language = implode(',', $inputs['supported_languages']);
						$partners->supported_language = $supported_language;
					}

					$partners->webhook = @$inputs['webhook'];
					$partners->webhook_api_key = @$inputs['webhook_api_key'];
					$partners->webhook_api_method = @$inputs['webhook_api_method'];
					$partners->is_encrypted_apikey = @$inputs['is_encrypted_apikey'];
					$partners->webhook_callback_attempts = @$inputs['webhook_callback_attempts'];
					$partners->payloads_key = @$inputs['payloads_key'];
					$partners->inapp_allowed = (!empty(@$inputs['inapp_allowed'])) ? @$inputs['inapp_allowed'] : 1;
					$partners->save();

					if (isset($partners) && !empty($partners)) {
						$partners->details()->create([
							'max_click_per_day'	=>@$inputs['max_click_per_day'],
							'blocked_hours_per_day'	=>@$inputs['blocked_hours_per_day'],
							'ads_tracker_enable'	=>@$inputs['ads_tracker_enable'],
							'mail_to'	=>@$inputs['mail_to'],
							'mail_cc'	=>@$inputs['mail_cc'],
							'mail_bcc'	=>@$inputs['mail_bcc'],
						]);

						$partners->packages()->create([
							'package_id'	=>@$inputs['package_id'],
						]);
					}

		           //trigger seo url
					//triggerSeoUrls($partners->id,'partner',$inputs['main_seo_title']);
						$message = 'Successfully created Partners!';
						$response['callback'] = url('/'.getCurrentUrlPrefix());
		            
		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Display the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function show($id)
			{
				//
				if($id=='quickupdate')
				{
					return $this->quickUpdate();
				}
				 $partners = Partner::find($id);

				 if(empty($partners))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;
				 	
				 }

		        // show the view and pass the partners to it
		        return view('cms::partners.show')->with(['partners'=> $partners, 'partner_details'=>$partners->details]);
			}

			public function quickUpdate()
			{
				$inputs = Request::all();
				if(isset($inputs['id']) && isset($inputs['action']))
				{
					if($inputs['action']==1){$action='ACTIVE';}
					else if($inputs['action']==2){$action='INACTIVE';}
					else if($inputs['action']==3){$action='DELETED';}
					if($action!='')
					{Partner::whereIn('id',explode(',', $inputs['id']))->update(['status'=>$action]);}
				}
			}

			/**
			 * Show the form for editing the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function edit($id)
			{
				//
				$partners = Partner::find($id);
				if(empty($partners))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;

				 }

		        // show the view and pass the partners to it
		        return view('cms::partners.edit')->with(['partners'=> $partners,'partner_details'=>$partners->details,'partner_packages'=>$partners->packages]);
			}

			/**
			 * Update the specified resource in storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function update($id)
			{

				// read more on validation at http://laravel.com/docs/validation
		       		$status = 200;
			        $response = array();
			        $message = '';
					$inputs = Request::all();
				$rules = array(
					'allocated_domain'=>'unique:partners,allocated_domain,'.$id,
					'partner_name'=>'required',
					'status'=>'required',
					'max_click_day'=>'integer',
					'logo'=>'max:100|mimes:jpeg,jpg,png,gif',
					//'blocked_hours_per_day'=>'integer',
				);

				if (@$inputs['blocked_hours_per_day'] != '') {
						$rules['blocked_hours_per_day']='integer';
					}

		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		            $status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		        } else {
					$partners = Partner::find($id);
					$partners->partner_name = $inputs['partner_name'];
				        		
					$import = \Request::file('logo');
					if(isset($import) and !empty($import))
					{
						$data = uploadImage($import,'partners');
						$partners->logo = $data;
					}
					$imageImport = \Request::file('image');
					if(isset($imageImport) and !empty($imageImport))
					{
						$imageData = uploadImage($imageImport,'partners');
						$partners->image = $imageData;
					}
					$partners->allocated_domain = $inputs['allocated_domain'];
					$partners->view_id = $inputs['view_id'];
					$partners->ga_ids = $inputs['ga_id'];
					$partners->ga_measurement_id = $inputs['ga_measurement_id'];
					$partners->scripts_1 = $inputs['scripts_1'];
					$partners->scripts_2 = $inputs['scripts_2'];					
					$partners->status = $inputs['status'];
					$partners->ads_enabled = (int)$inputs['ads_enabled'];
					$partners->ip_blocked  = $inputs['ip_blocked'];
					$partners->default_language = @$inputs['default_language'];
					if(isset($inputs['supported_languages']) && !empty($inputs['supported_languages'])){
						$supported_language = implode(',', $inputs['supported_languages']);
						$partners->supported_language = $supported_language;
					}

					$partners->webhook = @$inputs['webhook'];
					$partners->webhook_api_key = @$inputs['webhook_api_key'];
					$partners->webhook_api_method = @$inputs['webhook_api_method'];
					$partners->is_encrypted_apikey = @$inputs['is_encrypted_apikey'];
					$partners->webhook_callback_attempts = @$inputs['webhook_callback_attempts'];
					$partners->payloads_key = @$inputs['payloads_key'];
					$partners->inapp_allowed = @$inputs['inapp_allowed'];
					$partners->save();

					if (isset($partners) && !empty($partners)) {
						$partners->details()->updateOrCreate(['partner_id'=>$partners->id],[
							'max_click_per_day'	=>@$inputs['max_click_per_day'],
							'blocked_hours_per_day'	=>@$inputs['blocked_hours_per_day'],
							'ads_tracker_enable'	=>@$inputs['ads_tracker_enable'],
							'mail_to'	=>$inputs['mail_to'],
							'mail_cc'	=>$inputs['mail_cc'],
							'mail_bcc'	=>$inputs['mail_bcc'],
						]);

						$partners->packages()->updateOrCreate(['partner_id'=>$partners->id],[
							'package_id'	=>@$inputs['package_id'],
						]);
					}

		           //trigger seo url
					//triggerSeoUrls($partners->id,'partner',$inputs['main_seo_title']);
						$message = 'Successfully updated Partners!';
		           		$response['callback'] = url('/'.getCurrentUrlPrefix());
		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Remove the specified resource from storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function destroy($id)
			{
				//
					$status = 200;
			        $response = array();
			        $message = '';
				$partners = Partner::find($id);
				if(empty($partners))
				 { 
				 	$status=421; 
				 	$response['errors']=array('No Partners found!');
				 	$message='No Partners found!';
				 }
				 else
				 {
				 	 $partners->status = 'DELETED';
				 	 $partners->save();
				 	 $message = 'Successfully deleted Partners!';

				 }
				 return $this->response($response,$status,$message);
			 
			}

		}
		