<?php namespace App\modules\backend\cms\Controllers;

		    use App\Http\Requests;
			use App\Http\Controllers\Controller;
			use App\Http\Models\Page;
			use Validator;
			use Redirect;
			use Request;

			class PagesController extends Controller {

			/**
			 * Display a listing of the resource.
			 *
			 * @return Response
			 */
			public function __construct()
			{
				$this->middleware('adminauth');
			}

			public function index()
			{ 
				if(is_axios()==true)
				{
					//filter params 
					$inputs = Request::all();
					$inputs = array_filter($inputs);
				$page_name = @$inputs['page_name'];
				$page_code = @$inputs['page_code'];
				$image = @$inputs['image'];
				$status = @$inputs['status'];
				if(empty($inputs)){
					$status='ACTIVE';
				}
				$pages = Page::where(array())
				->when($page_name, function ($query) use ($page_name) {
				        	return $query->where('page_name','LIKE' ,'%'.$page_name.'%');})
				->when($page_code, function ($query) use ($page_code) {
				        	return $query->where('page_code','LIKE' ,$page_code.'%');})
				->when($image, function ($query) use ($image) {
				        	return $query->where('image','LIKE' ,$image.'%');})
				->when($status, function ($query) use ($status) {
				        	return $query->where('status','LIKE' ,$status.'%');})
				->paginate(20);
			      $pages->setPath('pages');
					$pages->appends(Request::except('page'));
					return view('cms::pages.index_view')->with('pages', $pages);

				}
				return view('cms::pages.index');
			}

			/**
			 * Show the form for creating a new resource.
			 *
			 * @return Response
			 */
			public function create()
			{
				//
				return view('cms::pages.create');
			}

			/**
			 * Store a newly created resource in storage.
			 *
			 * @return Response
			 */
			public function store()
			{
				//
				// validate
		        // read more on validation at http://laravel.com/docs/validation
		        	$status = 200;
			        $response = array();
			        $message = '';

		        
				$rules = array('page_name'=>'required',
			        'image'=>'required|max:500|mimes:jpeg,jpg,png,gif',
			        'page_code'=>'required|unique:pages,page_code|alpha_dash',
			        'status'=>'required',
			        );

				$inputs = Request::all();
				$inputs = array_filter($inputs);

		        
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		        	$status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		            
		        } else {
		            // store
		            $pages = new Page;
		            $pages->page_name = $inputs['page_name'];
				        		$pages->page_code = $inputs['page_code'];
				        		
				        		$import = \Request::file('image');
									if(isset($import) and !empty($import))
						            {
						            	$data = uploadImage($import,'pages');
						            	$pages->image = $data;
									}
									$pages->status = $inputs['status'];
				        		$pages->save();

		           	$message = 'Successfully created Pages!';
		           	$response['callback'] = url('/'.getCurrentUrlPrefix());
		            
		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Display the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function show($id)
			{
				//
				if($id=='quickupdate')
				{
					return $this->quickUpdate();
				}
				 $pages = Page::find($id);

				 if(empty($pages))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;
				 	
				 }

		        // show the view and pass the pages to it
		        return view('cms::pages.show')->with('pages', $pages);
			}

			public function quickUpdate()
			{
				$inputs = Request::all();
				if(isset($inputs['id']) && isset($inputs['action']))
				{
					if($inputs['action']==1){$action='ACTIVE';}
					else if($inputs['action']==2){$action='INACTIVE';}
					else if($inputs['action']==3){$action='DELETED';}
					if($action!='')
					{Page::whereIn('id',explode(',', $inputs['id']))->update(['status'=>$action]);}
				}
			}

			/**
			 * Show the form for editing the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function edit($id)
			{
				//
				$pages = Page::find($id);
				if(empty($pages))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;

				 }

		        // show the view and pass the pages to it
		        return view('cms::pages.edit')->with('pages', $pages);
			}

			/**
			 * Update the specified resource in storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function update($id)
			{
				// read more on validation at http://laravel.com/docs/validation
		       		$status = 200;
			        $response = array();
			        $message = '';
					$rules = array('page_code'=>'alpha_dash|unique:pages,page_code,'.$id,
					'image'=>'max:500|mimes:jpeg,jpg,png,gif');
				$inputs = Request::all();
				$inputs = array_filter($inputs);

		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		            $status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		        } else {
		            // store
		             $pages = Page::find($id);$pages->page_name = $inputs['page_name'];
				        		$pages->page_code = $inputs['page_code'];
				        		
				        		$import = \Request::file('image');
									if(isset($import) and !empty($import))
						            {
						            	$data = uploadImage($import,'pages');
						            	$pages->image = $data;
									}
									$pages->status = $inputs['status'];
				        		$pages->save();

		           	$message = 'Successfully updated Pages!';
		           	$response['callback'] = url('/'.getCurrentUrlPrefix());
		           
		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Remove the specified resource from storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function destroy($id)
			{
				//
					$status = 200;
			        $response = array();
			        $message = '';
				$pages = Page::find($id);
				if(empty($pages))
				 { 
				 	$status=421; 
				 	$response['errors']=array('No Pages found!');
				 	$message='No Pages found!';
				 }
				 else
				 {
				 	 $pages->status = 'DELETED';
				 	 $pages->save();
				 	 $message = 'Successfully deleted Pages!';

				 }
				 return $this->response($response,$status,$message);
			 
			}

		}
		