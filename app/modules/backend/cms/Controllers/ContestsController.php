<?php namespace App\modules\backend\cms\Controllers;

		    use App\Http\Requests;
			use App\Http\Controllers\Controller;
			use App\Http\Models\Contest;
			use App\Http\Models\Contestawardmatrix;
			use App\Http\Models\Category;
			use Validator;
			use Redirect;
			use Request;
			use Carbon\Carbon;

			class ContestsController extends Controller {

			/**
			 * Display a listing of the resource.
			 *
			 * @return Response
			 */
			public function __construct()
			{
				$this->middleware('adminauth');
			}

			public function index()
			{ 
				if(is_axios()==true)
				{
					//filter params 
					$inputs = Request::all();
					$inputs = array_filter($inputs);
				$parent_category_id = @$inputs['parent_category_id'];
				$category_id = @$inputs['category_id'];
				$contest_title = @$inputs['contest_title'];
				// $contest_image = @$inputs['contest_image'];
				$entry_fee_type = @$inputs['entry_fee_type'];
				$entry_fee = @$inputs['entry_fee'];
				$contest_block_period = @$inputs['contest_block_period'];
				$prize_type = @$inputs['prize_type'];
				$prize_money = @$inputs['prize_money'];
				//$contest_position = @$inputs['contest_position'];
				//$status = @$inputs['status'] ? @$inputs['status'] : 'ACTIVE';
				$status = @$inputs['status'];
				$total_users_played = @$inputs['total_users_played'];
				$contests = Contest::where(array())
				->when($parent_category_id, function ($query) use ($parent_category_id) {
					$subCategories = Category::select('id')->where('parent_id',$parent_category_id)->get()->toArray();
				    return $query->whereIn('category_id', $subCategories);
				  })
				->when($category_id, function ($query) use ($category_id) {
				        	return $query->where('category_id','=' ,$category_id);})
				->when($contest_title, function ($query) use ($contest_title) {
				        	return $query->where('contest_title','LIKE' ,$contest_title.'%');})
				// ->when($contest_image, function ($query) use ($contest_image) {
				//         	return $query->where('contest_image','LIKE' ,$contest_image.'%');})
				->when($entry_fee_type, function ($query) use ($entry_fee_type) {
				        	return $query->where('entry_fee_type','LIKE' ,$entry_fee_type.'%');})
				->when($entry_fee, function ($query) use ($entry_fee) {
				        	return $query->where('entry_fee','=' ,$entry_fee);})
				->when($contest_block_period, function ($query) use ($contest_block_period) {
				        	return $query->where('contest_block_period','=' ,$contest_block_period);})
				->when($prize_type, function ($query) use ($prize_type) {
				        	return $query->where('prize_type','LIKE' ,$prize_type.'%');})
				->when($prize_money, function ($query) use ($prize_money) {
				        	return $query->where('prize_money','=' ,$prize_money);})
				//->when($contest_position, function ($query) use ($contest_position) {
				  //      	return $query->where('contest_position','=' ,$contest_position);})
				->when($status, function ($query) use ($status) {
				        	return $query->where('status','LIKE' ,$status.'%');})
				->when($total_users_played, function ($query) use ($total_users_played) {
				        	return $query->where('total_users_played','=' ,$total_users_played);})
				->whereDate('created_at','>=' ,Carbon::now()->subDays(30))
				->orderBy('id', 'desc')
				->paginate(50);
			      $contests->setPath('contests');
					$contests->appends(Request::except('page'));

					$categoryList = getCategories();
					return view('cms::contests.index_view')->with(['contests'=> $contests, 'categoryList'=>$categoryList]);

				}
				return view('cms::contests.index');
			}

			/**
			 * Show the form for creating a new resource.
			 *
			 * @return Response
			 */
			public function create()
			{
				//
				return view('cms::contests.create');
			}

			/**
			 * Store a newly created resource in storage.
			 *
			 * @return Response
			 */
			public function store()
			{
				//
				// validate
		        // read more on validation at http://laravel.com/docs/validation
		        	$status = 200;
			        $response = array();
			        $message = '';

		        
				$rules = array('category_id'=>'required',
			        'contest_title'=>'required',
			        //'contest_image'=>'required|mimes:jpg,jpeg,png,webp',
			        'entry_fee_type'=>'required',
			        'entry_fee'=>'required|numeric',
			        'contest_block_period'=>'required',
			        'prize_type'=>'required',
			        'prize_money'=>'required|numeric',
			        //'contest_position'=>'required',
			        'status'=>'required',
			        //'total_users_played'=>'required',
			        'start_time'=>'required',
			        'end_time'=>'required|after:start_time',
			        );

				$inputs = Request::all();
				$inputs = array_filter($inputs);

				if (@$inputs['entry_fee_type'] == 'COIN') {
						$rules['entry_fee']='numeric|gt:0';
					}
		        
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		        	$status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		            
		        } else {
		            // store
		            $contests = new Contest;
		            $contests->category_id = $inputs['category_id'];
				        		$contests->contest_title = $inputs['contest_title'];
				        		//$contests->contest_image = $inputs['contest_image'];
				    //     		$import = \Request::file('contest_image');
								// if(isset($import) and !empty($import))
					   //          {
					   //          	$data = uploadImage($import,'contests');
					   //          	$contests->contest_image = $data;
								// }
				        		$contests->entry_fee_type = $inputs['entry_fee_type'];
				        		$contests->entry_fee = @$inputs['entry_fee'];
				        		$contests->contest_block_period = $inputs['contest_block_period'];
				        		$contests->prize_type = $inputs['prize_type'];
				        		$contests->prize_money = $inputs['prize_money'];
				        		//$contests->contest_position = $inputs['contest_position'];
								$contests->start_time = $inputs['start_time'];
	        					$contests->end_time = $inputs['end_time'];		        		
				        		$contests->status = $inputs['status'];
				        		if(isset($inputs['tags']) && !empty($inputs['tags'])){
									$contests->tags = implode(',', array_filter($inputs['tags']));
								}
				        		//$contests->total_users_played = $inputs['total_users_played'];
				        		$contests->save();

		           //trigger seo url
					//triggerSeoUrls($contests->id,'contest',$inputs['main_seo_title']);
						$message = 'Successfully created Contests!';
						$response['callback'] = url('/'.getCurrentUrlPrefix());

		            
		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Display the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function show($id)
			{
				//
				if($id=='quickupdate')
				{
					return $this->quickUpdate();
				}
				
				 $contests = Contest::find($id);

				 if(empty($contests))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;
				 	
				 }

				 $contestMatrix = Contestawardmatrix::where('contest_id', $id)->get();	
				 $categoryList = getCategories();
		        // show the view and pass the contests to it
		        return view('cms::contests.show')->with(['contests'=> $contests, 'matrix'=> $contestMatrix,'categoryList'=>$categoryList]);
			}


			//to update status quickly

			public function quickUpdate()
			{
				$inputs = Request::all();
				if(isset($inputs['id']) && isset($inputs['action']))
				{
					if($inputs['action']==1){$action='ACTIVE';}
					else if($inputs['action']==2){$action='INACTIVE';}
					else if($inputs['action']==3){$action='DELETED';}
					else if($inputs['action']==4){$action='EXPIRED';}
					if($action!='')
					{Contest::whereIn('id',explode(',', $inputs['id']))->update(['status'=>$action]);}
				}
			}

			/**
			 * Show the form for editing the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function edit($id)
			{
				//
				$contests = Contest::find($id);
				if(empty($contests))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;

				 }

		        // show the view and pass the contests to it
		        return view('cms::contests.edit')->with('contests', $contests);
			}

			/**
			 * Update the specified resource in storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function update($id)
			{
				// read more on validation at http://laravel.com/docs/validation
		       		$status = 200;
			        $response = array();
			        $message = '';

			        $inputs = Request::all();
					$inputs = array_filter($inputs);

		      		$rules = [
		      			'category_id'=>'required',
						'entry_fee_type'=>'required',
						'prize_type'=>'required',
						'status'=>'required',
		      			'entry_fee'=>'numeric',
			        	'prize_money'=>'numeric',
						'end_time'=>'after:start_time'
					];	

					if (@$inputs['entry_fee_type'] == 'COIN') {
						$rules['entry_fee']='numeric|gt:0';
					}

				
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		            $status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		        } else {
		            // store
		             $contests = Contest::find($id);$contests->category_id = $inputs['category_id'];
				        		$contests->contest_title = $inputs['contest_title'];
				        		//$contests->contest_image = $inputs['contest_image'];

				     //    		$import = \Request::file('contest_image');
									// if(isset($import) and !empty($import))
						   //          {
						   //          	$data = uploadImage($import,'contests');
						   //          	$contests->contest_image = $data;
									// }
				        		$contests->entry_fee_type = $inputs['entry_fee_type'];
				        		$contests->entry_fee = @$inputs['entry_fee'];
				        		$contests->contest_block_period = $inputs['contest_block_period'];
				        		$contests->prize_type = $inputs['prize_type'];
				        		$contests->prize_money = $inputs['prize_money'];
				        		//$contests->contest_position = $inputs['contest_position'];
				        		$contests->start_time = $inputs['start_time'];
	        					$contests->end_time = $inputs['end_time'];
				        		$contests->status = $inputs['status'];
				        		if(isset($inputs['tags']) && !empty($inputs['tags'])){
									$contests->tags = implode(',', array_filter($inputs['tags']));
								}else{
									$contests->tags = '';
								}
				        		//$contests->total_users_played = $inputs['total_users_played'];
				        		$contests->save();

		           //trigger seo url
					//triggerSeoUrls($contests->id,'contest',$inputs['main_seo_title']);
						$message = 'Successfully updated Contests!';
						//$response['callback'] = url('/'.getCurrentUrlPrefix());
		           
		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Remove the specified resource from storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function destroy($id)
			{
				//
					$status = 200;
			        $response = array();
			        $message = '';
				$contests = Contest::find($id);
				if(empty($contests))
				 { 
				 	$status=421; 
				 	$response['errors']=array('No Contests found!');
				 	$message='No Contests found!';
				 }
				 else
				 {
				 	// $contests->delete();
				 	$contests->status = 'DELETED';
				 	 $contests->save();
				 	 $message = 'Successfully deleted Contests!';

				 }
				 return $this->response($response,$status,$message);
			 
			}

			public function getMatrix($id){
				 
				$contest = Contest::where('id', $id)->select('id','contest_title')->first();
				$contest->matrix = Contestawardmatrix::where('contest_id', $id)->get();			 

				// show the view and pass the contests to it
		        return view('cms::contests.matrix')->with(['contests'=> $contest]);
			}

			public function updateMatrix($id)
			{
				$status = 200;
		        $response = array();
		        $message = '';
		        
		        $matrix = Request::get('matrix');
	            $matrixData = array();
        		if(!empty($matrix)){
        			foreach ($matrix as $key => $value) {
						if(isset($matrix[$key+1])){
							if($value["'winner_to'"] +1 != $matrix[$key+1]["'winner_from'"]) {
								return $this->response(array(),422,'Matrix Data should be in Sequence');
							}
						}
					}

					$contests = Contest::find($id);
					if(!empty($id) && !empty($contests))
					{	
						$now = Carbon::now()->toDateTimeString();
						foreach ($matrix as $key => $value) {
							if(@$value["'winner_from'"] > 0 && @$value["'winner_to'"] > 0 && in_array($value["'prize_type'"], array('COIN','CASH')) && $value["'prize_amount'"] > 0)
							{
								$records[] = array(
									'contest_id'=>$id,
									'winner_from'=>$value["'winner_from'"],
									'winner_to'=>$value["'winner_to'"],
									'prize_type'=>$value["'prize_type'"],
									'prize_amount'=>$value["'prize_amount'"],
									'created_at'=>$now,
									'updated_at'=>$now,
								);
							}
						}
						if(count($records) > 1)
						{
							Contestawardmatrix::where('contest_id',$id)->delete();
							Contestawardmatrix::insert($records);
						}
						
						$message = 'Successfully updated matrix!';
			 	 		return $this->response(array(),200,$message);
						
					}
					else
					{
						$message = 'Invalid Request!';
			 	 		return $this->response(array(),421,$message);
					}

				}
		      
			}


			public function clone($id)
			{
				//
				$contests = Contest::find($id);
				if(empty($contests))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;

				 }

		        // show the view and pass the contests to it
		        return view('cms::contests.clone')->with('contests', $contests);
			}

			public function saveClone()
			{
				//
				// validate
		        // read more on validation at http://laravel.com/docs/validation
		        	$status = 200;
			        $response = array();
			        $message = '';

		        
				$rules = array('category_id'=>'required',
			        'contest_title'=>'required',
			        //'contest_image'=>'image|mimes:jpeg,jpg,png,gif',
			        'entry_fee_type'=>'required',
			        'entry_fee'=>'required|numeric',
			        'contest_block_period'=>'required',
			        'prize_type'=>'required',
			        'prize_money'=>'required|numeric',
			        //'contest_position'=>'required',
			        'status'=>'required',
			        //'total_users_played'=>'required',
			        'start_time'=>'required',
			        'end_time'=>'required|after:start_time',
			        );

				$inputs = Request::all();
				$inputs = array_filter($inputs);

				if (@$inputs['entry_fee_type'] == 'COIN') {
						$rules['entry_fee']='numeric|gt:0';
					}

		        
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		        	$status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		            
		        } else {
		        	// store
		        //	p(Request::all());
		            $contests = new Contest;
		            $contests->category_id = $inputs['category_id'];
	        		$contests->contest_title = $inputs['contest_title'];
	        		/*$import = \Request::file('contest_image');
	        		$prev_contest_id = Request::get('contest_id');
	        		if ($import == null && !empty($prev_contest_id)) {
						$contest_image_loc = Contest::where('id',$prev_contest_id)->pluck('contest_image');
						$data = $contest_image_loc[0] ?? '';
					}
					if(isset($import) and !empty($import))
					{
						$data = uploadImage($import,'contests');
					}
					$contests->contest_image = $data; */
	        		$contests->entry_fee_type = $inputs['entry_fee_type'];
	        		$contests->entry_fee = @$inputs['entry_fee'];
	        		$contests->start_time = $inputs['start_time'];
	        		$contests->end_time = $inputs['end_time'];
	        		$contests->contest_block_period = $inputs['contest_block_period'];
	        		$contests->prize_type = $inputs['prize_type'];
	        		$contests->prize_money = $inputs['prize_money'];
	        		//$contests->contest_position = $inputs['contest_position'];
	        		if(isset($inputs['tags']) && !empty($inputs['tags'])){
						$contests->tags = implode(',', $inputs['tags']);
					}
	        		$contests->status = $inputs['status'];
	        		$contests->save();

	        	//	p($contests->id);
	        		if(@$inputs['clone_matrix'] === 'true') {
	        			$awardMatrix = Contestawardmatrix::where('contest_id', $inputs['contest_id'])->get()->toArray();
	        			if(!empty($awardMatrix)){
		        			$matrixArray = array();
		        			foreach($awardMatrix as $matrix){
		        				$data = ['contest_id'=>$contests->id, 'winner_from'=>$matrix['winner_from'], 'winner_to'=>$matrix['winner_to'], 'prize_type'=>$matrix['prize_type'], 'prize_amount'=>$matrix['prize_amount']];
		        				array_push($matrixArray, $data);
		        			}
		        			Contestawardmatrix::insert($matrixArray);
		        		}	
	        		}

		           //trigger seo url
					//triggerSeoUrls($contests->id,'contest',$inputs['main_seo_title']);
						$message = 'Successfully created Contests!';
						//$response['callback'] = url('/'.getCurrentUrlPrefix());
		        }
		        return $this->response($response,$status,$message);
			}

		}
		