<?php namespace App\modules\backend\cms\Controllers;

		    use App\Http\Requests;
			use App\Http\Controllers\Controller;
			use App\Http\Models\Contestparticipanthistory;
			use Validator;
			use Redirect;
			use Request;

			class ContestparticipanthistoryController extends Controller {

			/**
			 * Display a listing of the resource.
			 *
			 * @return Response
			 */
			public function __construct()
			{
				$this->middleware('adminauth');
			}

			public function index()
			{ 
				if(is_axios()==true)
				{
					//filter params 
					$inputs = Request::all();
					$inputs = array_filter($inputs);
				$contest_id = @$inputs['contest_id'];
				$user_id = @$inputs['user_id'];
				$earned_score = @$inputs['earned_score'];
				$contest_participant_history = Contestparticipanthistory::where(array())
				->when($contest_id, function ($query) use ($contest_id) {
				        	return $query->where('contest_id','=' ,$contest_id);})
				->when($user_id, function ($query) use ($user_id) {
				        	return $query->where('user_id','=' ,$user_id);})
				->when($earned_score, function ($query) use ($earned_score) {
				        	return $query->where('earned_score','=' ,$earned_score);})
				->paginate(20);
			      $contest_participant_history->setPath('contestparticipanthistory');
					$contest_participant_history->appends(Request::except('page'));
					return view('cms::contestparticipanthistory.index_view')->with('contest_participant_history', $contest_participant_history);

				}
				return view('cms::contestparticipanthistory.index');
			}

			/**
			 * Show the form for creating a new resource.
			 *
			 * @return Response
			 */
			public function create()
			{
				//
				return view('cms::contestparticipanthistory.create');
			}

			/**
			 * Store a newly created resource in storage.
			 *
			 * @return Response
			 */
			public function store()
			{
				//
				// validate
		        // read more on validation at http://laravel.com/docs/validation
		        	$status = 200;
			        $response = array();
			        $message = '';

		        
				$rules = array('contest_id'=>'required',
			        'user_id'=>'required',
			        'earned_score'=>'required',
			        );
		        
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		        	$status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		            
		        } else {
		            // store
		            $contestparticipanthistory = new Contestparticipanthistory;
		            $contestparticipanthistory->contest_id = Request::get('contest_id');
				        		$contestparticipanthistory->user_id = Request::get('user_id');
				        		$contestparticipanthistory->earned_score = Request::get('earned_score');
				        		$contestparticipanthistory->save();

		           //trigger seo url
					triggerSeoUrls($contestparticipanthistory->id,'contestparticipanthistory',Request::get('main_seo_title'));
						$message = 'Successfully created Contestparticipanthistory!';

		            
		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Display the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function show($id)
			{
				//
				if($id=='quickupdate')
				{
					return $this->quickUpdate();
				}
				 $contestparticipanthistory = Contestparticipanthistory::find($id);

				 if(empty($contestparticipanthistory))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;
				 	
				 }

		        // show the view and pass the contestparticipanthistory to it
		        return view('cms::contestparticipanthistory.show')->with('contestparticipanthistory', $contestparticipanthistory);
			}

			public function quickUpdate()
			{
				$inputs = Request::all();
				if(isset($inputs['id']) && isset($inputs['action']))
				{
					if($inputs['action']==1){$action='ACTIVE';}
					else if($inputs['action']==2){$action='INACTIVE';}
					else if($inputs['action']==3){$action='DELETED';}
					if($action!='')
					{Contestparticipanthistory::whereIn('id',explode(',', $inputs['id']))->update(['status'=>$action]);}
				}
			}

			/**
			 * Show the form for editing the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function edit($id)
			{
				//
				$contestparticipanthistory = Contestparticipanthistory::find($id);
				if(empty($contestparticipanthistory))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;

				 }

		        // show the view and pass the contestparticipanthistory to it
		        return view('cms::contestparticipanthistory.edit')->with('contestparticipanthistory', $contestparticipanthistory);
			}

			/**
			 * Update the specified resource in storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function update($id)
			{
				// read more on validation at http://laravel.com/docs/validation
		       		$status = 200;
			        $response = array();
			        $message = '';
		       $rules = array();
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		            $status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		        } else {
		            // store
		             $contestparticipanthistory = Contestparticipanthistory::find($id);$contestparticipanthistory->contest_id = Request::get('contest_id');
				        		$contestparticipanthistory->user_id = Request::get('user_id');
				        		$contestparticipanthistory->earned_score = Request::get('earned_score');
				        		$contestparticipanthistory->save();

		           //trigger seo url
					triggerSeoUrls($contestparticipanthistory->id,'contestparticipanthistory',Request::get('main_seo_title'));
						$message = 'Successfully updated Contestparticipanthistory!';
		           
		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Remove the specified resource from storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function destroy($id)
			{
				//
					$status = 200;
			        $response = array();
			        $message = '';
				$contestparticipanthistory = Contestparticipanthistory::find($id);
				if(empty($contestparticipanthistory))
				 { 
				 	$status=421; 
				 	$response['errors']=array('No department found!');
				 	$message='No department found!';
				 }
				 else
				 {
				 	 $contestparticipanthistory->delete();
				 	 $message = 'Successfully deleted Contestparticipanthistory!';

				 }
				 return $this->response($response,$status,$message);
			 
			}

		}
		