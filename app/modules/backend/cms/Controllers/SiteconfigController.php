<?php namespace App\modules\backend\cms\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Models\SiteConfig;
use Validator;
use Request;

class SiteconfigController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct()
    {
        $this->middleware('adminauth');
    }

    public function index()
    {
        if(is_axios()==true)
        {
            // filter params
            $inputs = Request::all();
            $inputs = array_filter($inputs);
            $keyname = @$inputs['keyname'];
            $value = @$inputs['value'];
            $description = @$inputs['description'];
            $status = @$inputs['status'];
            if(empty($inputs)){
                $status='ACTIVE';
            }
            $site_config = SiteConfig::where([])
            ->when($keyname, function ($query) use ($keyname) {return $query->where('keyname','LIKE' ,'%'.$keyname.'%');})
            ->when($value, function ($query) use ($value) {return $query->where('value','LIKE' ,'%'.$value.'%');})
            ->when($description, function ($query) use ($description) {return $query->where('description','LIKE' ,'%'.$description.'%');})
            ->when($status, function ($query) use ($status) {return $query->where('status','=' ,$status);})
            ->paginate(20);
            
            $site_config->setPath('siteconfig');
            $site_config->appends(Request::except('page'));
            return view('cms::siteconfig.index_view')->with('configs', $site_config);

        }
        return view('cms::siteconfig.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('cms::siteconfig.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $inputs = Request::all();
        $status = 200;
        $response = array();
        $message = '';

        $rules = array('key'=>'required|alpha_dash|unique:site_configs,keyname',
            'value'=>'required',
            'desc'=>'required',
            'status'=>'required',
            );

        $inputs = array_filter($inputs);

        $validator = Validator::make(Request::all(), $rules);

        // process the login
        if ($validator->fails()) {
            $status = 422;
            $response['errors'] = laravel_error_parser($validator->errors()->toArray());
            $message = 'Validation Errors';

        } else {
            $siteconfig = new SiteConfig;
            $siteconfig->keyname        = $inputs['key'];
            $siteconfig->value          = @$inputs['value'];
            $siteconfig->description    = @$inputs['desc'];
            $siteconfig->status         = @$inputs['status'];
            $siteconfig->save();

            $message = 'Successfully created Siteconfig!';
        }
        return $this->response($response,$status,$message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        if($id=='quickupdate')
        {
            return $this->quickUpdate();
        }

            $siteconfig = SiteConfig::find($id);

            if(empty($siteconfig))
            {
            echo 'Oops! Id doesn\'t exists';exit;

            }

        // show the view and pass the siteconfig to it
        return view('cms::siteconfig.show')->with('siteconfig', $siteconfig);
    }

    // public function quickUpdate()
    // {
    //     $inputs = Request::all();

    //     if(isset($inputs['id']) && isset($inputs['action']))
    //     {
    //         if($inputs['action']==1){$action='ACTIVE';}
    //         else if($inputs['action']==2){$action='INACTIVE';}
    //         else if($inputs['action']==3){$action='DELETED';}
    //         else if($inputs['action']==4){
    //             return view('cms::siteconfig.bulk_create')->with('config_ids', $inputs['id']);
    //         }
    //         if($action!='')
    //         {SiteConfig::whereIn('id',explode(',', $inputs['id']))->update(['status'=>$action]);}
    //     }
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $siteconfig = SiteConfig::find($id);
        if(empty($siteconfig)){
            echo 'Oops! Id doesn\'t exists';exit;
        }

        // show the view and pass the siteconfig to it
        return view('cms::siteconfig.edit')->with('siteconfig', $siteconfig);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $status = 200;
        $response = array();
        $message = '';
        $rules = array();
        $inputs = Request::all();
        $inputs = array_filter($inputs);

        $validator = Validator::make(Request::all(), $rules);

        // process the login
        if ($validator->fails()) {
            $status = 422;
            $response['errors'] = laravel_error_parser($validator->errors()->toArray());
            $message = 'Validation Errors';
        } else {
            $siteconfig = SiteConfig::find($id);
            $siteconfig->value          = $inputs['value'];
            $siteconfig->description    = $inputs['description'];
            $siteconfig->status         = $inputs['status'];
            $siteconfig->save();

            $message = 'Successfully updated SiteConfig!';

        }
        return $this->response($response,$status,$message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $status = 200;
        $response = array();
        $message = '';
        $siteconfig = SiteConfig::find($id);
        if(empty($siteconfig)){
            $status=421;
            $response['errors']=array('No SiteConfig found!');
            $message='No SiteConfig found!';
        }
        else{
            $siteconfig->delete();
            $message = 'Successfully deleted SiteConfig!';
        }
        return $this->response($response,$status,$message);
    }

}