<?php 
namespace App\modules\backend\cms\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Solrsync;
use Validator;
use Input;
use Redirect;
use DB;
use Request;
use Mail;

class CmsController extends Controller
{

    /**14583
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct()
    {
        $code = \Session::get('current_lang');
        if (!empty($code))
            \App::setLocale($code);
        else
            \App::setLocale('en');
    }

    public function getHomePage()
    {

        $response = array();
        return view('cms::cms.homeview')->with($response);
    }

    public function getSolrSyncPage()
    {
        $type = Request::get('type');
        if(isset($type) && !empty($type))
        {
            Solrsync::updateOrCreate(
                    [
                        "object_id"=>"0",
                        "object_type"=>$type,
                        "status"=>"0",
                    ],
                    [
                        "message"=>"Request Queued!",
                    ]

            );
             \Session::flash('message', 'Request Queued Successfully!');
             return Redirect::to(getCurrentUrlPrefix());
        }
        $response = array();
        $response['solrstatus'] = objToArray(Solrsync::where('object_id',0)->whereIn('status',[0,1])->get()->toArray(),'object_type');
        // p($response['solrstatus']);
        $response['nodes'] = array('doctors','hospitals','treatments','hotels');
        return view('cms::cms.solrsync')->with($response);
    }


    public function basic_email() {
      $data = array('name'=>"Virat Gandhi");
   
      Mail::send(['text'=>'mail'], $data, function($message) {
         $message->to('rajkamalsingh083@gmail.com', 'Tutorials Point')->subject
            ('Laravel Basic Testing Mail');
         $message->from('reports@qurekalite.com','Qureka Reports');
      });
      echo "Basic Email Sent. Check your inbox.";
   }

   public function testEmail()
    {

            #Send Email to user
            $data = array();
            $data['url'] = \Config::get("qureka.app_url");
            $data['name'] = 'Anjali';
            $data['email'] = 'anjali.rai@coolbootsmedia.com';
            $data['password'] = '123456';
            $data['fromEmail'] = \Config::get("qureka.from_email");

            \Mail::send('emails.new_user_email', ['data'=>$data], function ($message) use ($data)
                {
                    $message->from($data['fromEmail'], 'CoolBoots Media');
                    $message->to('anjali.rai@coolbootsmedia.com');
                    $message->bcc(array('anup.kumar@coolbootsmedia.com',));
                    $message->subject('Qureka Lite Backend Creadentials');
                }); 

            echo "Email Sent. Check your inbox.";
        }   

   public function unauthorized()
   {
    return view('admin.unauthorized');
   }

    
   

   
}
		