<?php namespace App\modules\backend\cms\Controllers;

		    use App\Http\Requests;
			use App\Http\Controllers\Controller;
			use App\Http\Models\Contestawardmatrix;
			use Validator;
			use Redirect;
			use Request;

			class ContestawardmatrixController extends Controller {

			/**
			 * Display a listing of the resource.
			 *
			 * @return Response
			 */
			public function __construct()
			{
				$this->middleware('adminauth');
			}

			public function index()
			{ 
				if(is_axios()==true)
				{
					//filter params 
					$inputs = Request::all();
					$inputs = array_filter($inputs);
				$contest_id = @$inputs['contest_id'];
				$winner_from = @$inputs['winner_from'];
				$winner_to = @$inputs['winner_to'];
				$prize_type = @$inputs['prize_type'];
				$prize_amount = @$inputs['prize_amount'];
				$contest_award_matrix = Contestawardmatrix::where(array())
				->when($contest_id, function ($query) use ($contest_id) {
				        	return $query->where('contest_id','=' ,$contest_id);})
				->when($winner_from, function ($query) use ($winner_from) {
				        	return $query->where('winner_from','=' ,$winner_from);})
				->when($winner_to, function ($query) use ($winner_to) {
				        	return $query->where('winner_to','=' ,$winner_to);})
				->when($prize_type, function ($query) use ($prize_type) {
				        	return $query->where('prize_type','LIKE' ,$prize_type.'%');})
				->when($prize_amount, function ($query) use ($prize_amount) {
				        	return $query->where('prize_amount','=' ,$prize_amount);})
				->paginate(20);
			      $contest_award_matrix->setPath('contestawardmatrix');
					$contest_award_matrix->appends(Request::except('page'));
					return view('cms::contestawardmatrix.index_view')->with('contest_award_matrix', $contest_award_matrix);

				}
				return view('cms::contestawardmatrix.index');
			}

			/**
			 * Show the form for creating a new resource.
			 *
			 * @return Response
			 */
			public function create()
			{
				//
				return view('cms::contestawardmatrix.create');
			}

			/**
			 * Store a newly created resource in storage.
			 *
			 * @return Response
			 */
			public function store()
			{
				//
				// validate
		        // read more on validation at http://laravel.com/docs/validation
		        	$status = 200;
			        $response = array();
			        $message = '';

		        
				$rules = array('contest_id'=>'required',
			        'winner_from'=>'required',
			        'winner_to'=>'required',
			        'prize_type'=>'required',
			        'prize_amount'=>'required',
			        );
		        
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		        	$status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		            
		        } else {
		            // store
		            $contestawardmatrix = new Contestawardmatrix;
		            $contestawardmatrix->contest_id = Request::get('contest_id');
				        		$contestawardmatrix->winner_from = Request::get('winner_from');
				        		$contestawardmatrix->winner_to = Request::get('winner_to');
				        		$contestawardmatrix->prize_type = Request::get('prize_type');
				        		$contestawardmatrix->prize_amount = Request::get('prize_amount');
				        		$contestawardmatrix->save();

		           //trigger seo url
					triggerSeoUrls($contestawardmatrix->id,'contestawardmatrix',Request::get('main_seo_title'));
						$message = 'Successfully created Contestawardmatrix!';

		            
		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Display the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function show($id)
			{
				//
				if($id=='quickupdate')
				{
					return $this->quickUpdate();
				}
				 $contestawardmatrix = Contestawardmatrix::find($id);

				 if(empty($contestawardmatrix))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;
				 	
				 }

		        // show the view and pass the contestawardmatrix to it
		        return view('cms::contestawardmatrix.show')->with('contestawardmatrix', $contestawardmatrix);
			}

			public function quickUpdate()
			{
				$inputs = Request::all();
				if(isset($inputs['id']) && isset($inputs['action']))
				{
					if($inputs['action']==1){$action='ACTIVE';}
					else if($inputs['action']==2){$action='INACTIVE';}
					else if($inputs['action']==3){$action='DELETED';}
					if($action!='')
					{Contestawardmatrix::whereIn('id',explode(',', $inputs['id']))->update(['status'=>$action]);}
				}
			}

			/**
			 * Show the form for editing the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function edit($id)
			{
				//
				$contestawardmatrix = Contestawardmatrix::find($id);
				if(empty($contestawardmatrix))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;

				 }

		        // show the view and pass the contestawardmatrix to it
		        return view('cms::contestawardmatrix.edit')->with('contestawardmatrix', $contestawardmatrix);
			}

			/**
			 * Update the specified resource in storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function update($id)
			{
				// read more on validation at http://laravel.com/docs/validation
		       		$status = 200;
			        $response = array();
			        $message = '';
		       $rules = array();
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		            $status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		        } else {
		            // store
		             $contestawardmatrix = Contestawardmatrix::find($id);$contestawardmatrix->contest_id = Request::get('contest_id');
				        		$contestawardmatrix->winner_from = Request::get('winner_from');
				        		$contestawardmatrix->winner_to = Request::get('winner_to');
				        		$contestawardmatrix->prize_type = Request::get('prize_type');
				        		$contestawardmatrix->prize_amount = Request::get('prize_amount');
				        		$contestawardmatrix->save();

		           //trigger seo url
					triggerSeoUrls($contestawardmatrix->id,'contestawardmatrix',Request::get('main_seo_title'));
						$message = 'Successfully updated Contestawardmatrix!';
		           
		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Remove the specified resource from storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function destroy($id)
			{
				//
					$status = 200;
			        $response = array();
			        $message = '';
				$contestawardmatrix = Contestawardmatrix::find($id);
				if(empty($contestawardmatrix))
				 { 
				 	$status=421; 
				 	$response['errors']=array('No department found!');
				 	$message='No department found!';
				 }
				 else
				 {
				 	 $contestawardmatrix->delete();
				 	 $message = 'Successfully deleted Contestawardmatrix!';

				 }
				 return $this->response($response,$status,$message);
			 
			}

		}
		