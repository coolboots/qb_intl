<?php namespace App\modules\backend\cms\Controllers;

		    use App\Http\Requests;
			use App\Http\Controllers\Controller;
			use App\Http\Models\Userwalletcoin;
			use Validator;
			use Redirect;
			use Request;

			class UserwalletcoinsController extends Controller {

			/**
			 * Display a listing of the resource.
			 *
			 * @return Response
			 */
			public function __construct()
			{
				$this->middleware('adminauth');
			}

			public function index()
			{ 
				if(is_axios()==true)
				{
					//filter params 
					$inputs = Request::all();
					$inputs = array_filter($inputs);
				$user_id = @$inputs['user_id'];
				$available_coins = @$inputs['available_coins'];
				$user_wallet_coins = Userwalletcoin::where(array())
				->when($user_id, function ($query) use ($user_id) {
				        	return $query->where('user_id','=' ,$user_id);})
				->when($available_coins, function ($query) use ($available_coins) {
				        	return $query->where('available_coins','=' ,$available_coins);})
				->paginate(20);
			      $user_wallet_coins->setPath('userwalletcoins');
					$user_wallet_coins->appends(Request::except('page'));
					return view('cms::userwalletcoins.index_view')->with('user_wallet_coins', $user_wallet_coins);

				}
				return view('cms::userwalletcoins.index');
			}

			/**
			 * Show the form for creating a new resource.
			 *
			 * @return Response
			 */
			public function create()
			{
				//
				return view('cms::userwalletcoins.create');
			}

			/**
			 * Store a newly created resource in storage.
			 *
			 * @return Response
			 */
			public function store()
			{
				//
				// validate
		        // read more on validation at http://laravel.com/docs/validation
		        	$status = 200;
			        $response = array();
			        $message = '';

		        
				$rules = array('user_id'=>'required',
			        'available_coins'=>'required',
			        );
		        
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		        	$status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		            
		        } else {
		            // store
		            $userwalletcoins = new Userwalletcoin;
		            $userwalletcoins->user_id = Request::get('user_id');
				        		$userwalletcoins->available_coins = Request::get('available_coins');
				        		$userwalletcoins->save();

		           //trigger seo url
					triggerSeoUrls($userwalletcoins->id,'userwalletcoin',Request::get('main_seo_title'));
						$message = 'Successfully created Userwalletcoins!';

		            
		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Display the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function show($id)
			{
				//
				if($id=='quickupdate')
				{
					return $this->quickUpdate();
				}
				 $userwalletcoins = Userwalletcoin::find($id);

				 if(empty($userwalletcoins))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;
				 	
				 }

		        // show the view and pass the userwalletcoins to it
		        return view('cms::userwalletcoins.show')->with('userwalletcoins', $userwalletcoins);
			}

			public function quickUpdate()
			{
				$inputs = Request::all();
				if(isset($inputs['id']) && isset($inputs['action']))
				{
					if($inputs['action']==1){$action='ACTIVE';}
					else if($inputs['action']==2){$action='INACTIVE';}
					else if($inputs['action']==3){$action='DELETED';}
					if($action!='')
					{Userwalletcoin::whereIn('id',explode(',', $inputs['id']))->update(['status'=>$action]);}
				}
			}

			/**
			 * Show the form for editing the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function edit($id)
			{
				//
				$userwalletcoins = Userwalletcoin::find($id);
				if(empty($userwalletcoins))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;

				 }

		        // show the view and pass the userwalletcoins to it
		        return view('cms::userwalletcoins.edit')->with('userwalletcoins', $userwalletcoins);
			}

			/**
			 * Update the specified resource in storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function update($id)
			{
				// read more on validation at http://laravel.com/docs/validation
		       		$status = 200;
			        $response = array();
			        $message = '';
		       $rules = array();
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		            $status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		        } else {
		            // store
		             $userwalletcoins = Userwalletcoin::find($id);$userwalletcoins->user_id = Request::get('user_id');
				        		$userwalletcoins->available_coins = Request::get('available_coins');
				        		$userwalletcoins->save();

		           //trigger seo url
					triggerSeoUrls($userwalletcoins->id,'userwalletcoin',Request::get('main_seo_title'));
						$message = 'Successfully updated Userwalletcoins!';
		           
		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Remove the specified resource from storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function destroy($id)
			{
				//
					$status = 200;
			        $response = array();
			        $message = '';
				$userwalletcoins = Userwalletcoin::find($id);
				if(empty($userwalletcoins))
				 { 
				 	$status=421; 
				 	$response['errors']=array('No department found!');
				 	$message='No department found!';
				 }
				 else
				 {
				 	 $userwalletcoins->delete();
				 	 $message = 'Successfully deleted Userwalletcoins!';

				 }
				 return $this->response($response,$status,$message);
			 
			}

		}
		