<?php namespace App\modules\backend\cms\Controllers;

		    use App\Http\Requests;
			use App\Http\Controllers\Controller;
			use App\Http\Models\Adsconfig;
			use App\Http\Models\Partner;
			use App\Http\Models\Page;
			use App\Http\Models\Publisher;
			use Validator;
			use Redirect;
			use Request;

			class AdsconfigController extends Controller {

			/**
			 * Display a listing of the resource.
			 *
			 * @return Response
			 */
			public function __construct()
			{
				$this->middleware('adminauth');
			}

			public function index()
			{ 
				if(is_axios()==true)
				{
					//filter params 
					$inputs = Request::all();
					$inputs = array_filter($inputs);
				$page_id = @$inputs['page_id'];
				$publisher_id = @$inputs['publisher_id'];
				$partner_id = @$inputs['partner_id'];
				$status = @$inputs['status'];
				if(empty($inputs)){
					$status='ACTIVE';
				}
				$publisher_configs = Adsconfig::where(array())
				->when($page_id, function ($query) use ($page_id) {
				        	return $query->where('page_id','LIKE' ,'%'.$page_id.'%');})
				->when($publisher_id, function ($query) use ($publisher_id) {
				        	return $query->where('publisher_id','LIKE' ,'%'.$publisher_id.'%');})
				->when($partner_id, function ($query) use ($partner_id) {
				        	return $query->where('partner_id','=' ,$partner_id);})
				->when($status, function ($query) use ($status) {
				        	return $query->where('status','LIKE' ,$status.'%');})
				->paginate(20);
			      $publisher_configs->setPath('adsconfig');
					$publisher_configs->appends(Request::except('page'));
					return view('cms::adsconfig.index_view')->with('publisher_configs', $publisher_configs);

				}
				return view('cms::adsconfig.index');
			}

			/**
			 * Show the form for creating a new resource.
			 *
			 * @return Response
			 */
			public function create()
			{
				//
				return view('cms::adsconfig.create');
			}

			/**
			 * Store a newly created resource in storage.
			 *
			 * @return Response
			 */
			public function store()
			{
				$inputs = Request::all();				
				$status = 200;
				$response = array();
				$message = '';

				if (isset($inputs['data']['bulk']) && $inputs['data']['bulk'] == true) {
					return $this->bulkUpdate($inputs['data']);
				}
				
				$rules = array('page_id'=>'required',
			        'publisher_id'=>'required',
			        'partner_id'=>'required',
			        'status'=>'required',
			        );

				$inputs = array_filter($inputs);
		        
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		        	$status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		            
		        } else {
					
		            // store
		            $adsconfig = new Adsconfig;
		            $adsconfig->page_id = $inputs['page_id'];
				        		$adsconfig->publisher_id = @$inputs['publisher_id'];
				        		$adsconfig->partner_id = @$inputs['partner_id'];
				        		$adsconfig->scripts_1 = @$inputs['scripts_1'];
				        		$adsconfig->scripts_2 = @$inputs['scripts_2'];
				        		$adsconfig->scripts_3 = @$inputs['scripts_3'];
				        		$adsconfig->scripts_4 = @$inputs['scripts_4'];
				        		$adsconfig->scripts_5 = @$inputs['scripts_5'];
				        		$adsconfig->status = @$inputs['status'];
				        		$adsconfig->save();

		           //trigger seo url
					//triggerSeoUrls($adsconfig->id,'adsconfig',$inputs['main_seo_title']);
						$message = 'Successfully created Adsconfig!';
						$response['callback'] = url('/'.getCurrentUrlPrefix());
		            
		        }
		        return $this->response($response,$status,$message);
			}

			private function bulkUpdate($inputs)
			{
				$status = 200;
				$response = array();
				$message = '';
				
				$inputs = array_filter($inputs);
				// $response['errors'] = laravel_error_parser($validator->errors()->toArray());

				$config_ids = explode(',',$inputs['config_ids']);
				foreach ($inputs['partner_ids'] as $partnerkey => $partnervalue) {
					foreach ($config_ids as $configkey => $configvalue) {
						$getConfig = Adsconfig::find($configvalue);
						$adsconfig = Adsconfig::updateOrCreate(
							['page_id'=>$getConfig->page_id, 'publisher_id'=>$getConfig->publisher_id, 'partner_id'=>$partnervalue],
							['scripts_1'=>$getConfig->scripts_1,'scripts_2'=>$getConfig->scripts_2,'scripts_3'=>$getConfig->scripts_3,'scripts_4'=>$getConfig->scripts_4,'scripts_5'=>$getConfig->scripts_5,'status'=>$getConfig->status]
						);
						
					}
				}
				$message = 'Bulk Configs Assigned';
				return $this->response($response,$status,$message);
			}

			/**
			 * Display the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function show($id)
			{
				//
				if($id=='quickupdate')
				{
					return $this->quickUpdate();
				}
				
				 $adsconfig = Adsconfig::find($id);

				 if(empty($adsconfig))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;
				 	
				 }

		        // show the view and pass the adsconfig to it
		        return view('cms::adsconfig.show')->with('adsconfig', $adsconfig);
			}


			//to update status quickly

			public function quickUpdate()
			{
				$inputs = Request::all();
				
				if(isset($inputs['id']) && isset($inputs['action']))
				{
					if($inputs['action']==1){$action='ACTIVE';}
					else if($inputs['action']==2){$action='INACTIVE';}
					else if($inputs['action']==3){$action='DELETED';}
					else if($inputs['action']==4){
						return view('cms::adsconfig.bulk_create')->with('config_ids', $inputs['id']);
					}
					if($action!='')
					{Adsconfig::whereIn('id',explode(',', $inputs['id']))->update(['status'=>$action]);}
				}
			}

			/**
			 * Show the form for editing the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function edit($id)
			{
				//
				$adsconfig = Adsconfig::find($id);
				if(empty($adsconfig))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;

				 }

		        // show the view and pass the adsconfig to it
		        return view('cms::adsconfig.edit')->with('adsconfig', $adsconfig);
			}

			/**
			 * Update the specified resource in storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function update($id)
			{
				// read more on validation at http://laravel.com/docs/validation
		       		$status = 200;
			        $response = array();
			        $message = '';
		       $rules = array();
				$inputs = Request::all();
				$inputs = array_filter($inputs);

		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		            $status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		        } else {
		            // store
					$adsconfig = Adsconfig::find($id);$adsconfig->page_id = $inputs['page_id'];
					$adsconfig->publisher_id = $inputs['publisher_id'];
					$adsconfig->partner_id = $inputs['partner_id'];
					$adsconfig->scripts_1 = @$inputs['scripts_1'];
					$adsconfig->scripts_2 = @$inputs['scripts_2'];
					$adsconfig->scripts_3 = @$inputs['scripts_3'];
					$adsconfig->scripts_4 = @$inputs['scripts_4'];
					$adsconfig->scripts_5 = @$inputs['scripts_5'];
					$adsconfig->status = $inputs['status'];
					$adsconfig->save();

		           //trigger seo url
					triggerSeoUrls($adsconfig->id,'adsconfig',@$inputs['main_seo_title']);
						$message = 'Successfully updated Adsconfig!';
		           		$response['callback'] = url('/'.getCurrentUrlPrefix());
		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Remove the specified resource from storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function destroy($id)
			{
				//
					$status = 200;
			        $response = array();
			        $message = '';
				$adsconfig = Adsconfig::find($id);
				if(empty($adsconfig))
				 { 
				 	$status=421; 
				 	$response['errors']=array('No Adsconfig found!');
				 	$message='No Adsconfig found!';
				 }
				 else
				 {
				 	// $adsconfig->delete();
				 	$adsconfig->status = 'DELETED';
				 	 $adsconfig->save();
				 	 $message = 'Successfully deleted Adsconfig!';

				 }
				 return $this->response($response,$status,$message);
			 
			}

			public function import()
			{
		        return view('cms::adsconfig.excel-import');
			}

			public function importExcel(Request $request)
			{
		        // read more on validation at http://laravel.com/docs/validation
				$status = 200;
				$response = array();
				$message = '';
		        
				$rules = array(
			        'excel_import'=>'required|file|max:2048|mimes:xls,xlsx',
			        );

				$inputs = Request::all();
				$inputs = array_filter($inputs);

		        
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		        	$status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		            
		        } else {
		        	$import = \Request::file('excel_import');

		        	if(isset($import) and !empty($import)){

		        		$path = \Request::file('excel_import')->getRealPath();
     					$data = \Excel::toArray('', $path, null, \Maatwebsite\Excel\Excel::XLSX);
						// dd($data);
     					unset($data[0][0]);	

     					$rawQuery = 'INSERT INTO publisher_configs(page_id, publisher_id, partner_id, scripts_1,scripts_2,scripts_3,scripts_4,scripts_5, status, created_at, updated_at) VALUES';

						if(isset($data[0])){
							$loopRawQuery = "";
							$pages = Page::pluck('id','page_code')->toArray();
							$publishers = Publisher::pluck('id','publisher_code')->toArray();
							$partners = Partner::pluck('id','partner_name')->toArray();
							// p($pages);
							foreach ($data[0] as $key => $row) {
								// $row[0] = null;
								$page_id = $pages[$row[0]] ?? null;
								$row[0] = $page_id;
	
								$publisher_id = $publishers[$row[1]] ??null;
								$row[1] = $publisher_id;
	
								$partner_id = $partners[$row[2]] ??null;
								$row[2] = $partner_id;
								
								if(!empty(@$row['0']) && !empty(@$row['1']) && !empty(@$row['2']) ) {
									$loopRawQuery.= ",($row[0],$row[1],$row[2],".@json_encode($row[3]).",".@json_encode($row[4]).",".@json_encode($row[5]).",'".@json_encode($row[6])."',".@json_encode($row[7]).",'$row[8]','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."')";
								}
							}

		                if ($loopRawQuery != '') {
							$rawQuery.=substr($loopRawQuery,1);
							$rawQuery.= ' ON DUPLICATE KEY UPDATE page_id=page_id,publisher_id=publisher_id,partner_id=partner_id';
							\DB::statement($rawQuery);
						}

		                $message = 'Successfully Uploaded Ads Configs!';
						$response['callback'] = route('adsconfig.index');
			            
			        }
			        }		        

		        }
		        return $this->response($response,$status,$message);
			}

		}
		