<?php namespace App\modules\backend\cms\Controllers;

		    use App\Http\Requests;
			use App\Http\Controllers\Controller;
			use App\Http\Models\User;
			use Validator;
			use Redirect;
			use Request;
			use DB;
			
			class UsersController extends Controller {

			/**
			 * Display a listing of the resource.
			 *
			 * @return Response
			 */
			public function __construct()
			{
				$this->middleware('adminauth');
			}

			public function index()
			{ 
				if(is_axios()==true)
				{
					//filter params 
					$inputs = Request::all();
					$inputs = array_filter($inputs);
				$name = @$inputs['name'];
				$mobile = @$inputs['mobile'];
				$email = @$inputs['email'];
				$password = @$inputs['password'];
				$remember_token = @$inputs['remember_token'];
				$source = @$inputs['source'];
				$status = @$inputs['status'];
				
				if(empty($inputs)){
					$status='ACTIVE';
				}
				$users = User::where(array())
				->when($name, function ($query) use ($name) {
				        	return $query->where('name','LIKE' ,'%'.$name.'%');})
				->when($mobile, function ($query) use ($mobile) {
				        	return $query->where('mobile','LIKE' ,$mobile.'%');})
				->when($email, function ($query) use ($email) {
				        	return $query->where('email','LIKE' ,$email.'%');})
				->when($password, function ($query) use ($password) {
				        	return $query->where('password','LIKE' ,$password.'%');})
				->when($remember_token, function ($query) use ($remember_token) {
				        	return $query->where('remember_token','LIKE' ,$remember_token.'%');})
				
				->when($source, function ($query) use ($source) {
									return $query->where('source','LIKE' ,$source.'%');})
				->when($status, function ($query) use ($status) {
				        	return $query->where('status','LIKE' ,$status.'%');})
				->paginate(20);
				$users->appends(Request::except('page'));
				 $users->setPath('users');
				
				return view('cms::users.index_view')->with('users', $users);
				// return user::find(1)->coin;
				}
				
				
				return view('cms::users.index');
			}

			/**
			 * Show the form for creating a new resource.
			 *
			 * @return Response
			 */
			public function create()
			{
				//
				return view('cms::users.create');
			}

			/**
			 * Store a newly created resource in storage.
			 *
			 * @return Response
			 */
			public function store()
			{
				//
				// validate
		        // read more on validation at http://laravel.com/docs/validation
		        	$status = 200;
			        $response = array();
			        $message = '';

		        
				$rules = array('name'=>'required',
			        'mobile'=>'required',
			        'email'=>'required',
			        'password'=>'required',
			        'remember_token'=>'required',
			        'source'=>'required',
			        'status'=>'required',
			        );
		        
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		        	$status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		            
		        } else {
		            // store
		            $users = new User;
		            $users->name = Request::get('name');
				        		$users->mobile = Request::get('mobile');
				        		$users->email = Request::get('email');
				        		$users->password = Request::get('password');
				        		$users->remember_token = Request::get('remember_token');
				        		$users->source = Request::get('source');
				        		$users->status = Request::get('status');
				        		$users->save();

		           //trigger seo url
					triggerSeoUrls($users->id,'user',Request::get('main_seo_title'));
						$message = 'Successfully created Users!';

		            
		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Display the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function show($id)
			{
				//
				if($id=='quickupdate')
				{
					return $this->quickUpdate();
				}
				 $users = User::find($id);

				 if(empty($users))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;
				 	
				 }
				
			

		        // show the view and pass the users to it
		        return view('cms::users.show')->with('users', $users);
			}

			public function quickUpdate()
			{
				$inputs = Request::all();
				if(isset($inputs['id']) && isset($inputs['action']))
				{
					if($inputs['action']==1){$action='ACTIVE';}
					else if($inputs['action']==2){$action='INACTIVE';}
					else if($inputs['action']==3){$action='DELETED';}
					if($action!='')
					{User::whereIn('id',explode(',', $inputs['id']))->update(['status'=>$action]);}
				}
			}

			/**
			 * Show the form for editing the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function edit($id)
			{
				//
				$users = User::find($id);
				if(empty($users))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;

				 }

		        // show the view and pass the users to it
		        return view('cms::users.edit')->with('users', $users);
			}

			/**
			 * Update the specified resource in storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function update($id)
			{
				// read more on validation at http://laravel.com/docs/validation
		       		$status = 200;
			        $response = array();
			        $message = '';
		       $rules = array();
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		            $status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		        } else {
		            // store
		             $users = User::find($id);$users->name = Request::get('name');
				        		$users->mobile = Request::get('mobile');
				        		$users->email = Request::get('email');
				        		$users->password = Request::get('password');
				        		$users->remember_token = Request::get('remember_token');
				        		$users->source = Request::get('source');
				        		$users->status = Request::get('status');
				        		$users->save();

		           //trigger seo url
					triggerSeoUrls($users->id,'user',Request::get('main_seo_title'));
						$message = 'Successfully updated Users!';
		           
		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Remove the specified resource from storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function destroy($id)
			{
				//
					$status = 200;
			        $response = array();
			        $message = '';
				$users = User::find($id);
				if(empty($users))
				 { 
				 	$status=421; 
				 	$response['errors']=array('No department found!');
				 	$message='No department found!';
				 }
				 else
				 {
				 	 $users->delete();
				 	 $message = 'Successfully deleted Users!';

				 }
				 return $this->response($response,$status,$message);
			 
			}
			public function getHistory($id)
			{
				$response = array();
				
				$response=	DB::table('user_wallet_trans_history')->join('users', 'user_wallet_trans_history.user_id', '=', 'users.id')
				->where('user_id', '=', $id)->limit(19)->orderBy('user_wallet_trans_history.created_at', 'asc')->get();
				 return view('cms::users.history')->with('histories', $response);
			  
			}

		
		
	}
		
		