<?php namespace App\modules\backend\cms\Controllers;

		    use App\Http\Requests;
			use App\Http\Controllers\Controller;
			use App\Http\Models\Userwalletcash;
			use Validator;
			use Redirect;
			use Request;

			class UserwalletcashController extends Controller {

			/**
			 * Display a listing of the resource.
			 *
			 * @return Response
			 */
			public function __construct()
			{
				$this->middleware('adminauth');
			}

			public function index()
			{ 
				if(is_axios()==true)
				{
					//filter params 
					$inputs = Request::all();
					$inputs = array_filter($inputs);
				$user_id = @$inputs['user_id'];
				$cash_available = @$inputs['cash_available'];
				$cash_withdrawal = @$inputs['cash_withdrawal'];
				$total_earned = @$inputs['total_earned'];
				$user_wallet_cash = Userwalletcash::where(array())
				->when($user_id, function ($query) use ($user_id) {
				        	return $query->where('user_id','=' ,$user_id);})
				->when($cash_available, function ($query) use ($cash_available) {
				        	return $query->where('cash_available','=' ,$cash_available);})
				->when($cash_withdrawal, function ($query) use ($cash_withdrawal) {
				        	return $query->where('cash_withdrawal','=' ,$cash_withdrawal);})
				->when($total_earned, function ($query) use ($total_earned) {
				        	return $query->where('total_earned','=' ,$total_earned);})
				->paginate(20);
			      $user_wallet_cash->setPath('userwalletcash');
					$user_wallet_cash->appends(Request::except('page'));
					return view('cms::userwalletcash.index_view')->with('user_wallet_cash', $user_wallet_cash);

				}
				return view('cms::userwalletcash.index');
			}

			/**
			 * Show the form for creating a new resource.
			 *
			 * @return Response
			 */
			public function create()
			{
				//
				return view('cms::userwalletcash.create');
			}

			/**
			 * Store a newly created resource in storage.
			 *
			 * @return Response
			 */
			public function store()
			{
				//
				// validate
		        // read more on validation at http://laravel.com/docs/validation
		        	$status = 200;
			        $response = array();
			        $message = '';

		        
				$rules = array('user_id'=>'required',
			        'cash_available'=>'required',
			        'cash_withdrawal'=>'required',
			        'total_earned'=>'required',
			        );
		        
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		        	$status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		            
		        } else {
		            // store
		            $userwalletcash = new Userwalletcash;
		            $userwalletcash->user_id = Request::get('user_id');
				        		$userwalletcash->cash_available = Request::get('cash_available');
				        		$userwalletcash->cash_withdrawal = Request::get('cash_withdrawal');
				        		$userwalletcash->total_earned = Request::get('total_earned');
				        		$userwalletcash->save();

		           //trigger seo url
					triggerSeoUrls($userwalletcash->id,'userwalletcash',Request::get('main_seo_title'));
						$message = 'Successfully created Userwalletcash!';

		            
		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Display the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function show($id)
			{
				//
				if($id=='quickupdate')
				{
					return $this->quickUpdate();
				}
				 $userwalletcash = Userwalletcash::find($id);

				 if(empty($userwalletcash))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;
				 	
				 }

		        // show the view and pass the userwalletcash to it
		        return view('cms::userwalletcash.show')->with('userwalletcash', $userwalletcash);
			}

			public function quickUpdate()
			{
				$inputs = Request::all();
				if(isset($inputs['id']) && isset($inputs['action']))
				{
					if($inputs['action']==1){$action='ACTIVE';}
					else if($inputs['action']==2){$action='INACTIVE';}
					else if($inputs['action']==3){$action='DELETED';}
					if($action!='')
					{Userwalletcash::whereIn('id',explode(',', $inputs['id']))->update(['status'=>$action]);}
				}
			}

			/**
			 * Show the form for editing the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function edit($id)
			{
				//
				$userwalletcash = Userwalletcash::find($id);
				if(empty($userwalletcash))
				 {
				 	echo 'Oops! Id doesn\'t exists';exit;

				 }

		        // show the view and pass the userwalletcash to it
		        return view('cms::userwalletcash.edit')->with('userwalletcash', $userwalletcash);
			}

			/**
			 * Update the specified resource in storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function update($id)
			{
				// read more on validation at http://laravel.com/docs/validation
		       		$status = 200;
			        $response = array();
			        $message = '';
		       $rules = array();
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		            $status = 422;
		        	$response['errors'] = laravel_error_parser($validator->errors()->toArray());
		        	$message = 'Validation Errors';
		        } else {
		            // store
		             $userwalletcash = Userwalletcash::find($id);$userwalletcash->user_id = Request::get('user_id');
				        		$userwalletcash->cash_available = Request::get('cash_available');
				        		$userwalletcash->cash_withdrawal = Request::get('cash_withdrawal');
				        		$userwalletcash->total_earned = Request::get('total_earned');
				        		$userwalletcash->save();

		           //trigger seo url
					triggerSeoUrls($userwalletcash->id,'userwalletcash',Request::get('main_seo_title'));
						$message = 'Successfully updated Userwalletcash!';
		           
		        }
		        return $this->response($response,$status,$message);
			}

			/**
			 * Remove the specified resource from storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function destroy($id)
			{
				//
					$status = 200;
			        $response = array();
			        $message = '';
				$userwalletcash = Userwalletcash::find($id);
				if(empty($userwalletcash))
				 { 
				 	$status=421; 
				 	$response['errors']=array('No department found!');
				 	$message='No department found!';
				 }
				 else
				 {
				 	 $userwalletcash->delete();
				 	 $message = 'Successfully deleted Userwalletcash!';

				 }
				 return $this->response($response,$status,$message);
			 
			}

		}
		