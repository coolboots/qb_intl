 @include('cms::games.filters')
		     <div class="row">
		     <div class="col-md-12">
 				<section class="tile">
 					<div class="tile-header dvd dvd-btm"><h1 class="custom-font">View All Games</h1></div>
            			<div class="tile-body">
		     		
			            	 <div class="table-responsive">
			                <table class="table mb-0" id="">
			                   
			                    <thead>
			                        <tr>
			                          	<th class="head0 nosort"><input type="checkbox" class="checkall" /></th>
			                          	<th class="head0 sortby" sortby="game_name">Game Name</th>
						        <th class="head0 sortby" sortby="logo">Logo</th>
						        <th class="head0 sortby" sortby="banner_image">Banner Image</th>
						        <th class="head0 sortby" sortby="game_instruction_video">Game Instruction Video</th>
						        <th class="head0 sortby" sortby="game_position">Game Position</th>
						        <th class="head0 sortby" sortby="game_url">Game Url</th>
						        <th class="head0 sortby" sortby="game_layout">Game Layout</th>
						        <th class="head0 sortby" sortby="status">Status</th>
						        <th class="head0">Actions</th>
			                           
			                        </tr>
			                    </thead>
			                    <tbody>
			                     @foreach($games as $key => $value)
						        <tr><td class="aligncenter"><span class="center">
			                            <input type="checkbox" actionid={{ $value->id }} />
			                          </span></td><td>{{ $value->game_name }}</td>
							        <td><img class="lazy" data-src="{{ asset($value->logo) }}" height="100" /></td>
							        <td><img  class="lazy" data-src="{{ asset($value->banner_image) }}" width="100px" /></td>
							        <td><a href="{{ $value->game_instruction_video }}" target="_NEW">Video Link</a></td>
							        <td>{{ $value->game_position }}</td>
							        <td><a href="{{ $value->game_url }}">Game Link</a></td>
							        <td>{{ $value->game_layout }}</td>
							        <td>{{ $value->status }}</td>
							            
						           <!-- we will also add show, edit, and delete buttons -->
						            <td>

						               <a class="btn btn-small btn-success" href="{{ URL::to(getCurrentUrlPrefix() .'/'. $value->id) }}" storage="modal">Show</a>

						                <a class="btn btn-small btn-info" href="{{ URL::to(getCurrentUrlPrefix() .'/'. $value->id . '/edit') }}">Edit</a>

						                <a class="btn btn-small btn-warning delete" data-token="{{ csrf_token() }}" deleteid="{{$value->id}}"  url="{{url(getCurrentUrlPrefix())}}">Delete</a>

						            </td>
						        </tr>
						    @endforeach
			                    </tbody>
			                </table>
			                </div>
			                </div>
			                </section>
			                </div>
			               </div>
			                <div class="tile-footer dvd dvd-top">
			              <div class="row">
			                <div class="col-sm-3 hidden-xs">
			                  <select class="input-sm form-control w-sm inline bulk_action_type">
			                    <option value="">Select Action</option>
			                    <option value="1">Mark Active</option>
			                    <option value="2">Mark Inactive</option>
			                    <option value="3">Mark Deleted</option>
			                    
			                  </select>
			                  <button class="btn btn-sm btn-default br-3 bulk_action_trigger">Apply</button>
			                </div>
			                <div class="col-sm-4 text-left"><small class="text-muted">Total {{$games->total()}} records found!</small></div>
			                <div class="col-sm-5 text-right">
			                 <ul class="pagination pagination-sm m-0">
			                <?php echo $games->render(); ?>
			            	</ul>
			                 
			                </div>
			              </div>
			            </div>
			             <script src="{{asset('/ba/assets/js/common.js')}}"></script> 