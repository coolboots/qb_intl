
		   <div class="row">
		    <div class="col-md-12">
 <section class="tile filterblock">
 <div class="tile-header dvd dvd-btm">
              <h1 class="custom-font">Games filter
              </h1>
             </div>
            <div class="tile-body">
              <form class="form-inline" role="form" id="filterForm" action="{{url('/'.getCurrentUrlPrefix())}}">
                
              <div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="sgame_name" name="game_name" value="{{Request::get("game_name")}}" placeholder="Enter Game Name" title="Game Name">
		                </div><div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="sgame_position" name="game_position" value="{{Request::get("game_position")}}" placeholder="Enter Game Position" title="Game Position">
		                </div><div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="sgame_url" name="game_url" value="{{Request::get("game_url")}}" placeholder="Enter Game Url" title="Game Url">
		                </div><div class="form-group col-md-2">
		                	<select class="form-control" id="sgame_layout" name="game_layout" changedValue="{{Request::get("game_layout")}}" placeholder="Enter Game Layout" title="Game Layout">
		                		{{getGameLayout()}}
		                	</select>
		                 
		                </div><div class="form-group col-md-2">
		                	<select class="form-control" id="sstatus" name="status" changedValue="{{Request::get("status")}}">
		                		{{getStatuses()}}
		                	</select>
		                </div>
                    <button type="submit" class="btn btn-primary">Search</button>
                    <button type="reset" class="btn btn-default">Reset</button>
              </form>
            </div>
 </section>

 </div>
 </div>