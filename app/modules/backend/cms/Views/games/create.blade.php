
		    <div class="row">
		    <div class="col-md-8 col-md-offset-2">
		    <div class="panel panel-default panel-filled">
                <div class="panel-heading">
                  <h3 class="panel-title custom-font">Add New Game</h3>
                </div>
                <div class="panel-body">
		   
		    <form class="form-horizontal" role="form" method="post" id="createForm" action="{{url('/'.getCurrentUrlPrefix())}}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                  				<label for="game_name" class="col-sm-2 control-label">Game Name</label>
		                                <div class="col-sm-10"><input type="text" name="game_name" id="game_name" class="form-control title"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        {!!seoUrlsField("game")!!}
					        <div class="form-group">
                                <label for="logo" class="col-sm-2 control-label">Logo</label>
                                        <div class="col-sm-10"><input type="file" id="logo" name="logo" class="form-control"></div>
                                        
                                        </div>
                            <hr class="line-dashed line-full"/><div class="form-group">
                  				<label for="banner_image" class="col-sm-2 control-label">Banner Image</label>
		                                <div class="col-sm-10"><input type="file" name="banner_image" id="banner_image" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
				        		  <label for="game_long_desc" class="col-sm-2 control-label">Game Long Desc</label>
                                <div class="col-sm-10"><textarea cols="80" rows="5" name="game_long_desc" id="game_long_desc" class="form-control"></textarea></div>
                                </div>
			        			<hr class="line-dashed line-full"/>
			        			<div class="form-group">
				        		  <label for="game_short_desc" class="col-sm-2 control-label">Game Short Desc</label>
                                <div class="col-sm-10"><textarea cols="80" rows="5" name="game_short_desc" id="game_short_desc" class="form-control"></textarea></div>
                                </div>
			        			<hr class="line-dashed line-full"/>
			        			<div class="form-group">
                  				<label for="game_instruction_video" class="col-sm-2 control-label">Game Instruction Video</label>
		                                <div class="col-sm-10"><input type="text" name="game_instruction_video" id="game_instruction_video" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="game_position" class="col-sm-2 control-label">Game Position</label>
		                                <div class="col-sm-10"><input type="text" name="game_position" id="game_position" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="game_url" class="col-sm-2 control-label">Game Url</label>
		                                <div class="col-sm-10"><input type="text" name="game_url" id="game_url" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="game_layout" class="col-sm-2 control-label">Game Layout</label>
		                                <div class="col-sm-10">
		                                	<select name="game_layout" id="game_layout" class="form-control">
		                                		{{getGameLayout()}}
		                                	</select>
		                                </div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="status" class="col-sm-2 control-label">Status</label>
		                                <div class="col-sm-10">
		                                	<select  name="status" id="status" class="form-control">
		                                		{{getStatuses()}}
		                                	</select>
		                                </div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					           {{generateLanguageDependentData("games")}}                             
                            <p class="stdformbutton">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="reset" class="btn">Reset</button>
                            </p>
                        </form>
                        <script src="{{asset('/ba/assets/js/common.js')}}"></script> 
                         <script type="text/javascript">
						  var arr_error_label = ['game_name','logo','banner_image','game_instruction_video','game_position','game_url','game_layout','status',];
						   if (typeof(arr_error_label) !== 'undefined') 
						    {addErrorLabel(arr_error_label);} 
						 </script>  
                    </div>
                    </div>
                    </div>
                    </div>