
		   <div class="row">
		    <div class="col-md-12">
		    <div class="panel panel-default panel-filled">
                <div class="panel-heading">
                  <h3 class="panel-title custom-font">Game Detail</h3>
                </div>
                <div class="panel-body">
		   
            	 <div class="table-responsive">
                <table class="table mb-0" id=""><tbody>     <tr>
                          <td class="width30">Game Name</td>
                          <td class="width70"><strong>{{$games->game_name}}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Logo</td>
                          <td class="width70"><strong><img class="lazy" data-src="{{$games->logo}}"" width="100%"/></strong></td>
                      </tr>     <tr>
                          <td class="width30">Banner Image</td>
                          <td class="width70"><strong><img class="lazy" data-src="{{$games->banner_image}}" width="100%" /></td>
                      </tr>     <tr>
                          <td class="width30">Game Long Desc</td>
                          <td class="width70">{!!$games->game_long_desc!!}</td>
                      </tr>     <tr>
                          <td class="width30">Game Short Desc</td>
                          <td class="width70">{!!$games->game_short_desc!!}</td>
                      </tr>     <tr>
                          <td class="width30">Game Instruction Video</td>
                          <td class="width70">{!!$games->game_instruction_video!!}</td>
                      </tr>     <tr>
                          <td class="width30">Game Position</td>
                          <td class="width70"><strong>{{$games->game_position}}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Game Url</td>
                          <td class="width70"><strong>{{$games->game_url}}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Game Layout</td>
                          <td class="width70"><strong>{{$games->game_layout}}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Status</td>
                          <td class="width70"><strong>{{$games->status}}</strong></td>
                      </tr>
                  </tbody></table>
                </div>
                </div>
                </div>
                </div>
                </div>
                     <script src="{{asset('/ba/assets/js/common.js')}}"></script> 