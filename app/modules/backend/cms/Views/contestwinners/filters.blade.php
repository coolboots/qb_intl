
		   <div class="row">
		    <div class="col-md-12">
 <section class="tile filterblock">
 <div class="tile-header dvd dvd-btm">
              <h1 class="custom-font">Contestwinners filter
              </h1>
             </div>
            <div class="tile-body">
              <form class="form-inline" role="form" id="filterForm" action="{{url('/'.getCurrentUrlPrefix())}}">
                
              <div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="scontest_id" name="contest_id" value="{{Request::get("contest_id")}}" placeholder="Enter Contest Id" title="Contest Id">
		                </div><div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="suser_id" name="user_id" value="{{Request::get("user_id")}}" placeholder="Enter User Id" title="User Id">
		                </div><div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="sposition" name="position" value="{{Request::get("position")}}" placeholder="Enter Position" title="Position">
		                </div><div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="searned_prize" name="earned_prize" value="{{Request::get("earned_prize")}}" placeholder="Enter Earned Prize" title="Earned Prize">
		                </div>
                    <button type="submit" class="btn btn-primary">Search</button>
                    <button type="reset" class="btn btn-default">Reset</button>
              </form>
            </div>
 </section>

 </div>
 </div>