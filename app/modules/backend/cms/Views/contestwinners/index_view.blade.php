 @include('cms::contestwinners.filters')
		     <div class="row">
		     <div class="col-md-12">
 				<section class="tile">
 					<div class="tile-header dvd dvd-btm"><h1 class="custom-font">View All Contestwinners</h1></div>
            			<div class="tile-body">
		     		
			            	 <div class="table-responsive">
			                <table class="table mb-0" id="">
			                   
			                    <thead>
			                        <tr>
			                          	<!-- <th class="head0 nosort"><input type="checkbox" class="checkall" /></th> -->
			                          	<th class="head0 sortby" sortby="game_id">Game</th>
			                          	<th class="head0 sortby" sortby="contest_id">Contest</th>
						        <th class="head0 sortby" sortby="user_id">User</th>
						        <th class="head0 sortby" sortby="position">Position</th>
						        <th class="head0 sortby" sortby="earned_prize">Earned Prize</th>
						        <th class="head0 sortby" sortby="score">Score</th>
						        <th class="head0 sortby" sortby="score">Date</th>
						        <th class="head0">Actions</th>
			                           
			                        </tr>
			                    </thead>
			                    <tbody>
			                     @foreach($contest_winners as $key => $value)
						        <tr>
						        	<!-- <td class="aligncenter"><span class="center">
			                            <input type="checkbox" actionid={{ $value->id }} />
			                          </span></td> -->
			                          <td>{{ $value->game->game_name }}</td>
			                          <td>{{ $value->contest->contest_title }}</td>
							        <td>{{ $value->user->name }}</td>
							        <td>{{ $value->position }}</td>
							        <td>{{ $value->earned_prize }}</td>
							        <td>{{ $value->score }}</td>
							        <td>{{ $value->created_at }}</td>
							            
						           <!-- we will also add show, edit, and delete buttons -->
						            <td>

						              <!--  <a class="btn btn-small btn-success" href="{{ URL::to(getCurrentUrlPrefix() .'/'. $value->id) }}">Show</a>

						                <a class="btn btn-small btn-info" href="{{ URL::to(getCurrentUrlPrefix() .'/'. $value->id . '/edit') }}">Edit</a> -->

						                <a class="btn btn-small btn-warning delete" data-token="{{ csrf_token() }}" deleteid="{{$value->id}}"  url="{{url(getCurrentUrlPrefix())}}">Delete</a>

						            </td>
						        </tr>
						    @endforeach
			                    </tbody>
			                </table>
			                </div>
			                </div>
			                </section>
			                </div>
			               </div>
			                <div class="tile-footer dvd dvd-top">
			              <div class="row">
			                <!-- <div class="col-sm-3 hidden-xs">
			                  <select class="input-sm form-control w-sm inline bulk_action_type">
			                    <option value="">Select Action</option>
			                    <option value="1">Mark Active</option>
			                    <option value="2">Mark Inactive</option>
			                    <option value="3">Mark Deleted</option>
			                    
			                  </select>
			                  <button class="btn btn-sm btn-default br-3 bulk_action_trigger">Apply</button>
			                </div> -->
			                <div class="col-sm-4 text-left"><small class="text-muted">Total {{$contest_winners->total()}} records found!</small></div>
			                <div class="col-sm-5 text-right">
			                 <ul class="pagination pagination-sm m-0">
			                <?php echo $contest_winners->render(); ?>
			            	</ul>
			                 
			                </div>
			              </div>
			            </div>
			            <script src="{{asset('/ba/assets/js/common.js')}}"></script> 