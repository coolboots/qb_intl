
<div class="row">
		    <div class="col-md-8 col-md-offset-2">
		    <div class="panel panel-default panel-filled">
                <div class="panel-heading">
                  <h3 class="panel-title custom-font">Edit Seourl</h3>
                </div>
                <div class="panel-body">

<form class="form-horizontal" action="{{ url('/'.getCurrentUrlPrefix().'/'.$cmsseourls->id) }}" method="POST" enctype="multipart/form-data" id="editForm">
            {{ method_field('PUT') }} 
            <input type="hidden" name="_token" value="{{ csrf_token() }}"><div class="form-group">
                  				<label for="object_type" class="col-sm-2 control-label">Object Type</label>
		                                <div class="col-sm-10"><input type="text" name="object_type" id="object_type" value="{{$cmsseourls->object_type}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="object_id" class="col-sm-2 control-label">Object Id</label>
		                                <div class="col-sm-10"><input type="text" name="object_id" id="object_id" value="{{$cmsseourls->object_id}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="object_data" class="col-sm-2 control-label">Object Data</label>
		                                <div class="col-sm-10"><input type="text" name="object_data" id="object_data" value="{{$cmsseourls->object_data}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					           {{generateLanguageDependentData("cms_seo_urls")}}                             
                            <p class="stdformbutton">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="reset" class="btn">Reset</button>
                            </p>
                        </form>
                        <script src="{{asset('/ba/assets/js/common.js')}}"></script> 
                         <script type="text/javascript">
						  var arr_error_label = ['object_type','object_id','object_data',];
						   if (typeof(arr_error_label) !== 'undefined') 
						    {addErrorLabel(arr_error_label);} 
						 </script>  
                   </div>
                   </div>
                   </div>
                   </div>
                    