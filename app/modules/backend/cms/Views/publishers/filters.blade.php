
		   <div class="row">
		    <div class="col-md-12">
 <section class="tile filterblock">
 <div class="tile-header dvd dvd-btm">
              <h1 class="custom-font">Publishers filter
              </h1>
             </div>
            <div class="tile-body">
              <form class="form-inline" role="form" id="filterForm" action="{{url('/'.getCurrentUrlPrefix())}}">
                
              <div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="spublisher_name" name="publisher_name" value="{{Request::get("publisher_name")}}" placeholder="Enter Publisher Name" title="Publisher Name">
		                </div><div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="spublisher_code" name="publisher_code" value="{{Request::get("publisher_code")}}" placeholder="Enter Publisher Code" title="Publisher Code">
		                </div><div class="form-group col-md-2">
                     <select  name="status" id="sstatus" class="form-control" changedValue="{{Request::get("status")}}">
                                        {{getStatuses()}}
                                      </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Search</button>
                    <button type="reset" class="btn btn-default">Reset</button>
              </form>
            </div>
 </section>

 </div>
 </div>