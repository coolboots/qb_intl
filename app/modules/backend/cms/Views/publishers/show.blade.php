
		   <div class="row">
		    <div class="col-md-8 col-md-offset-2">
		    <div class="panel panel-default panel-filled">
                <div class="panel-heading">
                  <h3 class="panel-title custom-font">Publisher Detail</h3>
                </div>
                <div class="panel-body">
		   
            	 <div class="table-responsive">
                <table class="table mb-0" id=""><tbody>     <tr>
                          <td class="width30">Publisher Name</td>
                          <td class="width70"><strong>{!!$publishers->publisher_name!!}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Publisher Code</td>
                          <td class="width70"><strong>{!!$publishers->publisher_code!!}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Descriptions</td>
                          <td class="width70"><strong>{!!$publishers->descriptions!!}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Scripts 1</td>
                          <td class="width70"><strong>{!!$publishers->scripts_1!!}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Scripts 2</td>
                          <td class="width70"><strong>{!!$publishers->scripts_2!!}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Scripts 3</td>
                          <td class="width70"><strong>{!!$publishers->scripts_3!!}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Scripts 4</td>
                          <td class="width70"><strong>{!!$publishers->scripts_4!!}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Scripts 5</td>
                          <td class="width70"><strong>{!!$publishers->scripts_5!!}</strong></td>
                      </tr>
                        <tr>
                            <td class="width30">Status</td>
                            <td class="width70"><strong>{!!$publishers->status!!}</strong></td>
                        </tr>
                        <tr>
                            <td class="width30">Ads Enable</td>
                            <td class="width70"><strong>{{$publishers->ads_enabled}}</strong></td>
                        </tr>
                  </tbody></table>
                </div>
                </div>
                </div>
                </div>
                </div>
                