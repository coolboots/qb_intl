
		    <div class="row">
		    <div class="col-md-8 col-md-offset-2">
		    <div class="panel panel-default panel-filled">
                <div class="panel-heading">
                  <h3 class="panel-title custom-font">Add New Page</h3>
                </div>
                <div class="panel-body">
		   
		    <form class="form-horizontal" role="form" method="post" id="createForm" action="{{url('/'.getCurrentUrlPrefix())}}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                  				<label for="page_name" class="col-sm-2 control-label">Page Name</label>
		                                <div class="col-sm-10"><input type="text" name="page_name" id="page_name" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="page_code" class="col-sm-2 control-label">Page Code</label>
		                                <div class="col-sm-10"><input type="text" name="page_code" id="page_code" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                                <label for="image" class="col-sm-2 control-label">Image</label>
                                        <div class="col-sm-10"><input type="file" id="image" name="image" class="form-control"></div>
                                        
                                        </div>
                            <hr class="line-dashed line-full"/>
                            <div class="form-group">
                                <label for="status" class="col-sm-2 control-label">Status</label>
                                        <div class="col-sm-10">
                                            <select  name="status" id="status" class="form-control">
                                                {{getStatuses()}}
                                            </select>
                                            </div>
                                        </div>
					        <hr class="line-dashed line-full"/>
					           <p class="stdformbutton">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="reset" class="btn">Reset</button>
                            </p>
                        </form>
                        <script src="{{asset('/ba/assets/js/common.js')}}"></script> 
                         <script type="text/javascript">
						  var arr_error_label = ['page_name','page_code','image','status',];
						   if (typeof(arr_error_label) !== 'undefined') 
						    {addErrorLabel(arr_error_label);} 
						 </script>  
                    </div>
                    </div>
                    </div>
                    </div>