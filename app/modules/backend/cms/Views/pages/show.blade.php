
		   <div class="row">
		    <div class="col-md-8 col-md-offset-2">
		    <div class="panel panel-default panel-filled">
                <div class="panel-heading">
                  <h3 class="panel-title custom-font">Page Detail</h3>
                </div>
                <div class="panel-body">
		   
            	 <div class="table-responsive">
                <table class="table mb-0" id=""><tbody>     <tr>
                          <td class="width30">Page Name</td>
                          <td class="width70"><strong>{{$pages->page_name}}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Page Code</td>
                          <td class="width70"><strong>{{$pages->page_code}}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Image</td>
                          <td class="width70"><strong>{{$pages->image}}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Status</td>
                          <td class="width70"><strong>{{$pages->status}}</strong></td>
                      </tr>
                  </tbody></table>
                </div>
                </div>
                </div>
                </div>
                </div>
                