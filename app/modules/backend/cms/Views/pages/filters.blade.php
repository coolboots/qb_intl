
		   <div class="row">
		    <div class="col-md-12">
 <section class="tile filterblock">
 <div class="tile-header dvd dvd-btm">
              <h1 class="custom-font">Pages filter
              </h1>
             </div>
            <div class="tile-body">
              <form class="form-inline" role="form" id="filterForm" action="{{url('/'.getCurrentUrlPrefix())}}">
                
              <div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="spage_name" name="page_name" value="{{Request::get("page_name")}}" placeholder="Enter Page Name" title="Page Name">
		                </div><div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="spage_code" name="page_code" value="{{Request::get("page_code")}}" placeholder="Enter Page Code" title="Page Code">
                    </div>
                    <div class="form-group col-md-2">
                      <select  name="status" id="sstatus" class="form-control" changedValue="{{Request::get("status")}}">
                        {{getStatuses()}}
                      </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Search</button>
                    <button type="reset" class="btn btn-default">Reset</button>
              </form>
            </div>
 </section>

 </div>
 </div>