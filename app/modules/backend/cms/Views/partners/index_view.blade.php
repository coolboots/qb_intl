 @include('cms::partners.filters')
    <div class="row">
        <div class="col-md-12">
            <section class="tile">
                <div class="tile-header dvd dvd-btm"><h1 class="custom-font">View All Partners</h1></div>
                <div class="tile-body">
                    <div class="table-responsive">
                        <table class="table mb-0" id="">
                            <thead>
                                <tr>
                                    <th class="head0 nosort"><input type="checkbox" class="checkall" /></th>
                                    <th class="head0 sortby" sortby="partner_name">Partner Name</th>
                                    <th class="head0 sortby" sortby="logo">Logo</th>
                                    <th class="head0 sortby" sortby="allocated_domain">Allocated Domain</th>
                                    <th class="head0 sortby" sortby="status">Status</th>
                                    <th class="head0 sortby" sortby="ip_blocked">Ip Blocked</th>
                                    <th class="head0 sortby" sortby="ads_enabled">Ads Enabled</th>
                                    <th class="head0">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($partners as $key => $value)
                                    <tr>
                                        <td class="aligncenter">
                                            <span class="center"><input type="checkbox" actionid={{ $value->id }} /></span>
                                        </td>
                                        <td>{{ $value->partner_name }}</td>
                                        <td><img class="lazy" data-src="{{ asset($value->logo) }}" width="100px"/></td>
                                        <td>{{ $value->allocated_domain }}</td>
                                        <td>{{ $value->status }}</td>
                                        <td>{{ $value->ip_blocked }}</td>
                                        <td>{{ $value->ads_enabled }}</td>
                                        <!-- we will also add show, edit, and delete buttons -->
                                        <td>
                                            <a class="btn btn-small btn-success" href="{{ URL::to(getCurrentUrlPrefix() .'/'. $value->id) }}" storage="modal">Show</a>
                                            <a class="btn btn-small btn-info" href="{{ URL::to(getCurrentUrlPrefix() .'/'. $value->id . '/edit') }}">Edit</a>
                                            <a class="btn btn-small btn-warning delete" data-token="{{ csrf_token() }}" deleteid="{{$value->id}}"  url="{{url(getCurrentUrlPrefix())}}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <div class="tile-footer dvd dvd-top">
        <div class="row">
            <div class="col-sm-3 hidden-xs">
                <select class="input-sm form-control w-sm inline bulk_action_type">
                    <option value="">Select Action</option>
                    <option value="1">Mark Active</option>
                    <option value="2">Mark Inactive</option>
                    <option value="3">Mark Deleted</option>
                </select>
                <button class="btn btn-sm btn-default br-3 bulk_action_trigger">Apply</button>
            </div>
            <div class="col-sm-4 text-left"><small class="text-muted">Total {{$partners->total()}} records found!</small></div>
            <div class="col-sm-5 text-right">
                <ul class="pagination pagination-sm m-0">
                    <?php echo $partners->render(); ?>
                </ul>
            </div>
        </div>
    </div>
<script src="{{asset('/ba/assets/js/common.js')}}"></script>