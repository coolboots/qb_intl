
		   <div class="row">
		    <div class="col-md-12">
 <section class="tile filterblock">
 <div class="tile-header dvd dvd-btm">
              <h1 class="custom-font">Partners filter
              </h1>
             </div>
            <div class="tile-body">
              <form class="form-inline" role="form" id="filterForm" action="{{url('/'.getCurrentUrlPrefix())}}">
                
              <div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="spartner_name" name="partner_name" value="{{Request::get("partner_name")}}" placeholder="Enter Partner Name" title="Partner Name">
		                </div><div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="sallocated_domain" name="allocated_domain" value="{{Request::get("allocated_domain")}}" placeholder="Enter Allocated Domain" title="Allocated Domain">
		                </div>
		                <div class="form-group col-md-2">
			              <select  name="default_language" id="sdefault_language" class="form-control chosen-select" changedValue="{{Request::get("default_language")}}">
			                    {!!getLanguages()!!}
			                </select>

			            </div>
		                <div class="form-group col-md-2">
		                 <select  name="status" id="sstatus" class="form-control" changedValue="{{Request::get("status")}}">
		                                		{{getStatuses()}}
		                                	</select>
		                </div>
		                <div class="form-group col-md-2">
		                 <select  name="ads_enabled" id="sads_enabled" class="form-control" changedValue="{{Request::get("ads_enabled")}}">
		                 	<option value="" >Ads Enabled</option>
                    		<option value=1 >Enable</option>
                            <option value=0 >Disable</option>
                    	</select>
		                </div>
                    <button type="submit" class="btn btn-primary">Search</button>
                    <button type="reset" class="btn btn-default">Reset</button>
              </form>
            </div>
 </section>

 </div>
 </div>