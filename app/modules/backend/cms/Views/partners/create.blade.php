
		    <div class="row">
		    <div class="col-md-8 col-md-offset-2">
		    <div class="panel panel-default panel-filled">
                <div class="panel-heading">
                  <h3 class="panel-title custom-font">Add New Partner</h3>
                </div>
                <div class="panel-body">
		   
		    <form class="form-horizontal" role="form" method="post" id="createForm" action="{{url('/'.getCurrentUrlPrefix())}}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                  				<label for="partner_name" class="col-sm-2 control-label">Partner Name</label>
		                                <div class="col-sm-10"><input type="text" name="partner_name" id="partner_name" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                                <label for="logo" class="col-sm-2 control-label">Logo</label>
                                        <div class="col-sm-10"><input type="file" id="logo" name="logo" class="form-control"></div>
                                        
                                        </div>
                             <hr class="line-dashed line-full"/>
                            <div class="form-group">
                                <label for="image" class="col-sm-2 control-label">Image</label>
                                        <div class="col-sm-10"><input type="file" id="image" name="image" class="form-control"></div>
                                        
                                        </div>
                            <hr class="line-dashed line-full"/><div class="form-group">
                  				<label for="allocated_domain" class="col-sm-2 control-label">Allocated Domain</label>
		                                <div class="col-sm-10"><input type="text" name="allocated_domain" id="allocated_domain" class="form-control"></div>
		                                </div>

                            <hr class="line-dashed line-full" />
                            <div class="form-group">
                               <label for="view_id" class="col-sm-2 control-label">View Id</label>
                               <div class="col-sm-10"><input type="text" name="view_id" id="view_id" class="form-control"></div>
                            </div>

                            <hr class="line-dashed line-full"/><div class="form-group">
                                <label for="package_id" class="col-sm-2 control-label">Package ID</label>
                                        <div class="col-sm-10"><input type="text" name="package_id" id="package_id" class="form-control"></div>
                                        </div>
                            <hr class="line-dashed line-full"/><div class="form-group">
                                <label for="ga_id" class="col-sm-2 control-label">GA ID</label>
                                        <div class="col-sm-10"><input type="text" name="ga_id" id="ga_id" class="form-control"></div>
                                        </div>
                            <hr class="line-dashed line-full"/><div class="form-group">
                                <label for="ga_mesurment_id" class="col-sm-2 control-label">GA Measurement ID</label>
                                <div class="col-sm-10"><input type="text" name="ga_measurement_id" id="ga_measurement_id" class="form-control"></div>
                            </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                                <label for="scripts_1" class="col-sm-2 control-label">Scripts 1</label>
                                <div class="col-sm-10"><textarea cols="80" rows="5" name="scripts_1" id="scripts_1" class="form-control ignoreeditor"></textarea></div>
                            </div>
                            <hr class="line-dashed line-full"/>
                            <div class="form-group">
                                <label for="scripts_2" class="col-sm-2 control-label">Scripts 2</label>
                                <div class="col-sm-10"><textarea cols="80" rows="5" name="scripts_2" id="scripts_2" class="form-control ignoreeditor"></textarea></div>
                            </div>
                            <hr class="line-dashed line-full"/>
                            <div class="form-group">
                                <label for="ip_blocked" class="col-sm-2 control-label">IP Blocked</label>
                                <div class="col-sm-10"><textarea cols="80" rows="5" name="ip_blocked" id="ip_blocked" class="form-control ignoreeditor"></textarea></div>
                            </div>	
                            <hr class="line-dashed line-full"/>
                            <div class="form-group">
                                <label for="mail_to" class="col-sm-2 control-label">Mail To</label>
                                <div class="col-sm-10"><textarea cols="80" rows="5" name="mail_to" id="mail_to" class="form-control ignoreeditor"></textarea></div>
                            </div>	
                            <hr class="line-dashed line-full"/>
                            <div class="form-group">
                                <label for="mail_cc" class="col-sm-2 control-label">Mail Cc</label>
                                <div class="col-sm-10"><textarea cols="80" rows="5" name="mail_cc" id="mail_cc" class="form-control ignoreeditor"></textarea></div>
                            </div>	
                            <hr class="line-dashed line-full"/>
                            <div class="form-group">
                                <label for="mail_bcc" class="col-sm-2 control-label">Mail Bcc</label>
                                <div class="col-sm-10"><textarea cols="80" rows="5" name="mail_bcc" id="mail_bcc" class="form-control ignoreeditor"></textarea></div>
                            </div>	
                            <hr class="line-dashed line-full"/>
                            <div class="form-group">
                                <label for="default_language" class="col-sm-2 control-label">Default Language</label>
                                <div class="col-sm-10">
                                    <select  name="default_language" id="default_language" class="form-control chosen-select">
                                        {!!getLanguages()!!}
                                    </select>
                                </div>
                            </div>
                            <hr class="line-dashed line-full"/>
                            <div class="form-group">
                                <label for="supported_languages" class="col-sm-2 control-label">Supported Languages</label>
                                <div class="col-sm-10">
                                    <select  name="supported_languages[]" id="supported_languages" class="form-control chosen-select" multiple>
                                        {!!getLanguages()!!}
                                    </select>
                                </div>
                            </div>

                            <hr class="line-dashed line-full"/>
                            <div class="form-group">
                                <label for="webhook" class="col-sm-2 control-label">Webhook</label>
                                <div class="col-sm-10"><input type="text" name="webhook" id="webhook" class="form-control"></div>
                            </div>

                            <hr class="line-dashed line-full"/>
                            <div class="form-group">
                                <label for="webhook_api_key" class="col-sm-2 control-label">Webhook API Key</label>
                                <div class="col-sm-10"><input type="text" name="webhook_api_key" id="webhook_api_key" class="form-control"></div>
                            </div>

                            <hr class="line-dashed line-full" />
                            <div class="form-group">
                                <label for="webhook_api_method" class="col-sm-2 control-label">Webhook Api Method</label>
                                <div class="col-sm-10">
                                    <select name="webhook_api_method" id="webhook_api_method" class="form-control">
                                        <option value="POST">POST</option>
                                        <option value="GET">GET</option>
                                    </select>
                                </div>
                            </div>

                            <hr class="line-dashed line-full" />
                            <div class="form-group">
                                <label for="is_encrypted_apikey" class="col-sm-2 control-label">Is encrypted Api Key</label>
                                <div class="col-sm-10">
                                    <select name="is_encrypted_apikey" id="is_encrypted_apikey" class="form-control">
                                        <option value="False">False</option>
                                        <option value="True" >True</option>
                                    </select>
                                </div>
                            </div>

                            <hr class="line-dashed line-full"/>
                            <div class="form-group">
                                <label for="webhook_callback_attempts" class="col-sm-2 control-label">Webhook Callback Attempts</label>
                                <div class="col-sm-10"><input type="text" name="webhook_callback_attempts" id="webhook_callback_attempts" class="form-control"></div>
                            </div>

                            <hr class="line-dashed line-full"/>
                             <div class="form-group">
                                <label for="payloads_key" class="col-sm-2 control-label">Payloads key</label>
                                <div class="col-sm-10"><input type="text" name="payloads_key" id="payloads_key" class="form-control"></div>
                            </div>
                            
                            <hr class="line-dashed line-full"/>
                            <div class="form-group">
                                <label for="max_click_per_day" class="col-sm-2 control-label">Max Click PerDay</label>
                                <div class="col-sm-10"><input type="text" name="max_click_per_day" id="max_click_per_day" class="form-control"></div>
                            </div>
                            <hr class="line-dashed line-full"/>
                            <div class="form-group">
                                <label for="blocked_hours_per_day" class="col-sm-2 control-label">Blocked Hours PerDay</label>
                                <div class="col-sm-10"><input type="text" name="blocked_hours_per_day" id="blocked_hours_per_day" class="form-control"></div>
                            </div>
                            <hr class="line-dashed line-full"/>
                             <div class="form-group">
                            <label for="inapp_allowed" class="col-sm-2 control-label">In App Allowed</label>
                                    <div class="col-sm-10">
                                        <select  name="inapp_allowed" id="inapp_allowed" class="form-control">
                                            <option value="0">No</option>
                                            <option value="1" selected>Yes</option>
                                        </select>
                                        </div>
                                    </div>

                            <hr class="line-dashed line-full"/>
                                 <div class="form-group">
                                <label for="status" class="col-sm-2 control-label">Status</label>
                                        <div class="col-sm-10">
                                            <select  name="status" id="status" class="form-control">
                                                {{getStatuses()}}
                                            </select>
                                            </div>
                                        </div>
                            <hr class="line-dashed line-full"/>
                            <div class="form-group">
                                <label for="ads_enabled" class="col-sm-2 control-label">Ads Enabled</label>
                                <div class="col-sm-10">
                                    <select  name="ads_enabled" id="ads_enabled" class="form-control">
                                        <option value=1 >Enable</option>
                                        <option value=0 >Disable</option>
                                    </select>
                                </div>
                            </div>
                            <hr class="line-dashed line-full"/>
                            <div class="form-group">
                                <label for="ads_tracker_enable" class="col-sm-2 control-label">Ads Tracker Enabled</label>
                                <div class="col-sm-10">
                                    <select  name="ads_tracker_enable" id="ads_tracker_enable" class="form-control">
                                        <option value=1 >Enable</option>
                                        <option value=0 >Disable</option>
                                    </select>
                                </div>
                            </div>
                            
                            <hr class="line-dashed line-full"/>
					           <p class="stdformbutton">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="reset" class="btn">Reset</button>
                            </p>
                        </form>
                        <script src="{{asset('/ba/assets/js/common.js')}}"></script> 
                         <script type="text/javascript">
						  var arr_error_label = ['partner_name','allocated_domain','view_id','ga_id','status',];
						   if (typeof(arr_error_label) !== 'undefined') 
						    {addErrorLabel(arr_error_label);} 
						 </script>  
                    </div>
                    </div>
                    </div>
                    </div>