<?php 
$language = \Config::get("qureka.language");
$inAppAllowed = array('0'=>'No', '1'=>'Yes');
?>
		   <div class="row">
		    <div class="col-md-12">
		    <div class="panel panel-default panel-filled">
                <div class="panel-heading">
                  <h3 class="panel-title custom-font">Partner Detail</h3>
                </div>
                <div class="panel-body">
		   
            	 <div class="table-responsive">
                <table class="table mb-0" id=""><tbody>     <tr>
                          <td class="width30">Partner Name</td>
                          <td class="width70"><strong>{{$partners->partner_name}}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Logo</td>
                          <td class="width70"><img class="lazy" data-src="{{ asset($partners->logo) }}" width="100px"/></td>
                      </tr>     <tr>
                          <td class="width30">Allocated Domain</td>
                          <td class="width70"><strong>{{$partners->allocated_domain}}</strong></td>
                      </tr>
                      <tr>
                        <td class="width30">View Id</td>
                        <td class="width70"><strong>{{@$partners->view_id}}</strong></td>
                    </tr>
                        <tr>
                          <td class="width30">GA ID</td>
                          <td class="width70"><strong>{{$partners->ga_ids}}</strong></td>
                      </tr>
                        <tr>
                            <td class="width30">Scripts 1</td>
                            <td class="width70"><strong>{{$partners->scripts_1}}</strong></td>
                        </tr>
                        <tr>
                            <td class="width30">IP Blocked</td>
                            <td class="width70"><strong>{{$partners->ip_blocked}}</strong></td>
                        </tr>
                        <tr>
                            <td class="width30">Default Language</td>
                            <td class="width70"><strong>{{@$language[$partners->default_language]}}</strong></td>
                        </tr>
                        <tr>
                            <td class="width30">Supported Languages</td>
                            <td class="width70"><strong>{{$partners->supported_language}}</strong></td>
                        </tr>

                        <tr>
                            <td class="width30">Webhook</td>
                              <td class="width70"><strong>{{$partners->webhook}}</strong></td>
                          </tr>
                          <tr>
                              <td class="width30">Webhook API Key</td>
                              <td class="width70"><strong>{{$partners->webhook_api_key}}</strong></td>
                          </tr>
                          
                        <tr>
                            <td class="width30">Mail To</td>
                            <td class="width70"><strong>{{@$partner_details->mail_to}}</strong></td>
                        </tr>
                        <tr>
                            <td class="width30">Mail Cc</td>
                            <td class="width70"><strong>{{@$partner_details->mail_cc}}</strong></td>
                        </tr>
                        <tr>
                            <td class="width30">Mail Bcc</td>
                            <td class="width70"><strong>{{@$partner_details->mail_bcc}}</strong></td>
                        </tr>
                        <tr>
                            <td class="width30">In App Allowed</td>
                            <td class="width70"><strong>{{@$inAppAllowed[$partners->inapp_allowed]}}</strong></td>
                        </tr>
                        <tr>
                            <td class="width30">Max Click Per Day</td>
                            <td class="width70"><strong>{{@$partner_details->max_click_per_day}}</strong></td>
                          </tr>
                          <tr>
                            <td class="width30">Blocked Hours Per Day</td>
                            <td class="width70"><strong>{{@$partner_details->blocked_hours_per_day}}</strong></td>
                          </tr>
                        <tr>
                            <td class="width30">Ads Enabled</td>
                            <td class="width70"><strong>{{$partners->ads_enabled}}</strong></td>
                        </tr>
                        <tr>
                            <td class="width30">Ads Tracker Enabled</td>
                            <td class="width70"><strong>{{@$partner_details->ads_tracker_enable}}</strong></td>
                          </tr>
                        <tr>
                          <td class="width30">Status</td>
                          <td class="width70"><strong>{{$partners->status}}</strong></td>
                        </tr>
                  </tbody></table>
                </div>
                </div>
                </div>
                </div>
                </div>
                <script src="{{asset('/ba/assets/js/common.js')}}"></script> 
                