<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default panel-filled">
            <div class="panel-heading">
                <h3 class="panel-title custom-font">Clone Contest</h3>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" action="{{ url('/'.getCurrentUrlPrefix().'/clone/save') }}" method="POST" enctype="multipart/form-data" id="editForm">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="contest_id" value="{{$contests->id}}">
                    <div class="form-group">
                        <label for="game_id" class="col-sm-2 control-label">Category</label>
                        <div class="col-sm-10">
                                        <select  name="category_id" id="category_id" class="form-control chosen-select" changedValue="{{$contests->category_id}}">
                                        <option value="">Select Category</option>
                                          {!!parentChildDropdown(buildTree(getCategories()))!!}
                                      </select>
                                        </div>
                    </div><hr class="line-dashed line-full"/>
                    <div class="form-group">
                        <label for="contest_title" class="col-sm-2 control-label">Contest Title</label>
                        <div class="col-sm-10"><input type="text" name="contest_title" id="contest_title" value="{{$contests->contest_title}}" class="form-control title"></div>
                    </div><hr class="line-dashed line-full"/>
                    <!-- {!!seoUrlsFieldEdit($contests->id,"contest")!!} -->
                    <!-- <hr class="line-dashed line-full"/> -->
                    <!-- <div class="form-group">
                        <label for="contest_image" class="col-sm-2 control-label">Contest Image</label>
                        <div class="col-sm-10"><input type="file" name="contest_image" id="contest_image" value="{{$contests->contest_image}}" class="form-control">
                            @if($contests->contest_image)
                                <img src="{{asset($contests->contest_image)}}" width="100px" />
                            @endif
                        </div>
                    </div><hr class="line-dashed line-full"/> -->
                    <div class="form-group">
                                <label for="entry_fee_type" class="col-sm-2 control-label">Entry Fee Type</label>
                                        <div class="col-sm-10">
                                     <select  name="entry_fee_type" id="entry_fee_type" class="form-control chosen-select" changedValue="{{$contests->entry_fee_type}}">
                                        {!!getEntryFeeTypes()!!}
                                     </select>
                                        </div>
                                        </div>
                            <hr class="line-dashed line-full"/>
                            <div class="form-group">
                                <label for="entry_fee" class="col-sm-2 control-label">Entry Fee</label>
                                        <div class="col-sm-10"><input type="text" name="entry_fee" id="entry_fee" value="{{$contests->entry_fee}}" class="form-control"></div>
                                        </div>
                            <hr class="line-dashed line-full"/>
                            <div class="form-group">
                                <label for="contest_block_period" class="col-sm-2 control-label">Contest Block Period</label>
                                        <div class="col-sm-10"><input type="text" name="contest_block_period" id="contest_block_period" value="{{$contests->contest_block_period}}" class="form-control"></div>
                                        </div>
                            <hr class="line-dashed line-full"/>
                            <div class="form-group">
                                <label for="prize_type" class="col-sm-2 control-label">Prize Type</label>
                                        <div class="col-sm-10">
                                            <select  name="prize_type" id="prize_type" class="form-control chosen-select" changedValue="{{$contests->prize_type}}">
                                               {!!getPrizeTypes()!!}
                                            </select>
                                        </div>
                                        </div>
                            <hr class="line-dashed line-full"/>
                            <div class="form-group">
                                <label for="prize_money" class="col-sm-2 control-label">Prize Money</label>
                                        <div class="col-sm-10"><input type="text" name="prize_money" id="prize_money" value="{{$contests->prize_money}}" class="form-control"></div>
                                        </div>
                            <hr class="line-dashed line-full"/>
                            <!-- <div class="form-group">
                                <label for="contest_position" class="col-sm-2 control-label">Contest Position</label>
                                        <div class="col-sm-10"><input type="text" name="contest_position" id="contest_position" value="{{$contests->contest_position}}" class="form-control"></div>
                                        </div>
                            <hr class="line-dashed line-full"/> -->
                    <div class="form-group">
                        <label for="start_time" class="col-sm-2 control-label">Start Time</label>
                        <div class="col-sm-10">
                            <div class="input-group datepicker w-360">
                            <input type="text" class="form-control" id="start_time" name="start_time" value="{{$contests->start_time}}"  data-format="Y-m-d H:i:s"/>
                            <span class="input-group-addon"> <span class="fa fa-calendar"></span> </span>
                            </div>
                        </div>
                    </div><hr class="line-dashed line-full"/>
                    <div class="form-group">
                        <label for="end_time" class="col-sm-2 control-label">End Time</label>
                        <div class="col-sm-10">
                            <div class="input-group datepicker w-360">
                                <input type="text" class="form-control" id="end_time" name="end_time" value="{{$contests->end_time}}" data-format="Y-m-d H:i:s" />
                                <span class="input-group-addon"> <span class="fa fa-calendar"></span> </span>
                            </div>
                        </div>
                    </div>
                    <hr class="line-dashed line-full"/>
                    <div class="form-group">
                        <label for="tags" class="col-sm-2 control-label">Tags</label>
                        <div class="col-sm-10">
                            <select  name="tags[]" id="tags" class="form-control chosen-select" changedValue="{{$contests->tags}}" multiple>
                               {!!getTagsTypes()!!}
                            </select>
                        </div>
                        </div>
                    <hr class="line-dashed line-full"/>
                    <div class="form-group">
                        <label for="status" class="col-sm-2 control-label">Status</label>
                        <div class="col-sm-10">
                            <select  name="status" id="status" class="form-control" changedValue="{{$contests->status}}">
                                {{getContestStatuses()}}
                            </select>
                        </div>
                    </div><hr class="line-dashed line-full"/>
                    <div class="form-group">
                        <input name="clone_matrix" id="clone_matrix" checked type="checkbox" value="true">
                        <label for="clone_matrix" class="col-sm-2 control-label">Clone Matrix</label>
                        <div class="col-sm-10">
                        </div>
                    </div><hr class="line-dashed line-full"/>

                    <p class="stdformbutton">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <button type="reset" class="btn">Reset</button>
                    </p>
                </form>

                <link rel="stylesheet" href="{{asset('/ba/assets/js/vendor/datetimepicker/css/bootstrap-datetimepicker.min.css')}}">
                <script src="{{asset('/ba/assets/js/vendor/datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script>
                <script src="{{asset('/ba/assets/js/common.js')}}"></script>
                <script type="text/javascript">
                    var arr_error_label = ['contest_title','entry_fee','contest_block_period','prize_money','start_time','end_time'];
                    if (typeof(arr_error_label) !== 'undefined')
                        {addErrorLabel(arr_error_label);}

                        if ($('.datepicker').length > 0) {
                            $('.datepicker').each(function() {
                            var element = $(this);
                            var format = element.data('format')
                            element.datetimepicker({
                                format:'YYYY-MM-DD HH:mm:ss',
                                sideBySide : true,
                            });
                            });
                        }


                        
                        $('#entry_fee_type').change(function()
                        {
                            var cval = $(this).val();
                            if(cval=='FREE')
                            {
                                $('#entry_fee').val(0);
                                $('#entry_fee').attr('readonly',true);
                            }
                            else
                            {
                                $('#entry_fee').val('');
                                $('#entry_fee').attr('readonly',false);
                            }
                        })

                        $( document ).ready(function() {
                            var cval = $('#entry_fee_type').val();
                            if(cval=='FREE')
                            {
                                $('#entry_fee').val(0);
                                $('#entry_fee').attr('readonly',true);
                            }else{
                                $('#entry_fee').attr('readonly',false);
                            }
                        });
                </script>
            </div>
        </div>
    </div>
</div>
