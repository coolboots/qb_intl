<?php $tagsList = \Config::get("qureka.tags"); 
    if(isset($categoryList[$contests->category_id])){
      $catDetails = $categoryList[$contests->category_id];
      $parentCategory = (isset($categoryList[$catDetails['parent_id']])) ? $categoryList[$catDetails['parent_id']] : '';
    }
?>
		   <div class="row">
		    <div class="col-md-12">
		    <div class="panel panel-default panel-filled">
                <div class="panel-heading">
                  <h3 class="panel-title custom-font">Contest Detail</h3>
                </div>
                <div class="panel-body">
		   
            	 <div class="table-responsive">
                <table class="table mb-0" id=""><tbody>     <tr>
                          <td class="width30">Category</td>
                          <td class="width70"><strong>
                            @if(!empty($contests->category_id) || ($contests->category_id != 0))
                                @if(isset($categoryList[$contests->category_id]))
                                  {{@$parentCategory['title']}} - {{@$categoryList[$contests->category_id]['title'] }}
                                 @endif
                              @endif
                          </strong></td>
                      </tr>     <tr>
                          <td class="width30">Contest Title</td>
                          <td class="width70"><strong>{{$contests->contest_title}}</strong></td>
                      </tr>     
                      <!-- <tr>
                          <td class="width30">Contest Image</td>
                          <td class="width70"><img src="{{ asset($contests->contest_image) }}" alt="" class="br-5 size-30x30"></td>
                      </tr>  --> 
                      <tr>
                          <td class="width30">Entry Fee Type</td>
                          <td class="width70"><strong>{{$contests->entry_fee_type}}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Entry Fee</td>
                          <td class="width70"><strong>{{$contests->entry_fee}}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Contest Block Period</td>
                          <td class="width70"><strong>{{$contests->contest_block_period}}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Prize Type</td>
                          <td class="width70"><strong>{{$contests->prize_type}}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Prize Money</td>
                          <td class="width70"><strong>{{$contests->prize_money}}</strong></td>
                      </tr>     
                      <!-- <tr>
                          <td class="width30">Contest Position</td>
                          <td class="width70"><strong>{{$contests->contest_position}}</strong></td>
                      </tr> -->
                       <tr>
                          <td class="width30">Start Time</td>
                          <td class="width70"><strong>{{$contests->start_time}}</strong></td>
                      </tr> <tr>
                          <td class="width30">End Time</td>
                          <td class="width70"><strong>{{$contests->end_time}}</strong></td>
                      </tr> <tr>
                          <td class="width30">Tags</td>
                          <td class="width70"><strong>
                            @if(!empty($contests->tags))
                              <?php $tags = explode(',', $contests->tags); ?>
                              @foreach($tags as $tag)
                              {{$tagsList[$tag]}}
                              @endforeach
                            @endif</strong></td>
                      </tr>     <tr>
                          <td class="width30">Status</td>
                          <td class="width70"><strong>{{$contests->status}}</strong></td>
                      </tr>   <!--   <tr>
                          <td class="width30">Total Users Played</td>
                          <td class="width70"><strong>{{$contests->total_users_played}}</strong></td>
                      </tr> -->
                  </tbody></table>
                  <hr class="line-dashed line-full"/>
                  <h3 class="panel-title custom-font"><strong>Contest Matrix</strong></h3>
                  <div class="matrixContainer">
                       <div class="row mt-10 matrix-row-heading">
                      <div class="col-xs-3">Winner From</div>
                      <div class="col-xs-3">Winner To</div>
                      <div class="col-xs-3">Prize Type</div>
                      <div class="col-xs-3">Prize Money</div>
                    </div>
                      
                    
                    
                      @if(!empty($matrix))
                        @foreach($matrix as $matrix)

                    <div class="row mt-10 matrix-row">
                      <div class="col-xs-3"><span><strong>{{$matrix->winner_from}}</strong></span></div>
                      <div class="col-xs-3"><span><strong>{{$matrix->winner_to}}</strong></span></div>
                      <div class="col-xs-3"><span><strong>{{$matrix->prize_type}}</strong></span></div>
                      <div class="col-xs-3"><span><strong>{{$matrix->prize_amount}}</strong></span></div>
                    
                    </div>
                      @endforeach
                      @endif

                      
                  </div>
                </div>
                </div>
                </div>
                </div>
                </div>
                