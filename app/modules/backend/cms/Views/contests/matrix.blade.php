
		    <div class="row">
		    <div class="col-md-12">
		    <div class="panel panel-default panel-filled">
                <div class="panel-heading">
                  <h3 class="panel-title custom-font">Matrix Configuration for {{$contests->contest_title}} contest</h3>
                </div>
                <div class="panel-body">

                	<span class="navigationlink">
					               &nbsp; &nbsp; &nbsp;<a class="btn btn-info add-new-matrix">Add New Row</a>&nbsp;  &nbsp;&nbsp; 
					              
					             
					             </span>
		   
		    <form class="form-horizontal" role="form" method="post" id="createForm" action="{{url('/'.getCurrentUrlPrefix().'/matrix/'.$contests->id)}}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="contest_id" value="{{$contests->id}}">
                    <div class="matrixContainer">
                    	 <div class="row mt-10 matrix-row-heading">
	                    <div class="col-xs-2">From</div>
	                    <div class="col-xs-2">To</div>
	                    <div class="col-xs-3">Prize Type</div>
	                    <div class="col-xs-3">Prize Money</div>
	                    <div class="col-xs-2">Action</div>
	                	</div>
	                    
	                  
                    
                    	@if(isset($contests->matrix) && !empty($contests->matrix))
                    		@foreach($contests->matrix as $k=>$matrix)
                    		
                    <div class="row mt-10 matrix-row">
	                    <div class="col-xs-2"><input name="matrix[{{$k}}]['winner_from']" type="text" class="form-control" placeholder="Winner from" value="{{$matrix->from}}" required></div>
	                    <div class="col-xs-2"><input name="matrix[{{$k}}]['winner_to']"  type="text" class="form-control" placeholder="Winner to" value="{{$matrix->to}}" required></div>
	                    <div class="col-xs-3">
	                    	<select name="matrix[{{$k}}]['prize_type']" class="form-control" placeholder="Prize Type" changedValue="{{$matrix->type}}" required>
								 {!!getPrizeTypes()!!}
							</select>
						 </div>
	                    <div class="col-xs-3"><input name="matrix[{{$k}}]['prize_amount']"  type="text" class="form-control" placeholder="Prize Amount" value="{{$matrix->amount}}" required></div>
	                    <div class="col-xs-2"><input type="button" class="btn btn-danger matrix-row-delete" value="Delete"></div>
	                  
                    </div>
                    	@endforeach

                    	@else

                    	 <div class="row mt-10 matrix-row">
	                    <div class="col-xs-2"><input name="matrix[0]['winner_from']" type="text" class="form-control" id="" placeholder="Winner from" required></div>
	                    <div class="col-xs-2"><input name="matrix[0]['winner_to']"  type="text" class="form-control" id="" placeholder="Winner to" required></div>
	                    <div class="col-xs-3"><select name="matrix[0]['prize_type']" class="form-control"  placeholder="Prize Type" required>
	                    		 {!!getPrizeTypes()!!}
	                    	</select></div>
	                    <div class="col-xs-3"><input name="matrix[0]['prize_amount']"  type="text" class="form-control" id="" placeholder="Prize Amount" required></div>
	                    <div class="col-xs-2"><input type="button" class="btn btn-primary matrix-row-delete" value="Delete"></div>
	                  
                   		 </div>


                    @endif
                  </div>
                    
                    
                   
                    
                               
                            <p class="stdformbutton mt-20">
                                <button type="submit" class="btn btn-primary">Submit</button>
                               
                            </p>
                        </form>
                        <div class="row mt-10 matrix-row-default" style="display: none;">
	                    <div class="col-xs-2"><input name="matrix[]['winner_from']" type="text" class="form-control" id="" placeholder="Winner from" required></div>
	                    <div class="col-xs-2"><input name="matrix[]['winner_to']"  type="text" class="form-control" id="" placeholder="Winner to" required></div>
	                    <div class="col-xs-3"><select name="matrix[]['prize_type']" class="form-control"  placeholder="Prize Type" required>
	                    		 {!!getPrizeTypes()!!}
	                    	</select></div>
	                    <div class="col-xs-3"><input name="matrix[]['prize_amount']"  type="text" class="form-control" id="" placeholder="Prize Amount" required></div>
	                    <div class="col-xs-2"><input type="button" class="btn btn-danger matrix-row-delete" value="Delete"></div>
	                  
                   		 </div>
                         <script type="text/javascript">
						  var arr_error_label = ['winner_from','winner_to','prize_type','prize_amount'];
						   if (typeof(arr_error_label) !== 'undefined') 
						    {addErrorLabel(arr_error_label);} 
						$('.add-new-matrix').click(function()
						{
							var length = $('.matrixContainer').find('.matrix-row').length;
							var content = $('.matrix-row-default').html();
							
							$('.matrixContainer').append('<div class="row mt-10 matrix-row">'+ content.replace(/matrix\[]/g, 'matrix['+length+']') +'</div>');
							$( ".matrix-row-delete" ).on( "click", function() 
						{
							$(this).closest('.matrix-row').remove();
						})

						})
						$( ".matrix-row-delete" ).on( "click", function() 
						{
							$(this).closest('.matrix-row').remove();
						})
						 </script>  
						 <script src="{{asset('/ba/assets/js/common.js')}}"></script>
                    </div>
                    </div>
                    </div>
                    </div>