
		   <div class="row">
		    <div class="col-md-12">
 <section class="tile filterblock">
 <div class="tile-header dvd dvd-btm">
              <h1 class="custom-font">Contests filter
              </h1>
             </div>
            <div class="tile-body">
              <form class="form-inline" role="form" id="filterForm" action="{{url('/'.getCurrentUrlPrefix())}}">
                
                <div class="form-group col-md-2">
                  <select  name="parent_category_id" id="sparent_category_id" class="form-control chosen-select" changedValue="{{Request::get("parent_category_id")}}">
                          <option value="">Select Parent Category</option>
                            {!!prepareTreeDropdown(buildTree(getCategories(array('parent_id'=>0))))!!}
                        </select>
              </div>
              	<div class="form-group col-md-2">
		            <select  name="category_id" id="scategory_id" class="form-control chosen-select" changedValue="{{Request::get("category_id")}}">
                        <option value="">Select Category</option>
                          {!!parentChildDropdown(buildTree(getCategories()))!!}
                      </select>
		        </div>
		        <div class="form-group col-md-2">
		            <input type="text" class="form-control" id="scontest_title" name="contest_title" value="{{Request::get("contest_title")}}" placeholder="Enter Contest Title" title="Contest Title">
		        </div>
		        <div class="form-group col-md-2">
		            <select  name="entry_fee_type" id="sentry_fee_type" class="form-control chosen-select" changedValue="{{Request::get("entry_fee_type")}}">
                          {!!getEntryFeeTypes()!!}
                      </select>
		        </div>
		        <div class="form-group col-md-2">
		            <select  name="prize_type" id="sprize_type" class="form-control chosen-select" changedValue="{{Request::get("prize_type")}}">
                        {!!getPrizeTypes()!!}
                      </select>
		        </div>
		        <div class="form-group col-md-2">
		            <select  name="status" id="sstatus" class="form-control chosen-select" changedValue="{{Request::get("status")}}">
                        	{!!getContestStatuses()!!}
                      </select>
		        </div>
                    <button type="submit" class="btn btn-primary">Search</button>
                    <button type="reset" class="btn btn-default">Reset</button>
              </form>
            </div>
 </section>

 </div>
 </div>