 @include('cms::contests.filters')
 		     <div class="row">
		     <div class="col-md-12">
 				<section class="tile">
 					<div class="tile-header dvd dvd-btm"><h1 class="custom-font">View All Contests</h1></div>
            			<div class="tile-body">
		     		
			            	 <div class="table-responsive">
			                <table class="table mb-0" id="">
			                   
			                    <thead>
			                        <tr>
			                          	<th class="head0 nosort"><input type="checkbox" class="checkall" /></th>
			                          	<th class="head0 sortby" sortby="category_id">Category</th>
						        <th class="head0 sortby" sortby="contest_title">Title</th>
						       <!--  <th class="head0 sortby" sortby="contest_image">Contest Image</th> -->
						        <th class="head0 sortby" sortby="entry_fee_type">Fee Type</th>
						        <th class="head0 sortby" sortby="entry_fee">Entry Fee</th>
						        <th class="head0 sortby" sortby="contest_block_period">Block Period</th>
						        <th class="head0 sortby" sortby="prize_type">Prize Type</th>
						        <th class="head0 sortby" sortby="prize_money">Prize Money</th>
						        <th class="head0 sortby" sortby="prize_money">Start/End Time</th>
						       <!--  <th class="head0 sortby" sortby="contest_position">Contest Position</th> -->
						        <th class="head0 sortby" sortby="status">Status</th>
						        <th class="head0">Actions</th>
			                           
			                        </tr>
			                    </thead>
			                    <tbody>
			                     @foreach($contests as $key => $value)
						        <tr>
						        	<td class="aligncenter"><span class="center">
			                            <input type="checkbox" actionid={{ $value->id }} />
			                          </span></td>
			                        <td>
			                        	@if(!empty($value->category_id) || ($value->category_id != 0))
			                        	@if(isset($categoryList[$value->category_id]))
							        	{{@$categoryList[$value->category_id]['title'] }}
							        	@endif
							        	@endif
			                        </td>
							        <td>{{ $value->contest_title }}</td>
							        <!-- <td><img src="{{ asset($value->contest_image) }}" alt="" class="br-5 size-30x30"></td> -->
							        <td>{{ $value->entry_fee_type }}</td>
							        <td>{{ $value->entry_fee }}</td>
							        <td>{{ $value->contest_block_period }}</td>
							        <td>{{ $value->prize_type }}</td>
							        <td>{{ $value->prize_money }}</td>
							        <td>{{ date_format(date_create($value->start_time),"d M Y H:i:s") }} </br> {{ date_format(date_create($value->end_time),"d M Y H:i:s") }}</td>
							       <!--  <td>{{ $value->contest_position }}</td> -->
							        <td>{{ $value->status }}</td>
							        <!-- <td>{{ $value->total_users_played }}</td> -->
							            
						           <!-- we will also add show, edit, and delete buttons -->
						            <td>

						               <a class="btn btn-small btn-success" href="{{ URL::to(getCurrentUrlPrefix() .'/'. $value->id) }}" storage="modal">Show</a>

						                <a class="btn btn-small btn-info" href="{{ URL::to(getCurrentUrlPrefix() .'/'. $value->id . '/edit') }}" storage="modal">Edit</a>

						                <a class="btn btn-small btn-primary" href="{{ URL::to(getCurrentUrlPrefix() .'/matrix/'. $value->id) }}" storage="modal">Matrix</a>

						                <a class="btn btn-small clone-btn btn-warning" href="{{ URL::to(getCurrentUrlPrefix() .'/'. $value->id. '/clone') }}" storage="modal">Clone</a>

						                <a class="btn btn-small btn-danger delete" data-token="{{ csrf_token() }}" deleteid="{{$value->id}}"  url="{{url(getCurrentUrlPrefix())}}">Delete</a>

						            </td>
						        </tr>
						    @endforeach
			                    </tbody>
			                </table>
			                </div>
			                </div>
			                </section>
			                </div>
			               </div>
			                <div class="tile-footer dvd dvd-top">
			              <div class="row">
			                <div class="col-sm-3 hidden-xs">
			                  <select class="input-sm form-control w-sm inline bulk_action_type">
			                    <option value="">Select Action</option>
			                    <option value="1">Mark Active</option>
			                    <option value="2">Mark Inactive</option>
			                    <option value="3">Mark Deleted</option>
			                    <option value="4">Mark EXPIRED</option>
			                    
			                  </select>
			                  <button class="btn btn-sm btn-default br-3 bulk_action_trigger">Apply</button>
			                </div>
			                <div class="col-sm-4 text-left"><small class="text-muted">Total {{$contests->total()}} records found!</small></div>
			                <div class="col-sm-5 text-right">
			                 <ul class="pagination pagination-sm m-0">
			                <?php echo $contests->render(); ?>
			            	</ul>
			                 
			                </div>
			              </div>
			            </div>
			            <link rel="stylesheet" href="{{asset('/ba/assets/js/vendor/datetimepicker/css/bootstrap-datetimepicker.min.css')}}">
                		<script src="{{asset('/ba/assets/js/vendor/datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script>
			            <script src="{{asset('/ba/assets/js/common.js')}}"></script> 