
		   <div class="row">
		    <div class="col-md-12">
 <section class="tile filterblock">
 <div class="tile-header dvd dvd-btm">
              <h1 class="custom-font">Serverinfo filter
              </h1>
             </div>
            <div class="tile-body">
              <form class="form-inline" role="form" id="filterForm" action="{{url('/'.getCurrentUrlPrefix())}}">
                
              <div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="sdomain_name" name="domain_name" value="{{Request::get("domain_name")}}" placeholder="Enter Domain Name" title="Domain Name">
		                </div><div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="sdomain_expiry" name="domain_expiry" value="{{Request::get("domain_expiry")}}" placeholder="Enter Domain Expiry" title="Domain Expiry">
		                </div><div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="sssl_expiry" name="ssl_expiry" value="{{Request::get("ssl_expiry")}}" placeholder="Enter Ssl Expiry" title="Ssl Expiry">
		                </div><div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="supdated_by" name="updated_by" value="{{Request::get("updated_by")}}" placeholder="Enter Updated By" title="Updated By">
		                </div><div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="sstatus" name="status" value="{{Request::get("status")}}" placeholder="Enter Status" title="Status">
		                </div>
                    <button type="submit" class="btn btn-primary">Search</button>
                    <button type="reset" class="btn btn-default">Reset</button>
              </form>
            </div>
 </section>

 </div>
 </div>