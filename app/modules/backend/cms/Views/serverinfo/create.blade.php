
		    <div class="row">
		    <div class="col-md-10 col-md-offset-1">
		    <div class="panel panel-default panel-filled">
                <div class="panel-heading">
                  <h3 class="panel-title custom-font">Add New Serverinfo</h3>
                </div>
                <div class="panel-body">
		   
		    <form class="form-horizontal" role="form" method="post" id="createForm" action="{{url('/'.getCurrentUrlPrefix())}}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                  				<label for="domain_name" class="col-sm-2 control-label">Domain Name</label>
		                                <div class="col-sm-10"><input type="text" name="domain_name" id="domain_name" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
				        		  <label for="used_for" class="col-sm-2 control-label">Used For</label>
                                <div class="col-sm-10"><textarea cols="80" rows="5" name="used_for" id="used_for" class="form-control ignoreeditor"></textarea></div>
                                </div>
			        			<hr class="line-dashed line-full"/>
			        			<div class="form-group">
                  				<label for="domain_expiry" class="col-sm-2 control-label">Domain Expiry</label>
		                                <div class="col-sm-10"><input type="date" name="domain_expiry" id="domain_expiry" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="ssl_expiry" class="col-sm-2 control-label">Ssl Expiry</label>
		                                <div class="col-sm-10"><input type="date" name="ssl_expiry" id="ssl_expiry" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					       <div class="form-group">
                  				<label for="status" class="col-sm-2 control-label">Status</label>
		                                <div class="col-sm-10"><input type="text" name="status" id="status" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					           <p class="stdformbutton">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="reset" class="btn">Reset</button>
                            </p>
                        </form>
                        <script src="{{asset('/ba/assets/js/common.js')}}"></script> 
                         <script type="text/javascript">
						  var arr_error_label = ['domain_name','ssl_expiry','status',];
						   if (typeof(arr_error_label) !== 'undefined') 
						    {addErrorLabel(arr_error_label);} 
						 </script>  
                    </div>
                    </div>
                    </div>
                    </div>