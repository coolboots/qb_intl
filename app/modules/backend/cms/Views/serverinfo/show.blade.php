
		   <div class="row">
		    <div class="col-md-8 col-md-offset-2">
		    <div class="panel panel-default panel-filled">
                <div class="panel-heading">
                  <h3 class="panel-title custom-font">Serverinfo Detail</h3>
                </div>
                <div class="panel-body">
		   
            	 <div class="table-responsive">
                <table class="table mb-0" id=""><tbody>     <tr>
                          <td class="width30">Domain Name</td>
                          <td class="width70"><strong>{{$serverinfo->domain_name}}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Used For</td>
                          <td class="width70"><strong>{{$serverinfo->used_for}}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Domain Expiry</td>
                          <td class="width70"><strong>{{$serverinfo->domain_expiry}}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Ssl Expiry</td>
                          <td class="width70"><strong>{{$serverinfo->ssl_expiry}}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Updated By</td>
                          <td class="width70"><strong>{{$serverinfo->updated_by}}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Status</td>
                          <td class="width70"><strong>{{$serverinfo->status}}</strong></td>
                      </tr>
                  </tbody></table>
                </div>
                </div>
                </div>
                </div>
                </div>
                