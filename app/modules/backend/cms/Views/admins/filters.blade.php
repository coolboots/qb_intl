
		   <div class="row">
		    <div class="col-md-12">
 <section class="tile filterblock">
 <div class="tile-header dvd dvd-btm">
              <h1 class="custom-font">Admins filter
              </h1>
             </div>
            <div class="tile-body">
              <form class="form-inline" role="form" id="filterForm" action="{{url('/'.getCurrentUrlPrefix())}}">
                
              <div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="sname" name="name" value="{{Request::get("name")}}" placeholder="Enter Name" title="Name">
		                </div><div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="semail" name="email" value="{{Request::get("email")}}" placeholder="Enter Email" title="Email">
		                </div><div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="spassword" name="password" value="{{Request::get("password")}}" placeholder="Enter Password" title="Password">
		                </div><div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="susergroup_id" name="usergroup_id" value="{{Request::get("usergroup_id")}}" placeholder="Enter Usergroup Id" title="Usergroup Id">
		                </div><div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="sremember_token" name="remember_token" value="{{Request::get("remember_token")}}" placeholder="Enter Remember Token" title="Remember Token">
		                </div>
                    <button type="submit" class="btn btn-primary">Search</button>
                    <button type="reset" class="btn btn-default">Reset</button>
              </form>
            </div>
 </section>

 </div>
 </div>