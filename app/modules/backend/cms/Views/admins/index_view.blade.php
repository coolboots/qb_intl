 @include('cms::admins.filters')
		     <div class="row">
		     <div class="col-md-12">
 				<section class="tile">
 					<div class="tile-header dvd dvd-btm"><h1 class="custom-font">View All Admins</h1></div>
            			<div class="tile-body">
		     		
			            	 <div class="table-responsive">
			                <table class="table mb-0" id="">
			                   
			                    <thead>
			                        <tr>
			                          	<th class="head0 nosort"><input type="checkbox" class="checkall" /></th>
			                          	<th class="head0 sortby" sortby="name">Name</th>
						        <th class="head0 sortby" sortby="email">Email</th>
						        <th class="head0 sortby" sortby="password">Password</th>
						        <th class="head0 sortby" sortby="usergroup_id">Usergroup Id</th>
						        <th class="head0 sortby" sortby="remember_token">Remember Token</th>
						        <th class="head0">Actions</th>
			                           
			                        </tr>
			                    </thead>
			                    <tbody>
			                     @foreach($admins as $key => $value)
						        <tr><td class="aligncenter"><span class="center">
			                            <input type="checkbox" actionid={{ $value->id }} />
			                          </span></td><td>{{ $value->name }}</td>
							        <td>{{ $value->email }}</td>
							        <td>{{ $value->password }}</td>
							        <td>{{ $value->usergroup_id }}</td>
							        <td>{{ $value->remember_token }}</td>
							            
						           <!-- we will also add show, edit, and delete buttons -->
						            <td>

						               <a class="btn btn-small btn-success" href="{{ URL::to(getCurrentUrlPrefix() .'/'. $value->id) }}">Show</a>

						                <a class="btn btn-small btn-info" href="{{ URL::to(getCurrentUrlPrefix() .'/'. $value->id . '/edit') }}">Edit</a>

						                <a class="btn btn-small btn-warning delete" data-token="{{ csrf_token() }}" deleteid="{{$value->id}}"  url="{{url(getCurrentUrlPrefix())}}">Delete</a>

						            </td>
						        </tr>
						    @endforeach
			                    </tbody>
			                </table>
			                </div>
			                </div>
			                </section>
			                </div>
			               </div>
			                <div class="tile-footer dvd dvd-top">
			              <div class="row">
			                <div class="col-sm-3 hidden-xs">
			                  <select class="input-sm form-control w-sm inline bulk_action_type">
			                    <option value="">Select Action</option>
			                    <option value="1">Mark Active</option>
			                    <option value="2">Mark Inactive</option>
			                    <option value="3">Mark Deleted</option>
			                    
			                  </select>
			                  <button class="btn btn-sm btn-default br-3 bulk_action_trigger">Apply</button>
			                </div>
			                <div class="col-sm-4 text-left"><small class="text-muted">Total {{$admins->total()}} records found!</small></div>
			                <div class="col-sm-5 text-right">
			                 <ul class="pagination pagination-sm m-0">
			                <?php echo $admins->render(); ?>
			            	</ul>
			                 
			                </div>
			              </div>
			            </div>