
		   <div class="row">
		    <div class="col-md-8 col-md-offset-2">
		    <div class="panel panel-default panel-filled">
                <div class="panel-heading">
                  <h3 class="panel-title custom-font">Admin Detail</h3>
                </div>
                <div class="panel-body">
		   
            	 <div class="table-responsive">
                <table class="table mb-0" id=""><tbody>     <tr>
                          <td class="width30">Name</td>
                          <td class="width70"><strong>{{$admins->name}}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Email</td>
                          <td class="width70"><strong>{{$admins->email}}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Password</td>
                          <td class="width70"><strong>{{$admins->password}}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Usergroup Id</td>
                          <td class="width70"><strong>{{$admins->usergroup_id}}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Remember Token</td>
                          <td class="width70"><strong>{{$admins->remember_token}}</strong></td>
                      </tr>
                  </tbody></table>
                </div>
                </div>
                </div>
                </div>
                </div>
                