
<div class="row">
		    <div class="col-md-8 col-md-offset-2">
		    <div class="panel panel-default panel-filled">
                <div class="panel-heading">
                  <h3 class="panel-title custom-font">Edit Admin</h3>
                </div>
                <div class="panel-body">

<form class="form-horizontal" action="{{ url('/'.getCurrentUrlPrefix().'/'.$admins->id) }}" method="POST" enctype="multipart/form-data" id="editForm">
            {{ method_field('PUT') }} 
            <input type="hidden" name="_token" value="{{ csrf_token() }}"><div class="form-group">
                  				<label for="name" class="col-sm-2 control-label">Name</label>
		                                <div class="col-sm-10"><input type="text" name="name" id="name" value="{{$admins->name}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="email" class="col-sm-2 control-label">Email</label>
		                                <div class="col-sm-10"><input type="text" name="email" id="email" value="{{$admins->email}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="password" class="col-sm-2 control-label">Password</label>
		                                <div class="col-sm-10"><input type="text" name="password" id="password" value="{{$admins->password}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="usergroup_id" class="col-sm-2 control-label">Usergroup Id</label>
		                                <div class="col-sm-10"><input type="text" name="usergroup_id" id="usergroup_id" value="{{$admins->usergroup_id}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="remember_token" class="col-sm-2 control-label">Remember Token</label>
		                                <div class="col-sm-10"><input type="text" name="remember_token" id="remember_token" value="{{$admins->remember_token}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					           {{generateLanguageDependentData("admins")}}                             
                            <p class="stdformbutton">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="reset" class="btn">Reset</button>
                            </p>
                        </form>
                        <script src="{{asset('/ba/assets/js/common.js')}}"></script> 
                         <script type="text/javascript">
						  var arr_error_label = ['name','email','password','usergroup_id','remember_token',];
						   if (typeof(arr_error_label) !== 'undefined') 
						    {addErrorLabel(arr_error_label);} 
						 </script>  
                   </div>
                   </div>
                   </div>
                   </div>
                    