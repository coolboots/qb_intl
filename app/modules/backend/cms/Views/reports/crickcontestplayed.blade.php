 @include('cms::reports.filters')
		     <div class="row">
		     <div class="col-md-12">
 				<section class="tile">
 					<div class="tile-header dvd dvd-btm"><h1 class="custom-font">IPL/Cricket Constest Played Report</h1></div>
            			<div class="tile-body">
		     		
			            	 <div class="table-responsive">
			                <table class="table mb-0" id="">
			                   
			                    <thead>
			                        <tr>
			                          <th class="head0 sortby" sortby="title">Date</th>
						        	  <th class="head0 sortby" sortby="image">Contest Played</th>
			                           
			                        </tr>
			                    </thead>
			                    <tbody>
			                     @foreach($data as $key => $value)
						        <tr>
						        	 <td>{{date_format(date_create($value->date),"d M Y") }}</td>
							        <td>{{ $value->total_contest }}</td>
							          
						        </tr>
						    @endforeach
			                    </tbody>
			                </table>
			                </div>
			                </div>
			                </section>
			                </div>
			               </div>
			                <div class="tile-footer dvd dvd-top">
			              <div class="row">
			                
			                <div class="col-sm-4 text-left"><small class="text-muted">Total {{$data->total()}} records found!</small></div>
			                <div class="col-sm-5 text-right">
			                 <ul class="pagination pagination-sm m-0">
			                <?php echo $data->render(); ?>
			            	</ul>
			                 
			                </div>
			              </div>
			            </div>

			            <script src="{{asset('/ba/assets/js/common.js')}}"></script> 
			            <link rel="stylesheet" href="{{asset('/ba/assets/js/vendor/datetimepicker/css/bootstrap-datetimepicker.min.css')}}">
			              <script src="{{asset('/ba/assets/js/vendor/datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script>
			              <script type="text/javascript">
			                  
			                  $(document).ready(function(){

			                    $('.datepicker').each(function() {
			                              var element = $(this);
			                              var format = element.data('format')
			                              element.datetimepicker({
			                                  format:'YYYY-MM-DD',
			                                  sideBySide : true,
			                              });
			                              });

			                  });

			              
			                              
			                          
			             </script>  