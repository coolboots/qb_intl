
		   <div class="row">
		    <div class="col-md-12">
 <section class="tile filterblock">
 <div class="tile-header dvd dvd-btm">
              <h1 class="custom-font">Reports filter
              </h1>
             </div>
            <div class="tile-body">
              <form class="form-inline" role="form" id="filterForm" action="{{url('/'.getCurrentUrlPrefix())}}">
                
              <div class="form-group col-md-2">
                  <div class="input-group datepicker">
                  <input type="text" class="form-control" id="sdate_from" name="date_from" value="{{Request::get("date_from")}}" placeholder="From Date" title="From Date" data-format="Y-m-d">
                  <span class="input-group-addon"> <span class="fa fa-calendar"></span> </span>
                </div>
              </div>
              <div class="form-group col-md-2">
                  <div class="input-group datepicker">
                  <input type="text" class="form-control" id="sdate_to" name="date_to" value="{{Request::get("date_to")}}" placeholder="To Date" title="To Date" data-format="Y-m-d">
                  <span class="input-group-addon"> <span class="fa fa-calendar"></span> </span>
                </div>
              </div>
                    
                   
                    <button type="submit" class="btn btn-primary">Search</button>
                    <button type="reset" class="btn btn-default">Reset</button>
              </form>
            </div>
 </section>

 </div>
 </div>