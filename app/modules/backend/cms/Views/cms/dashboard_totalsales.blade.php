<table class="table table-custom" id="project-progress">
	 <thead>
  <tr>
    <th>Date</th>
    <th>Order Id</th>
    <th>Username</th>
    <!-- <th>Product</th> -->
    <th>Amount</th>
    
    <!-- <th>Stats</th> -->
    
  </tr>
</thead>
  <tbody>
    @if(isset($dataset) && !empty($dataset))

      @foreach($dataset as $record)

        <tr>
        <td>{{$record->order_date}}</td>
        <td>{{$record->invoice_id}}</td>
        <td>{{$record->username}}</td>
        <!-- <td>{{$record->product_name}}</td> -->
        <td>{{$record->total_amount}}</td>
        
        <!-- <td><i class="fa fa-caret-up text-success"></i></td> -->
      </tr>

      @endforeach


    @endif
   
  
  </tbody>
</table>
