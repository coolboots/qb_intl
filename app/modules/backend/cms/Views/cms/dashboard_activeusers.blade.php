<table class="table table-custom" id="project-progress">
	 <thead>
  <tr>
    <th>Username</th>
    <th>Mobile</th>
    <th>Created</th>
    <!-- <th>Action</th> -->
    <!-- <th>Stats</th> -->
    
  </tr>
</thead>
  <tbody>
    @if(isset($dataset) && !empty($dataset))

      @foreach($dataset as $record)

        <tr>
        <td>{{$record->username}}</td>
        <td>{{$record->mobile}}</td>
        <td>{{$record->created_at}}</td>
        <!-- <td><a class="btn btn-primary triggerOfferBtn" href="#" user_id="{{$record->user_id}}">Offer</a></td> -->
        <!-- <td><i class="fa fa-caret-up text-success"></i></td> -->
      </tr>

      @endforeach


    @endif
   
  
  </tbody>
</table>
