<p>
  @if($total_earning>0)
Total Earning:<strong>{{$total_earning}}</strong>
@endif
  @if($total_game_earning>0)
, By Game:<strong>{{$total_game_earning}}</strong>
@endif
@if($total_quiz_earning>0)
, By Quiz:<strong>{{$total_quiz_earning}}</strong>
@endif</p>
<table class="table table-custom" id="project-progress">
	 <thead>
  <tr>
    <th>Username</th>
    <th>Mobile</th>
    <th>Total Reward</th>
    <!-- <th>Action</th> -->
    <!-- <th>Stats</th> -->
    
  </tr>
</thead>
  <tbody>
    @if(isset($dataset) && !empty($dataset))

      @foreach($dataset as $record)

        <tr>
        <td>{{$record->username}}</td>
        <td>{{$record->mobile}}</td>
        <td>{{$record->total_points}}</td>
        <!-- <td><a class="btn btn-primary triggerOfferBtn" href="#" user_id="{{$record->user_id}}">Offer</a></td> -->
        <!-- <td><i class="fa fa-caret-up text-success"></i></td> -->
      </tr>

      @endforeach


    @endif
   
  
  </tbody>
</table>
