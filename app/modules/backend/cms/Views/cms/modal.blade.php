<div class="modal fade" id="myActionModal" tabindex="-1" role="dialog" aria-labelledby="myActionModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title custom-font myActionTitle">Select Target Audience</h3>
      </div>
      <div class="message"></div>
      <div class="modal-body myActionBody"> 
    
      </div>
     
    </div>
  </div>
</div>
