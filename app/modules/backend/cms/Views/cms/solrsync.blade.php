@extends('admin.layouts.mainlayout')
					@section('content')

					<div class="row">
					   <div class="col-md-12">
					      <section class="tile">
					         <div class="tile-header dvd dvd-btm">
					            <h1 class="custom-font"><strong>Manage</strong> Solr Sync Activity
					             
					            </h1>
					            
					         </div>
					         <div class="tile-body">
					          <!-- if there are creation errors, they will show here -->
					          <div class="message alert" style="display: none;"></div>
					         
					              <div class="dynamiccontainer">
					              <div class="row">
		     <div class="col-md-12">
 				<section class="tile">
 					<div class="tile-header dvd dvd-btm"><h1 class="custom-font">View All Nodes</h1></div>

 					@if (Session::has('message'))
					    <div class="alert alert-info">{{ Session::get('message') }}</div>
					@endif
            			<div class="tile-body">
		     		
			            	 <div class="table-responsive">
			                <table class="table mb-0" id="">
			                   
			                    <thead>
			                        <tr>
			                          	<th class="head0 sortby" sortby="title">Title</th>
			                          	<th class="head0 sortby" sortby="title">Last Status</th>
			                          	
						        <th class="head0">Actions</th>
			                           
			                        </tr>
			                    </thead>
			                    <tbody>
			                     @foreach($nodes as $key => $value)
						        <tr>
			                          <td>{{ $value }}</td>
			                          <td>{{@$solrstatus[$value]['message']}} {{@$solrstatus[$value]['updated_at']}}</td>
			                          <!-- we will also add show, edit, and delete buttons -->
						            <td>

						                <a class="btn btn-small btn-info" href="{{ URL::to(getCurrentUrlPrefix()) }}?type={{$value}}">Sync Now</a>

						              
						            </td>
						        </tr>
						    @endforeach
			                    </tbody>
			                </table>
			                </div>
			                </div>
			                </section>
			                </div>
			               </div>
			                <div class="tile-footer dvd dvd-top">
			              
			            </div>
					              </div>
					          </div>
					      </section>
					      <div class="divider15"></div>
					   </div>
					   <!--span8-->
					</div>
					<!--row-fluid-->

					@endsection

					@section('dynamic_js')

					<script type="text/javascript">
					  var page_url = "<?php echo url(getCurrentUrlPrefix());?>";
					</script>

					<!-- <script src="{{asset('/ba/vue/pages/crud.js')}}"></script>  -->

					@endsection
					