
		   <div class="row">
		    <div class="col-md-12">
 <section class="tile filterblock">
 <div class="tile-header dvd dvd-btm">
              <h1 class="custom-font">Awardmatrix filter
              </h1>
             </div>
            <div class="tile-body">
              <form class="form-inline" role="form" id="filterForm" action="{{url('/'.getCurrentUrlPrefix())}}">
                
              <div class="form-group col-md-2">
		                 <select name="contest_id" id="scontest_id" class="form-control" changedValue="{{Request::get("contest_id")}}">
		                                		 <option value="">Select Contest</option>
                                                {!!prepareTreeDropdown(buildTree(getContests()))!!}
		                                	</select>
		                </div><div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="sprize_amount" name="prize_amount" value="{{Request::get("prize_amount")}}" placeholder="Enter Prize Amount" title="Prize Amount">
		                </div>
                    <button type="submit" class="btn btn-primary">Search</button>
                    <button type="reset" class="btn btn-default">Reset</button>
              </form>
            </div>
 </section>

 </div>
 </div>