
		   <div class="row">
		    <div class="col-md-12">
 <section class="tile filterblock">
 <div class="tile-header dvd dvd-btm">
              <h1 class="custom-font">Adsconfig filter
              </h1>
             </div>
            <div class="tile-body">
              <form class="form-inline" role="form" id="filterForm" action="{{url('/'.getCurrentUrlPrefix())}}">
                
              <div class="form-group col-md-2">
              	<select  name="page_id" id="spage_id" class="form-control chosen-select" changedValue="{{Request::get("page_id")}}">
                                                <option value="">Select Page</option>
                                                {!!prepareTreeDropdown(buildTree(getPages()))!!}
                                            </select>
		                  
		                </div><div class="form-group col-md-2">
		                <select  name="publisher_id" id="spublisher_id" class="form-control chosen-select" changedValue="{{Request::get("publisher_id")}}">
                                                <option value="">Select Publisher</option>
                                                {!!prepareTreeDropdown(buildTree(getPublishers()))!!}
                                            </select> 
		                </div><div class="form-group col-md-2">
		                	<select  name="partner_id" id="spartner_id" class="form-control chosen-select" changedValue="{{Request::get("partner_id")}}">
                                                <option value="">Select Partner</option>
                                                {!!prepareTreeDropdown(buildTree(getPartners()))!!}
                                            </select> 
		                  
		                </div><div class="form-group col-md-2">
		                 <select  name="status" id="sstatus" class="form-control" changedValue="{{Request::get("status")}}">
		                                		{{getStatuses()}}
		                                	</select>
		                </div>
                    <button type="submit" class="btn btn-primary">Search</button>
                    <button type="reset" class="btn btn-default">Reset</button>
              </form>
            </div>
 </section>

 </div>
 </div>