
		   <div class="row">
		    <div class="col-md-8 col-md-offset-2">
		    <div class="panel panel-default panel-filled">
                <div class="panel-heading">
                  <h3 class="panel-title custom-font">Adsconfig Detail</h3>
                </div>
                <div class="panel-body">
		   
            	 <div class="table-responsive">
                <table class="table mb-0" id=""><tbody>     <tr>
                          <td class="width30">Page name</td>
                          <td class="width70"><strong>{!!$adsconfig->page->page_name!!}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Publisher name</td>
                          <td class="width70"><strong>{!!$adsconfig->publisher->publisher_name!!}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Partner name</td>
                          <td class="width70"><strong>{!!$adsconfig->partner->partner_name!!}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Scripts 1</td>
                          <td class="width70"><strong>{!!$adsconfig->scripts_1!!}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Scripts 2</td>
                          <td class="width70"><strong>{!!$adsconfig->scripts_2!!}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Scripts 3</td>
                          <td class="width70"><strong>{!!$adsconfig->scripts_3!!}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Scripts 4</td>
                          <td class="width70"><strong>{!!$adsconfig->scripts_4!!}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Scripts 5</td>
                          <td class="width70"><strong>{!!$adsconfig->scripts_5!!}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Status</td>
                          <td class="width70"><strong>{!!$adsconfig->status!!}</strong></td>
                      </tr>
                  </tbody></table>
                </div>
                </div>
                </div>
                </div>
                </div>
                