
		    <div class="row">
		    <div class="col-md-12">
		    <div class="panel panel-default panel-filled">
                <div class="panel-heading">
                  <h3 class="panel-title custom-font">Import AdsConfigs</h3>
                </div>
                <div class="panel-body">
		   
		    <form class="form-horizontal" role="form" method="post" id="createForm" action="{{route('adsconf.import.upload')}}" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                
                <div class="form-group">
                    <label for="excel_import" class="col-sm-2 control-label">Upload</label>
                    <div class="col-sm-10">
                        <input type="file" name="excel_import" id="excel_import" class="form-control">
                    </div>
                </div>
		        <hr class="line-dashed line-full"/>

	           <p class="stdformbutton">
                <button type="submit" class="btn btn-primary">Upload</button>
                <!-- <button type="reset" class="btn">Reset</button> -->
            </p>
            <a href="{{ asset('ba/assets/adsconfig_sample.xlsx') }}" class="text-muted">Adsconfig Sample Excel Download</a>
        </form>
        <script src="{{asset('/ba/assets/js/common.js')}}"></script> 
         <script type="text/javascript">
		  var arr_error_label = ['excel_import'];
		   if (typeof(arr_error_label) !== 'undefined') 
		    {addErrorLabel(arr_error_label);} 
		 </script>  
    </div>
    </div>
    </div>
    </div>