<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default panel-filled">
            <div class="panel-heading">
                <h3 class="panel-title custom-font">Bulk Config Assign</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table mb-0">
                        <tbody>
                            <form class="form-horizontal" id="bulkForm" role="form" method="post" action="{{url('/'.getCurrentUrlPrefix())}}">
                                <input type="hidden" name="config_ids" id="config_ids" value="{{$config_ids}}">
                                <div class="form-group">
                                    <label for="bulk_partners" class="col-sm-2 control-label">Partners</label>
                                    <div class="col-sm-10">
                                        <select name="bulk_partners" id="bulk_partners" class="chosen-select" multiple>
                                            <option value="">Select Partner</option>
                                            {!!prepareTreeDropdown(buildTree(getPartners()))!!}
                                        </select>
                                    </div>
                                </div>
                                <hr class="line-dashed line-full"/>
                                <p class="stdformbutton">
                                    <button type="submit" class="btn btn-primary" id="bulksubmit" action="{{url('/'.getCurrentUrlPrefix())}}">Submit</button>
                                    <button type="reset" class="btn">Reset</button>
                                </p>
                            </form>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('#bulksubmit').on('click', function () {
            showLoader();
            var partner_ids = $('#bulk_partners').val();
            var config_ids = $('#config_ids').val();
            
            if (partner_ids == '' || partner_ids == null) {
                alert('Please select a partner');
                return false;
            }
            let paramsData = {"partner_ids":partner_ids,"config_ids":config_ids,"bulk":true};
            
            var url = $(this).attr('action');
            axios.post(url,{
                data:paramsData,
                headers: {
                'axios_request': true,
                'Access-Control-Allow-Origin': '*',
                'radxsoft': 'labs',
                }
            })
            .then(function (response) {
                res_data = response.data;
                if (res_data.status == 'success') {
                    $('.message').addClass('alert').removeClass('alert-danger').addClass('alert-info').html('<strong>'+res_data.message+'</strong>').show().fadeOut(10000);
                    toastr["success"](res_data.message);
                }
            })
            .catch(function (error) {
                console.log(error);
            });
            hideLoader();
            return false;
    });
</script>
<script src="{{asset('/ba/assets/js/common.js')}}"></script>
{{-- <script src="{{asset('/ba/vue/pages/crud.js')}}"></script> --}}