
<div class="row">
		    <div class="col-md-8 col-md-offset-2">
		    <div class="panel panel-default panel-filled">
                <div class="panel-heading">
                  <h3 class="panel-title custom-font">Edit Adsconfig</h3>
                </div>
                <div class="panel-body">

<form class="form-horizontal" action="{{ url('/'.getCurrentUrlPrefix().'/'.$adsconfig->id) }}" method="POST" enctype="multipart/form-data" id="editForm">
            {{ method_field('PUT') }} 
            <input type="hidden" name="_token" value="{{ csrf_token() }}"><div class="form-group">
                  				<label for="page_id" class="col-sm-2 control-label">Page Name</label>
		                                <div class="col-sm-10">
		                                	<select  name="page_id" id="page_id" class="form-control chosen-select" changedValue="{{$adsconfig->page_id}}">
                                                <option value="">Select Page</option>
                                                {!!prepareTreeDropdown(buildTree(getPages()))!!}
                                            </select>
		                                	</div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="publisher_id" class="col-sm-2 control-label">Publisher Name</label>
		                                <div class="col-sm-10">
		                                	<select  name="publisher_id" id="publisher_id" class="form-control chosen-select" changedValue="{{$adsconfig->publisher_id}}">
                                                <option value="">Select Publisher</option>
                                                {!!prepareTreeDropdown(buildTree(getPublishers()))!!}
                                            </select>

		                                	</div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="partner_id" class="col-sm-2 control-label">Partner Name</label>
		                                <div class="col-sm-10">
		                                	<select  name="partner_id" id="partner_id" class="form-control chosen-select" changedValue="{{$adsconfig->partner_id}}">
                                                <option value="">Select Partner</option>
                                                {!!prepareTreeDropdown(buildTree(getPartners()))!!}
                                            </select>
                                        </div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
				        		  <label for="scripts_1" class="col-sm-2 control-label">Scripts 1</label>
                                <div class="col-sm-10"><textarea cols="80" rows="5" name="scripts_1" id="scripts_1" class="form-control ignoreeditor">{{$adsconfig->scripts_1}}</textarea></div>
                                </div>
			        			<hr class="line-dashed line-full"/>
			        			<div class="form-group">
				        		  <label for="scripts_2" class="col-sm-2 control-label">Scripts 2</label>
                                <div class="col-sm-10"><textarea cols="80" rows="5" name="scripts_2" id="scripts_2" class="form-control ignoreeditor">{{$adsconfig->scripts_2}}</textarea></div>
                                </div>
			        			<hr class="line-dashed line-full"/>
			        			<div class="form-group">
				        		  <label for="scripts_3" class="col-sm-2 control-label">Scripts 3</label>
                                <div class="col-sm-10"><textarea cols="80" rows="5" name="scripts_3" id="scripts_3" class="form-control ignoreeditor">{{$adsconfig->scripts_3}}</textarea></div>
                                </div>
			        			<hr class="line-dashed line-full"/>
			        			<div class="form-group">
				        		  <label for="scripts_4" class="col-sm-2 control-label">Scripts 4</label>
                                <div class="col-sm-10"><textarea cols="80" rows="5" name="scripts_4" id="scripts_4" class="form-control ignoreeditor">{{$adsconfig->scripts_4}}</textarea></div>
                                </div>
			        			<hr class="line-dashed line-full"/>
			        			<div class="form-group">
				        		  <label for="scripts_5" class="col-sm-2 control-label">Scripts 5</label>
                                <div class="col-sm-10"><textarea cols="80" rows="5" name="scripts_5" id="scripts_5" class="form-control ignoreeditor">{{$adsconfig->scripts_5}}</textarea></div>
                                </div>
			        			<hr class="line-dashed line-full"/>
			        			<div class="form-group">
                  				<label for="status" class="col-sm-2 control-label">Status</label>
		                                <div class="col-sm-10">
		                                	<select  name="status" id="status" class="form-control" changedValue="{{$adsconfig->status}}">
		                                		{{getStatuses()}}
		                                	</select>
		                                </div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					           <p class="stdformbutton">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="reset" class="btn">Reset</button>
                            </p>
                        </form>
                        <script src="{{asset('/ba/assets/js/common.js')}}"></script> 
                         <script type="text/javascript">
						  var arr_error_label = ['page_id','publisher_id','partner_id','status',];
						   if (typeof(arr_error_label) !== 'undefined') 
						    {addErrorLabel(arr_error_label);} 
						 </script>  
                   </div>
                   </div>
                   </div>
                   </div>
                    