
		   <div class="row">
		    <div class="col-md-8 col-md-offset-2">
		    <div class="panel panel-default panel-filled">
                <div class="panel-heading">
                  <h3 class="panel-title custom-font">Article Detail</h3>
                </div>
                <div class="panel-body">
		   
            	 <div class="table-responsive">
                <table class="table mb-0" id=""><tbody>     <tr>
                          <td class="width30">Article Type</td>
                          <td class="width70"><strong>{{$articles->article_type}}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Title</td>
                          <td class="width70"><strong>{{$articles->title}}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Subtitle</td>
                          <td class="width70"><strong>{{$articles->subtitle}}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Image</td>
                          <td class="width70"><strong>{{$articles->image}}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Video Url</td>
                          <td class="width70"><strong>{{$articles->video_url}}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Descriptions</td>
                          <td class="width70"><strong>{{$articles->descriptions}}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Status</td>
                          <td class="width70"><strong>{{$articles->status}}</strong></td>
                      </tr>
                  </tbody></table>
                </div>
                </div>
                </div>
                </div>
                </div>
                