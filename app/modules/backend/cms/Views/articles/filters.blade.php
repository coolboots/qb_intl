
		   <div class="row">
		    <div class="col-md-12">
 <section class="tile filterblock">
 <div class="tile-header dvd dvd-btm">
              <h1 class="custom-font">Articles filter
              </h1>
             </div>
            <div class="tile-body">
              <form class="form-inline" role="form" id="filterForm" action="{{url('/'.getCurrentUrlPrefix())}}">
                
              <div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="sarticle_type" name="article_type" value="{{Request::get("article_type")}}" placeholder="Enter Article Type" title="Article Type">
		                </div><div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="stitle" name="title" value="{{Request::get("title")}}" placeholder="Enter Title" title="Title">
		                </div><div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="ssubtitle" name="subtitle" value="{{Request::get("subtitle")}}" placeholder="Enter Subtitle" title="Subtitle">
		                </div><div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="simage" name="image" value="{{Request::get("image")}}" placeholder="Enter Image" title="Image">
		                </div><div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="svideo_url" name="video_url" value="{{Request::get("video_url")}}" placeholder="Enter Video Url" title="Video Url">
		                </div><div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="sstatus" name="status" value="{{Request::get("status")}}" placeholder="Enter Status" title="Status">
		                </div>
                    <button type="submit" class="btn btn-primary">Search</button>
                    <button type="reset" class="btn btn-default">Reset</button>
              </form>
            </div>
 </section>

 </div>
 </div>