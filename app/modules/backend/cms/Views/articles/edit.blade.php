
<div class="row">
		    <div class="col-md-8 col-md-offset-2">
		    <div class="panel panel-default panel-filled">
                <div class="panel-heading">
                  <h3 class="panel-title custom-font">Edit Article</h3>
                </div>
                <div class="panel-body">

<form class="form-horizontal" action="{{ url('/'.getCurrentUrlPrefix().'/'.$articles->id) }}" method="POST" enctype="multipart/form-data" id="editForm">
            {{ method_field('PUT') }} 
            <input type="hidden" name="_token" value="{{ csrf_token() }}"><div class="form-group">
                  				<label for="article_type" class="col-sm-2 control-label">Article Type</label>
		                                <div class="col-sm-10"><input type="text" name="article_type" id="article_type" value="{{$articles->article_type}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="title" class="col-sm-2 control-label">Title</label>
		                                <div class="col-sm-10"><input type="text" name="title" id="title" value="{{$articles->title}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					         <hr class="line-dashed line-full"/>
			    			{!!seoUrlsFieldEdit($articles->id,"article")!!}<div class="form-group">
                  				<label for="subtitle" class="col-sm-2 control-label">Subtitle</label>
		                                <div class="col-sm-10"><input type="text" name="subtitle" id="subtitle" value="{{$articles->subtitle}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                                <label for="image" class="col-sm-2 control-label">Image</label>
                                        <div class="col-sm-10"><input type="file" id="image" name="image" class="form-control">
                                        @if($articles->image)
		                                	<img src="{{asset($articles->image)}}" width="100px" />
		                                	@endif
                                        </div>
                                        
                                        </div>
                            <hr class="line-dashed line-full"/><div class="form-group">
                  				<label for="video_url" class="col-sm-2 control-label">Video Url</label>
		                                <div class="col-sm-10"><input type="text" name="video_url" id="video_url" value="{{$articles->video_url}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
				        		  <label for="descriptions" class="col-sm-2 control-label">Descriptions</label>
                                <div class="col-sm-10"><textarea cols="80" rows="5" name="descriptions" id="descriptions" class="form-control">{{$articles->descriptions}}</textarea></div>
                                </div>
			        			<hr class="line-dashed line-full"/>
			        			<div class="form-group">
                  				<label for="status" class="col-sm-2 control-label">Status</label>
		                                <div class="col-sm-10"><input type="text" name="status" id="status" value="{{$articles->status}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					           {{generateLanguageDependentData("articles")}}                             
                            <p class="stdformbutton">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="reset" class="btn">Reset</button>
                            </p>
                        </form>
                        <script src="{{asset('/ba/assets/js/common.js')}}"></script> 
                         <script type="text/javascript">
						  var arr_error_label = ['article_type','title','subtitle','video_url','status',];
						   if (typeof(arr_error_label) !== 'undefined') 
						    {addErrorLabel(arr_error_label);} 
						 </script>  
                   </div>
                   </div>
                   </div>
                   </div>
                    