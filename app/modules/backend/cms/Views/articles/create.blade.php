
		    <div class="row">
		    <div class="col-md-8 col-md-offset-2">
		    <div class="panel panel-default panel-filled">
                <div class="panel-heading">
                  <h3 class="panel-title custom-font">Add New Article</h3>
                </div>
                <div class="panel-body">
		   
		    <form class="form-horizontal" role="form" method="post" id="createForm" action="{{url('/'.getCurrentUrlPrefix())}}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                  				<label for="article_type" class="col-sm-2 control-label">Article Type</label>
		                                <div class="col-sm-10"><input type="text" name="article_type" id="article_type" class="form-control title"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="title" class="col-sm-2 control-label">Title</label>
		                                <div class="col-sm-10"><input type="text" name="title" id="title" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					         
   								{!!seoUrlsField("article")!!}<div class="form-group">
                  				<label for="subtitle" class="col-sm-2 control-label">Subtitle</label>
		                                <div class="col-sm-10"><input type="text" name="subtitle" id="subtitle" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                                <label for="image" class="col-sm-2 control-label">Image</label>
                                        <div class="col-sm-10"><input type="file" id="image" name="image" class="form-control"></div>
                                        
                                        </div>
                            <hr class="line-dashed line-full"/><div class="form-group">
                  				<label for="video_url" class="col-sm-2 control-label">Video Url</label>
		                                <div class="col-sm-10"><input type="text" name="video_url" id="video_url" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
				        		  <label for="descriptions" class="col-sm-2 control-label">Descriptions</label>
                                <div class="col-sm-10"><textarea cols="80" rows="5" name="descriptions" id="descriptions" class="form-control"></textarea></div>
                                </div>
			        			<hr class="line-dashed line-full"/>
			        			<div class="form-group">
                  				<label for="status" class="col-sm-2 control-label">Status</label>
		                                <div class="col-sm-10"><input type="text" name="status" id="status" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					           {{generateLanguageDependentData("articles")}}                             
                            <p class="stdformbutton">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="reset" class="btn">Reset</button>
                            </p>
                        </form>
                         <script type="text/javascript">
						  var arr_error_label = ['article_type','title','subtitle','image','video_url','status',];
						   if (typeof(arr_error_label) !== 'undefined') 
						    {addErrorLabel(arr_error_label);} 
						 </script>  
                    </div>
                    </div>
                    </div>
                    </div>