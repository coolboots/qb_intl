
		   <div class="row">
		    <div class="col-md-12">
		    <div class="panel panel-default panel-filled">
                <div class="panel-heading">
                  <h3 class="panel-title custom-font">Userwallettranshistory Detail</h3>
                </div>
                <div class="panel-body">
		   
            	 <div class="table-responsive">
                <table class="table mb-0" id=""><tbody> 
                    <tr>
                         {{-- <th class="width30">User Id</th> --}}
                           <th class="width30">Name</th>  
                         <th class="width30">Mode Type</th>
                         <th class="width30">Coin Type</th>
                         <th class="width30">Amount</th>
                         <th class="width30">Remarks</th>
                         <th class="width30">Created At</th>
                        </tr> 
                    @foreach($histories as $userwallettranshistory)
                    <tr>
                         
                          {{-- <td class="width70"><strong>{{$userwallettranshistory->user_id}}</strong></td> --}}
                            <td class="width70"><strong>{{$userwallettranshistory->name}}</strong></td>  
                          
                          <td class="width70"><strong>{{$userwallettranshistory->mode_type}}</strong></td>
                      
                          
                          <td class="width70"><strong>{{$userwallettranshistory->coin_type}}</strong></td>
                      
                          
                          <td class="width70"><strong>{{$userwallettranshistory->amount}}</strong></td>
                      
                          
                          <td class="width70"><strong>{{$userwallettranshistory->remarks}}</strong></td>
                       
                       
                          <td class="width70"><strong>{{$userwallettranshistory->created_at}}</strong></td>
                    </tr>
                      @endforeach
                  </tbody></table>
                </div>
                </div>
                </div>
                </div>
                </div>
                <script src="{{asset('/ba/assets/js/common.js')}}"></script>       