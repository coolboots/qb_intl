
		    <div class="row">
		    <div class="col-md-8 col-md-offset-2">
		    <div class="panel panel-default panel-filled">
                <div class="panel-heading">
                  <h3 class="panel-title custom-font">Add New User</h3>
                </div>
                <div class="panel-body">
		   
		    <form class="form-horizontal" role="form" method="post" id="createForm" action="{{url('/'.getCurrentUrlPrefix())}}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                  				<label for="name" class="col-sm-2 control-label">Name</label>
		                                <div class="col-sm-10"><input type="text" name="name" id="name" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="mobile" class="col-sm-2 control-label">Mobile</label>
		                                <div class="col-sm-10"><input type="text" name="mobile" id="mobile" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="email" class="col-sm-2 control-label">Email</label>
		                                <div class="col-sm-10"><input type="text" name="email" id="email" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="password" class="col-sm-2 control-label">Password</label>
		                                <div class="col-sm-10"><input type="text" name="password" id="password" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="remember_token" class="col-sm-2 control-label">Remember Token</label>
		                                <div class="col-sm-10"><input type="text" name="remember_token" id="remember_token" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="source" class="col-sm-2 control-label">Source</label>
		                                <div class="col-sm-10"><input type="text" name="source" id="source" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="status" class="col-sm-2 control-label">Status</label>
		                                <div class="col-sm-10">
		                                <select class="form-control mb-10" name="status" id="status">
					                      <option value="">Select status</option>
					                      <option value="Active">Active</option>
					                      <option value="Inactive">Inactive</option>
					                    </select>
		                                </div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					           {{generateLanguageDependentData("users")}}                             
                            <p class="stdformbutton">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="reset" class="btn">Reset</button>
                            </p>
                        </form>
                         <script type="text/javascript">
						  var arr_error_label = ['name','mobile','email','password','remember_token','source','status',];
						   if (typeof(arr_error_label) !== 'undefined') 
						    {addErrorLabel(arr_error_label);} 
						 </script>  
                    </div>
                    </div>
                    </div>
                    </div>