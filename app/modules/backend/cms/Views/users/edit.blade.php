
<div class="row">
		    <div class="col-md-8 col-md-offset-2">
		    <div class="panel panel-default panel-filled">
                <div class="panel-heading">
                  <h3 class="panel-title custom-font">Edit User</h3>
                </div>
                <div class="panel-body">

<form class="form-horizontal" action="{{ url('/'.getCurrentUrlPrefix().'/'.$users->id) }}" method="POST" enctype="multipart/form-data" id="editForm">
            {{ method_field('PUT') }} 
            <input type="hidden" name="_token" value="{{ csrf_token() }}"><div class="form-group">
                  				<label for="name" class="col-sm-2 control-label">Name</label>
		                                <div class="col-sm-10"><input type="text" name="name" id="name" value="{{$users->name}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="mobile" class="col-sm-2 control-label">Mobile</label>
		                                <div class="col-sm-10"><input type="text" name="mobile" id="mobile" value="{{$users->mobile}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="email" class="col-sm-2 control-label">Email</label>
		                                <div class="col-sm-10"><input type="text" name="email" id="email" value="{{$users->email}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="password" class="col-sm-2 control-label">Password</label>
		                                <div class="col-sm-10"><input type="text" name="password" id="password" value="{{$users->password}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="remember_token" class="col-sm-2 control-label">Remember Token</label>
		                                <div class="col-sm-10"><input type="text" name="remember_token" id="remember_token" value="{{$users->remember_token}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="source" class="col-sm-2 control-label">Source</label>
		                                <div class="col-sm-10"><input type="text" name="source" id="source" value="{{$users->source}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="status" class="col-sm-2 control-label">Status</label>
		                                <div class="col-sm-10">
		                                <select class="form-control mb-10" name="status" id="status">
					                      <option value="">Select status</option>
					                      @if($users->status=="Active")
					                      <option value="Active" selected>Active</option>
					                      <option value="Inactive">Inactive</option>
					                      
					                      @elseif($users->status=="Inactive")
					                      <option value="Active">Active</option>
					                      <option value="Inactive" selected>Inactive</option>
					                      @endif
					                    </select>
		                                </div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					           {{generateLanguageDependentData("users")}}                             
                            <p class="stdformbutton">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="reset" class="btn">Reset</button>
                            </p>
                        </form>
                        <script src="{{asset('/ba/assets/js/common.js')}}"></script> 
                         <script type="text/javascript">
						  var arr_error_label = ['name','mobile','email','password','remember_token','source','status',];
						   if (typeof(arr_error_label) !== 'undefined') 
						    {addErrorLabel(arr_error_label);} 
						 </script>  
                   </div>
                   </div>
                   </div>
                   </div>
                    