
		   <div class="row">
		    <div class="col-md-8 col-md-offset-2">
		    <div class="panel panel-default panel-filled">
                <div class="panel-heading">
                  <h3 class="panel-title custom-font">User Detail</h3>
                </div>
                <div class="panel-body">
		   
            	 <div class="table-responsive">
                <table class="table mb-0" id=""><tbody>     <tr>
                          <td class="width30">Name</td>
                          <td class="width70"><strong>{{$users->name}}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Mobile</td>
                          <td class="width70"><strong>{{$users->mobile}}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Email</td>
                          <td class="width70"><strong>{{$users->email}}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Password</td>
                          <td class="width70"><strong>{{$users->password}}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Remember Token</td>
                          <td class="width70"><strong>{{$users->remember_token}}</strong></td>
                      </tr>  <tr>
                          <td class="width30">Source</td>
                          <td class="width70"><strong>{{$users->source}}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Status</td>
                          <td class="width70"><strong>{{$users->status}}</strong></td>
                      </tr>
                  </tbody></table>
                </div>
                </div>
                </div>
                </div>
                </div>
                