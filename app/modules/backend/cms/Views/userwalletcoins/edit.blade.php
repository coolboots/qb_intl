
<div class="row">
		    <div class="col-md-8 col-md-offset-2">
		    <div class="panel panel-default panel-filled">
                <div class="panel-heading">
                  <h3 class="panel-title custom-font">Edit Userwalletcoin</h3>
                </div>
                <div class="panel-body">

<form class="form-horizontal" action="{{ url('/'.getCurrentUrlPrefix().'/'.$userwalletcoins->id) }}" method="POST" enctype="multipart/form-data" id="editForm">
            {{ method_field('PUT') }} 
            <input type="hidden" name="_token" value="{{ csrf_token() }}"><div class="form-group">
                  				<label for="user_id" class="col-sm-2 control-label">User Id</label>
		                                <div class="col-sm-10"><input type="text" name="user_id" id="user_id" value="{{$userwalletcoins->user_id}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="available_coins" class="col-sm-2 control-label">Available Coins</label>
		                                <div class="col-sm-10"><input type="text" name="available_coins" id="available_coins" value="{{$userwalletcoins->available_coins}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					           {{generateLanguageDependentData("user_wallet_coins")}}                             
                            <p class="stdformbutton">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="reset" class="btn">Reset</button>
                            </p>
                        </form>
                        <script src="{{asset('/ba/assets/js/common.js')}}"></script> 
                         <script type="text/javascript">
						  var arr_error_label = ['user_id','available_coins',];
						   if (typeof(arr_error_label) !== 'undefined') 
						    {addErrorLabel(arr_error_label);} 
						 </script>  
                   </div>
                   </div>
                   </div>
                   </div>
                    