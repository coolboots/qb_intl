
		   <div class="row">
		    <div class="col-md-12">
 <section class="tile filterblock">
 <div class="tile-header dvd dvd-btm">
              <h1 class="custom-font">Userwalletcash filter
              </h1>
             </div>
            <div class="tile-body">
              <form class="form-inline" role="form" id="filterForm" action="{{url('/'.getCurrentUrlPrefix())}}">
                
              <div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="suser_id" name="user_id" value="{{Request::get("user_id")}}" placeholder="Enter User Id" title="User Id">
		                </div><div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="scash_available" name="cash_available" value="{{Request::get("cash_available")}}" placeholder="Enter Cash Available" title="Cash Available">
		                </div><div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="scash_withdrawal" name="cash_withdrawal" value="{{Request::get("cash_withdrawal")}}" placeholder="Enter Cash Withdrawal" title="Cash Withdrawal">
		                </div><div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="stotal_earned" name="total_earned" value="{{Request::get("total_earned")}}" placeholder="Enter Total Earned" title="Total Earned">
		                </div>
                    <button type="submit" class="btn btn-primary">Search</button>
                    <button type="reset" class="btn btn-default">Reset</button>
              </form>
            </div>
 </section>

 </div>
 </div>