
<div class="row">
		    <div class="col-md-8 col-md-offset-2">
		    <div class="panel panel-default panel-filled">
                <div class="panel-heading">
                  <h3 class="panel-title custom-font">Edit Userwalletcash</h3>
                </div>
                <div class="panel-body">

<form class="form-horizontal" action="{{ url('/'.getCurrentUrlPrefix().'/'.$userwalletcash->id) }}" method="POST" enctype="multipart/form-data" id="editForm">
            {{ method_field('PUT') }} 
            <input type="hidden" name="_token" value="{{ csrf_token() }}"><div class="form-group">
                  				<label for="user_id" class="col-sm-2 control-label">User Id</label>
		                                <div class="col-sm-10"><input type="text" name="user_id" id="user_id" value="{{$userwalletcash->user_id}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="cash_available" class="col-sm-2 control-label">Cash Available</label>
		                                <div class="col-sm-10"><input type="text" name="cash_available" id="cash_available" value="{{$userwalletcash->cash_available}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="cash_withdrawal" class="col-sm-2 control-label">Cash Withdrawal</label>
		                                <div class="col-sm-10"><input type="text" name="cash_withdrawal" id="cash_withdrawal" value="{{$userwalletcash->cash_withdrawal}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="total_earned" class="col-sm-2 control-label">Total Earned</label>
		                                <div class="col-sm-10"><input type="text" name="total_earned" id="total_earned" value="{{$userwalletcash->total_earned}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					           {{generateLanguageDependentData("user_wallet_cash")}}                             
                            <p class="stdformbutton">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="reset" class="btn">Reset</button>
                            </p>
                        </form>
                        <script src="{{asset('/ba/assets/js/common.js')}}"></script> 
                         <script type="text/javascript">
						  var arr_error_label = ['user_id','cash_available','cash_withdrawal','total_earned',];
						   if (typeof(arr_error_label) !== 'undefined') 
						    {addErrorLabel(arr_error_label);} 
						 </script>  
                   </div>
                   </div>
                   </div>
                   </div>
                    