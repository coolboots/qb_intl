 @include('cms::categories.filters')
		     <div class="row">
		     <div class="col-md-12">
 				<section class="tile">
 					<div class="tile-header dvd dvd-btm"><h1 class="custom-font">View All Categories</h1></div>
            			<div class="tile-body">
		     		
			            	 <div class="table-responsive">
			                <table class="table mb-0" id="">
			                   
			                    <thead>
			                        <tr>
			                          	<!-- <th class="head0 nosort"><input type="checkbox" class="checkall" /></th> -->
			                          	<th class="head0 sortby" sortby="title">Title</th>
						        <th class="head0 sortby" sortby="image">Image</th>
						        <th class="head0 sortby" sortby="parent_id">Parent Category</th>
						        <th class="head0 sortby" sortby="tags">Tags</th>
						        <th class="head0 sortby" sortby="status">Status</th>
						        <th class="head0">Actions</th>
			                           
			                        </tr>
			                    </thead>
			                    <tbody>
			                     @foreach($categories as $key => $value)
						        <tr>
						        	<!-- <td class="aligncenter"><span class="center">
			                            <input type="checkbox" actionid={{ $value->id }} />
			                          </span></td> -->
			                         <td>{{ $value->title }}</td>
							        <td><img src="{{ asset($value->image) }}" alt="" class="br-5 size-30x30"></td>
							        <td>
							        	@if(!empty($value->parent_id) || ($value->parent_id != 0))
							        	{{ @$categoryList[$value->parent_id]['title'] }}
							        	@endif
							        </td>
							        <td>{{ $value->tags }}</td>
							        <td>{{ $value->status }}</td>
							            
						           <!-- we will also add show, edit, and delete buttons -->
						            <td>

						               <!-- <a class="btn btn-small btn-success" href="{{ URL::to(getCurrentUrlPrefix() .'/'. $value->id) }}" storage="modal">Show</a> -->

						                <a class="btn btn-small btn-info" href="{{ URL::to(getCurrentUrlPrefix() .'/'. $value->id . '/edit') }}" storage="modal">Edit</a>

						                @if($value->parent_id != 0)
						                <a class="btn btn-small btn-warning delete" data-token="{{ csrf_token() }}" deleteid="{{$value->id}}"  url="{{url(getCurrentUrlPrefix())}}">Delete</a>
						                @endif

						            </td>
						        </tr>
						    @endforeach
			                    </tbody>
			                </table>
			                </div>
			                </div>
			                </section>
			                </div>
			               </div>
			                <div class="tile-footer dvd dvd-top">
			              <div class="row">
			                <!-- <div class="col-sm-3 hidden-xs">
			                  <select class="input-sm form-control w-sm inline bulk_action_type">
			                    <option value="">Select Action</option>
			                    <option value="1">Mark Active</option>
			                    <option value="2">Mark Inactive</option>
			                    <option value="3">Mark Deleted</option>
			                    
			                  </select>
			                  <button class="btn btn-sm btn-default br-3 bulk_action_trigger">Apply</button>
			                </div> -->
			                <div class="col-sm-4 text-left"><small class="text-muted">Total {{$categories->total()}} records found!</small></div>
			                <div class="col-sm-5 text-right">
			                 <ul class="pagination pagination-sm m-0">
			                <?php echo $categories->render(); ?>
			            	</ul>
			                 
			                </div>
			              </div>
			            </div>

			            <script src="{{asset('/ba/assets/js/common.js')}}"></script> 