
		   <div class="row">
		    <div class="col-md-12">
 <section class="tile filterblock">
 <div class="tile-header dvd dvd-btm">
              <h1 class="custom-font">Categories filter
              </h1>
             </div>
            <div class="tile-body">
              <form class="form-inline" role="form" id="filterForm" action="{{url('/'.getCurrentUrlPrefix())}}">
                
              <div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="stitle" name="title" value="{{Request::get("title")}}" placeholder="Enter Title" title="Title">
		                </div>
                    
                    <div class="form-group col-md-2">
		                  <select  name="parent_id" id="sparent_id" class="form-control chosen-select" changedValue="{{Request::get("parent_id")}}">
                        <option value="">Select Parent</option>
                          {!!prepareTreeDropdown(buildTree(getCategories()))!!}
                      </select>
		                </div>
                    <div class="form-group col-md-2">
                      <select  name="status" id="sstatus" class="form-control chosen-select" changedValue="{{Request::get("status")}}">
                        <option value="">Select Status</option>
                        <option value="ACTIVE">ACTIVE</option>
                        <option value="INACTIVE">INACTIVE</option>
                      </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Search</button>
                    <button type="reset" class="btn btn-default">Reset</button>
              </form>
            </div>
 </section>

 </div>
 </div>