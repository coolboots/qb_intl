<?php $tagsList = \Config::get("qureka.tags"); ?>
		   <div class="row">
		    <div class="col-md-8 col-md-offset-2">
		    <div class="panel panel-default panel-filled">
                <div class="panel-heading">
                  <h3 class="panel-title custom-font">Category Detail</h3>
                </div>
                <div class="panel-body">
		   
            	 <div class="table-responsive">
                <table class="table mb-0" id=""><tbody>     <tr>
                          <td class="width30">Title</td>
                          <td class="width70"><strong>{{$categories->title}}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Image</td>
                          <td class="width70"><img src="{{ asset($categories->image) }}" alt="" class="br-5 size-30x30"></td>
                      </tr>     <tr>
                          <td class="width30">Parent Category</td>
                          <td class="width70"><strong>
                            @if(!empty($categories->parent_id) || ($categories->parent_id != 0))
                            {{@$categoryList[$categories->parent_id]['title']}}
                            @endif
                          </strong></td>
                          </tr>     <tr>
                          <td class="width30">Tags</td>
                          <td class="width70"><strong>
                            @if(!empty($categories->tags))
                            <?php $tags = explode(',', $categories->tags); ?>
                            @foreach($tags as $tag)
                            {{$tagsList[$tag]}}
                            @endforeach
                            @endif
                          </strong></td>
                      </tr>
                  </tbody></table>
                </div>
                </div>
                </div>
                </div>
                </div>
                