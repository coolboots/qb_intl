<?php //p($categories); ?>
<div class="row">
		    <div class="col-md-12">
		    <div class="panel panel-default panel-filled">
                <div class="panel-heading">
                  <h3 class="panel-title custom-font">Edit Category</h3>
                </div>
                <div class="panel-body">

<form class="form-horizontal" action="{{ url('/'.getCurrentUrlPrefix().'/'.$categories->id) }}" method="POST" enctype="multipart/form-data" id="editForm">
            {{ method_field('PUT') }} 
            <input type="hidden" name="_token" value="{{ csrf_token() }}"><div class="form-group">
                  				<label for="title" class="col-sm-2 control-label">Title</label>
		                                <div class="col-sm-10"><input type="text" name="title" id="title" value="{{$categories->title}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					         <hr class="line-dashed line-full"/>
			    			{!!seoUrlsFieldEdit($categories->id,"category")!!}<div class="form-group">
                                <label for="image" class="col-sm-2 control-label">Image</label>
                                        <div class="col-sm-10"><input type="file" id="image" name="image" class="form-control">
                                        @if($categories->image)
		                                	<img src="{{asset($categories->image)}}" width="100px" />
		                                	@endif
                                        </div>
                                        
                                        </div>
                            <hr class="line-dashed line-full"/><div class="form-group">
                  				<label for="parent_id" class="col-sm-2 control-label">Parent Category</label>
		                                <div class="col-sm-10">

                                      <select  name="parent_id" id="parent_id" class="form-control chosen-select" changedValue="{{$categories->parent_id}}">
                                        <option value="">Select Parent</option>
                                        <option value="0">Self</option>
                                          {!!prepareTreeDropdown(buildTree(getCategories()))!!}
                                      </select>

                                    </div>
		                                </div>
                  <hr class="line-dashed line-full"/><div class="form-group">
                          <label for="tags" class="col-sm-2 control-label">Tags</label>
                                    <div class="col-sm-10">

                                      <select  name="tags[]" id="tags" class="form-control chosen-select" changedValue="{{$categories->tags}}" multiple>
                                        {!!getTagsTypes()!!}
                                      </select>

                                    </div>
                                    </div>

                  @if($categories->parent_id != 0)
                  <hr class="line-dashed line-full"/><div class="form-group">
                          <label for="Status" class="col-sm-2 control-label">Status</label>
                                    <div class="col-sm-10">

                                      <select  name="status" id="status" class="form-control chosen-select" changedValue="{{$categories->status}}">
                                        <option value="">Select Status</option>
                                          <option value="ACTIVE">ACTIVE</option>
                                           <option value="INACTIVE">INACTIVE</option>
                                      </select>

                                    </div>
                                    </div>
                  @endif
					        <hr class="line-dashed line-full"/>
					           <p class="stdformbutton">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="reset" class="btn">Reset</button>
                            </p>
                        </form>
                        <script src="{{asset('/ba/assets/js/common.js')}}"></script> 
                         <script type="text/javascript">
						  var arr_error_label = ['title','parent_id',];
						   if (typeof(arr_error_label) !== 'undefined') 
						    {addErrorLabel(arr_error_label);} 
						 </script>  
                   </div>
                   </div>
                   </div>
                   </div>
                    