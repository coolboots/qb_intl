
		    <div class="row">
		    <div class="col-md-12">
		    <div class="panel panel-default panel-filled">
                <div class="panel-heading">
                  <h3 class="panel-title custom-font">Import Questions</h3>
                </div>
                <div class="panel-body">
		   
		    <form class="form-horizontal" role="form" method="post" id="createForm" action="{{url('/'.getCurrentUrlPrefix().'/importExcel')}}" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                	<label for="category_id" class="col-sm-2 control-label">Category </label>
		                <div class="col-sm-10">
		                    <select  name="category_id" id="category_id" class="form-control chosen-select">
		                        <option value="">Select Category</option>
		                         {!!parentChildDropdown(buildTree(getCategories()))!!}
		                   	</select>
		             	</div>
		        </div>
		        
		        <hr class="line-dashed line-full"/>
		        <div class="form-group">
      				<label for="language_code" class="col-sm-2 control-label">Language Code</label>
                    <div class="col-sm-10">
                    	<select  name="language_code" id="language_code" class="form-control chosen-select">
		                    {!!getLanguages()!!}
		                </select>
                    </div>
                </div>
		        
		        <hr class="line-dashed line-full"/>
		        <div class="form-group">
      				<label for="tags" class="col-sm-2 control-label">Tags</label>
                    <div class="col-sm-10">
                    	<select  name="tags[]" id="tags" class="form-control chosen-select" multiple>
		                    {!!getTagsTypes()!!}
		                </select>
                    </div>
                </div>
                <hr class="line-dashed line-full"/>
                <div class="form-group">
                    <label for="status" class="col-sm-2 control-label">Status</label>
                    <div class="col-sm-10">
                    <select  name="status" id="status" class="form-control chosen-select">
                      {!!getStatuses()!!}
                    </select>
                    </div>
                    </div>
                <hr class="line-dashed line-full"/>
                <div class="form-group">
                    <label for="excel_import" class="col-sm-2 control-label">Upload</label>
                    <div class="col-sm-10">
                        <input type="file" name="excel_import" id="excel_import" class="form-control">
                    </div>
                </div>
		        <hr class="line-dashed line-full"/>

	           <p class="stdformbutton">
                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="reset" class="btn">Reset</button>
            </p>
        </form>
        <a href="{{ asset('ba/assets/question_import_sample.xlsx') }}" class="text-muted">Sample Excel Download</a>
        <script src="{{asset('/ba/assets/js/common.js')}}"></script> 
         <script type="text/javascript">
		  var arr_error_label = ['category_id','language_code','tags','excel_import'];
		   if (typeof(arr_error_label) !== 'undefined') 
		    {addErrorLabel(arr_error_label);} 
		 </script>  
    </div>
    </div>
    </div>
    </div>