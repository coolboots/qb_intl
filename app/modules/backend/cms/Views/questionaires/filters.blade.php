
		   <div class="row">
		    <div class="col-md-12">
 <section class="tile filterblock">
 <div class="tile-header dvd dvd-btm">
              <h1 class="custom-font">Questionaires filter
              </h1>
             </div>
            <div class="tile-body">
              <form class="form-inline" role="form" id="filterForm" action="{{url('/'.getCurrentUrlPrefix())}}">

            <div class="form-group col-md-2">
              <input type="text" class="form-control" id="squestion_title" name="question_title" value="{{Request::get("question_title")}}" placeholder="Enter Question Title" title="Question Title">
            </div>                
              <div class="form-group col-md-2">
              	<select  name="category_id" id="scategory_id" class="form-control chosen-select" changedValue="{{Request::get("category_id")}}">
                        <option value="">Select Category</option>
                          {!!parentChildDropdown(buildTree(getCategories()))!!}
                </select>
            </div>
            <div class="form-group col-md-2">
              <select  name="language_code" id="slanguage_code" class="form-control chosen-select" changedValue="{{Request::get("language_code")}}">
                    {!!getLanguages()!!}
                </select>

            </div>
            <div class="form-group col-md-2">
              <select  name="tags" id="stags" class="form-control chosen-select" changedValue="{{Request::get("tags")}}">
                    {!!getTagsTypes()!!}
                </select>
            </div>
            <div class="form-group col-md-2">
                <select  name="status" id="sstatus" class="form-control chosen-select" changedValue="{{Request::get("status")}}">
                          {!!getStatuses()!!}
                      </select>
            </div>

            <div class="form-group col-md-2">
              <div class="input-group datepicker">
              <input type="text" class="form-control" id="screated_date" name="created_date" value="{{Request::get("created_date")}}" placeholder="Created Date" title="Created Date" data-format="Y-m-d">
              <span class="input-group-addon"> <span class="fa fa-calendar"></span> </span>
            </div>
          </div>
            
                    <button type="submit" class="btn btn-primary">Search</button>
                    <button type="reset" class="btn btn-default">Reset</button>
              </form>


            </div>
 </section>

 </div>
 </div>