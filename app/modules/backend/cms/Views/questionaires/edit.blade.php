<?php
	$option = @$questionaires->option;
 ?>
<div class="row">
		    <div class="col-md-12">
		    <div class="panel panel-default panel-filled">
                <div class="panel-heading">
                  <h3 class="panel-title custom-font">Edit Question</h3>
                </div>
                <div class="panel-body">

<form class="form-horizontal" action="{{ url('/'.getCurrentUrlPrefix().'/'.$questionaires->id) }}" method="POST" enctype="multipart/form-data" id="editForm">
            {{ method_field('PUT') }} 
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
  				<label for="category_id" class="col-sm-2 control-label">Category</label>
                <div class="col-sm-10">
                	<select  name="category_id" id="category_id" class="form-control chosen-select" changedValue="{{$questionaires->category_id}}">
                        <option value="">Select Category</option>
                        {!!parentChildDropdown(buildTree(getCategories()))!!}
                    </select>
                </div>
            </div>
	        <hr class="line-dashed line-full"/>
	        <div class="form-group">
  				<label for="question_title" class="col-sm-2 control-label">Question Title</label>
                <div class="col-sm-10"><input type="text" name="question_title" id="question_title" value="{{$questionaires->question_title}}" class="form-control"></div>
            </div>

            <hr class="line-dashed line-full"/>
	        <div class="form-group">
  				<label for="question_title" class="col-sm-2 control-label">Option A</label>
                <div class="col-sm-10"><input type="text" name="option_A" id="option_A" value="{{(!empty($option)) ? $option->option_1 : ''}}" class="form-control"></div>
            </div>
            <hr class="line-dashed line-full"/>
	        <div class="form-group">
  				<label for="question_title" class="col-sm-2 control-label">Option B</label>
                <div class="col-sm-10"><input type="text" name="option_B" id="option_B" value="{{(!empty($option)) ? $option->option_2 : ''}}" class="form-control"></div>
            </div>
            <hr class="line-dashed line-full"/>
	        <div class="form-group">
  				<label for="question_title" class="col-sm-2 control-label">Option C</label>
                <div class="col-sm-10"><input type="text" name="option_C" id="option_C" value="{{(!empty($option)) ? $option->option_3 : ''}}" class="form-control"></div>
            </div>
	        <hr class="line-dashed line-full"/>
	        <div class="form-group">
  				<label for="language_code" class="col-sm-2 control-label">Language Code</label>
                <div class="col-sm-10">
                	<select  name="language_code" id="language_code" class="form-control chosen-select" changedValue="{{$questionaires->language_code}}">
                        {!!getLanguages()!!}
                    </select>
                </div>
            </div>
			<hr class="line-dashed line-full"/>
	        <div class="form-group">
  				<label for="correct_answer" class="col-sm-2 control-label">Correct Answer</label>
                <div class="col-sm-10">
                	<select  name="correct_answer" id="correct_answer" class="form-control chosen-select" changedValue="{{$questionaires->correct_answer}}">
                        {!!getOptionTypes()!!}
                    </select>

                </div>
            </div>
	        <hr class="line-dashed line-full"/>
	        <div class="form-group">
  				<label for="tags" class="col-sm-2 control-label">Tags</label>
                <div class="col-sm-10">
                	<select  name="tags[]" id="tags" class="form-control chosen-select" changedValue="{{$questionaires->tags}}" multiple>
                     {!!getTagsTypes()!!}
                    </select>
                </div>
            </div>
          <hr class="line-dashed line-full"/>
          <div class="form-group">
            <label for="status" class="col-sm-2 control-label">Status</label>
              <div class="col-sm-10">
                <select  name="status" id="status" class="form-control chosen-select" changedValue="{{$questionaires->status}}">
                   {!!getStatuses()!!}
                </select>
              </div>
              </div>
	        <hr class="line-dashed line-full"/>
	           <p class="stdformbutton">
                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="reset" class="btn">Reset</button>
            </p>
        </form>
        <script src="{{asset('/ba/assets/js/common.js')}}"></script> 
         <script type="text/javascript">
		  var arr_error_label = ['category_id','question_title','option_A','option_B','option_C','language_code','correct_answer','tags',];
		   if (typeof(arr_error_label) !== 'undefined') 
		    {addErrorLabel(arr_error_label);} 
		 </script>  
   </div>
   </div>
   </div>
   </div>
                    