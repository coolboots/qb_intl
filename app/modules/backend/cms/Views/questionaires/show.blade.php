<?php 
  $language = \Config::get("qureka.language");
  $tagsList = \Config::get("qureka.tags");
  $option = @$questionaires->option;
  if(isset($categoryList[$questionaires->category_id])){
      $catDetails = $categoryList[$questionaires->category_id];
      $parentCategory = (isset($categoryList[$catDetails['parent_id']])) ? $categoryList[$catDetails['parent_id']] : '';
      $parentTitle = (!empty($parentCategory))? $parentCategory['title'].' - ' : '';
    }
 ?>
		   <div class="row">
		    <div class="col-md-12">
		    <div class="panel panel-default panel-filled">
                <div class="panel-heading">
                  <h3 class="panel-title custom-font">Question Detail</h3>
                </div>
                <div class="panel-body">
		   
            	 <div class="table-responsive">
                <table class="table mb-0" id=""><tbody>     <tr>
                          <td class="width30">Category</td>
                          <td class="width70"><strong>
                            @if(!empty($questionaires->category_id) || ($questionaires->category_id != 0))
                                @if(isset($categoryList[$questionaires->category_id]))
                                  {{@$parentTitle}} 
                                  {{$categoryList[$questionaires->category_id]['title'] }}
                                 @endif
                              @endif
                          </strong></td>
                      </tr>     <tr>
                          <td class="width30">Question Title</td>
                          <td class="width70"><strong>{{$questionaires->question_title}}</strong></td>
                      </tr> 
                      <tr>
                          <td class="width30">Option A</td>
                          <td class="width70"><strong>{{@$option->option_1}}</strong></td>
                      </tr> 
                      <tr>
                          <td class="width30">Option B</td>
                          <td class="width70"><strong>{{@$option->option_2}}</strong></td>
                      </tr> 
                      <tr>
                          <td class="width30">Option C</td>
                          <td class="width70"><strong>{{@$option->option_3}}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Language Code</td>
                          <td class="width70"><strong>{{@$language[$questionaires->language_code]}}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Correct Answer</td>
                          <td class="width70"><strong>{{$questionaires->correct_answer}}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Tags</td>
                          <td class="width70"><strong>
                            @if(!empty($questionaires->tags))
                              <?php $tags = explode(',', $questionaires->tags); ?>
                              @foreach($tags as $tag)
                              {{$tagsList[$tag]}}
                              @endforeach
                            @endif</strong></td>
                      </tr>
                      <tr>
                          <td class="width30">Status</td>
                          <td class="width70"><strong>{{$questionaires->status}}</strong></td>
                      </tr> 
                  </tbody></table>
                </div>
                </div>
                </div>
                </div>
                </div>
                