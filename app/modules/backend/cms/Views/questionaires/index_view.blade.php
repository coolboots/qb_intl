 @include('cms::questionaires.filters')
 <?php 
 	$language = \Config::get("qureka.language");
 	$tagsList = \Config::get("qureka.tags");
 ?>
		     <div class="row">
		     <div class="col-md-12">
 				<section class="tile">
 					<div class="tile-header dvd dvd-btm"><h1 class="custom-font">View All Questionaires</h1></div>
            			<div class="tile-body">
		     		
			            	 <div class="table-responsive">
			                <table class="table mb-0" id="">
			                   
			                    <thead>
			                        <tr>
			                          	<th class="head0 nosort"><input type="checkbox" class="checkall" /></th>
			                          	<!-- <th class="head0 sortby" sortby="category_id">Category </th> -->
						        <th class="head0 sortby" sortby="question_title">Question</th>
						        <!-- <th class="head0 sortby" sortby="language_code">Language</th> -->
						        <th class="head0 sortby" sortby="question_title">Option A</th>
						        <th class="head0 sortby" sortby="question_title">Option B</th>
						        <th class="head0 sortby" sortby="question_title">Option C</th>
						        <th class="head0 sortby" sortby="correct_answer">Answer</th>
						        <th class="head0 sortby" sortby="tags">Tags</th>
						        <th class="head0 sortby" sortby="status">Status</th>
						        <th class="head0 sortby" sortby="created_at">Created date</th>
						        <th class="head0">Actions</th>
			                           
			                        </tr>
			                    </thead>
			                    <tbody>
			                     @foreach($questionaires as $key => $value)
						        <tr>
						        	<td class="aligncenter"><span class="center">
			                            <input type="checkbox" actionid={{ $value->id }} />
			                          </span></td>
			                        <!-- <td>
			                        	@if(!empty($value->category_id) || ($value->category_id != 0))
			                        	@if(isset($categoryList[$value->category_id]))
							        	{{$categoryList[$value->category_id]['title'] }}
							        	@endif
							        	@endif
			                        </td> -->
							        <td>{{ $value->question_title }}</td>
							        <!-- <td>{{ $language[$value->language_code] }}</td> -->
							        <td>{{ $value->option_1 }}</td>
							        <td>{{ $value->option_2 }}</td>
							        <td>{{ $value->option_3 }}</td>
							        <td>{{ $value->correct_answer }}</td>
							        <td>@if(!empty($value->tags))
			                              <?php $tags = explode(',', $value->tags); ?>
			                              @foreach($tags as $tag)
			                              {{$tagsList[$tag]}}
			                              @endforeach
			                            @endif</td>
			                        <td>{{ $value->status }}</td>
			                        <td>{{ date_format(date_create($value->created_at),"d M Y") }}</td>
							            
						           <!-- we will also add show, edit, and delete buttons -->
						            <td>

						               <a class="btn btn-small btn-success" href="{{ URL::to(getCurrentUrlPrefix() .'/'. $value->id) }}" storage="modal">Show</a>

						                <a class="btn btn-small btn-info" href="{{ URL::to(getCurrentUrlPrefix() .'/'. $value->id . '/edit') }}" storage="modal">Edit</a>

						                <a class="btn btn-small btn-warning delete" data-token="{{ csrf_token() }}" deleteid="{{$value->id}}"  url="{{url(getCurrentUrlPrefix())}}">Delete</a>

						            </td>
						        </tr>
						    @endforeach
			                    </tbody>
			                </table>
			                </div>
			                </div>
			                </section>
			                </div>
			               </div>
			                <div class="tile-footer dvd dvd-top">
			              <div class="row">
			                <div class="col-sm-3 hidden-xs">
			                  <select class="input-sm form-control w-sm inline bulk_action_type">
			                    <option value="">Select Action</option>
			                    <option value="1">Mark Active</option>
			                    <option value="2">Mark Inactive</option>
			                    <option value="3">Mark Deleted</option>
			                    
			                  </select>
			                  <button class="btn btn-sm btn-default br-3 bulk_action_trigger">Apply</button>
			                </div>
			                <div class="col-sm-4 text-left"><small class="text-muted">Total {{$questionaires->total()}} records found!</small></div>
			                <div class="col-sm-5 text-right">
			                 <ul class="pagination pagination-sm m-0">
			                <?php echo $questionaires->render(); ?>
			            	</ul>
			                 
			                </div>
			              </div>
			            </div>
	<script src="{{asset('/ba/assets/js/common.js')}}"></script>
	<link rel="stylesheet" href="{{asset('/ba/assets/js/vendor/datetimepicker/css/bootstrap-datetimepicker.min.css')}}">
              <script src="{{asset('/ba/assets/js/vendor/datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script>
              <script type="text/javascript">
                  
                  $(document).ready(function(){

                    $('.datepicker').each(function() {
                              var element = $(this);
                              var format = element.data('format')
                              element.datetimepicker({
                                  format:'YYYY-MM-DD',
                                  sideBySide : true,
                              });
                              });

                  });

              
                              
                          
             </script>  