
		   <div class="row">
		    <div class="col-md-12">
 <section class="tile filterblock">
 <div class="tile-header dvd dvd-btm">
              <h1 class="custom-font">Userwallettranshistory filter
              </h1>
             </div>
            <div class="tile-body">
              <form class="form-inline" role="form" id="filterForm" action="{{url('/'.getCurrentUrlPrefix())}}">
                
              <div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="suser_id" name="user_id" value="{{Request::get("user_id")}}" placeholder="Enter User Id" title="User Id">
		                </div><div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="smode_type" name="mode_type" value="{{Request::get("mode_type")}}" placeholder="Enter Mode Type" title="Mode Type">
		                </div><div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="scoin_type" name="coin_type" value="{{Request::get("coin_type")}}" placeholder="Enter Coin Type" title="Coin Type">
		                </div><div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="samount" name="amount" value="{{Request::get("amount")}}" placeholder="Enter Amount" title="Amount">
		                </div><div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="sremarks" name="remarks" value="{{Request::get("remarks")}}" placeholder="Enter Remarks" title="Remarks">
		                </div>
                    <button type="submit" class="btn btn-primary">Search</button>
                    <button type="reset" class="btn btn-default">Reset</button>
              </form>
            </div>
 </section>

 </div>
 </div>