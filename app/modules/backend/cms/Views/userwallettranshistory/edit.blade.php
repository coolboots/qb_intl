
<div class="row">
		    <div class="col-md-8 col-md-offset-2">
		    <div class="panel panel-default panel-filled">
                <div class="panel-heading">
                  <h3 class="panel-title custom-font">Edit Userwallettranshistory</h3>
                </div>
                <div class="panel-body">

<form class="form-horizontal" action="{{ url('/'.getCurrentUrlPrefix().'/'.$userwallettranshistory->id) }}" method="POST" enctype="multipart/form-data" id="editForm">
            {{ method_field('PUT') }} 
            <input type="hidden" name="_token" value="{{ csrf_token() }}"><div class="form-group">
                  				<label for="user_id" class="col-sm-2 control-label">User Id</label>
		                                <div class="col-sm-10"><input type="text" name="user_id" id="user_id" value="{{$userwallettranshistory->user_id}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="mode_type" class="col-sm-2 control-label">Mode Type</label>
		                                <div class="col-sm-10"><input type="text" name="mode_type" id="mode_type" value="{{$userwallettranshistory->mode_type}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="coin_type" class="col-sm-2 control-label">Coin Type</label>
		                                <div class="col-sm-10"><input type="text" name="coin_type" id="coin_type" value="{{$userwallettranshistory->coin_type}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="amount" class="col-sm-2 control-label">Amount</label>
		                                <div class="col-sm-10"><input type="text" name="amount" id="amount" value="{{$userwallettranshistory->amount}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="remarks" class="col-sm-2 control-label">Remarks</label>
		                                <div class="col-sm-10"><input type="text" name="remarks" id="remarks" value="{{$userwallettranshistory->remarks}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					           {{generateLanguageDependentData("user_wallet_trans_history")}}                             
                            <p class="stdformbutton">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="reset" class="btn">Reset</button>
                            </p>
                        </form>
                        <script src="{{asset('/ba/assets/js/common.js')}}"></script> 
                         <script type="text/javascript">
						  var arr_error_label = ['user_id','mode_type','coin_type','amount','remarks',];
						   if (typeof(arr_error_label) !== 'undefined') 
						    {addErrorLabel(arr_error_label);} 
						 </script>  
                   </div>
                   </div>
                   </div>
                   </div>
                    