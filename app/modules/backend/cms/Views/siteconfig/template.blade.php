@extends('admin.layouts.mainlayout')
@section('content')

<div class="row">
    <div class="col-md-12">
        <section class="tile">
            <div class="tile-header dvd dvd-btm">
            <h1 class="custom-font"><strong>Manage</strong> Siteconfig
                <span class="navigationlink">
                &nbsp; &nbsp; &nbsp;<a class="btn btn-primary" storage="modal" href="{{ url('/'.getCurrentUrlPrefix().'/create') }}">Add New</a>&nbsp;  &nbsp;&nbsp;
                <a class="btn btn-primary" href="{{ url('/'.getCurrentUrlPrefix()) }}">View All</a>&nbsp; &nbsp; &nbsp;

                </span>
            </h1>

            </div>
            <div class="tile-body">
            <!-- if there are creation errors, they will show here -->
            <div class="message alert" style="display: none;"></div>

                <div class="dynamiccontainer">
                @yield('page_content')
                </div>
            </div>
        </section>
        <div class="divider15"></div>
    </div>
    <!--span8-->
</div>
<!--row-fluid-->

@endsection

@section('dynamic_js')

<script type="text/javascript">
    var page_url = "<?php echo url(getCurrentUrlPrefix());?>";
</script>

<script src="{{asset('/ba/vue/pages/crud.js')}}"></script>

@endsection
