<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default panel-filled">
            <div class="panel-heading">
                <h3 class="panel-title custom-font">Siteconfig Detail</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table mb-0" id="">
                        <tbody>
                            <tr>
                                <td class="width30">Key name</td>
                                <td class="width70"><strong>{!!$siteconfig->keyname!!}</strong></td>
                            </tr>
                            <tr>
                                <td class="width30">Value</td>
                                <td class="width70"><strong>{!!$siteconfig->value!!}</strong></td>
                            </tr>
                            <tr>
                                <td class="width30">Description</td>
                                <td class="width70"><strong>{!!$siteconfig->description!!}</strong></td>
                            </tr>
                            <tr>
                                <td class="width30">Status</td>
                                <td class="width70"><strong>{!!$siteconfig->status!!}</strong></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>