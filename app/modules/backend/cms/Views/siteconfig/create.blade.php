<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default panel-filled">
            <div class="panel-heading">
                <h3 class="panel-title custom-font">Add New Siteconfig</h3>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" role="form" method="post" id="createForm" action="{{url('/'.getCurrentUrlPrefix())}}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group">
                        <label for="key" class="col-sm-2 control-label">Key</label>
                        <div class="col-sm-10"><input type="text" name="key" id="key" class="form-control"></div>
                    </div><hr class="line-dashed line-full"/>
                    <div class="form-group">
                        <label for="value" class="col-sm-2 control-label">Value</label>
                        <div class="col-sm-10"><textarea cols="80" rows="5" name="value" id="value" class="form-control ignoreeditor"></textarea></div>
                    </div><hr class="line-dashed line-full"/>
                    <div class="form-group">
                        <label for="desc" class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10"><textarea cols="80" rows="5" name="desc" id="desc" class="form-control ignoreeditor"></textarea></div>
                    </div><hr class="line-dashed line-full"/>

                    <div class="form-group">
                        <label for="status" class="col-sm-2 control-label">Status</label>
                        <div class="col-sm-10">
                            <select  name="status" id="status" class="form-control">
                                {{getStatuses()}}
                            </select>
                        </div>
                    </div><hr class="line-dashed line-full"/>
                    <p class="stdformbutton">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <button type="reset" class="btn">Reset</button>
                    </p>
                </form>
                <script src="{{asset('/ba/assets/js/common.js')}}"></script>
                <script type="text/javascript">
                    var arr_error_label = ['page_id','publisher_id','partner_id','status',];
                    if (typeof(arr_error_label) !== 'undefined')
                    {addErrorLabel(arr_error_label);}
                </script>
            </div>
        </div>
    </div>
</div>