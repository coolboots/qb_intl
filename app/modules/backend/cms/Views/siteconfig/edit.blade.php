<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default panel-filled">
            <div class="panel-heading"><h3 class="panel-title custom-font">Edit Siteconfig</h3></div>
            <div class="panel-body">
                <form class="form-horizontal" action="{{ url('/'.getCurrentUrlPrefix().'/'.$siteconfig->id) }}" method="POST" enctype="multipart/form-data" id="editForm">
                    {{ method_field('PUT') }}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"><div class="form-group">
                    <div class="form-group">
                        <label for="keyname" class="col-sm-2 control-label">Key Name</label>
                        <div class="col-sm-10"><input type="text" id="keyname" class="form-control" value="{{$siteconfig->keyname}}" readonly></div>
                    </div><hr class="line-dashed line-full"/>
                    <div class="form-group">
                        <label for="value" class="col-sm-2 control-label">Value</label>
                        <div class="col-sm-10"><textarea cols="80" rows="5" name="value" id="value" class="form-control ignoreeditor">{{$siteconfig->value}}</textarea></div>
                    </div><hr class="line-dashed line-full"/>
                    <div class="form-group">
                        <label for="description" class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10"><textarea cols="80" rows="5" name="description" id="description" class="form-control ignoreeditor">{{$siteconfig->description}}</textarea></div>
                    </div><hr class="line-dashed line-full"/>          
                    <div class="form-group">
                        <label for="status" class="col-sm-2 control-label">Status</label>
                        <div class="col-sm-10">
                            <select  name="status" id="status" class="form-control" changedValue="{{$siteconfig->status}}">
                                {{getStatuses()}}
                            </select>
                        </div>
                    </div><hr class="line-dashed line-full"/>
                    <p class="stdformbutton">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <button type="reset" class="btn">Reset</button>
                    </p>
                </form>
                <script src="{{asset('/ba/assets/js/common.js')}}"></script>
                <script type="text/javascript">
                    var arr_error_label = ['page_id','publisher_id','partner_id','status',];
                    if (typeof(arr_error_label) !== 'undefined')
                    {addErrorLabel(arr_error_label);}
                </script>
            </div>
        </div>
    </div>
</div>