<div class="row">
    <div class="col-md-12">
        <section class="tile filterblock">
            <div class="tile-header dvd dvd-btm">
                <h1 class="custom-font">SiteConfig filter</h1>
            </div>
            <div class="tile-body">
                <form class="form-inline" role="form" id="filterForm" action="{{url('/'.getCurrentUrlPrefix())}}">
                    <div class="form-group col-md-2">
                        <input name="keyname" id="skeyname" class="form-control" type="text" placeholder="Search KeyName">
                    </div>
                    <div class="form-group col-md-2">
                        <input name="value" id="svalue" class="form-control" type="text" placeholder="Search Value">
                    </div>
                    <div class="form-group col-md-2">
                        <input name="description" id="sdescription" class="form-control" type="text" placeholder="Search Description">
                    </div>
                    <div class="form-group col-md-2">
                        <select  name="status" id="sstatus" class="form-control" changedValue="{{Request::get("status")}}">
                            {{getStatuses()}}
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Search</button>
                    <button type="reset" class="btn btn-default">Reset</button>
                </form>
            </div>
        </section>
    </div>
</div>