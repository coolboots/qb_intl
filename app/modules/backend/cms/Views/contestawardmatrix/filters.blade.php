
		   <div class="row">
		    <div class="col-md-12">
 <section class="tile filterblock">
 <div class="tile-header dvd dvd-btm">
              <h1 class="custom-font">Contestawardmatrix filter
              </h1>
             </div>
            <div class="tile-body">
              <form class="form-inline" role="form" id="filterForm" action="{{url('/'.getCurrentUrlPrefix())}}">
                
              <div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="scontest_id" name="contest_id" value="{{Request::get("contest_id")}}" placeholder="Enter Contest Id" title="Contest Id">
		                </div><div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="swinner_from" name="winner_from" value="{{Request::get("winner_from")}}" placeholder="Enter Winner From" title="Winner From">
		                </div><div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="swinner_to" name="winner_to" value="{{Request::get("winner_to")}}" placeholder="Enter Winner To" title="Winner To">
		                </div><div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="sprize_type" name="prize_type" value="{{Request::get("prize_type")}}" placeholder="Enter Prize Type" title="Prize Type">
		                </div><div class="form-group col-md-2">
		                  <input type="text" class="form-control" id="sprize_amount" name="prize_amount" value="{{Request::get("prize_amount")}}" placeholder="Enter Prize Amount" title="Prize Amount">
		                </div>
                    <button type="submit" class="btn btn-primary">Search</button>
                    <button type="reset" class="btn btn-default">Reset</button>
              </form>
            </div>
 </section>

 </div>
 </div>