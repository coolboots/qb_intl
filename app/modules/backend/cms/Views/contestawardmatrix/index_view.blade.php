 @include('cms::contestawardmatrix.filters')
		     <div class="row">
		     <div class="col-md-12">
 				<section class="tile">
 					<div class="tile-header dvd dvd-btm"><h1 class="custom-font">View All Contestawardmatrix</h1></div>
            			<div class="tile-body">
		     		
			            	 <div class="table-responsive">
			                <table class="table mb-0" id="">
			                   
			                    <thead>
			                        <tr>
			                          	<th class="head0 nosort"><input type="checkbox" class="checkall" /></th>
			                          	<th class="head0 sortby" sortby="contest_id">Contest Id</th>
						        <th class="head0 sortby" sortby="winner_from">Winner From</th>
						        <th class="head0 sortby" sortby="winner_to">Winner To</th>
						        <th class="head0 sortby" sortby="prize_type">Prize Type</th>
						        <th class="head0 sortby" sortby="prize_amount">Prize Amount</th>
						        <th class="head0">Actions</th>
			                           
			                        </tr>
			                    </thead>
			                    <tbody>
			                     @foreach($contest_award_matrix as $key => $value)
						        <tr><td class="aligncenter"><span class="center">
			                            <input type="checkbox" actionid={{ $value->id }} />
			                          </span></td><td>{{ $value->contest_id }}</td>
							        <td>{{ $value->winner_from }}</td>
							        <td>{{ $value->winner_to }}</td>
							        <td>{{ $value->prize_type }}</td>
							        <td>{{ $value->prize_amount }}</td>
							            
						           <!-- we will also add show, edit, and delete buttons -->
						            <td>

						               <a class="btn btn-small btn-success" href="{{ URL::to(getCurrentUrlPrefix() .'/'. $value->id) }}">Show</a>

						                <a class="btn btn-small btn-info" href="{{ URL::to(getCurrentUrlPrefix() .'/'. $value->id . '/edit') }}">Edit</a>

						                <a class="btn btn-small btn-warning delete" data-token="{{ csrf_token() }}" deleteid="{{$value->id}}"  url="{{url(getCurrentUrlPrefix())}}">Delete</a>

						            </td>
						        </tr>
						    @endforeach
			                    </tbody>
			                </table>
			                </div>
			                </div>
			                </section>
			                </div>
			               </div>
			                <div class="tile-footer dvd dvd-top">
			              <div class="row">
			                <div class="col-sm-3 hidden-xs">
			                  <select class="input-sm form-control w-sm inline bulk_action_type">
			                    <option value="">Select Action</option>
			                    <option value="1">Mark Active</option>
			                    <option value="2">Mark Inactive</option>
			                    <option value="3">Mark Deleted</option>
			                    
			                  </select>
			                  <button class="btn btn-sm btn-default br-3 bulk_action_trigger">Apply</button>
			                </div>
			                <div class="col-sm-4 text-left"><small class="text-muted">Total {{$contest_award_matrix->total()}} records found!</small></div>
			                <div class="col-sm-5 text-right">
			                 <ul class="pagination pagination-sm m-0">
			                <?php echo $contest_award_matrix->render(); ?>
			            	</ul>
			                 
			                </div>
			              </div>
			            </div>