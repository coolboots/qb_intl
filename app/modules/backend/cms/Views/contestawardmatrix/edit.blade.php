
<div class="row">
		    <div class="col-md-8 col-md-offset-2">
		    <div class="panel panel-default panel-filled">
                <div class="panel-heading">
                  <h3 class="panel-title custom-font">Edit Contestawardmatrix</h3>
                </div>
                <div class="panel-body">

<form class="form-horizontal" action="{{ url('/'.getCurrentUrlPrefix().'/'.$contestawardmatrix->id) }}" method="POST" enctype="multipart/form-data" id="editForm">
            {{ method_field('PUT') }} 
            <input type="hidden" name="_token" value="{{ csrf_token() }}"><div class="form-group">
                  				<label for="contest_id" class="col-sm-2 control-label">Contest Id</label>
		                                <div class="col-sm-10"><input type="text" name="contest_id" id="contest_id" value="{{$contestawardmatrix->contest_id}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="winner_from" class="col-sm-2 control-label">Winner From</label>
		                                <div class="col-sm-10"><input type="text" name="winner_from" id="winner_from" value="{{$contestawardmatrix->winner_from}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="winner_to" class="col-sm-2 control-label">Winner To</label>
		                                <div class="col-sm-10"><input type="text" name="winner_to" id="winner_to" value="{{$contestawardmatrix->winner_to}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="prize_type" class="col-sm-2 control-label">Prize Type</label>
		                                <div class="col-sm-10"><input type="text" name="prize_type" id="prize_type" value="{{$contestawardmatrix->prize_type}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="prize_amount" class="col-sm-2 control-label">Prize Amount</label>
		                                <div class="col-sm-10"><input type="text" name="prize_amount" id="prize_amount" value="{{$contestawardmatrix->prize_amount}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					           {{generateLanguageDependentData("contest_award_matrix")}}                             
                            <p class="stdformbutton">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="reset" class="btn">Reset</button>
                            </p>
                        </form>
                        <script src="{{asset('/ba/assets/js/common.js')}}"></script> 
                         <script type="text/javascript">
						  var arr_error_label = ['contest_id','winner_from','winner_to','prize_type','prize_amount',];
						   if (typeof(arr_error_label) !== 'undefined') 
						    {addErrorLabel(arr_error_label);} 
						 </script>  
                   </div>
                   </div>
                   </div>
                   </div>
                    