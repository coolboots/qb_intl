
<div class="row">
		    <div class="col-md-8 col-md-offset-2">
		    <div class="panel panel-default panel-filled">
                <div class="panel-heading">
                  <h3 class="panel-title custom-font">Edit Questionoption</h3>
                </div>
                <div class="panel-body">

<form class="form-horizontal" action="{{ url('/'.getCurrentUrlPrefix().'/'.$questionairesopt->id) }}" method="POST" enctype="multipart/form-data" id="editForm">
            {{ method_field('PUT') }} 
            <input type="hidden" name="_token" value="{{ csrf_token() }}"><div class="form-group">
                  				<label for="questionaire_id" class="col-sm-2 control-label">Questionaire Id</label>
		                                <div class="col-sm-10"><input type="text" name="questionaire_id" id="questionaire_id" value="{{$questionairesopt->questionaire_id}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="option_1" class="col-sm-2 control-label">Option 1</label>
		                                <div class="col-sm-10"><input type="text" name="option_1" id="option_1" value="{{$questionairesopt->option_1}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="option_2" class="col-sm-2 control-label">Option 2</label>
		                                <div class="col-sm-10"><input type="text" name="option_2" id="option_2" value="{{$questionairesopt->option_2}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="option_3" class="col-sm-2 control-label">Option 3</label>
		                                <div class="col-sm-10"><input type="text" name="option_3" id="option_3" value="{{$questionairesopt->option_3}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					           <p class="stdformbutton">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="reset" class="btn">Reset</button>
                            </p>
                        </form>
                        <script src="{{asset('/ba/assets/js/common.js')}}"></script> 
                         <script type="text/javascript">
						  var arr_error_label = ['questionaire_id','option_1','option_2','option_3',];
						   if (typeof(arr_error_label) !== 'undefined') 
						    {addErrorLabel(arr_error_label);} 
						 </script>  
                   </div>
                   </div>
                   </div>
                   </div>
                    