<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//die('amgetting loaded');
//array('xssprotection','frameguard','admin')
Route::group(['module'=>'cms','namespace' => 'App\modules\backend\cms\Controllers','prefix'=>'admin','middleware'=>['web','admin','adminroleauthenticate']], function() {

		Route::get('/', 'CmsController@getHomePage');
		Route::get('/mail', 'CmsController@basic_email');
		Route::get('/home', 'CmsController@getHomePage');
		Route::get('/dashboard', 'CmsController@getHomePage');

		Route::resource('categories', 'CategoriesController');
		Route::get('questionaires/import', 'QuestionairesController@import');
		Route::post('questionaires/importExcel', 'QuestionairesController@importExcel');
		Route::resource('questionaires', 'QuestionairesController');
		Route::resource('questionairesopt', 'QuestionairesoptController');
		Route::get('contests/matrix/{id}', 'ContestsController@getMatrix');
		Route::post('contests/matrix/{id}', 'ContestsController@updateMatrix');
		Route::resource('contests', 'ContestsController');
		Route::get('contests/{id}/clone', 'ContestsController@clone');
		Route::post('contests/clone/save', 'ContestsController@saveClone');

		Route::resource('pages', 'PagesController');
		Route::resource('partners', 'PartnersController');
		Route::resource('publishers', 'PublishersController');
		Route::resource('adsconfig', 'AdsconfigController');
		Route::get('ads_config/import', 'AdsconfigController@import')->name('adsconf.import.view');
		Route::post('ads_config_import', 'AdsconfigController@importExcel')->name('adsconf.import.upload');
		Route::resource('siteconfig', 'SiteconfigController');




		Route::resource('awardmatrix', 'AwardmatrixController');

		Route::resource('admins', 'AdminsController');
		Route::resource('articles', 'ArticlesController');
		Route::resource('cmsseourls', 'CmsseourlsController');
		Route::resource('contestawardmatrix', 'ContestawardmatrixController');
		Route::resource('contestparticipanthistory', 'ContestparticipanthistoryController');
		
		Route::resource('contestwinners', 'ContestwinnersController');
		
		
		Route::resource('users', 'UsersController');
		Route::get('users/{id}/history', 'UsersController@getHistory');
		Route::resource('userwalletcash', 'UserwalletcashController');
		Route::resource('userwalletcoins', 'UserwalletcoinsController');
		Route::resource('userwallettranshistory', 'UserwallettranshistoryController');
	
		# Reports Management
		Route::get('quizuniqueuser', 'ReportsController@quizUniqueUser');
		Route::get('crickuniqueuser', 'ReportsController@crickuniqueuser');
		Route::get('quizcontestplayed', 'ReportsController@quizContestPlayed');
		Route::get('crickcontestplayed', 'ReportsController@crickContestPlayed');
		Route::get('loginusers', 'ReportsController@loginUsers');
		Route::get('newloginusers', 'ReportsController@newLoginUsers');
		Route::get('goolelogins', 'ReportsController@gooleLogins');
		Route::get('facebooklogins', 'ReportsController@facebookLogins');

		//common elements
		Route::resource('serverinfo', 'ServerinfoController');

		Route::get('/unauthorized', 'CmsController@unauthorized');
		Route::get('testemail', 'CmsController@testEmail');
		
}); 
