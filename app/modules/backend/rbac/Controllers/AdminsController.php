<?php namespace App\modules\backend\rbac\Controllers;

		use App\Http\Requests;
		use App\Http\Controllers\Controller;
		use Request;
		use App\modules\backend\rbac\Models\Admin;
		use App\modules\backend\rbac\Models\Usergroup;
		use Validator;
		use Input;
		use Redirect;

		class AdminsController extends Controller {

			/**
			 * Display a listing of the resource.
			 *
			 * @return Response
			 */
			public function __construct()
			{
				$this->middleware('adminauth');
			}

			public function index()
			{
				//
				// get all the Admin
		        $admins = Admin::paginate(50);
		        $admins = Admin::select('admins.id','admins.name','admins.email','rk_usergroups.name as usergroup_id')
		        ->leftJoin('rk_usergroups', 'admins.usergroup_id', '=', 'rk_usergroups.usergroup_id')
		        ->paginate(50);
		        $admins->setPath('adminuser');
				$admins->appends(Request::except('page'));
				return view('rbac::admins.index')->with('admins', $admins);
			}

			/**
			 * Show the form for creating a new resource.
			 *
			 * @return Response
			 */
			public function create()
			{
				//
				$data['usergroups']= Usergroup::where('status','Active')->where('user_type','A')->get()->toArray();
				//echo '<pre>';print_r($data);die;
				return view('rbac::admins.create')->with($data);
			}

			/**
			 * Store a newly created resource in storage.
			 *
			 * @return Response
			 */
			public function store()
			{
				//
				// validate
		        // read more on validation at http://laravel.com/docs/validation
				$rules = array('name'=>'required',
			        'email'=>'required',
			        'password'=>'required',
			        'usergroup_id'=>'required',
			        // 'remember_token'=>'required',
			        );
		        
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		            return Redirect::to(getCurrentUrlPrefix().'/create')
		                ->withErrors($validator)
		                ->withInput(Request::except('password'));
		        } else {
		            // store
		            
		            $admins = new Admin;$admins->name = Request::get('name');	
						$admins->email = Request::get('email');	
						$admins->password = bcrypt(Request::get('password'));	
						$admins->usergroup_id = Request::get('usergroup_id');	
						// $admins->remember_token = Request::get('remember_token');	
						$admins->save();

					#Send Email to user
					
		            $data = Request::all();
		            $data['url'] = \Config::get("qureka.app_url");
		            $data['fromEmail'] = \Config::get("qureka.from_email");

					\Mail::send('emails.new_user_email', ['data'=>$data], function ($message) use ($data)
						{
						    $message->from($data['fromEmail'], 'CoolBoots Media');
						    $message->to($data['email']);
						    $message->bcc(array('raj.kamal@coolbootsmedia.com','anup.kumar@coolbootsmedia.com','anjali.rai@coolbootsmedia.com'));
						    $message->subject('Qureka Lite Backend Creadentials');
						}); 

		            // redirect
		            \Session::flash('message', 'Successfully created Admins!');
		            return Redirect::to(getCurrentUrlPrefix());
		        }
			}

			/**
			 * Display the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function show($id)
			{
				//
				 $admins = Admin::find($id);

				 if(empty($admins))
				 {
				 	// redirect
		            \Session::flash('message', 'Oops! Id doesn\'t exists');
		            return Redirect::to(getCurrentUrlPrefix());

				 }

		        // show the view and pass the admins to it
		        return view('rbac::admins.show')->with('admins', $admins);
			}

			/**
			 * Show the form for editing the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function edit($id)
			{
				//
				$admins = Admin::find($id);
				if(empty($admins))
				 {
				 	// redirect
		            \Session::flash('message', 'Oops! Id doesn\'t exists');
		            return Redirect::to(getCurrentUrlPrefix());

				 }
				 $data = array();
				 $data['admins']=Admin::find($id);
				 $data['usergroups']= Usergroup::where('status','Active')->where('user_type','A')->get()->toArray();
				//echo '<pre>';print_r($data);die;
		        // show the view and pass the admins to it
		        return view('rbac::admins.edit')->with($data);
			}

			/**
			 * Update the specified resource in storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function update($id)
			{
				// read more on validation at http://laravel.com/docs/validation
		       $rules = array();
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		            return Redirect::to(getCurrentUrlPrefix().'/'.$id.'/edit')
		                ->withErrors($validator)
		                ->withInput(Request::except('password'));
		        } else {
		            // store
		             $admins = Admin::find($id);
		            $admins->name       = Request::get('name');	
					$admins->email       = Request::get('email');	
					$password = Request::get('password');
					if(!empty($password))
					$admins->password       = bcrypt(Request::get('password'));

					$admins->usergroup_id       = Request::get('usergroup_id');	
					// $admins->remember_token       = Request::get('remember_token');	
					$admins->save();
		           

		            // redirect
		            \Session::flash('message', 'Successfully Updated Admins!');
		            return Redirect::to(getCurrentUrlPrefix());
		        }
			}

			/**
			 * Remove the specified resource from storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function destroy($id)
			{
				//
				$admins = Admin::find($id);
			 
			    $admins->delete();
			    \Session::flash('message', 'Successfully Deleted Admins!');
		         //return Redirect::to('adminuser');
			    echo 'Successfully Deleted Admins!';
			}

			public function getSettings()
			{
				$admindata = auth()->guard('admin')->user();
	    		$id = $admindata->id;
				$admins = Admin::find($id);
				if(empty($admins))
				 {
				 	// redirect
		            \Session::flash('message', 'Oops! Id doesn\'t exists');
		            return Redirect::to(getCurrentUrlPrefix());

				 }
				 $data = array();
				 $data['admins']=$admins;
				 $data['usergroups']= Usergroup::where('status','Active')->where('user_type','A')->get()->toArray();
				//echo '<pre>';print_r($data);die;
		        // show the view and pass the admins to it
		        return view('rbac::admins.settings')->with($data);

			}

			public function applySettings()
			{
				// read more on validation at http://laravel.com/docs/validation
		       $rules = array();
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		            return Redirect::to(getCurrentUrlPrefix().'/settings')
		                ->withErrors($validator)
		                ->withInput(Request::except('password'));
		        } else {

		        	$admindata = auth()->guard('admin')->user();
	    			$id = $admindata->id;
		            // store
		            $admins = Admin::find($id);
		            $admins->name       = Request::get('name');	
					//$admins->email       = Request::get('email');	
					$password = Request::get('password');
					if(!empty($password))
					$admins->password       = bcrypt(Request::get('password'));

					//$admins->usergroup_id       = Request::get('usergroup_id');	
					// $admins->remember_token       = Request::get('remember_token');	
					$admins->save();
		           

		            // redirect
		            \Session::flash('message', 'Successfully updated your details!');
		            return Redirect::to(getCurrentUrlPrefix());
		        }
			}

		}
		
