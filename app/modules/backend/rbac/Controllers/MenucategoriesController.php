<?php namespace App\modules\backend\rbac\Controllers;

		use App\Http\Requests;
		use App\Http\Controllers\Controller;
		use Request;
		use App\modules\backend\rbac\Models\Menucategory;
		use Validator;
		use Input;
		use Redirect;

		class MenucategoriesController extends Controller {

			/**
			 * Display a listing of the resource.
			 *
			 * @return Response
			 */
			public function __construct()
			{
				$this->middleware('adminauth');
			}

			public function index()
			{
				//
				// get all the Menucategory
		        $rk_menu_categories = Menucategory::paginate(50);
		        $rk_menu_categories->setPath('menucategory');
				$rk_menu_categories->appends(Request::except('page'));
				return view('rbac::menucategories.index')->with('rk_menu_categories', $rk_menu_categories);
			}

			/**
			 * Show the form for creating a new resource.
			 *
			 * @return Response
			 */
			public function create()
			{
				//
				return view('rbac::menucategories.create');
			}

			/**
			 * Store a newly created resource in storage.
			 *
			 * @return Response
			 */
			public function store()
			{
				//
				// validate
		        // read more on validation at http://laravel.com/docs/validation
				$rules = array('name'=>'required',
			        'category_menu_link'=>'required',
			        'status'=>'required',
			        );
		        
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		            return Redirect::to(getCurrentUrlPrefix().'/create')
		                ->withErrors($validator)
		                ->withInput(Request::except('password'));
		        } else {
		            // store
		            $menucategories = new Menucategory;$menucategories->name = Request::get('name');	
						$menucategories->category_menu_link = Request::get('category_menu_link');	
						$menucategories->description = Request::get('description');	
						$menucategories->status = Request::get('status');	
						$menucategories->save();

		            // redirect
		            \Session::flash('message', 'Successfully created Menucategories!');
		            return Redirect::to(getCurrentUrlPrefix());
		        }
			}

			/**
			 * Display the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function show($id)
			{
				//
				 $menucategories = Menucategory::find($id);

				 if(empty($menucategories))
				 {
				 	// redirect
		            \Session::flash('message', 'Oops! Id doesn\'t exists');
		            return Redirect::to(getCurrentUrlPrefix());

				 }

		        // show the view and pass the menucategories to it
		        return view('rbac::menucategories.show')->with('menucategories', $menucategories);
			}

			/**
			 * Show the form for editing the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function edit($id)
			{
				//
				$menucategories = Menucategory::find($id);
				if(empty($menucategories))
				 {
				 	// redirect
		            \Session::flash('message', 'Oops! Id doesn\'t exists');
		            return Redirect::to(getCurrentUrlPrefix());

				 }

		        // show the view and pass the menucategories to it
		        return view('rbac::menucategories.edit')->with('menucategories', $menucategories);
			}

			/**
			 * Update the specified resource in storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function update($id)
			{
				// read more on validation at http://laravel.com/docs/validation
		       $rules = array();
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		            return Redirect::to(getCurrentUrlPrefix().'/'.$id.'/edit')
		                ->withErrors($validator)
		                ->withInput(Request::except('password'));
		        } else {
		            // store
		             $menucategories = Menucategory::find($id);$menucategories->name       = Request::get('name');	
					$menucategories->category_menu_link       = Request::get('category_menu_link');	
					$menucategories->description       = Request::get('description');	
					$menucategories->status       = Request::get('status');	
					$menucategories->save();
		           

		            // redirect
		            \Session::flash('message', 'Successfully Updated Menucategories!');
		            return Redirect::to(getCurrentUrlPrefix());
		        }
			}

			/**
			 * Remove the specified resource from storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function destroy($id)
			{
				//
				$menucategories = Menucategory::find($id);
			 
			    $menucategories->delete();
			    \Session::flash('message', 'Successfully Deleted Menucategories!');
		         //return Redirect::to('menucategory');
			    echo 'Successfully Deleted Menucategories!';
			}

		}
		