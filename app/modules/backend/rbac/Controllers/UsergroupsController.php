<?php namespace App\modules\backend\rbac\Controllers;

		use App\Http\Requests;
		use App\Http\Controllers\Controller;
		use Request;
		use App\modules\backend\rbac\Models\Usergroup;
		use App\modules\backend\rbac\Models\Module;
		use App\modules\backend\rbac\Models\Privilege;
		use Validator;
		use Input;
		use Redirect;

		class UsergroupsController extends Controller {

			/**
			 * Display a listing of the resource.
			 *
			 * @return Response
			 */
			public function __construct()
			{
				$this->middleware('adminauth');
			}

			public function index()
			{
				//
				// get all the Usergroup
		        $rk_usergroups = Usergroup::paginate(50);
		        $rk_usergroups->setPath('usergroup');
				$rk_usergroups->appends(Request::except('page'));
				return view('rbac::usergroups.index')->with('rk_usergroups', $rk_usergroups);
			}

			/**
			 * Show the form for creating a new resource.
			 *
			 * @return Response
			 */
			public function create()
			{
				//
				return view('rbac::usergroups.create');
			}

			/**
			 * Store a newly created resource in storage.
			 *
			 * @return Response
			 */
			public function store()
			{
				//
				// validate
		        // read more on validation at http://laravel.com/docs/validation
				$rules = array('lang_code'=>'required',
			        'name'=>'required',
			        'user_type'=>'required',
			        'status'=>'required',
			        // 'parent_usergroup'=>'required',
			        );
		        
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		            return Redirect::to(getCurrentUrlPrefix().'/create')
		                ->withErrors($validator)
		                ->withInput(Request::except('password'));
		        } else {
		            // store
		            $usergroups = new Usergroup;
		            $usergroups->lang_code = Request::get('lang_code');	
						$usergroups->name = Request::get('name');	
						$usergroups->user_type = Request::get('user_type');	
						$usergroups->status = Request::get('status');	
						// $usergroups->parent_usergroup = Request::get('parent_usergroup');	
						$usergroups->save();

		            // redirect
		            \Session::flash('message', 'Successfully created Usergroups!');
		            return Redirect::to(getCurrentUrlPrefix());
		        }
			}

			/**
			 * Display the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function show($id)
			{
				//
				 $usergroups = Usergroup::find($id);

				 if(empty($usergroups))
				 {
				 	// redirect
		            \Session::flash('message', 'Oops! Id doesn\'t exists');
		            return Redirect::to(getCurrentUrlPrefix());

				 }

		        // show the view and pass the usergroups to it
		        return view('rbac::usergroups.show')->with('usergroups', $usergroups);
			}

			/**
			 * Show the form for editing the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function edit($id)
			{
				//
				$usergroups = Usergroup::find($id);
				if(empty($usergroups))
				 {
				 	// redirect
		            \Session::flash('message', 'Oops! Id doesn\'t exists');
		            return Redirect::to(getCurrentUrlPrefix());

				 }

		        // show the view and pass the usergroups to it
		        return view('rbac::usergroups.edit')->with('usergroups', $usergroups);
			}

			/**
			 * Update the specified resource in storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function update($id)
			{
				// read more on validation at http://laravel.com/docs/validation
		       $rules = array();
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		            return Redirect::to(getCurrentUrlPrefix().'/'.$id.'/edit')
		                ->withErrors($validator)
		                ->withInput(Request::except('password'));
		        } else {
		            // store
		             $usergroups = Usergroup::find($id);$usergroups->lang_code       = Request::get('lang_code');	
					$usergroups->name       = Request::get('name');	
					$usergroups->user_type       = Request::get('user_type');	
					$usergroups->status       = Request::get('status');	
					// $usergroups->parent_usergroup       = Request::get('parent_usergroup');	
					$usergroups->save();
		           

		            // redirect
		            \Session::flash('message', 'Successfully Updated Usergroups!');
		            return Redirect::to(getCurrentUrlPrefix());
		        }
			}

			/**
			 * Remove the specified resource from storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function destroy($id)
			{
				//
				$usergroups = Usergroup::find($id);
			 
			    $usergroups->delete();
			    \Session::flash('message', 'Successfully Deleted Usergroups!');
		         //return Redirect::to('usergroup');
			    echo 'Successfully Deleted Usergroups!';
			}

			public function getModules($id)
			{

				\DB::table('rk_usergroup_privilege')->where('usergroup_id', '=', $id)
				->where('status','Inactive')->delete();

				//echo $id;
				$data = array();
				$data['modules'] = objToArray(Module::select('rk_modules.*','rk_menu_categories.name as menu_group')
				->leftJoin('rk_menu_categories', 'rk_modules.menu_type', '=', 'rk_menu_categories.menu_category_id')
				->where('rk_modules.status','Active')
				->get()->toArray(),'mapping_url');
				$data['privilege'] = objToArray(Privilege::select('privilege_id','usergroup_id','module_id','is_access','is_write')
									->where('usergroup_id',$id)->where('status','Active')->get()->toArray(),'module_id');
					$data['usergroup']=Usergroup::find($id);

				//echo '<pre>';print_r($data);die;
				return view('rbac::usergroups.assign')->with($data);


			}
			public function assignModules($id)
			{
				//echo $id;
				$accessData  = Request::get('is_access');
				$writeData  = Request::get('is_write');
				$privilegeData  = Request::get('privilege');
				if(!empty($accessData))
				{
					\DB::table('rk_usergroup_privilege')->where('usergroup_id', '=', $id)->update(array('status' => 'Inactive'));
					foreach ($accessData as $key => $value) 
					{
						# code...
						$privilege = new Privilege;
		            	$privilege->user_id = 0;	
						$privilege->usergroup_id = $id;	
						$privilege->module_id = $key;	
						$privilege->is_access = $value;	
						$privilege->is_write = $writeData[$key];	
						// $usergroups->parent_usergroup = Request::get('parent_usergroup');	
						$privilege->save();
						
					}
				}

				 \Session::flash('message', 'Successfully Assigned Modules!');
		            return Redirect::to(getCurrentUrlPrefix());
				//echo '<pre>';print_r(Request::all()); die;
				


			}

		}
		