<?php namespace App\modules\backend\rbac\Controllers;

		use App\Http\Requests;
		use App\Http\Controllers\Controller;
		use Request;
		use App\modules\backend\rbac\Models\Module;
		use App\modules\backend\rbac\Models\Menucategory;
		use Validator;
		use Input;
		use Redirect;

		class ModulesController extends Controller {

			/**
			 * Display a listing of the resource.
			 *
			 * @return Response
			 */
			public function __construct()
			{
				$this->middleware('adminauth');
			}

			public function index()
			{
				//
				// get all the Module
		        //$rk_modules = Module::paginate(50);
		        $rk_modules = Module::select('rk_modules.*','rk_menu_categories.name as menu_group')
		        ->leftJoin('rk_menu_categories', 'rk_modules.menu_type', '=', 'rk_menu_categories.menu_category_id')
		        ->paginate(50);
		        $rk_modules->setPath('module');
				$rk_modules->appends(Request::except('page'));
				return view('rbac::modules.index')->with('rk_modules', $rk_modules);
			}

			/**
			 * Show the form for creating a new resource.
			 *
			 * @return Response
			 */
			public function create()
			{
				//
				$data = array();
				$data['menucategory'] = Menucategory::where('status','Active')->get()->toArray();
				return view('rbac::modules.create')->with($data);
			}

			/**
			 * Store a newly created resource in storage.
			 *
			 * @return Response
			 */
			public function store()
			{
				//
				// validate
		        // read more on validation at http://laravel.com/docs/validation
				$rules = array('module_name'=>'required',
			        'mapping_url'=>'required',
			        // 'menu_type'=>'required',
			        // 'menu_name'=>'required',
			        // 'priority'=>'required',
			        'status'=>'required',
			        );
		        
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		            return Redirect::to(getCurrentUrlPrefix().'/create')
		                ->withErrors($validator)
		                ->withInput(Request::except('password'));
		        } else {
		            // store
		            $modules = new Module;$modules->module_name = Request::get('module_name');	
						$modules->descriptions = Request::get('descriptions');	
						$modules->module_type = Request::get('module_type');	
						$modules->mapping_url = Request::get('mapping_url');	
						$modules->menu_type = Request::get('menu_type');	
						$modules->menu_name = Request::get('menu_name');	
						$modules->priority = Request::get('priority');	
						$modules->status = Request::get('status');	
						$modules->save();

		            // redirect
		            \Session::flash('message', 'Successfully created Modules!');
		            return Redirect::to(getCurrentUrlPrefix());
		        }
			}

			/**
			 * Display the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function show($id)
			{
				//
				 $modules = Module::find($id);

				 if(empty($modules))
				 {
				 	// redirect
		            \Session::flash('message', 'Oops! Id doesn\'t exists');
		            return Redirect::to(getCurrentUrlPrefix());

				 }

		        // show the view and pass the modules to it
		        return view('rbac::modules.show')->with('modules', $modules);
			}

			/**
			 * Show the form for editing the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function edit($id)
			{
				//
				$data = array();
				$data['menucategory'] = Menucategory::where('status','Active')->get()->toArray();
				$modules = Module::find($id);
				if(empty($modules))
				 {
				 	// redirect
		            \Session::flash('message', 'Oops! Id doesn\'t exists');
		            return Redirect::to(getCurrentUrlPrefix());

				 }
				 $data['modules']=$modules;

		        // show the view and pass the modules to it
		        return view('rbac::modules.edit')->with($data);
			}

			/**
			 * Update the specified resource in storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function update($id)
			{
				// read more on validation at http://laravel.com/docs/validation
		       $rules = array();
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		            return Redirect::to(getCurrentUrlPrefix().'/'.$id.'/edit')
		                ->withErrors($validator)
		                ->withInput(Request::except('password'));
		        } else {
		            // store
		             $modules = Module::find($id);$modules->module_name       = Request::get('module_name');	
					$modules->descriptions       = Request::get('descriptions');	
					$modules->module_type = Request::get('module_type');	
					$modules->mapping_url       = Request::get('mapping_url');	
					$modules->menu_type       = Request::get('menu_type');	
					$modules->menu_name       = Request::get('menu_name');	
					$modules->priority       = Request::get('priority');	
					$modules->status       = Request::get('status');	
					$modules->save();
		           

		            // redirect
		            \Session::flash('message', 'Successfully Updated Modules!');
		            return Redirect::to(getCurrentUrlPrefix());
		        }
			}

			/**
			 * Remove the specified resource from storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function destroy($id)
			{
				//
				$modules = Module::find($id);
			 
			    $modules->delete();
			    \Session::flash('message', 'Successfully Deleted Modules!');
		         //return Redirect::to('module');
			    echo 'Successfully Deleted Modules!';
			}

		}
		