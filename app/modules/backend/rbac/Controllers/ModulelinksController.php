<?php namespace App\modules\backend\rbac\Controllers;

		use App\Http\Requests;
		use App\Http\Controllers\Controller;
		use Request;
		use App\modules\backend\rbac\Models\Modulelink;
		use Validator;
		use Input;
		use Redirect;

		class ModulelinksController extends Controller {

			/**
			 * Display a listing of the resource.
			 *
			 * @return Response
			 */
			public function __construct()
			{
				$this->middleware('adminauth');
			}

			public function index()
			{
				//
				// get all the Modulelink
		        $rk_module_links = Modulelink::paginate(50);
		        $rk_module_links->setPath('modulelink');
				$rk_module_links->appends(Request::except('page'));
				return view('rbac::modulelinks.index')->with('rk_module_links', $rk_module_links);
			}

			/**
			 * Show the form for creating a new resource.
			 *
			 * @return Response
			 */
			public function create()
			{
				//
				return view('rbac::modulelinks.create');
			}

			/**
			 * Store a newly created resource in storage.
			 *
			 * @return Response
			 */
			public function store()
			{
				//
				// validate
		        // read more on validation at http://laravel.com/docs/validation
				$rules = array('user_id'=>'required',
			        'usergroup_id'=>'required',
			        'module_id'=>'required',
			        'status'=>'required',
			        );
		        
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		            return Redirect::to(getCurrentUrlPrefix().'/create')
		                ->withErrors($validator)
		                ->withInput(Request::except('password'));
		        } else {
		            // store
		            $modulelinks = new Modulelink;$modulelinks->user_id = Request::get('user_id');	
						$modulelinks->usergroup_id = Request::get('usergroup_id');	
						$modulelinks->module_id = Request::get('module_id');	
						$modulelinks->status = Request::get('status');	
						$modulelinks->save();

		            // redirect
		            \Session::flash('message', 'Successfully created Modulelinks!');
		            return Redirect::to(getCurrentUrlPrefix());
		        }
			}

			/**
			 * Display the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function show($id)
			{
				//
				 $modulelinks = Modulelink::find($id);

				 if(empty($modulelinks))
				 {
				 	// redirect
		            \Session::flash('message', 'Oops! Id doesn\'t exists');
		            return Redirect::to(getCurrentUrlPrefix());

				 }

		        // show the view and pass the modulelinks to it
		        return view('rbac::modulelinks.show')->with('modulelinks', $modulelinks);
			}

			/**
			 * Show the form for editing the specified resource.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function edit($id)
			{
				//
				$modulelinks = Modulelink::find($id);
				if(empty($modulelinks))
				 {
				 	// redirect
		            \Session::flash('message', 'Oops! Id doesn\'t exists');
		            return Redirect::to(getCurrentUrlPrefix());

				 }

		        // show the view and pass the modulelinks to it
		        return view('rbac::modulelinks.edit')->with('modulelinks', $modulelinks);
			}

			/**
			 * Update the specified resource in storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function update($id)
			{
				// read more on validation at http://laravel.com/docs/validation
		       $rules = array();
		        $validator = Validator::make(Request::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		            return Redirect::to(getCurrentUrlPrefix().'/'.$id.'/edit')
		                ->withErrors($validator)
		                ->withInput(Request::except('password'));
		        } else {
		            // store
		             $modulelinks = Modulelink::find($id);$modulelinks->user_id       = Request::get('user_id');	
					$modulelinks->usergroup_id       = Request::get('usergroup_id');	
					$modulelinks->module_id       = Request::get('module_id');	
					$modulelinks->status       = Request::get('status');	
					$modulelinks->save();
		           

		            // redirect
		            \Session::flash('message', 'Successfully Updated Modulelinks!');
		            return Redirect::to(getCurrentUrlPrefix());
		        }
			}

			/**
			 * Remove the specified resource from storage.
			 *
			 * @param  int  $id
			 * @return Response
			 */
			public function destroy($id)
			{
				//
				$modulelinks = Modulelink::find($id);
			 
			    $modulelinks->delete();
			    \Session::flash('message', 'Successfully Deleted Modulelinks!');
		         //return Redirect::to('modulelink');
			    echo 'Successfully Deleted Modulelinks!';
			}

		}
		