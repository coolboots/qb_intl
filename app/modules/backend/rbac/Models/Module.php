<?php namespace App\modules\backend\rbac\Models;

use Illuminate\Database\Eloquent\Model;
class Module extends Model  {

	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'rk_modules';

	/**
	 * The table key used by the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'module_id';

	
}

			