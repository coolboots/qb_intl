<?php namespace App\modules\backend\rbac\Models;

use Illuminate\Database\Eloquent\Model;
class Privilege extends Model  {

	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'rk_usergroup_privilege';

	/**
	 * The table key used by the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'privilege_id';

	
}		