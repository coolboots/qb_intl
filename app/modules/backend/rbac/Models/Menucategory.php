<?php namespace App\modules\backend\rbac\Models;

use Illuminate\Database\Eloquent\Model;
class Menucategory extends Model  {

	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'rk_menu_categories';

	/**
	 * The table key used by the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'menu_category_id';

	
}

			