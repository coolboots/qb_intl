<?php namespace App\modules\backend\rbac\Models;

use Illuminate\Database\Eloquent\Model;
class Admin extends Model  {

	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'admins';

	/**
	 * The table key used by the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'id';

	
}

			