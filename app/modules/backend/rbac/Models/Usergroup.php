<?php namespace App\modules\backend\rbac\Models;

use Illuminate\Database\Eloquent\Model;
class Usergroup extends Model  {

	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'rk_usergroups';

	/**
	 * The table key used by the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'usergroup_id';

	
}

			