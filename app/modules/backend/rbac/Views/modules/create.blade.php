@extends('admin.layouts.mainlayout')

@section('content')

 <div class="row">
                	<div class="col-md-12">



            <section class="tile">
            <div class="tile-header dvd dvd-btm">
              <h1 class="custom-font"><strong>Add</strong> Module
&nbsp; &nbsp; &nbsp;&nbsp;  &nbsp;
                        <a class="btn btn-primary" href="{{ url('/'.getCurrentUrlPrefix()) }}">View All</a>
              </h1>
              <ul class="controls">
                <li class="dropdown"> <a role="button" tabindex="0" class="dropdown-toggle settings" data-toggle="dropdown"> <i class="fa fa-cog"></i> <i class="fa fa-spinner fa-spin"></i> </a>
                  <ul class="dropdown-menu pull-right with-arrow animated littleFadeInUp br-5">
                    <li> <a role="button" tabindex="0" class="tile-toggle"> <span class="minimize"><i class="fa fa-angle-down"></i>&nbsp;&nbsp;&nbsp;Minimize</span> <span class="expand"><i class="fa fa-angle-up"></i>&nbsp;&nbsp;&nbsp;Expand</span> </a> </li>
                   
                    <li> <a role="button" tabindex="0" class="tile-fullscreen"> <i class="fa fa-expand"></i> Fullscreen </a> </li>
                  </ul>
                </li>
                <li class="remove"><a role="button" tabindex="0" class="tile-close"><i class="fa fa-times"></i></a></li>
              </ul>
            </div>
            <div class="tile-body">

                    	
                        
                      
<!-- if there are creation errors, they will show here -->
@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br>
    
        @foreach ($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    
</div>
@endif
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
<!--<h4 class="widgettitle nomargin shadowed">&nbsp;</h4>
<div class="widgetcontent bordered shadowed nopadding">-->
                    <form class="form-horizontal" role="form" method="post" action="{{url('/'.getCurrentUrlPrefix())}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                          <label for="module_name" class="col-sm-2 control-label">Module Name</label>
                                    <div class="col-sm-10"><input type="text" name="module_name" id="module_name" class="form-control"></div>
                                    </div>
                  <hr class="line-dashed line-full"/>
                  <div class="form-group">
                  				<label for="module_name" class="col-sm-2 control-label">Module Block</label>
		                                <div class="col-sm-10"><input type="text" name="module_type" id="module_type" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
				        		  <label for="descriptions" class="col-sm-2 control-label">Descriptions</label>
                                <div class="col-sm-10"><textarea cols="80" rows="5" name="descriptions" id="descriptions" class="form-control"></textarea></div>
                                </div>
			        			<hr class="line-dashed line-full"/>
			        			<div class="form-group">
                  				<label for="mapping_url" class="col-sm-2 control-label">Mapping Url</label>
		                                <div class="col-sm-10"><input type="text" name="mapping_url" id="mapping_url" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="menu_type" class="col-sm-2 control-label">Menu Type</label>
		                                <div class="col-sm-10">
                                     <select class="form-control mb-10" name="menu_type" id="menu_type">
                                      <option value="0">Select Menutype</option>
                                      @if(!empty($menucategory))
                                      @foreach($menucategory as $value)
                                      <option value="{{$value['menu_category_id']}}">{{$value['name']}}</option>
                                      @endforeach
                                      @endif
                                      
                                    </select>
                                    </div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="menu_name" class="col-sm-2 control-label">Menu Name</label>
		                                <div class="col-sm-10"><input type="text" name="menu_name" id="menu_name" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="priority" class="col-sm-2 control-label">Priority</label>
		                                <div class="col-sm-10"><input type="text" name="priority" id="priority" class="form-control" value="0"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="status" class="col-sm-2 control-label">Status</label>
		                                <div class="col-sm-10">
		                                <select class="form-control mb-10" name="status" id="status">
					                      <option value="">Select status</option>
					                      <option value="Active">Active</option>
					                      <option value="Inactive">Inactive</option>
					                    </select>
		                                </div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					                                        
                            <p class="stdformbutton">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="reset" class="btn">Reset</button>
                            </p>
                        </form>
                    </div>
                    </section>
                
                
                <div class="divider15"></div>
              
                    </div><!--span8-->
                   
                </div><!--row-fluid-->

               
@endsection

			