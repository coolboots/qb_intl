@extends('admin.layouts.mainlayout')

@section('content')

 <div class="row">
                	<div class="col-md-12">



            <section class="tile">
            <div class="tile-header dvd dvd-btm">
              <h1 class="custom-font"><strong>Manage</strong> Modulelink
&nbsp; &nbsp; &nbsp;<a class="btn btn-primary" href="{{ url('/'.getCurrentUrlPrefix().'/create') }}">Add New</a>&nbsp;  &nbsp;&nbsp; &nbsp; &nbsp;
                        <a class="btn btn-primary" href="{{ url('/'.getCurrentUrlPrefix()) }}">View All</a>
              </h1>
              <ul class="controls">
                <li class="dropdown"> <a role="button" tabindex="0" class="dropdown-toggle settings" data-toggle="dropdown"> <i class="fa fa-cog"></i> <i class="fa fa-spinner fa-spin"></i> </a>
                  <ul class="dropdown-menu pull-right with-arrow animated littleFadeInUp br-5">
                    <li> <a role="button" tabindex="0" class="tile-toggle"> <span class="minimize"><i class="fa fa-angle-down"></i>&nbsp;&nbsp;&nbsp;Minimize</span> <span class="expand"><i class="fa fa-angle-up"></i>&nbsp;&nbsp;&nbsp;Expand</span> </a> </li>
                   
                    <li> <a role="button" tabindex="0" class="tile-fullscreen"> <i class="fa fa-expand"></i> Fullscreen </a> </li>
                  </ul>
                </li>
                <li class="remove"><a role="button" tabindex="0" class="tile-close"><i class="fa fa-times"></i></a></li>
              </ul>
            </div>
            <div class="tile-body">
            <!-- if there are creation errors, they will show here -->
			@if (count($errors) > 0)
			<div class="alert alert-danger">
			    <strong>Whoops!</strong> There were some problems with your input.<br>
			    
			        @foreach ($errors->all() as $error)
			            <p>{{ $error }}</p>
			        @endforeach
			    
			</div>
			@endif
			@if (Session::has('message'))
			    <div class="alert alert-info">{{ Session::get('message') }}</div>
			@endif
            	 <div class="table-responsive">
                <table class="table mb-0" id="">
                   
                    <thead>
                        <tr>
                          	<th class="head0 nosort"><input type="checkbox" class="checkall" /></th>
                          	<th class="head0">User Id</th>
			        <th class="head0">Usergroup Id</th>
			        <th class="head0">Module Id</th>
			        <th class="head0">Status</th>
			        <th class="head0">Actions</th>
                           
                        </tr>
                    </thead>
                    <tbody>
                     @foreach($rk_module_links as $key => $value)
			        <tr><td class="aligncenter"><span class="center">
                            <input type="checkbox" actionid={{ $value->module_link_id }} />
                          </span></td><td>{{ $value->user_id }}</td>
				        <td>{{ $value->usergroup_id }}</td>
				        <td>{{ $value->module_id }}</td>
				        <td>{{ $value->status }}</td>
				            
			           <!-- we will also add show, edit, and delete buttons -->
			            <td>

			               <a class="btn btn-small btn-success" href="{{ URL::to(getCurrentUrlPrefix() .'/'. $value->module_link_id) }}">Show</a>

			                <a class="btn btn-small btn-info" href="{{ URL::to(getCurrentUrlPrefix() .'/'. $value->module_link_id . '/edit') }}">Edit</a>

			                <a class="btn btn-small btn-warning delete" data-token="{{ csrf_token() }}" deleteid="{{$value->module_link_id}}">Delete</a>

			            </td>
			        </tr>
			    @endforeach
                    </tbody>
                </table>
                </div>
                <div class="tile-footer dvd dvd-top">
              <div class="row">
                <div class="col-sm-3 hidden-xs">
                  <select class="input-sm form-control w-sm inline bulk_action_type">
                    <option value="">Select Action</option>
                    <option value="1">Delete selected</option>
                    
                  </select>
                  <button class="btn btn-sm btn-default br-3 bulk_action_trigger">Apply</button>
                </div>
                <div class="col-sm-4 text-left"><small class="text-muted">Total {{$rk_module_links->total()}} records found!</small></div>
                <div class="col-sm-5 text-right">
                 <ul class="pagination pagination-sm m-0">
                <?php echo $rk_module_links->render(); ?>
            	</ul>
                 
                </div>
              </div>
            </div>
                </div>
                    </section>
                
                
                <div class="divider15"></div>
              
                    </div><!--span8-->
                   
                </div><!--row-fluid-->

               
<script type="text/javascript">

$('.delete').click(function()
{
    var token = $(this).data('token');
    var id = $(this).attr('deleteid');
    var url = '<?php echo url(getCurrentUrlPrefix());?>';

    conf = confirm('Are you Sure?');
    //alert(token);

    if(conf)
    {
        $.ajax({
        url:url+'/'+id,
        type: 'post',
        data: {_method: 'delete', _token :token},
        success:function(msg){
            location.reload();
        }
        });
    }

    

})
   
</script>
               
@endsection
