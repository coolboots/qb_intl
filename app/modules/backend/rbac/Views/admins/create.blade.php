@extends('admin.layouts.mainlayout')

@section('content')

 <div class="row">
                	<div class="col-md-12">



            <section class="tile">
            <div class="tile-header dvd dvd-btm">
              <h1 class="custom-font"><strong>Add</strong> Adminuser
&nbsp; &nbsp; &nbsp;&nbsp;  &nbsp;
                        <a class="btn btn-primary" href="{{ url('/'.getCurrentUrlPrefix()) }}">View All</a>
              </h1>
              <ul class="controls">
                <li class="dropdown"> <a role="button" tabindex="0" class="dropdown-toggle settings" data-toggle="dropdown"> <i class="fa fa-cog"></i> <i class="fa fa-spinner fa-spin"></i> </a>
                  <ul class="dropdown-menu pull-right with-arrow animated littleFadeInUp br-5">
                    <li> <a role="button" tabindex="0" class="tile-toggle"> <span class="minimize"><i class="fa fa-angle-down"></i>&nbsp;&nbsp;&nbsp;Minimize</span> <span class="expand"><i class="fa fa-angle-up"></i>&nbsp;&nbsp;&nbsp;Expand</span> </a> </li>
                   
                    <li> <a role="button" tabindex="0" class="tile-fullscreen"> <i class="fa fa-expand"></i> Fullscreen </a> </li>
                  </ul>
                </li>
                <li class="remove"><a role="button" tabindex="0" class="tile-close"><i class="fa fa-times"></i></a></li>
              </ul>
            </div>
            <div class="tile-body">

                    	
                        
                      
<!-- if there are creation errors, they will show here -->
@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br>
    
        @foreach ($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    
</div>
@endif
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
<!--<h4 class="widgettitle nomargin shadowed">&nbsp;</h4>
<div class="widgetcontent bordered shadowed nopadding">-->
                    <form class="form-horizontal" role="form" method="post" action="{{url('/'.getCurrentUrlPrefix())}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                  				<label for="name" class="col-sm-2 control-label">Name</label>
		                                <div class="col-sm-10"><input type="text" name="name" id="name" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="email" class="col-sm-2 control-label">Email</label>
		                                <div class="col-sm-10"><input type="text" name="email" id="email" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="password" class="col-sm-2 control-label">Password</label>
		                                <div class="col-sm-10"><input type="text" name="password" id="password" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="usergroup_id" class="col-sm-2 control-label">Usergroup</label>
		                                <div class="col-sm-10">
                                     <select class="form-control mb-10" name="usergroup_id" id="usergroup_id">
                                      <option value="">Select Usergroup</option>
                                      @if(!empty($usergroups))
                                      @foreach($usergroups as $usergroup)
                                      <option value="{{$usergroup['usergroup_id']}}">{{$usergroup['name']}}</option>
                                      @endforeach
                                      @endif
                                      
                                    </select>
                                    </div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        
					                                        
                            <p class="stdformbutton">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="reset" class="btn">Reset</button>
                            </p>
                        </form>
                    </div>
                    </section>
                
                
                <div class="divider15"></div>
              
                    </div><!--span8-->
                   
                </div><!--row-fluid-->

               
@endsection

			