@extends('admin.layouts.mainlayout')

@section('content')

 <div class="row">
                	<div class="col-md-12">



            <section class="tile">
            <div class="tile-header dvd dvd-btm">
              <h1 class="custom-font"><strong>Assign</strong> Modules ({{$usergroup->name}})
&nbsp; &nbsp; &nbsp;<a class="btn btn-primary" href="{{ url('/'.getCurrentUrlPrefix().'/create') }}">Add New</a>&nbsp;  &nbsp;&nbsp; &nbsp; &nbsp;
                        <a class="btn btn-primary" href="{{ url('/'.getCurrentUrlPrefix()) }}">View All</a>
              </h1>
              <ul class="controls">
                <li class="dropdown"> <a role="button" tabindex="0" class="dropdown-toggle settings" data-toggle="dropdown"> <i class="fa fa-cog"></i> <i class="fa fa-spinner fa-spin"></i> </a>
                  <ul class="dropdown-menu pull-right with-arrow animated littleFadeInUp br-5">
                    <li> <a role="button" tabindex="0" class="tile-toggle"> <span class="minimize"><i class="fa fa-angle-down"></i>&nbsp;&nbsp;&nbsp;Minimize</span> <span class="expand"><i class="fa fa-angle-up"></i>&nbsp;&nbsp;&nbsp;Expand</span> </a> </li>
                   
                    <li> <a role="button" tabindex="0" class="tile-fullscreen"> <i class="fa fa-expand"></i> Fullscreen </a> </li>
                  </ul>
                </li>
                <li class="remove"><a role="button" tabindex="0" class="tile-close"><i class="fa fa-times"></i></a></li>
              </ul>
            </div>
            <div class="tile-body">
            <!-- if there are creation errors, they will show here -->
			@if (count($errors) > 0)
			<div class="alert alert-danger">
			    <strong>Whoops!</strong> There were some problems with your input.<br>
			    
			        @foreach ($errors->all() as $error)
			            <p>{{ $error }}</p>
			        @endforeach
			    
			</div>
			@endif
			@if (Session::has('message'))
			    <div class="alert alert-info">{{ Session::get('message') }}</div>
			@endif

      <?php
      $modules = modulesgroup($modules,1);
      //echo '<pre>';print_r($modules);
      ?>
            	 <div class="table-responsive">
               <form class="form-horizontal" role="form" method="post" action="">
                <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                <table class="table mb-0" id="">
                    
                    <thead>
                        <tr>
                          	<!-- <th class="head0 nosort"><input type="checkbox" class="checkall" /></th> -->
                          	<th class="head0">Module Name</th>
              			        <th class="head0">Module Type</th>
              			        <th class="head0"><input type="checkbox" class="checkall_access" /> Is Accessible </th>
              			        <th class="head0"><input type="checkbox" class="checkall_write" /> Is writable </th>
              			       </tr>
                    </thead>
                    <tbody>
                    @if(isset($modules) && !empty($modules))
                     @foreach($modules as $modulegroup => $modulegroupv)
                     <tr><td><strong>{{$modulegroup}}</strong></td></tr>
                     @if(!empty($modulegroupv))
                      @foreach($modulegroupv as $module)
                      <tr>
                        <!-- <td><input type="checkbox" class="" /></td> -->
                        <td>{{$module['module_name']}}</td>
                        <td>{{$module['module_type']}}</td>
                        <td>

                        @if(!empty(@$privilege[$module['module_id']]))
                        <input type="hidden" name="privilege[{{$module['module_id']}}]" value="{{$module['module_id']}}">
                        @endif
                        @if(!empty(@$privilege[$module['module_id']]['is_access']=='Y'))
                        <input type="checkbox" class="accessbox"  inputName="is_access_{{$module['module_id']}}" checked="checked" />
                        <input type="hidden" id="is_access_{{$module['module_id']}}" name="is_access[{{$module['module_id']}}]" value="Y" class="accessbox_input">
                        @else
                        <input type="checkbox" class="accessbox"  inputName="is_access_{{$module['module_id']}}"/>
                        <input type="hidden" id="is_access_{{$module['module_id']}}" name="is_access[{{$module['module_id']}}]" value="N" class="accessbox_input">
                        @endif
                        </td>
                        <td>
                        @if(!empty(@$privilege[$module['module_id']]['is_access']=='Y'))
                        <input type="checkbox" class="writebox"  inputName="is_write_{{$module['module_id']}}" checked="checked" />
                        <input type="hidden" id="is_write_{{$module['module_id']}}" name="is_write[{{$module['module_id']}}]" value="Y" class="writebox_input">
                        @else
                        <input type="checkbox" class="writebox"  inputName="is_write_{{$module['module_id']}}"/>
                        <input type="hidden" id="is_write_{{$module['module_id']}}" name="is_write[{{$module['module_id']}}]" value="N" class="writebox_input">
                        @endif
                        </td>
                      </tr>  
                      @endforeach
                     @endif
			       
        			     @endforeach
                  @endif

                   
                  </tbody>
                </table>
                <p class="stdformbutton">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="reset" class="btn">Reset</button>
                            </p>
                </form>
                    
                </div>
                
                </div>
                    </section>
                
                
                <div class="divider15"></div>
              
                    </div><!--span8-->
                   
                </div><!--row-fluid-->

               
<script type="text/javascript">

$(".checkall_access").click(function(){
    $('input:checkbox.accessbox').not(this).prop('checked', this.checked);
     if($(this).prop('checked')==true){

          $('.accessbox_input').val('Y');
     }else{
          $('.accessbox_input').val('N');
     }
});
$(".checkall_write").click(function(){
    $('input:checkbox.writebox').not(this).prop('checked', this.checked);
    if($(this).prop('checked')==true){

          $('.writebox_input').val('Y');
     }else{
          $('.writebox_input').val('N');
     }
});

$('.writebox').change(function(){
  //alert();
  var inputName= $(this).attr('inputName');
     if($(this).prop('checked')==true){

          $('#'+inputName).val('Y');
     }else{
          $('#'+inputName).val('N');
     }
});
$('.accessbox').change(function(){
  //alert();
  var inputName= $(this).attr('inputName');
     if($(this).prop('checked')==true){
      
          $('#'+inputName).val('Y');
     }else{
          $('#'+inputName).val('N');
     }
});


$('.delete').click(function()
{
    var token = $(this).data('token');
    var id = $(this).attr('deleteid');
    var url = '<?php echo url(getCurrentUrlPrefix());?>';

    conf = confirm('Are you Sure?');
    //alert(token);

    if(conf)
    {
        $.ajax({
        url:url+'/'+id,
        type: 'post',
        data: {_method: 'delete', _token :token},
        success:function(msg){
            location.reload();
        }
        });
    }

    

})
   
</script>
               
@endsection
