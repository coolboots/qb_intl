@extends('admin.layouts.mainlayout')

@section('content')

 <div class="row">
                	<div class="col-md-12">



            <section class="tile">
            <div class="tile-header dvd dvd-btm">
              <h1 class="custom-font"><strong>Show</strong> Usergroup
&nbsp; &nbsp; &nbsp;<a class="btn btn-primary" href="{{ url('/'.getCurrentUrlPrefix().'/create') }}">Add New</a>&nbsp;  &nbsp;&nbsp; &nbsp; &nbsp;
                        <a class="btn btn-primary" href="{{ url('/'.getCurrentUrlPrefix()) }}">View All</a>
              </h1>
              <ul class="controls">
                <li class="dropdown"> <a role="button" tabindex="0" class="dropdown-toggle settings" data-toggle="dropdown"> <i class="fa fa-cog"></i> <i class="fa fa-spinner fa-spin"></i> </a>
                  <ul class="dropdown-menu pull-right with-arrow animated littleFadeInUp br-5">
                    <li> <a role="button" tabindex="0" class="tile-toggle"> <span class="minimize"><i class="fa fa-angle-down"></i>&nbsp;&nbsp;&nbsp;Minimize</span> <span class="expand"><i class="fa fa-angle-up"></i>&nbsp;&nbsp;&nbsp;Expand</span> </a> </li>
                   
                    <li> <a role="button" tabindex="0" class="tile-fullscreen"> <i class="fa fa-expand"></i> Fullscreen </a> </li>
                  </ul>
                </li>
                <li class="remove"><a role="button" tabindex="0" class="tile-close"><i class="fa fa-times"></i></a></li>
              </ul>
            </div>
            <div class="tile-body">
            	 <div class="table-responsive">
                <table class="table mb-0" id=""><tbody>     <tr>
                          <td class="width30">Lang Code</td>
                          <td class="width70"><strong>{{$usergroups->lang_code}}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Name</td>
                          <td class="width70"><strong>{{$usergroups->name}}</strong></td>
                      </tr>     <tr>
                          <td class="width30">User Type</td>
                          <td class="width70"><strong>{{$usergroups->user_type}}</strong></td>
                      </tr>     <tr>
                          <td class="width30">Status</td>
                          <td class="width70"><strong>{{$usergroups->status}}</strong></td>
                      </tr>    
                      <!--  <tr>
                          <td class="width30">Parent Usergroup</td>
                          <td class="width70"><strong>{{$usergroups->parent_usergroup}}</strong></td>
                      </tr> -->
                  </tbody></table>
                </div>
                
                </div>
                    </section>
                
                
                <div class="divider15"></div>
              
                    </div><!--span8-->
                   
                </div><!--row-fluid-->
               
@endsection

			