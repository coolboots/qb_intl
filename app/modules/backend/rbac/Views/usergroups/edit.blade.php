@extends('admin.layouts.mainlayout')

			@section('content')

			 <div class="row">
                	<div class="col-md-12">



            <section class="tile">
            <div class="tile-header dvd dvd-btm">
              <h1 class="custom-font"><strong>Update</strong> Usergroup
			&nbsp; &nbsp;&nbsp; &nbsp;<a class="btn btn-primary" href="{{ url('/'.getCurrentUrlPrefix().'/create') }}">Add New</a> &nbsp; &nbsp; &nbsp;
                        <a class="btn btn-primary" href="{{ url('/'.getCurrentUrlPrefix()) }}">View All</a>
              </h1>
              <ul class="controls">
                <li class="dropdown"> <a role="button" tabindex="0" class="dropdown-toggle settings" data-toggle="dropdown"> <i class="fa fa-cog"></i> <i class="fa fa-spinner fa-spin"></i> </a>
                  <ul class="dropdown-menu pull-right with-arrow animated littleFadeInUp br-5">
                    <li> <a role="button" tabindex="0" class="tile-toggle"> <span class="minimize"><i class="fa fa-angle-down"></i>&nbsp;&nbsp;&nbsp;Minimize</span> <span class="expand"><i class="fa fa-angle-up"></i>&nbsp;&nbsp;&nbsp;Expand</span> </a> </li>
                   
                    <li> <a role="button" tabindex="0" class="tile-fullscreen"> <i class="fa fa-expand"></i> Fullscreen </a> </li>
                  </ul>
                </li>
                <li class="remove"><a role="button" tabindex="0" class="tile-close"><i class="fa fa-times"></i></a></li>
              </ul>
            </div>
            <div class="tile-body">

                    	
                        
                      
			<!-- if there are creation errors, they will show here -->
			@if (count($errors) > 0)
			<div class="alert alert-danger">
			    <strong>Whoops!</strong> There were some problems with your input.<br>
			    
			        @foreach ($errors->all() as $error)
			            <p>{{ $error }}</p>
			        @endforeach
			    
			</div>
			@endif
			@if (Session::has('message'))
			    <div class="alert alert-info">{{ Session::get('message') }}</div>
			@endif
			
                    
<form class="form-horizontal" action="{{ url('/'.getCurrentUrlPrefix().'/'.$usergroups->usergroup_id) }}" method="POST" enctype="multipart/form-data">
            {{ method_field('PUT') }} 
            <input type="hidden" name="_token" value="{{ csrf_token() }}"><div class="form-group">
                  				<label for="lang_code" class="col-sm-2 control-label">Lang Code</label>
		                                <div class="col-sm-10"><input type="text" name="lang_code" id="lang_code" value="{{$usergroups->lang_code}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="name" class="col-sm-2 control-label">Name</label>
		                                <div class="col-sm-10"><input type="text" name="name" id="name" value="{{$usergroups->name}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="user_type" class="col-sm-2 control-label">User Type</label>
		                                <div class="col-sm-10"><input type="text" name="user_type" id="user_type" value="{{$usergroups->user_type}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					        <div class="form-group">
                  				<label for="status" class="col-sm-2 control-label">Status</label>
		                                <div class="col-sm-10">
		                                <select class="form-control mb-10" name="status" id="status">
					                      <option value="">Select status</option>
					                      @if($usergroups->status=="Active")
					                      <option value="Active" selected>Active</option>
					                      <option value="Inactive">Inactive</option>
					                      
					                      @elseif($usergroups->status=="Inactive")
					                      <option value="Active">Active</option>
					                      <option value="Inactive" selected>Inactive</option>
					                      @endif
					                    </select>
		                                </div>
		                                </div>
					        <hr class="line-dashed line-full"/>
					       <!--  <div class="form-group">
                  				<label for="parent_usergroup" class="col-sm-2 control-label">Parent Usergroup</label>
		                                <div class="col-sm-10"><input type="text" name="parent_usergroup" id="parent_usergroup" value="{{$usergroups->parent_usergroup}}" class="form-control"></div>
		                                </div>
					        <hr class="line-dashed line-full"/> -->
					                                        
                            <p class="stdformbutton">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="reset" class="btn">Reset</button>
                            </p>
                        </form>
                    </div>
                    </section>
                
                
                <div class="divider15"></div>
              
                    </div><!--span8-->
                   
                </div><!--row-fluid-->

               
@endsection

			