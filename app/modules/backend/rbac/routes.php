<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//die('amgetting loaded');
//array('xssprotection','frameguard','admin')
Route::group(['module'=>'rbac','namespace' => 'App\modules\backend\rbac\Controllers','prefix'=>'admin','middleware'=>['web','admin','adminroleauthenticate']], function() {

		//core modules
		Route::resource('adminuser', 'AdminsController');
		Route::resource('menucategory', 'MenucategoriesController');	
		Route::resource('modulelink', 'ModulelinksController');	
		Route::resource('module', 'ModulesController');	
		Route::resource('usergroup', 'UsergroupsController');
		Route::get('/settings', 'AdminsController@getSettings');	
		Route::post('/settings', 'AdminsController@applySettings');
		Route::get('/usergroup/{id}/assign', 'UsergroupsController@getModules');	
		Route::post('/usergroup/{id}/assign', 'UsergroupsController@assignModules');	

		// Route::get('/unauthorized', function(){
		// 	return view('admin.unauthorized');
		// });
			
}); 