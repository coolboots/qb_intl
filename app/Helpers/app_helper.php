<?php

function prepareAdminUrl() {

        $string = \Route::current()->uri();
        $prefix = \Route::current()->getAction()['prefix'];
        $prefix = '/'.str_replace('/', '',$prefix);
       $role_id = auth()->guard('admin')->user()->usergroup_id;
       $data = \Cache::get('user_capablities_'.$role_id);
       $data = json_decode($data,true); //echo '<pre>';print_r($data);die;
       
       // End Here
       $responsemenu = '';

       if(!empty($data))
       {
        $menuarray = array();
        $menuarray = modulesgroup($data);
        // echo '<pre>jj';print_r($menuarray);die;
        if(!empty($menuarray))
        {
          $responsemenu .= '<ul id="navigation">
          <li class="active open"><a href="'.url($prefix.'/home').'"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>';
          foreach ($menuarray as $menugroupk => $menugroupv) 
          {
            if($menugroupk=='NA')
            {
                if(!empty($menugroupv))
                {
                  foreach ($menugroupv as $menuk => $menuv) 
                  {
                    # code...
                    if(!empty($menuv['menu_name']) AND ($menuv['is_access']=='Y' OR $menuv['is_write']=='Y'))
                    $responsemenu .= '<li><a href="'.url($prefix.'/'.$menuv['mapping_url']).'"><i class="fa fa-dashboard"></i><span>'.$menuv['menu_name'].'</span></a></li>';
                  }
                  
                }
            }
            else
            {
                # code...
                $responsemenu .= '<li><a role="button" tabindex=0><i class="fa fa-list"></i> <span>'.$menugroupk.'</span></a>';
                if(!empty($menugroupv))
                {
                  $responsemenu .= '<ul>';
                  foreach ($menugroupv as $menuk => $menuv) 
                  {
                    # code...
                    
                        $responsemenu .= '<li><a href="'.url($prefix.'/'.$menuv['mapping_url']).'"><i class="fa fa-angle-right"></i>'.$menuv['menu_name'].'</a></li>';
                    
                  }
                  $responsemenu .= '</ul>';
                }
                $responsemenu .= '</li>';

            }
            

          }
          $responsemenu .= '</ul>';
        }
       }
       //die('am here');
       return $responsemenu;
}



function is_active_user($user_id)
{
    //
    if(empty($user_id))
    {
        //die('amhere');
        echo response([
         'error' => [
             'code' => 'INVALID_TOKEN',
             'description' => 'Invalid User Token!'
         ]
        ], 401);
        exit;
    }

}

if(!function_exists('seoUrlsField'))
{
    function seoUrlsField($object_type='')
    {
        return '<div class="form-group">
                          <label for="seo_title" class="col-sm-2 control-label">Seo Title</label>
                                    <div class="col-sm-10"><input type="text" name="seo_title" id="seo_title" class="form-control" routeUrl="'.$object_type.'" value="">
                                    <input type="hidden" name="main_seo_title" id="main_seo_title" value="" routeUrl="'.$object_type.'">
                                      <p class="help-block mb-0 seo_url"></p>
                                    
                                  </div>
                                    </div>
                  <hr class="line-dashed line-full"/>';
       
    }
} 

if(!function_exists('seoUrlsFieldEdit'))
{
    function seoUrlsFieldEdit($id='',$object_type='')
    {
        $slug = App\Http\Models\Seo::where('object_type',$object_type)->where('object_id',$id)->get()->toArray();
       return '<div class="form-group">
                          <label for="seo_title" class="col-sm-2 control-label">Seo Title</label>
                                    <div class="col-sm-10"><input type="text" name="seo_title" id="seo_title" class="form-control" routeUrl="'.$object_type.'" readonly="true" value="'.@$slug[0]['object_data'].'">
                                    <input type="hidden" name="main_seo_title" id="main_seo_title" value="'.@$slug[0]['object_data'].'" routeUrl="'.$object_type.'">
                                      <p class="help-block mb-0 seo_url">'.@$slug[0]['object_data'].'</p>
                                    <p class="help-block mb-0"><a href="javascript:void(0)" ajax-ignore="true" id="editTitleLink">Edit Link</a></p>
                                  </div>
                                    </div>
                  <hr class="line-dashed line-full"/>';
       
    }
} 


function getStatuses($where=array())
{

  echo '
                                                <option value="">Select Status</option>
                                                <option value="ACTIVE">ACTIVE</option>
                                                <option value="INACTIVE">INACTIVE</option>
                                                <option value="DELETED">DELETED</option>
                                             ';
 

}

function getContestStatuses($where=array())
{

  echo '
                                                <option value="">Select Status</option>
                                                <option value="ACTIVE">ACTIVE</option>
                                                <option value="INACTIVE">INACTIVE</option>
                                                <option value="DELETED">DELETED</option>
                                                <option value="EXPIRED">EXPIRED</option>
                                             ';
 

}

function getGameLayout($where=array())
{

  echo '
                                                <option value="">Select Layout</option>
                                                <option value="PORTRAIT">PORTRAIT</option>
                                                <option value="LANDSCAPE">LANDSCAPE</option>
                                                <option value="BOTH">BOTH</option>
                                            ';
 

}

function getEntryTypes($where=array())
{

  echo '
                                                <option value="">Select Entry Type</option>
                                                <option value="LIMITED">LIMITED</option>
                                                <option value="UNLIMITED">UNLIMITED</option>
                                             ';
}
function getEntryFeeTypes($where=array())
{

  echo '
                                                <option value="">Select Fee Type</option>
                                                <option value="COIN">COIN</option>
                                                <!--<option value="CASH">CASH</option>-->
                                                <option value="FREE">FREE</option>
                                             ';
}
function getPrizeTypes($where=array())
{

  echo '
                                                <option value="">Select Prize Type</option>
                                                <option value="COIN">COIN</option>
                                                <!--<option value="CASH">CASH</option>
                                                <option value="BOTH">BOTH</option>-->
                                             ';
}

function getLanguages()
{
  $language = \Config::get("qureka.language");

  $langOption = '<option value="">Select Language</option>';
  foreach ($language as $key => $value) {
      $langOption.= '<option value="'.$key.'">'.$value.'</option>';
  }
  echo $langOption;
}

function getOptionTypes()
{

  echo '
                                                <option value="">Select Correct Answer</option>
                                                <option value="A">A</option>
                                                <option value="B">B</option>
                                                <option value="C">C</option>
                                             ';
}

function getTagsTypes()
{
  $tags = \Config::get("qureka.tags");

  $tagOption = '<option value="">Select Tags</option>';
  foreach ($tags as $key => $value) {
      $tagOption.= '<option value="'.$key.'">'.$value.'</option>';
  }
  echo $tagOption;
}


//get User By Id
function getUserById($id)
{
    $node = App\Http\Models\User::find($id);
    return $node;
   //return false;
}

//get User By Id
function getUserByFields($fields=array())
{
  $where = array();
  if(@$fields['email']!='')
  {$where[] = ['email',$fields['email']];}
  if(@$fields['mobile']!='')
  {
    if (filter_var($fields['mobile'], FILTER_VALIDATE_EMAIL)) {
    $where[] = ['email',$fields['mobile']];
    }
    else
    {
    $where[] = ['mobile',$fields['mobile']];
    }
  }
  if(@$fields['unique_id']!='')
  {$where[] = ['unique_id',$fields['unique_id']];}

  if(!empty($where))
  {
    $node = App\Http\Models\User::where($where)->get();
    return (!empty($node[0]))?$node[0]:null;
  }
  return null;

}


//get all games
function getGames($where=array())
{
    $data = objToArray(\DB::table('games')->select('id','game_name as title')->where($where)->get()->toArray(),'id');
    return $data;

}

//get all categories
function getCategories($where=array())
{
    $data = objToArray(\DB::table('categories')->select('id','title','parent_id')->where($where)->where('status','ACTIVE')->get()->toArray(),'id');
    return $data;

}

//get all sub categories
function getSubCategories()
{
    $data = objToArray(\DB::table('categories')->select('id','title','parent_id')->where('parent_id', '!=', 0)->get()->toArray(),'id');
    return $data;

}

//get all games
function getContests($where=array())
{

    $data = objToArray(\DB::table('contests')->select('id','contest_title as title')->where($where)
        ->whereIn('status',['ACTIVE','INACTIVE'])->get()->toArray(),'id');
    return $data;

}

//get all matrix of contest
   function getMatrix($id)
    {
      return App\Http\Models\Awardmatrix::select('winner_from as from','winner_to as to','prize_type as type','prize_amount as amount')->where('contest_id',$id)->get()->toArray();
        //return $this->hasMany('App\Http\Models\Awardmatrix','contest_id');
    }


//get all games
function getPages($where=array())
{

    $data = objToArray(\DB::table('pages')->select('id','page_name as title')->where($where)
        ->whereIn('status',['ACTIVE','INACTIVE'])->get()->toArray(),'id');
    return $data;

}

//get all custom configs
function getConfigs($key)
{
  if ($key) {
    $configvalue = \DB::table('site_configs')->where(['status'=>'ACTIVE','keyname'=>$key])->pluck('value')->first();
    return $configvalue;
  }
}

//get all games
function getPublishers($where=array())
{

    $data = objToArray(\DB::table('publishers')->select('id','publisher_name as title')->where($where)
        ->whereIn('status',['ACTIVE','INACTIVE'])->get()->toArray(),'id');
    return $data;

}
//get all games
function getPartners($where=array())
{

    $data = objToArray(\DB::table('partners')->select('id','partner_name as title')->where($where)
        ->whereIn('status',['ACTIVE','INACTIVE'])->get()->toArray(),'id');
    return $data;

}


 /*get CategoryId by name*/

function getCategoryId($category)
{
    $response = array();
    if(!empty($category))
    {
        $response = \DB::table('categories')->select('id')->where('title', $category)->first();
        
    }
    return @$response->id;
    
}
 

  


function getUserDetails($id)
{
    $user = App\Http\Models\User::select('users.name','users.mobile','users.email','users.source','users.status','user_profile.*')
    ->leftJoin('user_profile', 'users.id', '=', 'user_profile.user_id')
    ->where('user_id',$id)->get()->toArray();
    return @$user[0];
}  


/*get seo object detail by seo name*/

function getSeoDetails($seo_name,$seo_type)
{
    $response = array();
    if(!empty($seo_name) && !empty($seo_type))
    {
        $response = App\Http\Models\Seo::select('object_id','object_data')->where('object_data',$seo_name)->where('object_type',$seo_type)->take(1)->get()->toArray();
        
    }
    return @$response;
    
}



function extractAliasUrl($data=array(),$types=array())
{
  $slug = array();
  if(!empty($data))
  {
    $slug = App\Http\Models\Seo::whereIn('object_data',array_keys($data))->whereIn('object_type',$types)->get()->toArray();
  }
  return objToArray($slug,'object_type');
  
}

//add daily bonus
function addUserDailyBonus($user)
{
      $record = App\Http\Models\Userwallettranshistory::where('user_id',$user->id)->whereDate('created_at',\Carbon\Carbon::today())->where('trans_type','daily_bonus')->where('mode_type','CREDIT')->first();

      if(empty($record))
      {
        $data = array( 
                      'user_id'=>$user->id,
                      'credit_type'=>'CREDIT',
                      'types'=>'daily_bonus',
                      'coin_type'=>'COIN',
                      'amount'=>'75',
                      'message'=>'Daily bonus',
                    );
        updateWalletWithHistory($data);
      }
    
}

//add winning coin to user
function addUserCoin($id,$coin,$contest_name)
{
      if(!empty($id) && $coin > 0 && !empty($contest_name))
      {
        $message = 'Contest win - ('.$contest_name.')';
        $data = array( 
                      'user_id'=>$id,
                      'credit_type'=>'CREDIT',
                      'types'=>'contest_win',
                      'coin_type'=>'COIN',
                      'amount'=>$coin,
                      'message'=>$message,
                    );
        updateWalletWithHistory($data);
      }
    
}

//entry fee charged
function chargeEntryFee($user,$coin,$message)
{
      if(!empty($user) && $coin > 0 && !empty($message))
      {
        $data = array( 
                      'user_id'=>$user->id,
                      'credit_type'=>'DEBIT',
                      'types'=>'contest_fee',
                      'coin_type'=>'COIN',
                      'amount'=>$coin,
                      'message'=>$message,
                    );
        $charge = updateWalletWithHistory($data);
        return array('status'=>'success','node'=>$charge);
      }
      
}

function updateWalletWithHistory($data=array())
{
  /*
  id: user_id
  amount = total amount
  credit_type = debit/credit
  types = wallet_add, wallet_request, direct_referral, chain_referral,rewards_encash,user_order
  ref_user_id = for whome product was purchased
  msg = activity description
  other_detail

  */
  // p($data);
  $mhistory = array();
  if(isset($data['other_detail']))
  {
    $other_detail = json_encode($data['other_detail']);
  }
  else
  {
    $other_detail = json_encode(array());
  }
  if(isset($data['credit_type']))
  {
      if($data['credit_type']=='CREDIT' && in_array($data['types'], array('daily_bonus','new_user_bonus','contest_win','port_coin')))
      {

          $mhistory = new App\Http\Models\Userwallettranshistory;
          $mhistory->user_id = $data['user_id'];
          $mhistory->mode_type = 'CREDIT';
          $mhistory->coin_type = $data['coin_type'];
          $mhistory->trans_type = $data['types'];
          $mhistory->amount = $data['amount'];
          $mhistory->remarks = $data['message'];
          $mhistory->save();

          $amount = $data['amount'];

          if($data['coin_type']=='COIN')
          {
            $mhistoryrecord = App\Http\Models\Userwalletcoin::updateOrCreate(
            ['user_id' => $data['user_id']],
            ['total_balance' => \DB::raw("total_balance + $amount")]
            );
          }
          

      }
      elseif($data['credit_type']=='DEBIT' && in_array($data['types'], array('contest_fee')))
      {

          $mhistory = new App\Http\Models\Userwallettranshistory;
          $mhistory->user_id = $data['user_id'];
          $mhistory->mode_type = 'DEBIT';
          $mhistory->coin_type = $data['coin_type'];
          $mhistory->trans_type = $data['types'];
          $mhistory->amount = $data['amount'];
          $mhistory->remarks = $data['message'];
          $mhistory->save();
          $amount = $data['amount'];
           if($data['coin_type']=='COIN')
          {
            $mhistoryrecord = App\Http\Models\Userwalletcoin::updateOrCreate(
             ['user_id' => $data['user_id']],
             ['available_balance' => \DB::raw("total_balance - $amount"),'withdrawl_balance' => \DB::raw("withdrawl_balance + $amount")]
            );
          }
        
      }
  }

  if(!empty($data['user_id']))
  {
    \DB::table('user_wallet_coins')
            ->where('user_id', $data['user_id'])
            ->update(['available_balance' =>DB::raw('total_balance - withdrawl_balance')]);
  }

  return $mhistory;
    
}

//check packege exist or not
function verifyPackage($key)
{
  if ($key) {
    $response = \DB::table('partner_packages')->where(['package_id'=>$key])->pluck('id')->first();
    return $response;
  }
}

//prepare menu as per module prepared in admin
if (!function_exists('modulesgroup')) {
function modulesgroup($data,$flag=0)
{
    $menuarray = array();
    if(!empty($data))
    {
        //print_r($data);
        foreach ($data as $key => $value) 
        {
          # code...
          if($flag==0 && $value['menu_name']!='' && $value['is_access']=='Y')
          $menuarray[$value['menu_group_position']][] = $value;
          elseif($value['menu_type'] >=0 && $flag==1)
          @$menuarray[$value['menu_group_position']][] = $value;      

          
        }


    }
    ksort($menuarray);

    //echo '<pre>';print_r(array_values($menuarray));die;
        $revisedArray = array();
     foreach ($menuarray as $key => $value) 
        {
          # code...
            // print_r($value);die;
            foreach ($value as $k => $v) {
                # code...
                // print_r($v); die;
                 if($flag==0 && $v['menu_name']!='' && $v['is_access']=='Y')
                  $revisedArray[$v['menu_group']][] = $v;
                  elseif($v['menu_type'] >=0 && $flag==1)
                  $revisedArray[$v['menu_group']][] = $v;    
            }
           

          
        }

        // echo '<pre>sdsd';print_r(($revisedArray));die;
    // $revisedArray = array();

    // //revised menu array
    // foreach ($menuarray as $key => $value) {
    //     # code...
    //     $revisedArray[$value]
    // }
    return ($revisedArray);
}

}


?>
