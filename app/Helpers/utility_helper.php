<?php
use App\Http\Models\APICommonparser;
use Illuminate\Cookie\Middleware\EncryptCookies;
use Illuminate\Cookie\CookieValuePrefix;
use Illuminate\Contracts\Encryption\Encrypter;
use Carbon\Carbon;


if (!function_exists('decodeCookie')) {

    function decodeCookie($key)
    {

          $cookiename = \Cookie::get($key);

         if(!empty($cookiename))
         {
            $value = substr(\Crypt::decrypt($cookiename, EncryptCookies::serialized($key)),41);
         return $value;
        }
        
 
    }
}
if (!function_exists('getCookies')) {

    function getCookies()
    {
        $cookies = \Cookie::get();
        
        $cookies['cutk'] = decodeCookie('cutk');
        $cookies['ustk'] = decodeCookie('ustk');
        $cookies['rstk'] = decodeCookie('rstk');
        $cookies['eustk'] = decodeCookie('eustk');
        $cookies['cutka'] = decodeCookie('cutka');

            return $cookies;        
 
    }
}

if (!function_exists('generateLanguageDependentData')) {

    function generateLanguageDependentData($table,$existRecords=array())
    {
       
        

    }
}
if (!function_exists('prepareFieldsParam')) {

    function prepareFieldsParam($table)
    {
       

    }

}

if(!function_exists('triggerSeoUrls'))
{
    function triggerSeoUrls($id,$type,$data)
    {
        if(!empty($id) && !empty($type) && !empty($data))
        {
            // $slug= str_slug($data.' '.$id,'-');
            if(!empty($data))
            {
                // echo $id;
                // echo $type;
                // echo $data;
                //find if name exists
                $records = \App\Http\Models\Seo::where('object_data',$data)->where('object_type',$type)->where('object_id','!=',$id)->get();
                // p($records);
                if(!$records->isEmpty())
                {
                  $data = $data.'-'.$id;
                }

                 $slug = App\Http\Models\Seo::updateOrCreate(['object_type'=>$type,'object_id'=>$id],
                        ['object_type'=>$type,'object_id'=>$id,'object_data'=>$data]);
           return $slug;
            }
          

        }
       
    }
} 

if (!function_exists('p')) {
function p($data)
{
    echo '<pre>';print_r($data);die;
}
}

if (!function_exists('isUserLoggedIn')) {
function isUserLoggedIn()
{
    $data = Session::all();
    if(!empty($data['cutk']) && !empty($data['ustk']) && !empty($data['rstk']))
    {
        return true;
    }
    else
    {
        return false;
    }
   // echo '<pre>';print_r($data);die;
}
}
if (!function_exists('loggedUserId')) {
function loggedUserId()
{
    $user_id = '';
        $data = Session::all();
        if(isset($data['cutk']))
        {
            $user = json_decode($data['cutk'],true);
            $user_id = $user['id'];
            
        }
       return $user_id;
   // echo '<pre>';print_r($data);die;
}
}



if (!function_exists('buildTree')) {

function buildTree(array $elements, $parentId = 0,$idKey='id',$parentKey='parent_id') {
    
    $branch = array();

    foreach ($elements as $element) 
    {
       // error_log('parent_id : '.$parentId. ' record key: '.$element[$parentKey]. 'condition: '.($element[$parentKey] == $parentId));
        if (isset($element[$parentKey]) && $element[$parentKey] == $parentId) 
        {
            //error_log('key : '.$element[$idKey]);
            $children = buildTree($elements, $element[$idKey]);
            
            if ($children) 
            { 
                //error_log('inside children: '.$parentId);
                $element['children'] = $children;
            }
            
            $branch[] = $element;
        }
        elseif(!isset($element[$parentKey]))
        {
             $branch[] = $element;
        }
    }

    return $branch;
}

}

if (!function_exists('prepareTreeDropdown')) {
function prepareTreeDropdown($array=array(),$level=0)
{
    foreach($array as $node)
    {
        echo '<option value="'.$node['id'].'">'.str_repeat("-", $level).$node['title'].'</option>';
        if(isset($node['children'])) {
            //echo '<br/>';
            prepareTreeDropdown($node['children'], $level + 1);
           //echo str_repeat("&emsp;", $level);
        }
        //echo "</option>", '<br>';
        
    }
}
}

if (!function_exists('parentChildDropdown')) {
function parentChildDropdown($array=array(),$level=0)
{
    foreach($array as $node)
    {   
        if(!isset($node['children']) && $node['parent_id'] == 0) {
             echo '<option value="'.$node['id'].'">'.$node['title'].'</option>';
        }
        if(isset($node['children'])) {
            foreach($node['children'] as $child){
                echo '<option value="'.$child['id'].'">'.$node['title'].'-'.$child['title'].'</option>';
            }
        }
        //echo "</option>", '<br>';
        
    }
}
}

//parse multilevel tree in hierarchy form
if (!function_exists('parseTree')) {
    function parseTree($tree, $root = 0) 
    {
        //print_r($tree);
        $return = array();
        # Traverse the tree and search for direct children of the root
        foreach($tree as $child => $parent) 
        {
            # A direct child is found
            //$parent = get_object_vars($parent);
            
            if(is_object($parent))
              $parent = get_object_vars($parent);  


           
            if($parent['parentid'] == $root) {
                # Remove item from tree (we don't need to traverse this again)
                unset($tree[$child]);
                # Append the child into result array and parse its children
                $return[$parent['categoryid']] = array(
                    'name' => $parent,
                    'children' => parseTree($tree, $parent['categoryid'])
                );
            }
        }
        return empty($return) ? null : $return;    
    }

}

//get prefix assigned in url
if (!function_exists('getCurrentUrlPrefix')) {

    function getCurrentUrlPrefix()
    {
         $string = \Route::current()->uri();
         $prefix = \Route::current()->getAction()['prefix'];

         // return $prefix;
        if(!empty($prefix))
         {
            $stringC = explode('/', $string);
           if(count($stringC) > 2)
            $urlprefix = $stringC[0].'/'.$stringC[1];
            else
            $urlprefix = $string;    
        }
        else
        {
            $urlprefix = $string;
        }
        return $urlprefix;
            
    }

}


//get system level prefix
if (!function_exists('getPrefix')) {

    function getPrefix()
    {
         $string = \Route::current()->uri();
         $prefix = \Route::current()->getAction()['prefix'];
         return $prefix;

         
            
    }

}


//convert any object to array based on key
if (!function_exists('objToArray')) {
    function objToArray($array = array(), $key = '') {
        $var = array();
        if (isset($array) and !empty($array)) {
            foreach ($array as $arr) {
                if(is_object($arr))
                $arr = get_object_vars($arr);
                if(!isset($var[$arr[$key]]))
                {$var[$arr[$key]] = $arr;}
            }
        }
        return $var;
    }

}



//hit multi curl and get response
if (!function_exists('getCurlResponse')) {
 function getCurlResponse($modules,$getData=array(),$postData=array()) 
{
    if (isset($argv[1])) {
                    $max_requests = $argv[1];
                } else {
                    $max_requests = 10;
                }
                $curl_options = array(
                    CURLOPT_SSL_VERIFYPEER => FALSE,
                    CURLOPT_SSL_VERIFYHOST => FALSE,
                    CURLOPT_USERAGENT, 'Parallel Curl test script',
                );
                $parallel_curl = new App\library\ParallelCurl($max_requests, $curl_options);
                $collector = new App\library\ReturnCollector();

                $requestList = $modules['modules'];
                //echo '<pre>';print_r($requestList); die;
                foreach ($requestList as $key=>$request) {

                    // p($key);
                    if(isset($request['url']) and !empty($request['url']))
                    {
                       // if (\Cache::has($key) and 1==2)
                        $cachekey = sha1($request['url']);
                        //$cachekey = sha1($request['url']);
                        if (\Cache::has($cachekey) and $request['method']=='GET' and $request['cache']==true)
                        {
                            
                            $collector->addCacheData(\Cache::get($cachekey),$key);
                           // echo 'coming from cache\n';
                        }
                        else
                        {
                            if($request['method']=='GET')
                            {
                                //$parallel_curl->startRequest($request['url'],array($collector,'addData'),$key);
                                if(!empty($getData))
                                {
                                    $url = prepareUrl($request['url'],$getData);
                                }
                                else
                                {
                                    $url = $request['url'];
                                }
                                $parallel_curl->startRequest($url,array($collector,'addData'),array('module'=>$key,'cache'=>@$request['cache'],'cache_interval'=>@$request['cache_interval'],'headers'=>@$request['headers']));
                            }
                            elseif($request['method']=='POST')
                            { 
                                $parallel_curl->startRequest($request['url'],array($collector,'addData'),array('module'=>$key,'cache'=>@$request['cache'],'cache_interval'=>@$request['cache_interval'],'headers'=>@$request['headers']),$request['params']);
                                // echo '<pre>';print_r($request);die;
                            }
                            elseif($request['method']=='PUT')
                            { 
                                $parallel_curl->startRequest($request['url'],array($collector,'addData'),array('module'=>$key,'cache'=>@$request['cache'],'cache_interval'=>@$request['cache_interval'],'headers'=>@$request['headers']),$request['params'],'PUT');
                               //echo '<pre>';print_r($request);die('end');
                            }
                        }
                        
                    }
                   
                }
                // This should be called when you need to wait for the requests to finish.
                // This will automatically run on destruct of the ParallelCurl object, so the next line is optional.
                $parallel_curl->finishAllRequests();
                return $collector->outputData();
                
                 
}
}

//dependent function of getCurlResponse
if (!function_exists('on_request_done')) {
function on_request_done($content, $url, $ch, $search) {
    
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);    
    if ($httpcode !== 200) {
       // print "Fetch error $httpcode for '$url'\n";
        //return;
        return array('error'=>$httpcode,'url'=>$url);
    }
    else
    {
        $responseobject = json_decode($content, true);
        return $responseobject;
    }
    
    // if (empty($responseobject['responseData']['results'])) {
    //     print "No results found for '$search'\n";
    //     return;
    // }
    // print "********\n";
    // print "$search:\n";
    // print "********\n";
    // $allresponseresults = $responseobject['responseData']['results'];
    // foreach ($allresponseresults as $responseresult) {
    //     $title = $responseresult['title'];
    //     print "$title\n";
    // }
}
}


//short form to translate text using language intelligence
if (!function_exists('lang')) {
function lang($string,$file='')
{
    if($file==''){$file='common';}

    return \Lang::get($file.'.'.$string);
}  
}

//standard function to upload image and return image path
if (!function_exists('uploadImage')) {

    function uploadImage($file,$directory)
    {
        $img_path = ' ';
        if(!empty($file))
            {
                # code...
                    // $imgname = date('Ymd').'_'.date('His').'_'.str_random(5).'_'.$file->getClientOriginalName();
                    //$img_path = Storage::putFile('cdn/'.$directory, $file);
                    // $import_storage = $file->move('./properties/'.$directory.'/', $imgname);   
                    // $img_path = '/properties/'.$directory.'/'.$imgname;

                    $filepath = '/storage/import/'.$directory.'/';
                    //Check if the directory already exists.
                        if(!is_dir('.'.$filepath)){
                            //Directory does not exist, so lets create it.
                            $path = public_path().$filepath;
                            File::makeDirectory($path, $mode = 0777, true, true);
                            
                        }
                    $filename = date('Ymd').'_'.date('His').'_'.str_random(5).'.'.$file->getClientOriginalExtension();
                    $import_storage = $file->move('.'.$filepath, $filename);

                    return $filepath.$filename;
                    

            }
            // return $img_path;
    }


    }

 

//generate api key for based on logged in user
if(!function_exists('getApiKey'))
{
     function getApiKey()
    {
        //check for cookie value for logged in user if available then ok or else
        //force to clear logout and use public key to get access
        $key = \Config::get('qureka.API_SERVER_PUB_KEY');
        if(!empty($key))
        {session(['api_key'=>'Bearer '.$key]);}
        return 'Bearer '.$key;

        
       
    } 
}  


//verify whether eloquent query is parsed fine or not
if(!function_exists('validateEloquentQuery'))   
{

    function validateEloquentQuery($data)
    {
        $response = array();
        if(!empty($data))
        {
            foreach ($data as $key => $value) {
                # code...
                if(empty($value))
                    $value = '';

                $response[$key]=$value;
            }
        }
        return $response;

    }


} 


if(!function_exists('checkEnv'))   
{

    function checkEnv($response)
    {
        $appenv = \Config::get('app.debug');
        if($appenv==false)
        {
         $response['modules'] = array();
         $response['headers'] = array();
         $response['params'] = array();
        }
        
        return $response;

    }


} 


if(!function_exists('generatePIN'))   
{
//Our custom function.
function generatePIN($digits = 4){
    $i = 0; //counter
    $pin = ""; //our default pin is blank.
    while($i < $digits){
        //generate a random number between 0 and 9.
        $pin .= mt_rand(0, 9);
        $i++;
    }
  
  return $pin;
}
}

function generateUniqueKey($length = 10)
{
  $random = "";
  srand((double) microtime() * 1000000);

  $data = "123456123456789071234567890890";
  $data .= "aBCdefghijklmn123opq45rs67tuv89wxyz"; // if you need alphabatic also

  for ($i = 0; $i < $length; $i++) {
          $random .= substr($data, (rand() % (strlen($data))), 1);
  }

  return strtoupper($random);

}

function getDropdownByValue($response=array())
{
  if(!empty($response))
  {
    echo '<option value="">Select </option>';
    foreach ($response as $key => $value) {
      # code...
      echo '<option value="'.$value.'">'.$value.'</option>';
    }
  }

}

//create select dropdown by key value array
function getDropdownByKValue($response=array())
{
  if(!empty($response))
  {
    echo '<option value="">Select </option>';
    foreach ($response as $key => $value) {
      # code...
      echo '<option value="'.$key.'">'.$value.'</option>';
    }
  }

}

function sendMails($options=array())
{
    if(config('app.env')=='production')
    {
            \Mail::send(['html'=>'emails.'.@$options['template']], 
                ['results' => @$options['results']], 
                function($message) use($options) {
                    // print_r($options);die;
                    //$options['to'] = array_merge($options['to'],'tanwar0802@gmail.com');
                    $message->to($options['to']);
                    if(isset($options['bcc']))
                    {$message->bcc($options['bcc']);}
                    $message->subject($options['subject']);
                    $message->from(config('mail.from.address'),config('mail.from.name'));
                                  
                }); 
    }
    
}


function checkCacheAvailability($key)
{
    $collector = new App\library\CacheManager();
    $cacheData = $collector->getData($key);
    // p($cacheData);
    if($cacheData!=false)
    {
        return $cacheData;
    }
}
function storeCacheStock($key,$data,$duration)
{
    $collector = new App\library\CacheManager();
    $cacheData = $collector->addData($key,$data,array('cache_interval'=>$duration));
    return $cacheData;exit;
}

function storeOrUpdateCache($key,$data,$duration)
{
    $collector = new App\library\CacheManager();
    $cacheData = $collector->addData($key,json_encode($data),array('cache_interval'=>$duration));
    return $cacheData;
}

function is_axios()
{
    $headers = Request::header();
    setLog($headers,'request type');
    if(isset($headers['axios_request'][0]) && $headers['axios_request'][0]==true)
    {
        return true;
    }
    elseif(isset($headers['radxsoft'][0]) && $headers['radxsoft'][0]=='labs')
    {
        return true;
    }
}

function setLog($message,$type = 'LOG TYPE'){
    if (config('app.debug')) {
        \Log::info('LOG '.@json_encode($type).'::'.@json_encode($message));
    }
}

function laravel_error_parser($inputs)
{
   
    $newformat = '';
    if(!empty($inputs))
    {
        $newformat = '<ul>';
        foreach ($inputs as $key => $value) {
            # code...
            $newformat .= '<li>'.$value[0].'</li>';
        }
        $newformat .= '</ul>';
    }

     return $newformat;
}

/* strpos that takes an array of values to match against a string
 * note the stupid argument order (to match strpos)
 */
function strpos_arr($haystack, $needle) {
    if(!is_array($needle)) $needle = array($needle);
    foreach($needle as $what) {
        if(($pos = strpos($haystack, $what))!==false) return $pos;
    }
    return false;
}

function slugify($text='')
{
  // replace non letter or digits by -
  $text = preg_replace('~[^\pL\d]+~u', '-', $text);

  // transliterate
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

  // remove unwanted characters
  $text = preg_replace('~[^-\w]+~', '', $text);

  // trim
  $text = trim($text, '-');

  // remove duplicate -
  $text = preg_replace('~-+~', '-', $text);

  // lowercase
  $text = strtolower($text);

  if (empty($text)) {
    return 'n-a';
  }

  return $text;
}


function getUserIP() {
    if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
              $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
              $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
    }
    $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = $_SERVER['REMOTE_ADDR'];

    if(filter_var($client, FILTER_VALIDATE_IP,FILTER_FLAG_IPV4)) { $ip = $client; }
    elseif(filter_var($forward, FILTER_VALIDATE_IP,FILTER_FLAG_IPV4)) { $ip = $forward; }
    else { $ip = $remote; }

    return $ip;
}


function updateOrigin()
{
        $userzone = Session::get('userzone');

        if(!empty($userzone))
        {
            $userzone = json_decode($userzone,true);
            if(in_array($userzone['country'], array('-','')))
             \Input::merge(array('origin'=>'IN'));
            else
             \Input::merge(array('origin'=>@$userzone['country']));
        }
        else
        {
            \Input::merge(array('origin'=>'IN'));
        }
}

function getSocialCredential()
{
    $host = Request::header('host');
        $socialcred = \Config::get('qureka.social_credentials');
        $socialcredential = array();
        $subdomain = explode('.', $host);
        $subdomain = $subdomain[0];
        if(!empty($socialcred))
        {
            $facebook = $socialcred['facebook'];
            if(!empty($facebook))
            {
                foreach ($facebook as $key => $fb) {
                    # code...
                    $domains = $fb['domains'];
                    

                    foreach ($domains as $key => $domain) {
                        # code...
                            if($key=='playseries')
                            {
                                $sd = (int) filter_var($subdomain, FILTER_SANITIZE_NUMBER_INT);
                                if($domain['from'] <= $sd && $domain['to'] >= $sd)
                                {
                                    $socialcredential['facebook'] = $fb['credential'];
                                }
                            }
                            if($key=='winseries')
                            {
                                $sd = (int) filter_var($subdomain, FILTER_SANITIZE_NUMBER_INT);
                                if($domain['from'] <= $sd && $domain['to'] >= $sd)
                                {
                                    $socialcredential['facebook'] = $fb['credential'];
                                }
                            }
                            if($key=='goseries')
                            {
                                $sd = (int) filter_var($subdomain, FILTER_SANITIZE_NUMBER_INT);
                                if($domain['from'] <= $sd && $domain['to'] >= $sd)
                                {
                                    $socialcredential['facebook'] = $fb['credential'];
                                }
                            }
                            
                         if($subdomain==$domain)
                            {
                                $socialcredential['facebook'] = $fb['credential'];
                            }
                         if($host==$domain)
                            {
                                $socialcredential['facebook'] = $fb['credential'];
                            }
                            

                    }

                   

                }
            }
            $google = $socialcred['google'];
            if(!empty($google))
            {
                foreach ($google as $key => $fb) {
                    # code...
                    $domains = $fb['domains'];
                    
                    foreach ($domains as $key => $domain) {
                        # code...
                            if($key=='playseries')
                            {
                                $sd = (int) filter_var($subdomain, FILTER_SANITIZE_NUMBER_INT);
                                if($domain['from'] <= $sd && $domain['to'] >= $sd)
                                {
                                    $socialcredential['google'] = $fb['credential'];
                                }
                            }
                            if($key=='winseries')
                            {
                                $sd = (int) filter_var($subdomain, FILTER_SANITIZE_NUMBER_INT);
                                if($domain['from'] <= $sd && $domain['to'] >= $sd)
                                {
                                    $socialcredential['google'] = $fb['credential'];
                                }
                            }
                            if($key=='goseries')
                            {
                                $sd = (int) filter_var($subdomain, FILTER_SANITIZE_NUMBER_INT);
                               if($domain['from'] <= $sd && $domain['to'] >= $sd)
                                {
                                    $socialcredential['google'] = $fb['credential'];
                                }
                            }
                            
                         if($subdomain==$domain)
                            {
                                $socialcredential['google'] = $fb['credential'];
                            }
                         if($host==$domain)
                            {
                                $socialcredential['google'] = $fb['credential'];
                            }
                            

                    }

                   

                }
            }
            
        }
        return $socialcredential;
}

function getGAId()
{
        $host = \Request::header('host');
        $domainInfo = \Cache::get('domain_info_'.$host);
        if(!empty($domainInfo))
        {
            $domainInfo = json_decode($domainInfo,true);
            return @$domainInfo['ga_ids'];
        }
}

function getGAPubliser()
{
        $host = \Request::header('host');
        $domainInfo = \Cache::get('domain_info_'.$host);
        if(!empty($domainInfo))
        {
            $domainInfo = json_decode($domainInfo,true);
            if(isset($domainInfo['publisher']) && !empty($domainInfo['publisher'])){
                return @$domainInfo['publisher']['publisher_code'];
            }
        }
}


function GoogleAd($pageCode,$elements=array())
{
    $host = Request::header('host');
        if(strpos($host, '127.0.0.1')!==false)
        {
            $host = 'funrewards.local.co';
        }
        
    $domainInfo = \Cache::get('domain_info_'.$host);
    if(!empty($domainInfo))
    {
        $domainInfo = json_decode($domainInfo,true);
        $network = @$domainInfo['google_network'];
        $publisher = @$domainInfo['publisher'];
        $pageBlock = @$network[$pageCode];
        if(!empty($network) && !empty($publisher) && $publisher['status']=='ACTIVE' && $publisher['ads_enabled']==1 && !empty($pageBlock))
        {
            //p($domainInfo);
            //p($pageBlock);
            $pageBlock['format'] = ($pageBlock['format']!='')?$pageBlock['format']:'auto';
            $pageBlock['responsive'] = strtolower($pageBlock['responsive']);
            $layoutkey = @$elements['datalayoutkey'];
            if(!empty($layoutkey))
            {
                $layoutkey = 'data-ad-layout-key="'.$layoutkey.'"';
            }
            else
            {
                $layoutkey = '';
            }

            $userCountry = (isset( (json_decode(Session::get('userzone')))->country )) ? (json_decode(Session::get('userzone')))->country : '' ;
            
            $optout = Session::get('optout');
            $complianceCountries = config('qureka.compliance_countries');

            if (in_array($userCountry,$complianceCountries) && $optout == 'true') {
                $dataprocessing = 'data-restrict-data-processing="true"';
            }else{$dataprocessing='';}

            //if(!Request::ajax()){
                echo '<ins id="'.@$elements['refresh_id'].'" class="adsbygoogle sq_ad_1" style="display:block;'.@$elements['customDesign'].'" data-ad-client="'.$publisher['publisher_code'].'" data-ad-slot="'.$pageBlock['slot'].'" data-ad-format="'.$pageBlock['format'].'" data-full-width-responsive="'.$pageBlock['responsive'].'" '.$layoutkey.' '.$dataprocessing.'></ins>';
            //}
            $menaCountry = config('qureka.mena_countries');

            $npa_ads = false;
            if (!empty($userCountry) && in_array($userCountry,$menaCountry)) {
                $npa_ads = true;
            }
            ?>
            <script type="text/javascript">
                var npa_ads = "<?php echo $npa_ads ?>";
                var isAjax = "<?php echo Request::ajax();?>";
                if(isAjax==1){
                    if (npa_ads ==1) {
                        console.log('npa true ajax');
                        setTimeout(function(){(adsbygoogle = window.adsbygoogle || []).requestNonPersonalizedAds=1}, 10);
                    }
                    setTimeout(function(){(adsbygoogle = window.adsbygoogle || []).push({})}, 10);
                }
                else{
                    if (npa_ads ==1) {
                        console.log('npa true not ajax');
                        (adsbygoogle = window.adsbygoogle || []).requestNonPersonalizedAds=1;
                    }
                    (adsbygoogle = window.adsbygoogle || []).push({});
                }
            </script>
            <?php

            

        }
        
    }
}

function setContestCache($data, $type){

    $fileName = public_path().'/'.$type.'contestlist.json';
    $now = time();

    if(!empty($data)){
    if(file_exists($fileName)){                   
        if ($now - filemtime($fileName) >= 60 * 15) { // 15 minutes older
             unlink($fileName);
       }

        $contestList = getContestCache($type);
        if(!empty($contestList)) {
         $data = array_merge($contestList,$data);
         $data = array_map("unserialize", array_unique(array_map("serialize", $data)));
        }
    }

    file_put_contents($fileName, json_encode($data));
    }
}

function getContestCache($type){
    $fileName = public_path().'/'.$type.'contestlist.json';
    if(file_exists($fileName)){
        $contestString = file_get_contents($fileName);         
        $data = json_decode($contestString, true);
        return $data; 
    }
}

function showCountryTag(){
    $sessions = array_filter(Session::all());
    $userzone = (isset($sessions['userzone'])) ? json_decode($sessions['userzone'],true) : '';
    return $userzone;
}


function removeExpireContest($contestList){
    #remove expire contest
    if(isset($contestList) && !empty($contestList)){
        foreach ($contestList as $key => $value) {
        
            $blockTime = $value['contest_block_period'];
            $endTime = $value['end_time'];
            $endDate = Carbon::createFromDate($endTime);
            $now = Carbon::now();
            $totalsecleft = $endDate->diffInSeconds($now);
            $contestLeftTime = $totalsecleft - $blockTime;
      
             if($contestLeftTime <= 0){
                  unset($contestList[$key]);
             }
          }
          return $contestList = array_values($contestList);
    }
    else{
        return $contestList;
    }
}

function validateHash($deviceId,$packageId,$hashKey){
    $response = ['status'=>false,'key'=>''];
    $key =  \Config::get("qureka.quizByteKey");

    $timestamp = strtotime(date('H:i Y-m-d'));
    $preTime = date("H:i Y-m-d", strtotime('-1 minutes', $timestamp));
    $nextTime = date("H:i Y-m-d", strtotime('+1 minutes', $timestamp));
    
    $hash = md5($timestamp.$deviceId.$packageId.$key);   
    $preHash = md5(strtotime($preTime).$deviceId.$packageId.$key);
    $nextHash = md5(strtotime($nextTime).$deviceId.$packageId.$key);

    if($hashKey == $hash || $hashKey == $preHash || $hashKey == $nextHash){
            $response = ['status'=>true,'key'=>$hash];
    }
    return $response;
  }

  function safeCountryCheck(){
    $data = array();
    $userzone = json_decode(Session::get('userzone'),true);
    // p($userzone);
    if(@$userzone['country']=='US' || @$userzone['country']=='CA'){

        return true;
        
    }
    return false;
    
}

function getCdnPath($path){
    $imgBaseUrl = \Config::get('app.MIX_CDN_URL');
    if($imgBaseUrl){
       return \Config::get('app.MIX_CDN_URL').$path;
    }else{
        return \Config::get('app.MIX_BKEND_URL').$path;
    }    
}

function getFecdnPath($path){
    $imgBaseUrl = \Config::get('app.MIX_FCDN_URL');
    if($imgBaseUrl){
       return \Config::get('app.MIX_FCDN_URL').$path;
    }else{
        return $path;
    }    
}

?>
