<?php return array (
  'app' => 
  array (
    'name' => 'Laravel',
    'env' => 'development',
    'debug' => true,
    'url' => 'http://localhost',
    'asset_url' => NULL,
    'MIX_CDN_URL' => 'https://qbe.local.com',
    'timezone' => 'Asia/Kolkata',
    'locale' => 'en',
    'fallback_locale' => 'en',
    'faker_locale' => 'en_US',
    'key' => 'base64:k9b2E/KGccdWcNJkOJEVstRNyDG2xrtsHzHFt/r7avo=',
    'cipher' => 'AES-256-CBC',
    'providers' => 
    array (
      0 => 'Illuminate\\Auth\\AuthServiceProvider',
      1 => 'Illuminate\\Broadcasting\\BroadcastServiceProvider',
      2 => 'Illuminate\\Bus\\BusServiceProvider',
      3 => 'Illuminate\\Cache\\CacheServiceProvider',
      4 => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
      5 => 'Illuminate\\Cookie\\CookieServiceProvider',
      6 => 'Illuminate\\Database\\DatabaseServiceProvider',
      7 => 'Illuminate\\Encryption\\EncryptionServiceProvider',
      8 => 'Illuminate\\Filesystem\\FilesystemServiceProvider',
      9 => 'Illuminate\\Foundation\\Providers\\FoundationServiceProvider',
      10 => 'Illuminate\\Hashing\\HashServiceProvider',
      11 => 'Illuminate\\Mail\\MailServiceProvider',
      12 => 'Illuminate\\Notifications\\NotificationServiceProvider',
      13 => 'Illuminate\\Pagination\\PaginationServiceProvider',
      14 => 'Illuminate\\Pipeline\\PipelineServiceProvider',
      15 => 'Illuminate\\Queue\\QueueServiceProvider',
      16 => 'Illuminate\\Redis\\RedisServiceProvider',
      17 => 'Illuminate\\Auth\\Passwords\\PasswordResetServiceProvider',
      18 => 'Illuminate\\Session\\SessionServiceProvider',
      19 => 'Illuminate\\Translation\\TranslationServiceProvider',
      20 => 'Illuminate\\Validation\\ValidationServiceProvider',
      21 => 'Illuminate\\View\\ViewServiceProvider',
      22 => 'App\\Providers\\AppServiceProvider',
      23 => 'App\\Providers\\AuthServiceProvider',
      24 => 'App\\Providers\\EventServiceProvider',
      25 => 'App\\Providers\\RouteServiceProvider',
      26 => 'App\\Modules\\ServiceProvider',
    ),
    'aliases' => 
    array (
      'App' => 'Illuminate\\Support\\Facades\\App',
      'Arr' => 'Illuminate\\Support\\Arr',
      'Artisan' => 'Illuminate\\Support\\Facades\\Artisan',
      'Auth' => 'Illuminate\\Support\\Facades\\Auth',
      'Blade' => 'Illuminate\\Support\\Facades\\Blade',
      'Broadcast' => 'Illuminate\\Support\\Facades\\Broadcast',
      'Bus' => 'Illuminate\\Support\\Facades\\Bus',
      'Cache' => 'Illuminate\\Support\\Facades\\Cache',
      'Config' => 'Illuminate\\Support\\Facades\\Config',
      'Cookie' => 'Illuminate\\Support\\Facades\\Cookie',
      'Crypt' => 'Illuminate\\Support\\Facades\\Crypt',
      'DB' => 'Illuminate\\Support\\Facades\\DB',
      'Eloquent' => 'Illuminate\\Database\\Eloquent\\Model',
      'Event' => 'Illuminate\\Support\\Facades\\Event',
      'File' => 'Illuminate\\Support\\Facades\\File',
      'Gate' => 'Illuminate\\Support\\Facades\\Gate',
      'Hash' => 'Illuminate\\Support\\Facades\\Hash',
      'Http' => 'Illuminate\\Support\\Facades\\Http',
      'Lang' => 'Illuminate\\Support\\Facades\\Lang',
      'Log' => 'Illuminate\\Support\\Facades\\Log',
      'Mail' => 'Illuminate\\Support\\Facades\\Mail',
      'Notification' => 'Illuminate\\Support\\Facades\\Notification',
      'Password' => 'Illuminate\\Support\\Facades\\Password',
      'Queue' => 'Illuminate\\Support\\Facades\\Queue',
      'Redirect' => 'Illuminate\\Support\\Facades\\Redirect',
      'Redis' => 'Illuminate\\Support\\Facades\\Redis',
      'Request' => 'Illuminate\\Support\\Facades\\Request',
      'Response' => 'Illuminate\\Support\\Facades\\Response',
      'Route' => 'Illuminate\\Support\\Facades\\Route',
      'Schema' => 'Illuminate\\Support\\Facades\\Schema',
      'Session' => 'Illuminate\\Support\\Facades\\Session',
      'Storage' => 'Illuminate\\Support\\Facades\\Storage',
      'Str' => 'Illuminate\\Support\\Str',
      'URL' => 'Illuminate\\Support\\Facades\\URL',
      'Validator' => 'Illuminate\\Support\\Facades\\Validator',
      'View' => 'Illuminate\\Support\\Facades\\View',
      'Input' => 'Illuminate\\Support\\Facades\\Request',
    ),
  ),
  'auth' => 
  array (
    'defaults' => 
    array (
      'guard' => 'web',
      'passwords' => 'users',
    ),
    'guards' => 
    array (
      'web' => 
      array (
        'driver' => 'session',
        'provider' => 'users',
      ),
      'api' => 
      array (
        'driver' => 'passport',
        'provider' => 'users',
        'hash' => false,
      ),
      'admin' => 
      array (
        'driver' => 'session',
        'provider' => 'admins',
      ),
    ),
    'providers' => 
    array (
      'users' => 
      array (
        'driver' => 'eloquent',
        'model' => 'App\\User',
      ),
      'admins' => 
      array (
        'driver' => 'eloquent',
        'model' => 'App\\Admin',
      ),
    ),
    'passwords' => 
    array (
      'users' => 
      array (
        'provider' => 'users',
        'table' => 'password_resets',
        'expire' => 60,
        'throttle' => 60,
      ),
      'admins' => 
      array (
        'provider' => 'admins',
        'table' => 'password_resets',
        'expire' => 60,
      ),
    ),
    'password_timeout' => 10800,
  ),
  'broadcasting' => 
  array (
    'default' => 'log',
    'connections' => 
    array (
      'pusher' => 
      array (
        'driver' => 'pusher',
        'key' => '',
        'secret' => '',
        'app_id' => '',
        'options' => 
        array (
          'cluster' => 'mt1',
          'useTLS' => true,
        ),
      ),
      'redis' => 
      array (
        'driver' => 'redis',
        'connection' => 'default',
      ),
      'log' => 
      array (
        'driver' => 'log',
      ),
      'null' => 
      array (
        'driver' => 'null',
      ),
    ),
  ),
  'cache' => 
  array (
    'default' => 'file',
    'stores' => 
    array (
      'apc' => 
      array (
        'driver' => 'apc',
      ),
      'array' => 
      array (
        'driver' => 'array',
        'serialize' => false,
      ),
      'database' => 
      array (
        'driver' => 'database',
        'table' => 'cache',
        'connection' => NULL,
      ),
      'file' => 
      array (
        'driver' => 'file',
        'path' => '/var/www/coolboots/funrewards/storage/framework/cache/data',
      ),
      'memcached' => 
      array (
        'driver' => 'memcached',
        'persistent_id' => NULL,
        'sasl' => 
        array (
          0 => NULL,
          1 => NULL,
        ),
        'options' => 
        array (
        ),
        'servers' => 
        array (
          0 => 
          array (
            'host' => '127.0.0.1',
            'port' => 11211,
            'weight' => 100,
          ),
        ),
      ),
      'redis' => 
      array (
        'driver' => 'redis',
        'connection' => 'cache',
      ),
      'dynamodb' => 
      array (
        'driver' => 'dynamodb',
        'key' => '',
        'secret' => '',
        'region' => 'us-east-1',
        'table' => 'cache',
        'endpoint' => NULL,
      ),
    ),
    'prefix' => 'laravel_cache',
  ),
  'cors' => 
  array (
    'paths' => 
    array (
      0 => 'api/*',
    ),
    'allowed_methods' => 
    array (
      0 => '*',
    ),
    'allowed_origins' => 
    array (
      0 => '*',
    ),
    'allowed_origins_patterns' => 
    array (
    ),
    'allowed_headers' => 
    array (
      0 => '*',
    ),
    'exposed_headers' => 
    array (
    ),
    'max_age' => 0,
    'supports_credentials' => false,
  ),
  'database' => 
  array (
    'default' => 'mysql',
    'connections' => 
    array (
      'sqlite' => 
      array (
        'driver' => 'sqlite',
        'url' => NULL,
        'database' => 'secure_quiz_bytes',
        'prefix' => '',
        'foreign_key_constraints' => true,
      ),
      'mysql' => 
      array (
        'driver' => 'mysql',
        'url' => NULL,
        'host' => '127.0.0.1',
        'port' => '3306',
        'database' => 'secure_quiz_bytes',
        'username' => 'root',
        'password' => 'password',
        'unix_socket' => '',
        'charset' => 'utf8mb4',
        'collation' => 'utf8mb4_unicode_ci',
        'prefix' => '',
        'prefix_indexes' => true,
        'strict' => false,
        'engine' => NULL,
        'options' => 
        array (
        ),
      ),
      'pgsql' => 
      array (
        'driver' => 'pgsql',
        'url' => NULL,
        'host' => '127.0.0.1',
        'port' => '3306',
        'database' => 'secure_quiz_bytes',
        'username' => 'root',
        'password' => 'password',
        'charset' => 'utf8',
        'prefix' => '',
        'prefix_indexes' => true,
        'schema' => 'public',
        'sslmode' => 'prefer',
      ),
      'sqlsrv' => 
      array (
        'driver' => 'sqlsrv',
        'url' => NULL,
        'host' => '127.0.0.1',
        'port' => '3306',
        'database' => 'secure_quiz_bytes',
        'username' => 'root',
        'password' => 'password',
        'charset' => 'utf8',
        'prefix' => '',
        'prefix_indexes' => true,
      ),
    ),
    'migrations' => 'migrations',
    'redis' => 
    array (
      'client' => 'phpredis',
      'options' => 
      array (
        'cluster' => 'redis',
        'prefix' => 'laravel_database_',
      ),
      'default' => 
      array (
        'url' => NULL,
        'host' => '127.0.0.1',
        'password' => NULL,
        'port' => '6379',
        'database' => '0',
      ),
      'cache' => 
      array (
        'url' => NULL,
        'host' => '127.0.0.1',
        'password' => NULL,
        'port' => '6379',
        'database' => '1',
      ),
    ),
  ),
  'filesystems' => 
  array (
    'default' => 'local',
    'cloud' => 's3',
    'disks' => 
    array (
      'local' => 
      array (
        'driver' => 'local',
        'root' => '/var/www/coolboots/funrewards/storage/app',
      ),
      'public' => 
      array (
        'driver' => 'local',
        'root' => '/var/www/coolboots/funrewards/storage/app/public',
        'url' => 'http://localhost/storage',
        'visibility' => 'public',
      ),
      's3' => 
      array (
        'driver' => 's3',
        'key' => '',
        'secret' => '',
        'region' => 'us-east-1',
        'bucket' => '',
        'url' => NULL,
        'endpoint' => NULL,
      ),
    ),
    'links' => 
    array (
      '/var/www/coolboots/funrewards/public/storage' => '/var/www/coolboots/funrewards/storage/app/public',
    ),
  ),
  'hashing' => 
  array (
    'driver' => 'bcrypt',
    'bcrypt' => 
    array (
      'rounds' => 10,
    ),
    'argon' => 
    array (
      'memory' => 1024,
      'threads' => 2,
      'time' => 2,
    ),
  ),
  'logging' => 
  array (
    'default' => 'stack',
    'channels' => 
    array (
      'stack' => 
      array (
        'driver' => 'stack',
        'channels' => 
        array (
          0 => 'single',
        ),
        'ignore_exceptions' => false,
      ),
      'single' => 
      array (
        'driver' => 'single',
        'path' => '/var/www/coolboots/funrewards/storage/logs/laravel.log',
        'level' => 'debug',
      ),
      'daily' => 
      array (
        'driver' => 'daily',
        'path' => '/var/www/coolboots/funrewards/storage/logs/laravel.log',
        'level' => 'debug',
        'days' => 14,
      ),
      'slack' => 
      array (
        'driver' => 'slack',
        'url' => NULL,
        'username' => 'Laravel Log',
        'emoji' => ':boom:',
        'level' => 'critical',
      ),
      'papertrail' => 
      array (
        'driver' => 'monolog',
        'level' => 'debug',
        'handler' => 'Monolog\\Handler\\SyslogUdpHandler',
        'handler_with' => 
        array (
          'host' => NULL,
          'port' => NULL,
        ),
      ),
      'stderr' => 
      array (
        'driver' => 'monolog',
        'handler' => 'Monolog\\Handler\\StreamHandler',
        'formatter' => NULL,
        'with' => 
        array (
          'stream' => 'php://stderr',
        ),
      ),
      'syslog' => 
      array (
        'driver' => 'syslog',
        'level' => 'debug',
      ),
      'errorlog' => 
      array (
        'driver' => 'errorlog',
        'level' => 'debug',
      ),
      'null' => 
      array (
        'driver' => 'monolog',
        'handler' => 'Monolog\\Handler\\NullHandler',
      ),
      'emergency' => 
      array (
        'path' => '/var/www/coolboots/funrewards/storage/logs/laravel.log',
      ),
    ),
  ),
  'mail' => 
  array (
    'default' => 'smtp',
    'mailers' => 
    array (
      'smtp' => 
      array (
        'transport' => 'smtp',
        'host' => 'smtp.mailtrap.io',
        'port' => '2525',
        'encryption' => NULL,
        'username' => NULL,
        'password' => NULL,
        'timeout' => NULL,
        'auth_mode' => NULL,
      ),
      'ses' => 
      array (
        'transport' => 'ses',
      ),
      'mailgun' => 
      array (
        'transport' => 'mailgun',
      ),
      'postmark' => 
      array (
        'transport' => 'postmark',
      ),
      'sendmail' => 
      array (
        'transport' => 'sendmail',
        'path' => '/usr/sbin/sendmail -bs',
      ),
      'log' => 
      array (
        'transport' => 'log',
        'channel' => NULL,
      ),
      'array' => 
      array (
        'transport' => 'array',
      ),
    ),
    'from' => 
    array (
      'address' => NULL,
      'name' => 'Laravel',
    ),
    'markdown' => 
    array (
      'theme' => 'default',
      'paths' => 
      array (
        0 => '/var/www/coolboots/funrewards/resources/views/vendor/mail',
      ),
    ),
  ),
  'module' => 
  array (
    'modules' => 
    array (
      'frontend' => 
      array (
        0 => 'authentication',
        1 => 'cms',
      ),
      'backend' => 
      array (
        0 => 'cms',
        1 => 'rbac',
      ),
      'api' => 
      array (
        0 => 'cms',
      ),
    ),
  ),
  'queue' => 
  array (
    'default' => 'sync',
    'connections' => 
    array (
      'sync' => 
      array (
        'driver' => 'sync',
      ),
      'database' => 
      array (
        'driver' => 'database',
        'table' => 'jobs',
        'queue' => 'default',
        'retry_after' => 90,
      ),
      'beanstalkd' => 
      array (
        'driver' => 'beanstalkd',
        'host' => 'localhost',
        'queue' => 'default',
        'retry_after' => 90,
        'block_for' => 0,
      ),
      'sqs' => 
      array (
        'driver' => 'sqs',
        'key' => '',
        'secret' => '',
        'prefix' => 'https://sqs.us-east-1.amazonaws.com/your-account-id',
        'queue' => 'your-queue-name',
        'suffix' => NULL,
        'region' => 'us-east-1',
      ),
      'redis' => 
      array (
        'driver' => 'redis',
        'connection' => 'default',
        'queue' => 'default',
        'retry_after' => 90,
        'block_for' => NULL,
      ),
    ),
    'failed' => 
    array (
      'driver' => 'database',
      'database' => 'mysql',
      'table' => 'failed_jobs',
    ),
  ),
  'qureka' => 
  array (
    'API_SERVER_PUB_KEY' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiOTlkNGU3ZjU4ZWZhOTIxMzRlODZmYWE3YTAzMjE2M2UzYzcyNDU3NjRkYTRhMDJlNGJjYjBjYmNjODQ3NmYwZThmZDFjNTJmNTYxNTA1NGYiLCJpYXQiOjE2Mzg1OTc4ODMsIm5iZiI6MTYzODU5Nzg4MywiZXhwIjoxNjcwMTMzODgzLCJzdWIiOiIiLCJzY29wZXMiOlsiKiJdfQ.rD5p8q2MhbCRvGofqIQEL0jGIl0MaB2JIS-HP56InUyD5Qc35iu43jwaJD0C11WVK7yRioOdxRwvHq0Y5daitj10MEPaowrXP-Hcx1kMtYAfNsM4kZjECZGPrI0ZqxvvFpzapnU0-L4S9xFCpQBUwSMAq81BjC5hZWxJeRQEsubLeYV025ULZl-9ScPZtLzXfUwizb_3gCupCen1qz39mX5PDvhY5jAjwtA23ODm1hyBPj8ka42YoufE6V_Jx0BmmswxHr2y7qoKQOiyBElF1Rk5349ChnTVlkflYvGcnWpJLmJ2fp7JPAutdiSjItFQ9u8jZLCjj9wId9GHMz0KwpOxqA55SBtqe1aWpwnlGBpKh1IIQUeoKFmlHUxs193mCjRylJvA4_kGcPm5myqxDgIN8ju2GEUnzEqlT22lq8xxcWNzRKvGIAOio4IuqZjC6tWAs3NOTXRzfYGu4RpP3Y59pI1Gx4nDMq-TOhzjgqD6Gi4_r-FaSilvEwR62bp3I31vb205u8ppsJNTfRgRrssOSxFHQpDFreUNUJWqehZBf-5x5Y9ktcUifL7eLeZgf9ubLZAgENR67D_iYWKM71VvlVSWFOWNjbF61wj30jq7BylmV0YeOna_iilj2me_cOKR0q4UqCPsEqC_XaJvZwvu5yR3CT-nbJeljTgmxtc',
    'social_credentials' => 
    array (
      'facebook' => 
      array (
        0 => 
        array (
          'domains' => 
          array (
            'play' => 'play',
            'playseries' => 
            array (
              'from' => '1',
              'to' => '99',
            ),
            'winseries' => 
            array (
              'from' => '100',
              'to' => '500',
            ),
            'goseries' => 
            array (
              'from' => '1',
              'to' => '500',
            ),
            'tb' => 'tb',
            'qfe' => 'qfe',
            'tb1' => 'tb1',
          ),
          'credential' => 
          array (
            'id' => '1267648160284733',
            'secret' => 'b31122b34bdea1c11cda688ffe4ee07e',
          ),
        ),
        1 => 
        array (
          'domains' => 
          array (
            'winseries' => 
            array (
              'from' => '501',
              'to' => '1000',
            ),
          ),
          'credential' => 
          array (
            'id' => '651663995480939',
            'secret' => 'e3fcd55f463ebbcb9ba8c4e2a6eabffc',
          ),
        ),
      ),
      'google' => 
      array (
        0 => 
        array (
          'domains' => 
          array (
            'play' => 'play',
            'playseries' => 
            array (
              'from' => '1',
              'to' => '99',
            ),
            'winseries' => 
            array (
              'from' => '100',
              'to' => '500',
            ),
            'goseries' => 
            array (
              'from' => '1',
              'to' => '500',
            ),
            'tb' => 'tb',
            'qfe' => 'qfe',
            'tb1' => 'tb1',
          ),
          'credential' => 
          array (
            'id' => '1017633277568-uesd45fu3gk8n00p0peksnuu371e0gf7.apps.googleusercontent.com',
            'secret' => 'tJX1X0m0GaS418ZtUzCOMakc',
          ),
        ),
        1 => 
        array (
          'domains' => 
          array (
            'winseries' => 
            array (
              'from' => '501',
              'to' => '1000',
            ),
          ),
          'credential' => 
          array (
            'id' => '431355601405-a5892m1u9pc2ijvf929nqcpoiiplt29u.apps.googleusercontent.com',
            'secret' => 'sflNkjLipibO6YDRNKWVAoFh',
          ),
        ),
      ),
    ),
    'pre_flow' => 
    array (
      'modules' => 
      array (
        'samplequestion' => 
        array (
          'url' => 'http://qapi.local.com/api/v1/sample-question/{origin}',
          'params' => 
          array (
          ),
          'method' => 'GET',
          'render' => 'no',
          'cache' => false,
          'cache_interval' => 5,
        ),
        'gamedetail' => 
        array (
          'url' => 'http://qapi.local.com/api/v1/game/{id}',
          'params' => 
          array (
          ),
          'method' => 'GET',
          'render' => 'no',
          'cache' => false,
          'cache_interval' => 5,
        ),
        'playcontest' => 
        array (
          'url' => 'http://qapi.local.com/api/v1/contest/{id}/play',
          'params' => 
          array (
          ),
          'method' => 'GET',
          'render' => 'no',
          'cache' => false,
          'cache_interval' => 5,
        ),
        'gamescore' => 
        array (
          'url' => 'http://qapi.local.com/api/v1/contest/{id}/score/{score}',
          'params' => 
          array (
          ),
          'method' => 'GET',
          'render' => 'no',
          'cache' => false,
          'cache_interval' => 5,
        ),
        'quizresult' => 
        array (
          'url' => 'http://qapi.local.com/api/v1/quiz/contests-result/{origin}',
          'params' => 
          array (
          ),
          'method' => 'GET',
          'render' => 'no',
          'cache' => false,
          'cache_interval' => 5,
        ),
        'cricketresult' => 
        array (
          'url' => 'http://qapi.local.com/api/v1/cricket/contests-result/{origin}',
          'params' => 
          array (
          ),
          'method' => 'GET',
          'render' => 'no',
          'cache' => false,
          'cache_interval' => 5,
        ),
        'winnerslist' => 
        array (
          'url' => 'http://qapi.local.com/api/v1/winnerlist/{id}',
          'params' => 
          array (
          ),
          'method' => 'GET',
          'render' => 'no',
          'cache' => false,
          'cache_interval' => 5,
        ),
        'checkconteststatus' => 
        array (
          'url' => 'http://qapi.local.com/api/v1/contest/{id}/status',
          'params' => 
          array (
          ),
          'method' => 'GET',
          'render' => 'no',
          'cache' => false,
          'cache_interval' => 5,
        ),
        'funquiz' => 
        array (
          'url' => 'http://qapi.local.com/api/v1/funquiz',
          'params' => 
          array (
          ),
          'method' => 'POST',
          'render' => 'no',
          'cache' => false,
          'cache_interval' => 5,
        ),
      ),
    ),
    'social_login' => 
    array (
      'modules' => 
      array (
        'signup' => 
        array (
          'url' => 'http://qapi.local.com/api/v1/signup',
          'params' => 
          array (
          ),
          'method' => 'POST',
          'render' => 'no',
          'cache' => false,
          'cache_interval' => 5,
        ),
        'validatetoken' => 
        array (
          'url' => 'http://qapi.local.com/api/v1/user',
          'params' => 
          array (
          ),
          'method' => 'POST',
          'render' => 'no',
          'cache' => false,
          'cache_interval' => 5,
        ),
        'coindistribute' => 
        array (
          'url' => 'http://qapi.local.com/api/v1/user/dailybonus',
          'params' => 
          array (
          ),
          'method' => 'GET',
          'render' => 'no',
          'cache' => false,
          'cache_interval' => 5,
        ),
        'walletbalance' => 
        array (
          'url' => 'http://qapi.local.com/api/v1/user/walletbalance',
          'params' => 
          array (
          ),
          'method' => 'GET',
          'render' => 'no',
          'cache' => false,
          'cache_interval' => 5,
        ),
      ),
    ),
    'dashboard_flow' => 
    array (
      'modules' => 
      array (
        'quizcontests' => 
        array (
          'url' => 'http://qapi.local.com/api/v1/quiz/contests/{origin}',
          'params' => 
          array (
          ),
          'method' => 'GET',
          'render' => 'no',
          'cache' => true,
          'cache_interval' => 3,
        ),
        'cricketcontests' => 
        array (
          'url' => 'http://qapi.local.com/api/v1/cricket/contests/{origin}',
          'params' => 
          array (
          ),
          'method' => 'GET',
          'render' => 'no',
          'cache' => true,
          'cache_interval' => 3,
        ),
        'mygames' => 
        array (
          'url' => 'http://qapi.local.com/api/v1/mygames/{id}',
          'params' => 
          array (
          ),
          'method' => 'GET',
          'render' => 'no',
          'cache' => true,
          'cache_interval' => 15,
        ),
        'mywallet' => 
        array (
          'url' => 'http://qapi.local.com/api/v1/wallethistory/{id}',
          'params' => 
          array (
          ),
          'method' => 'GET',
          'render' => 'no',
          'cache' => true,
          'cache_interval' => 15,
        ),
        'portcoinmobileverify' => 
        array (
          'url' => 'http://qapi.local.com/api/v1/portcoin/mobile-verify/{mobileNo}',
          'params' => 
          array (
          ),
          'method' => 'GET',
          'render' => 'no',
          'cache' => false,
          'cache_interval' => 15,
        ),
        'portcoin' => 
        array (
          'url' => 'http://qapi.local.com/api/v1/portcoin/{walletId}',
          'params' => 
          array (
          ),
          'method' => 'GET',
          'render' => 'no',
          'cache' => false,
          'cache_interval' => 15,
        ),
      ),
    ),
    'general' => 
    array (
      'modules' => 
      array (
        'domain' => 
        array (
          'url' => 'http://qapi.local.com/api/v1/domaininfo/{url}',
          'params' => 
          array (
          ),
          'method' => 'GET',
          'render' => 'no',
          'cache' => true,
          'cache_interval' => 15,
        ),
      ),
    ),
    'static_ads' => false,
    'api_url' => 'https://funrewards.local.com/',
    'app_url' => 'http://localhost',
    'contest_block_period' => '180 seconds',
    'language' => 
    array (
      'EN' => 'English',
      'HN' => 'Hindi',
    ),
    'tags' => 
    array (
      'IN' => 'India',
      'INT' => 'International',
      'ALL' => 'All',
    ),
    'quizByteKey' => 'SkOoLByTe',
  ),
  'services' => 
  array (
    'mailgun' => 
    array (
      'domain' => NULL,
      'secret' => NULL,
      'endpoint' => 'api.mailgun.net',
    ),
    'postmark' => 
    array (
      'token' => NULL,
    ),
    'ses' => 
    array (
      'key' => '',
      'secret' => '',
      'region' => 'us-east-1',
    ),
    'twitter' => 
    array (
      'client_id' => NULL,
      'client_secret' => NULL,
      'redirect' => NULL,
    ),
    'facebook' => 
    array (
      'client_id' => '651663995480939',
      'client_secret' => 'e3fcd55f463ebbcb9ba8c4e2a6eabffc',
      'redirect' => 'api/auth/facebook/callback',
    ),
    'github' => 
    array (
      'client_id' => NULL,
      'client_secret' => NULL,
      'redirect' => NULL,
    ),
    'google' => 
    array (
      'client_id' => '431355601405-a5892m1u9pc2ijvf929nqcpoiiplt29u.apps.googleusercontent.com',
      'client_secret' => 'sflNkjLipibO6YDRNKWVAoFh',
      'redirect' => 'api/auth/google/callback',
    ),
  ),
  'session' => 
  array (
    'driver' => 'file',
    'lifetime' => '120',
    'expire_on_close' => false,
    'encrypt' => true,
    'files' => '/var/www/coolboots/funrewards/storage/framework/sessions',
    'connection' => NULL,
    'table' => 'sessions',
    'store' => NULL,
    'lottery' => 
    array (
      0 => 2,
      1 => 100,
    ),
    'cookie' => 'laravel_session',
    'path' => '/',
    'domain' => NULL,
    'secure' => NULL,
    'http_only' => false,
    'same_site' => 'lax',
  ),
  'view' => 
  array (
    'paths' => 
    array (
      0 => '/var/www/coolboots/funrewards/resources/views',
    ),
    'compiled' => '/var/www/coolboots/funrewards/storage/framework/views',
  ),
  'flare' => 
  array (
    'key' => NULL,
    'reporting' => 
    array (
      'anonymize_ips' => true,
      'collect_git_information' => false,
      'report_queries' => true,
      'maximum_number_of_collected_queries' => 200,
      'report_query_bindings' => true,
      'report_view_data' => true,
      'grouping_type' => NULL,
    ),
    'send_logs_as_events' => true,
  ),
  'ignition' => 
  array (
    'editor' => 'phpstorm',
    'theme' => 'light',
    'enable_share_button' => true,
    'register_commands' => false,
    'ignored_solution_providers' => 
    array (
      0 => 'Facade\\Ignition\\SolutionProviders\\MissingPackageSolutionProvider',
    ),
    'enable_runnable_solutions' => NULL,
    'remote_sites_path' => '',
    'local_sites_path' => '',
    'housekeeping_endpoint_prefix' => '_ignition',
  ),
  'passport' => 
  array (
    'private_key' => NULL,
    'public_key' => NULL,
    'client_uuids' => false,
    'personal_access_client' => 
    array (
      'id' => NULL,
      'secret' => NULL,
    ),
    'storage' => 
    array (
      'database' => 
      array (
        'connection' => 'mysql',
      ),
    ),
  ),
  'trustedproxy' => 
  array (
    'proxies' => NULL,
    'headers' => 94,
  ),
  'tinker' => 
  array (
    'commands' => 
    array (
    ),
    'alias' => 
    array (
    ),
    'dont_alias' => 
    array (
      0 => 'App\\Nova',
    ),
  ),
);
