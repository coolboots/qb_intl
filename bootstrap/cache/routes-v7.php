<?php

/*
|--------------------------------------------------------------------------
| Load The Cached Routes
|--------------------------------------------------------------------------
|
| Here we will decode and unserialize the RouteCollection instance that
| holds all of the route information for an application. This allows
| us to instantaneously load the entire route map into the router.
|
*/

app('router')->setCompiledRoutes(
    array (
  'compiled' => 
  array (
    0 => false,
    1 => 
    array (
      '/oauth/authorize' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'passport.authorizations.authorize',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'passport.authorizations.approve',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        2 => 
        array (
          0 => 
          array (
            '_route' => 'passport.authorizations.deny',
          ),
          1 => NULL,
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/oauth/token' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'passport.token',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/oauth/tokens' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'passport.tokens.index',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/oauth/token/refresh' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'passport.token.refresh',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/oauth/clients' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'passport.clients.index',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'passport.clients.store',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/oauth/scopes' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'passport.scopes.index',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/oauth/personal-access-tokens' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'passport.personal.tokens.index',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'passport.personal.tokens.store',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/login' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'admin.',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'admin.generated::g6JO0lIt6aNSrI57',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/logout' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'admin.generated::IJ3iIlxgBybQB8m7',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/password/email' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'admin.generated::PqaFYpk34iK6Qfcu',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/password/reset' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'admin.generated::jVtFKn5RaDpuikWk',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'admin.generated::tPf0MPz8fgul9a9t',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::hTAyBKrh9Ov83vBE',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/mail' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::biN9zHyXPEclF5rR',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/home' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::QSR3oyBfjIqaFhsA',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/dashboard' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::5Aw3hWcnKNryAoeE',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/categories' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'categories.index',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'categories.store',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/categories/create' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'categories.create',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/questionaires/import' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::AjWOc6D4vFJtYDvq',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/questionaires/importExcel' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::QXdmkhwX1d8z0oGQ',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/questionaires' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'questionaires.index',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'questionaires.store',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/questionaires/create' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'questionaires.create',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/questionairesopt' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'questionairesopt.index',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'questionairesopt.store',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/questionairesopt/create' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'questionairesopt.create',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/contests' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'contests.index',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'contests.store',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/contests/create' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'contests.create',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/contests/clone/save' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::iEBCkrTZHGmF5i3k',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/pages' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'pages.index',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'pages.store',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/pages/create' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'pages.create',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/partners' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'partners.index',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'partners.store',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/partners/create' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'partners.create',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/publishers' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'publishers.index',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'publishers.store',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/publishers/create' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'publishers.create',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/adsconfig' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'adsconfig.index',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'adsconfig.store',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/adsconfig/create' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'adsconfig.create',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/ads_config/import' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'adsconf.import.view',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/ads_config_import' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'adsconf.import.upload',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/siteconfig' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'siteconfig.index',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'siteconfig.store',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/siteconfig/create' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'siteconfig.create',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/awardmatrix' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'awardmatrix.index',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'awardmatrix.store',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/awardmatrix/create' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'awardmatrix.create',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/admins' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'admins.index',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'admins.store',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/admins/create' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'admins.create',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/articles' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'articles.index',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'articles.store',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/articles/create' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'articles.create',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/cmsseourls' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'cmsseourls.index',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'cmsseourls.store',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/cmsseourls/create' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'cmsseourls.create',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/contestawardmatrix' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'contestawardmatrix.index',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'contestawardmatrix.store',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/contestawardmatrix/create' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'contestawardmatrix.create',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/contestparticipanthistory' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'contestparticipanthistory.index',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'contestparticipanthistory.store',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/contestparticipanthistory/create' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'contestparticipanthistory.create',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/contestwinners' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'contestwinners.index',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'contestwinners.store',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/contestwinners/create' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'contestwinners.create',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/users' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'users.index',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'users.store',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/users/create' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'users.create',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/userwalletcash' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'userwalletcash.index',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'userwalletcash.store',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/userwalletcash/create' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'userwalletcash.create',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/userwalletcoins' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'userwalletcoins.index',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'userwalletcoins.store',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/userwalletcoins/create' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'userwalletcoins.create',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/userwallettranshistory' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'userwallettranshistory.index',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'userwallettranshistory.store',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/userwallettranshistory/create' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'userwallettranshistory.create',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/quizuniqueuser' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::qLHu6PbkVzQcDd3j',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/crickuniqueuser' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::oVTPzOz8dblelu3g',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/quizcontestplayed' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::Em9yVZevdExfJNoo',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/crickcontestplayed' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::tgYkkE1OvtxJFxsl',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/loginusers' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::1RIERzZ86WGoQ9Cx',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/newloginusers' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::ZLe8EkUOtpNRCqV0',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/goolelogins' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::OKzpkWoY4mb3Gv2V',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/facebooklogins' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::gbmynxeKBTapDoUD',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/serverinfo' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'serverinfo.index',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'serverinfo.store',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/serverinfo/create' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'serverinfo.create',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/unauthorized' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::FbeqFMrkGsZtAyX7',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/testemail' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::ohDTTCg2d5neoPfH',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/adminuser' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'adminuser.index',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'adminuser.store',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/adminuser/create' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'adminuser.create',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/menucategory' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'menucategory.index',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'menucategory.store',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/menucategory/create' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'menucategory.create',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/modulelink' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'modulelink.index',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'modulelink.store',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/modulelink/create' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'modulelink.create',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/module' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'module.index',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'module.store',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/module/create' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'module.create',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/usergroup' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'usergroup.index',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'usergroup.store',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/usergroup/create' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'usergroup.create',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/admin/settings' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::YgIdmUsslgwkmE76',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'generated::8k5gjV9Kxi9UvdWo',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/validateapp' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::FNUIvHmlprpMK545',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/fireevent' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::BZ9cQRAM2JqU737u',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/validateevent' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::lwRTi1wc8UTayimN',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::qypCiIEjgsLrVrAm',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/intro' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::VmUfnMCnk8wixdSW',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/intro/question' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::VZf2pzGsjq0jvCcs',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/intro/success' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::w3KtBBUzcpToQx0U',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
    ),
    2 => 
    array (
      0 => '{^(?|/oauth/(?|tokens/([^/]++)(*:32)|clients/([^/]++)(?|(*:58))|personal\\-access\\-tokens/([^/]++)(*:99))|/a(?|dmin/(?|p(?|a(?|ssword/reset/([^/]++)(*:149)|ges/([^/]++)(?|(*:172)|/edit(*:185)|(*:193))|rtners/([^/]++)(?|(*:220)|/edit(*:233)|(*:241)))|ublishers/([^/]++)(?|(*:272)|/edit(*:285)|(*:293)))|c(?|ategories/([^/]++)(?|(*:328)|/edit(*:341)|(*:349))|ontest(?|s/(?|matrix/([^/]++)(?|(*:390))|([^/]++)(?|(*:410)|/(?|edit(*:426)|clone(*:439))|(*:448)))|awardmatrix/([^/]++)(?|(*:481)|/edit(*:494)|(*:502))|participanthistory/([^/]++)(?|(*:541)|/edit(*:554)|(*:562))|winners/([^/]++)(?|(*:590)|/edit(*:603)|(*:611)))|msseourls/([^/]++)(?|(*:642)|/edit(*:655)|(*:663)))|questionaires(?|/([^/]++)(?|(*:701)|/edit(*:714)|(*:722))|opt/([^/]++)(?|(*:746)|/edit(*:759)|(*:767)))|a(?|d(?|sconfig/([^/]++)(?|(*:804)|/edit(*:817)|(*:825))|min(?|s/([^/]++)(?|(*:853)|/edit(*:866)|(*:874))|user/([^/]++)(?|(*:899)|/edit(*:912)|(*:920))))|wardmatrix/([^/]++)(?|(*:953)|/edit(*:966)|(*:974))|rticles/([^/]++)(?|(*:1002)|/edit(*:1016)|(*:1025)))|s(?|iteconfig/([^/]++)(?|(*:1061)|/edit(*:1075)|(*:1084))|erverinfo/([^/]++)(?|(*:1115)|/edit(*:1129)|(*:1138)))|user(?|s/([^/]++)(?|(*:1169)|/(?|edit(*:1186)|history(*:1202))|(*:1212))|wallet(?|c(?|ash/([^/]++)(?|(*:1250)|/edit(*:1264)|(*:1273))|oins/([^/]++)(?|(*:1299)|/edit(*:1313)|(*:1322)))|transhistory/([^/]++)(?|(*:1357)|/edit(*:1371)|(*:1380)))|group/([^/]++)(?|(*:1408)|/(?|edit(*:1425)|assign(?|(*:1443)))|(*:1454)))|m(?|enucategory/([^/]++)(?|(*:1492)|/edit(*:1506)|(*:1515))|odule(?|link/([^/]++)(?|(*:1549)|/edit(*:1563)|(*:1572))|/([^/]++)(?|(*:1594)|/edit(*:1608)|(*:1617)))))|pi/v1/sample\\-question/([^/]++)(*:1661))|/((?!api).*)(*:1683))/?$}sDu',
    ),
    3 => 
    array (
      32 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'passport.tokens.destroy',
          ),
          1 => 
          array (
            0 => 'token_id',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      58 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'passport.clients.update',
          ),
          1 => 
          array (
            0 => 'client_id',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'passport.clients.destroy',
          ),
          1 => 
          array (
            0 => 'client_id',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      99 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'passport.personal.tokens.destroy',
          ),
          1 => 
          array (
            0 => 'token_id',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      149 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'admin.generated::DomZtS7s7LdZa4B2',
          ),
          1 => 
          array (
            0 => 'token',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      172 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'pages.show',
          ),
          1 => 
          array (
            0 => 'page',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      185 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'pages.edit',
          ),
          1 => 
          array (
            0 => 'page',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      193 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'pages.update',
          ),
          1 => 
          array (
            0 => 'page',
          ),
          2 => 
          array (
            'PUT' => 0,
            'PATCH' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'pages.destroy',
          ),
          1 => 
          array (
            0 => 'page',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      220 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'partners.show',
          ),
          1 => 
          array (
            0 => 'partner',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      233 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'partners.edit',
          ),
          1 => 
          array (
            0 => 'partner',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      241 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'partners.update',
          ),
          1 => 
          array (
            0 => 'partner',
          ),
          2 => 
          array (
            'PUT' => 0,
            'PATCH' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'partners.destroy',
          ),
          1 => 
          array (
            0 => 'partner',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      272 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'publishers.show',
          ),
          1 => 
          array (
            0 => 'publisher',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      285 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'publishers.edit',
          ),
          1 => 
          array (
            0 => 'publisher',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      293 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'publishers.update',
          ),
          1 => 
          array (
            0 => 'publisher',
          ),
          2 => 
          array (
            'PUT' => 0,
            'PATCH' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'publishers.destroy',
          ),
          1 => 
          array (
            0 => 'publisher',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      328 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'categories.show',
          ),
          1 => 
          array (
            0 => 'category',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      341 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'categories.edit',
          ),
          1 => 
          array (
            0 => 'category',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      349 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'categories.update',
          ),
          1 => 
          array (
            0 => 'category',
          ),
          2 => 
          array (
            'PUT' => 0,
            'PATCH' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'categories.destroy',
          ),
          1 => 
          array (
            0 => 'category',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      390 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::h7WJhnburpPGkMJc',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'generated::VgjdojkQQbrtXsC7',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      410 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'contests.show',
          ),
          1 => 
          array (
            0 => 'contest',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      426 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'contests.edit',
          ),
          1 => 
          array (
            0 => 'contest',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      439 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::sEk6bgPhs4CemKYd',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      448 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'contests.update',
          ),
          1 => 
          array (
            0 => 'contest',
          ),
          2 => 
          array (
            'PUT' => 0,
            'PATCH' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'contests.destroy',
          ),
          1 => 
          array (
            0 => 'contest',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      481 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'contestawardmatrix.show',
          ),
          1 => 
          array (
            0 => 'contestawardmatrix',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      494 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'contestawardmatrix.edit',
          ),
          1 => 
          array (
            0 => 'contestawardmatrix',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      502 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'contestawardmatrix.update',
          ),
          1 => 
          array (
            0 => 'contestawardmatrix',
          ),
          2 => 
          array (
            'PUT' => 0,
            'PATCH' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'contestawardmatrix.destroy',
          ),
          1 => 
          array (
            0 => 'contestawardmatrix',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      541 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'contestparticipanthistory.show',
          ),
          1 => 
          array (
            0 => 'contestparticipanthistory',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      554 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'contestparticipanthistory.edit',
          ),
          1 => 
          array (
            0 => 'contestparticipanthistory',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      562 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'contestparticipanthistory.update',
          ),
          1 => 
          array (
            0 => 'contestparticipanthistory',
          ),
          2 => 
          array (
            'PUT' => 0,
            'PATCH' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'contestparticipanthistory.destroy',
          ),
          1 => 
          array (
            0 => 'contestparticipanthistory',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      590 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'contestwinners.show',
          ),
          1 => 
          array (
            0 => 'contestwinner',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      603 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'contestwinners.edit',
          ),
          1 => 
          array (
            0 => 'contestwinner',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      611 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'contestwinners.update',
          ),
          1 => 
          array (
            0 => 'contestwinner',
          ),
          2 => 
          array (
            'PUT' => 0,
            'PATCH' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'contestwinners.destroy',
          ),
          1 => 
          array (
            0 => 'contestwinner',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      642 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'cmsseourls.show',
          ),
          1 => 
          array (
            0 => 'cmsseourl',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      655 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'cmsseourls.edit',
          ),
          1 => 
          array (
            0 => 'cmsseourl',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      663 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'cmsseourls.update',
          ),
          1 => 
          array (
            0 => 'cmsseourl',
          ),
          2 => 
          array (
            'PUT' => 0,
            'PATCH' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'cmsseourls.destroy',
          ),
          1 => 
          array (
            0 => 'cmsseourl',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      701 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'questionaires.show',
          ),
          1 => 
          array (
            0 => 'questionaire',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      714 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'questionaires.edit',
          ),
          1 => 
          array (
            0 => 'questionaire',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      722 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'questionaires.update',
          ),
          1 => 
          array (
            0 => 'questionaire',
          ),
          2 => 
          array (
            'PUT' => 0,
            'PATCH' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'questionaires.destroy',
          ),
          1 => 
          array (
            0 => 'questionaire',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      746 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'questionairesopt.show',
          ),
          1 => 
          array (
            0 => 'questionairesopt',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      759 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'questionairesopt.edit',
          ),
          1 => 
          array (
            0 => 'questionairesopt',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      767 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'questionairesopt.update',
          ),
          1 => 
          array (
            0 => 'questionairesopt',
          ),
          2 => 
          array (
            'PUT' => 0,
            'PATCH' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'questionairesopt.destroy',
          ),
          1 => 
          array (
            0 => 'questionairesopt',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      804 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'adsconfig.show',
          ),
          1 => 
          array (
            0 => 'adsconfig',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      817 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'adsconfig.edit',
          ),
          1 => 
          array (
            0 => 'adsconfig',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      825 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'adsconfig.update',
          ),
          1 => 
          array (
            0 => 'adsconfig',
          ),
          2 => 
          array (
            'PUT' => 0,
            'PATCH' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'adsconfig.destroy',
          ),
          1 => 
          array (
            0 => 'adsconfig',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      853 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'admins.show',
          ),
          1 => 
          array (
            0 => 'admin',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      866 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'admins.edit',
          ),
          1 => 
          array (
            0 => 'admin',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      874 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'admins.update',
          ),
          1 => 
          array (
            0 => 'admin',
          ),
          2 => 
          array (
            'PUT' => 0,
            'PATCH' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'admins.destroy',
          ),
          1 => 
          array (
            0 => 'admin',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      899 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'adminuser.show',
          ),
          1 => 
          array (
            0 => 'adminuser',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      912 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'adminuser.edit',
          ),
          1 => 
          array (
            0 => 'adminuser',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      920 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'adminuser.update',
          ),
          1 => 
          array (
            0 => 'adminuser',
          ),
          2 => 
          array (
            'PUT' => 0,
            'PATCH' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'adminuser.destroy',
          ),
          1 => 
          array (
            0 => 'adminuser',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      953 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'awardmatrix.show',
          ),
          1 => 
          array (
            0 => 'awardmatrix',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      966 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'awardmatrix.edit',
          ),
          1 => 
          array (
            0 => 'awardmatrix',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      974 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'awardmatrix.update',
          ),
          1 => 
          array (
            0 => 'awardmatrix',
          ),
          2 => 
          array (
            'PUT' => 0,
            'PATCH' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'awardmatrix.destroy',
          ),
          1 => 
          array (
            0 => 'awardmatrix',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1002 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'articles.show',
          ),
          1 => 
          array (
            0 => 'article',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1016 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'articles.edit',
          ),
          1 => 
          array (
            0 => 'article',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      1025 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'articles.update',
          ),
          1 => 
          array (
            0 => 'article',
          ),
          2 => 
          array (
            'PUT' => 0,
            'PATCH' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'articles.destroy',
          ),
          1 => 
          array (
            0 => 'article',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1061 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'siteconfig.show',
          ),
          1 => 
          array (
            0 => 'siteconfig',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1075 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'siteconfig.edit',
          ),
          1 => 
          array (
            0 => 'siteconfig',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      1084 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'siteconfig.update',
          ),
          1 => 
          array (
            0 => 'siteconfig',
          ),
          2 => 
          array (
            'PUT' => 0,
            'PATCH' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'siteconfig.destroy',
          ),
          1 => 
          array (
            0 => 'siteconfig',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1115 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'serverinfo.show',
          ),
          1 => 
          array (
            0 => 'serverinfo',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1129 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'serverinfo.edit',
          ),
          1 => 
          array (
            0 => 'serverinfo',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      1138 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'serverinfo.update',
          ),
          1 => 
          array (
            0 => 'serverinfo',
          ),
          2 => 
          array (
            'PUT' => 0,
            'PATCH' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'serverinfo.destroy',
          ),
          1 => 
          array (
            0 => 'serverinfo',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1169 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'users.show',
          ),
          1 => 
          array (
            0 => 'user',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1186 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'users.edit',
          ),
          1 => 
          array (
            0 => 'user',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      1202 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::mOHqiPL3W0t3KBKm',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      1212 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'users.update',
          ),
          1 => 
          array (
            0 => 'user',
          ),
          2 => 
          array (
            'PUT' => 0,
            'PATCH' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'users.destroy',
          ),
          1 => 
          array (
            0 => 'user',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1250 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'userwalletcash.show',
          ),
          1 => 
          array (
            0 => 'userwalletcash',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1264 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'userwalletcash.edit',
          ),
          1 => 
          array (
            0 => 'userwalletcash',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      1273 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'userwalletcash.update',
          ),
          1 => 
          array (
            0 => 'userwalletcash',
          ),
          2 => 
          array (
            'PUT' => 0,
            'PATCH' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'userwalletcash.destroy',
          ),
          1 => 
          array (
            0 => 'userwalletcash',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1299 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'userwalletcoins.show',
          ),
          1 => 
          array (
            0 => 'userwalletcoin',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1313 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'userwalletcoins.edit',
          ),
          1 => 
          array (
            0 => 'userwalletcoin',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      1322 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'userwalletcoins.update',
          ),
          1 => 
          array (
            0 => 'userwalletcoin',
          ),
          2 => 
          array (
            'PUT' => 0,
            'PATCH' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'userwalletcoins.destroy',
          ),
          1 => 
          array (
            0 => 'userwalletcoin',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1357 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'userwallettranshistory.show',
          ),
          1 => 
          array (
            0 => 'userwallettranshistory',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1371 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'userwallettranshistory.edit',
          ),
          1 => 
          array (
            0 => 'userwallettranshistory',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      1380 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'userwallettranshistory.update',
          ),
          1 => 
          array (
            0 => 'userwallettranshistory',
          ),
          2 => 
          array (
            'PUT' => 0,
            'PATCH' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'userwallettranshistory.destroy',
          ),
          1 => 
          array (
            0 => 'userwallettranshistory',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1408 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'usergroup.show',
          ),
          1 => 
          array (
            0 => 'usergroup',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1425 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'usergroup.edit',
          ),
          1 => 
          array (
            0 => 'usergroup',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      1443 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::bbyi6fMIAxJx2lCw',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'generated::igiLawrPattKDQsZ',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      1454 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'usergroup.update',
          ),
          1 => 
          array (
            0 => 'usergroup',
          ),
          2 => 
          array (
            'PUT' => 0,
            'PATCH' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'usergroup.destroy',
          ),
          1 => 
          array (
            0 => 'usergroup',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1492 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'menucategory.show',
          ),
          1 => 
          array (
            0 => 'menucategory',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1506 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'menucategory.edit',
          ),
          1 => 
          array (
            0 => 'menucategory',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      1515 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'menucategory.update',
          ),
          1 => 
          array (
            0 => 'menucategory',
          ),
          2 => 
          array (
            'PUT' => 0,
            'PATCH' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'menucategory.destroy',
          ),
          1 => 
          array (
            0 => 'menucategory',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1549 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'modulelink.show',
          ),
          1 => 
          array (
            0 => 'modulelink',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1563 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'modulelink.edit',
          ),
          1 => 
          array (
            0 => 'modulelink',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      1572 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'modulelink.update',
          ),
          1 => 
          array (
            0 => 'modulelink',
          ),
          2 => 
          array (
            'PUT' => 0,
            'PATCH' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'modulelink.destroy',
          ),
          1 => 
          array (
            0 => 'modulelink',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1594 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'module.show',
          ),
          1 => 
          array (
            0 => 'module',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1608 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'module.edit',
          ),
          1 => 
          array (
            0 => 'module',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      1617 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'module.update',
          ),
          1 => 
          array (
            0 => 'module',
          ),
          2 => 
          array (
            'PUT' => 0,
            'PATCH' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'module.destroy',
          ),
          1 => 
          array (
            0 => 'module',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1661 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::zJEdP8H9oGjOYJkR',
          ),
          1 => 
          array (
            0 => 'origin',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1683 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::VfiGbnJNhcI85cyQ',
          ),
          1 => 
          array (
            0 => 'any',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
        1 => 
        array (
          0 => NULL,
          1 => NULL,
          2 => NULL,
          3 => NULL,
          4 => false,
          5 => false,
          6 => 0,
        ),
      ),
    ),
    4 => NULL,
  ),
  'attributes' => 
  array (
    'passport.authorizations.authorize' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'oauth/authorize',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'auth',
        ),
        'uses' => '\\Laravel\\Passport\\Http\\Controllers\\AuthorizationController@authorize',
        'as' => 'passport.authorizations.authorize',
        'controller' => '\\Laravel\\Passport\\Http\\Controllers\\AuthorizationController@authorize',
        'namespace' => '\\Laravel\\Passport\\Http\\Controllers',
        'prefix' => 'oauth',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'passport.authorizations.approve' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'oauth/authorize',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'auth',
        ),
        'uses' => '\\Laravel\\Passport\\Http\\Controllers\\ApproveAuthorizationController@approve',
        'as' => 'passport.authorizations.approve',
        'controller' => '\\Laravel\\Passport\\Http\\Controllers\\ApproveAuthorizationController@approve',
        'namespace' => '\\Laravel\\Passport\\Http\\Controllers',
        'prefix' => 'oauth',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'passport.authorizations.deny' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'oauth/authorize',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'auth',
        ),
        'uses' => '\\Laravel\\Passport\\Http\\Controllers\\DenyAuthorizationController@deny',
        'as' => 'passport.authorizations.deny',
        'controller' => '\\Laravel\\Passport\\Http\\Controllers\\DenyAuthorizationController@deny',
        'namespace' => '\\Laravel\\Passport\\Http\\Controllers',
        'prefix' => 'oauth',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'passport.token' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'oauth/token',
      'action' => 
      array (
        'uses' => '\\Laravel\\Passport\\Http\\Controllers\\AccessTokenController@issueToken',
        'as' => 'passport.token',
        'middleware' => 'throttle',
        'controller' => '\\Laravel\\Passport\\Http\\Controllers\\AccessTokenController@issueToken',
        'namespace' => '\\Laravel\\Passport\\Http\\Controllers',
        'prefix' => 'oauth',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'passport.tokens.index' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'oauth/tokens',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'auth',
        ),
        'uses' => '\\Laravel\\Passport\\Http\\Controllers\\AuthorizedAccessTokenController@forUser',
        'as' => 'passport.tokens.index',
        'controller' => '\\Laravel\\Passport\\Http\\Controllers\\AuthorizedAccessTokenController@forUser',
        'namespace' => '\\Laravel\\Passport\\Http\\Controllers',
        'prefix' => 'oauth',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'passport.tokens.destroy' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'oauth/tokens/{token_id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'auth',
        ),
        'uses' => '\\Laravel\\Passport\\Http\\Controllers\\AuthorizedAccessTokenController@destroy',
        'as' => 'passport.tokens.destroy',
        'controller' => '\\Laravel\\Passport\\Http\\Controllers\\AuthorizedAccessTokenController@destroy',
        'namespace' => '\\Laravel\\Passport\\Http\\Controllers',
        'prefix' => 'oauth',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'passport.token.refresh' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'oauth/token/refresh',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'auth',
        ),
        'uses' => '\\Laravel\\Passport\\Http\\Controllers\\TransientTokenController@refresh',
        'as' => 'passport.token.refresh',
        'controller' => '\\Laravel\\Passport\\Http\\Controllers\\TransientTokenController@refresh',
        'namespace' => '\\Laravel\\Passport\\Http\\Controllers',
        'prefix' => 'oauth',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'passport.clients.index' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'oauth/clients',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'auth',
        ),
        'uses' => '\\Laravel\\Passport\\Http\\Controllers\\ClientController@forUser',
        'as' => 'passport.clients.index',
        'controller' => '\\Laravel\\Passport\\Http\\Controllers\\ClientController@forUser',
        'namespace' => '\\Laravel\\Passport\\Http\\Controllers',
        'prefix' => 'oauth',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'passport.clients.store' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'oauth/clients',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'auth',
        ),
        'uses' => '\\Laravel\\Passport\\Http\\Controllers\\ClientController@store',
        'as' => 'passport.clients.store',
        'controller' => '\\Laravel\\Passport\\Http\\Controllers\\ClientController@store',
        'namespace' => '\\Laravel\\Passport\\Http\\Controllers',
        'prefix' => 'oauth',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'passport.clients.update' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'oauth/clients/{client_id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'auth',
        ),
        'uses' => '\\Laravel\\Passport\\Http\\Controllers\\ClientController@update',
        'as' => 'passport.clients.update',
        'controller' => '\\Laravel\\Passport\\Http\\Controllers\\ClientController@update',
        'namespace' => '\\Laravel\\Passport\\Http\\Controllers',
        'prefix' => 'oauth',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'passport.clients.destroy' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'oauth/clients/{client_id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'auth',
        ),
        'uses' => '\\Laravel\\Passport\\Http\\Controllers\\ClientController@destroy',
        'as' => 'passport.clients.destroy',
        'controller' => '\\Laravel\\Passport\\Http\\Controllers\\ClientController@destroy',
        'namespace' => '\\Laravel\\Passport\\Http\\Controllers',
        'prefix' => 'oauth',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'passport.scopes.index' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'oauth/scopes',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'auth',
        ),
        'uses' => '\\Laravel\\Passport\\Http\\Controllers\\ScopeController@all',
        'as' => 'passport.scopes.index',
        'controller' => '\\Laravel\\Passport\\Http\\Controllers\\ScopeController@all',
        'namespace' => '\\Laravel\\Passport\\Http\\Controllers',
        'prefix' => 'oauth',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'passport.personal.tokens.index' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'oauth/personal-access-tokens',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'auth',
        ),
        'uses' => '\\Laravel\\Passport\\Http\\Controllers\\PersonalAccessTokenController@forUser',
        'as' => 'passport.personal.tokens.index',
        'controller' => '\\Laravel\\Passport\\Http\\Controllers\\PersonalAccessTokenController@forUser',
        'namespace' => '\\Laravel\\Passport\\Http\\Controllers',
        'prefix' => 'oauth',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'passport.personal.tokens.store' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'oauth/personal-access-tokens',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'auth',
        ),
        'uses' => '\\Laravel\\Passport\\Http\\Controllers\\PersonalAccessTokenController@store',
        'as' => 'passport.personal.tokens.store',
        'controller' => '\\Laravel\\Passport\\Http\\Controllers\\PersonalAccessTokenController@store',
        'namespace' => '\\Laravel\\Passport\\Http\\Controllers',
        'prefix' => 'oauth',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'passport.personal.tokens.destroy' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'oauth/personal-access-tokens/{token_id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'auth',
        ),
        'uses' => '\\Laravel\\Passport\\Http\\Controllers\\PersonalAccessTokenController@destroy',
        'as' => 'passport.personal.tokens.destroy',
        'controller' => '\\Laravel\\Passport\\Http\\Controllers\\PersonalAccessTokenController@destroy',
        'namespace' => '\\Laravel\\Passport\\Http\\Controllers',
        'prefix' => 'oauth',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'admin.' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/login',
      'action' => 
      array (
        'middleware' => 'web',
        'uses' => 'App\\Http\\Controllers\\AdminAuth\\LoginController@showLoginForm',
        'controller' => 'App\\Http\\Controllers\\AdminAuth\\LoginController@showLoginForm',
        'as' => 'admin.',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'admin.generated::g6JO0lIt6aNSrI57' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'admin/login',
      'action' => 
      array (
        'middleware' => 'web',
        'uses' => 'App\\Http\\Controllers\\AdminAuth\\LoginController@login',
        'controller' => 'App\\Http\\Controllers\\AdminAuth\\LoginController@login',
        'as' => 'admin.generated::g6JO0lIt6aNSrI57',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'admin.generated::IJ3iIlxgBybQB8m7' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/logout',
      'action' => 
      array (
        'middleware' => 'web',
        'uses' => 'App\\Http\\Controllers\\AdminAuth\\LoginController@logout',
        'controller' => 'App\\Http\\Controllers\\AdminAuth\\LoginController@logout',
        'as' => 'admin.generated::IJ3iIlxgBybQB8m7',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'admin.generated::PqaFYpk34iK6Qfcu' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'admin/password/email',
      'action' => 
      array (
        'middleware' => 'web',
        'uses' => 'App\\Http\\Controllers\\AdminAuth\\ForgotPasswordController@sendResetLinkEmail',
        'controller' => 'App\\Http\\Controllers\\AdminAuth\\ForgotPasswordController@sendResetLinkEmail',
        'as' => 'admin.generated::PqaFYpk34iK6Qfcu',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'admin.generated::jVtFKn5RaDpuikWk' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'admin/password/reset',
      'action' => 
      array (
        'middleware' => 'web',
        'uses' => 'App\\Http\\Controllers\\AdminAuth\\ResetPasswordController@reset',
        'controller' => 'App\\Http\\Controllers\\AdminAuth\\ResetPasswordController@reset',
        'as' => 'admin.generated::jVtFKn5RaDpuikWk',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'admin.generated::tPf0MPz8fgul9a9t' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/password/reset',
      'action' => 
      array (
        'middleware' => 'web',
        'uses' => 'App\\Http\\Controllers\\AdminAuth\\ForgotPasswordController@showLinkRequestForm',
        'controller' => 'App\\Http\\Controllers\\AdminAuth\\ForgotPasswordController@showLinkRequestForm',
        'as' => 'admin.generated::tPf0MPz8fgul9a9t',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'admin.generated::DomZtS7s7LdZa4B2' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/password/reset/{token}',
      'action' => 
      array (
        'middleware' => 'web',
        'uses' => 'App\\Http\\Controllers\\AdminAuth\\ResetPasswordController@showResetForm',
        'controller' => 'App\\Http\\Controllers\\AdminAuth\\ResetPasswordController@showResetForm',
        'as' => 'admin.generated::DomZtS7s7LdZa4B2',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::hTAyBKrh9Ov83vBE' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\CmsController@getHomePage',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\CmsController@getHomePage',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
        'as' => 'generated::hTAyBKrh9Ov83vBE',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::biN9zHyXPEclF5rR' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/mail',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\CmsController@basic_email',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\CmsController@basic_email',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
        'as' => 'generated::biN9zHyXPEclF5rR',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::QSR3oyBfjIqaFhsA' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/home',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\CmsController@getHomePage',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\CmsController@getHomePage',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
        'as' => 'generated::QSR3oyBfjIqaFhsA',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::5Aw3hWcnKNryAoeE' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/dashboard',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\CmsController@getHomePage',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\CmsController@getHomePage',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
        'as' => 'generated::5Aw3hWcnKNryAoeE',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'categories.index' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/categories',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'categories.index',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\CategoriesController@index',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\CategoriesController@index',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'categories.create' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/categories/create',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'categories.create',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\CategoriesController@create',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\CategoriesController@create',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'categories.store' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'admin/categories',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'categories.store',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\CategoriesController@store',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\CategoriesController@store',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'categories.show' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/categories/{category}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'categories.show',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\CategoriesController@show',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\CategoriesController@show',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'categories.edit' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/categories/{category}/edit',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'categories.edit',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\CategoriesController@edit',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\CategoriesController@edit',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'categories.update' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
        1 => 'PATCH',
      ),
      'uri' => 'admin/categories/{category}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'categories.update',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\CategoriesController@update',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\CategoriesController@update',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'categories.destroy' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'admin/categories/{category}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'categories.destroy',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\CategoriesController@destroy',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\CategoriesController@destroy',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::AjWOc6D4vFJtYDvq' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/questionaires/import',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\QuestionairesController@import',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\QuestionairesController@import',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
        'as' => 'generated::AjWOc6D4vFJtYDvq',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::QXdmkhwX1d8z0oGQ' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'admin/questionaires/importExcel',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\QuestionairesController@importExcel',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\QuestionairesController@importExcel',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
        'as' => 'generated::QXdmkhwX1d8z0oGQ',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'questionaires.index' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/questionaires',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'questionaires.index',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\QuestionairesController@index',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\QuestionairesController@index',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'questionaires.create' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/questionaires/create',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'questionaires.create',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\QuestionairesController@create',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\QuestionairesController@create',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'questionaires.store' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'admin/questionaires',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'questionaires.store',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\QuestionairesController@store',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\QuestionairesController@store',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'questionaires.show' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/questionaires/{questionaire}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'questionaires.show',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\QuestionairesController@show',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\QuestionairesController@show',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'questionaires.edit' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/questionaires/{questionaire}/edit',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'questionaires.edit',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\QuestionairesController@edit',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\QuestionairesController@edit',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'questionaires.update' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
        1 => 'PATCH',
      ),
      'uri' => 'admin/questionaires/{questionaire}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'questionaires.update',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\QuestionairesController@update',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\QuestionairesController@update',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'questionaires.destroy' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'admin/questionaires/{questionaire}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'questionaires.destroy',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\QuestionairesController@destroy',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\QuestionairesController@destroy',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'questionairesopt.index' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/questionairesopt',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'questionairesopt.index',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\QuestionairesoptController@index',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\QuestionairesoptController@index',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'questionairesopt.create' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/questionairesopt/create',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'questionairesopt.create',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\QuestionairesoptController@create',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\QuestionairesoptController@create',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'questionairesopt.store' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'admin/questionairesopt',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'questionairesopt.store',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\QuestionairesoptController@store',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\QuestionairesoptController@store',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'questionairesopt.show' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/questionairesopt/{questionairesopt}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'questionairesopt.show',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\QuestionairesoptController@show',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\QuestionairesoptController@show',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'questionairesopt.edit' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/questionairesopt/{questionairesopt}/edit',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'questionairesopt.edit',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\QuestionairesoptController@edit',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\QuestionairesoptController@edit',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'questionairesopt.update' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
        1 => 'PATCH',
      ),
      'uri' => 'admin/questionairesopt/{questionairesopt}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'questionairesopt.update',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\QuestionairesoptController@update',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\QuestionairesoptController@update',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'questionairesopt.destroy' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'admin/questionairesopt/{questionairesopt}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'questionairesopt.destroy',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\QuestionairesoptController@destroy',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\QuestionairesoptController@destroy',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::h7WJhnburpPGkMJc' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/contests/matrix/{id}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ContestsController@getMatrix',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ContestsController@getMatrix',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
        'as' => 'generated::h7WJhnburpPGkMJc',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::VgjdojkQQbrtXsC7' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'admin/contests/matrix/{id}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ContestsController@updateMatrix',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ContestsController@updateMatrix',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
        'as' => 'generated::VgjdojkQQbrtXsC7',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'contests.index' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/contests',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'contests.index',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ContestsController@index',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ContestsController@index',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'contests.create' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/contests/create',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'contests.create',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ContestsController@create',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ContestsController@create',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'contests.store' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'admin/contests',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'contests.store',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ContestsController@store',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ContestsController@store',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'contests.show' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/contests/{contest}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'contests.show',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ContestsController@show',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ContestsController@show',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'contests.edit' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/contests/{contest}/edit',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'contests.edit',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ContestsController@edit',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ContestsController@edit',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'contests.update' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
        1 => 'PATCH',
      ),
      'uri' => 'admin/contests/{contest}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'contests.update',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ContestsController@update',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ContestsController@update',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'contests.destroy' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'admin/contests/{contest}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'contests.destroy',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ContestsController@destroy',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ContestsController@destroy',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::sEk6bgPhs4CemKYd' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/contests/{id}/clone',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ContestsController@clone',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ContestsController@clone',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
        'as' => 'generated::sEk6bgPhs4CemKYd',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::iEBCkrTZHGmF5i3k' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'admin/contests/clone/save',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ContestsController@saveClone',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ContestsController@saveClone',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
        'as' => 'generated::iEBCkrTZHGmF5i3k',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'pages.index' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/pages',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'pages.index',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\PagesController@index',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\PagesController@index',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'pages.create' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/pages/create',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'pages.create',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\PagesController@create',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\PagesController@create',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'pages.store' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'admin/pages',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'pages.store',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\PagesController@store',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\PagesController@store',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'pages.show' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/pages/{page}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'pages.show',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\PagesController@show',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\PagesController@show',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'pages.edit' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/pages/{page}/edit',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'pages.edit',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\PagesController@edit',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\PagesController@edit',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'pages.update' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
        1 => 'PATCH',
      ),
      'uri' => 'admin/pages/{page}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'pages.update',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\PagesController@update',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\PagesController@update',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'pages.destroy' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'admin/pages/{page}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'pages.destroy',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\PagesController@destroy',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\PagesController@destroy',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'partners.index' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/partners',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'partners.index',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\PartnersController@index',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\PartnersController@index',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'partners.create' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/partners/create',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'partners.create',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\PartnersController@create',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\PartnersController@create',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'partners.store' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'admin/partners',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'partners.store',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\PartnersController@store',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\PartnersController@store',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'partners.show' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/partners/{partner}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'partners.show',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\PartnersController@show',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\PartnersController@show',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'partners.edit' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/partners/{partner}/edit',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'partners.edit',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\PartnersController@edit',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\PartnersController@edit',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'partners.update' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
        1 => 'PATCH',
      ),
      'uri' => 'admin/partners/{partner}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'partners.update',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\PartnersController@update',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\PartnersController@update',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'partners.destroy' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'admin/partners/{partner}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'partners.destroy',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\PartnersController@destroy',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\PartnersController@destroy',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'publishers.index' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/publishers',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'publishers.index',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\PublishersController@index',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\PublishersController@index',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'publishers.create' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/publishers/create',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'publishers.create',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\PublishersController@create',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\PublishersController@create',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'publishers.store' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'admin/publishers',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'publishers.store',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\PublishersController@store',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\PublishersController@store',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'publishers.show' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/publishers/{publisher}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'publishers.show',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\PublishersController@show',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\PublishersController@show',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'publishers.edit' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/publishers/{publisher}/edit',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'publishers.edit',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\PublishersController@edit',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\PublishersController@edit',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'publishers.update' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
        1 => 'PATCH',
      ),
      'uri' => 'admin/publishers/{publisher}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'publishers.update',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\PublishersController@update',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\PublishersController@update',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'publishers.destroy' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'admin/publishers/{publisher}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'publishers.destroy',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\PublishersController@destroy',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\PublishersController@destroy',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'adsconfig.index' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/adsconfig',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'adsconfig.index',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\AdsconfigController@index',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\AdsconfigController@index',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'adsconfig.create' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/adsconfig/create',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'adsconfig.create',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\AdsconfigController@create',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\AdsconfigController@create',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'adsconfig.store' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'admin/adsconfig',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'adsconfig.store',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\AdsconfigController@store',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\AdsconfigController@store',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'adsconfig.show' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/adsconfig/{adsconfig}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'adsconfig.show',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\AdsconfigController@show',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\AdsconfigController@show',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'adsconfig.edit' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/adsconfig/{adsconfig}/edit',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'adsconfig.edit',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\AdsconfigController@edit',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\AdsconfigController@edit',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'adsconfig.update' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
        1 => 'PATCH',
      ),
      'uri' => 'admin/adsconfig/{adsconfig}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'adsconfig.update',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\AdsconfigController@update',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\AdsconfigController@update',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'adsconfig.destroy' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'admin/adsconfig/{adsconfig}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'adsconfig.destroy',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\AdsconfigController@destroy',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\AdsconfigController@destroy',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'adsconf.import.view' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/ads_config/import',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\AdsconfigController@import',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\AdsconfigController@import',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
        'as' => 'adsconf.import.view',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'adsconf.import.upload' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'admin/ads_config_import',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\AdsconfigController@importExcel',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\AdsconfigController@importExcel',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
        'as' => 'adsconf.import.upload',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'siteconfig.index' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/siteconfig',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'siteconfig.index',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\SiteconfigController@index',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\SiteconfigController@index',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'siteconfig.create' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/siteconfig/create',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'siteconfig.create',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\SiteconfigController@create',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\SiteconfigController@create',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'siteconfig.store' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'admin/siteconfig',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'siteconfig.store',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\SiteconfigController@store',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\SiteconfigController@store',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'siteconfig.show' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/siteconfig/{siteconfig}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'siteconfig.show',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\SiteconfigController@show',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\SiteconfigController@show',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'siteconfig.edit' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/siteconfig/{siteconfig}/edit',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'siteconfig.edit',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\SiteconfigController@edit',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\SiteconfigController@edit',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'siteconfig.update' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
        1 => 'PATCH',
      ),
      'uri' => 'admin/siteconfig/{siteconfig}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'siteconfig.update',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\SiteconfigController@update',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\SiteconfigController@update',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'siteconfig.destroy' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'admin/siteconfig/{siteconfig}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'siteconfig.destroy',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\SiteconfigController@destroy',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\SiteconfigController@destroy',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'awardmatrix.index' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/awardmatrix',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'awardmatrix.index',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\AwardmatrixController@index',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\AwardmatrixController@index',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'awardmatrix.create' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/awardmatrix/create',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'awardmatrix.create',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\AwardmatrixController@create',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\AwardmatrixController@create',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'awardmatrix.store' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'admin/awardmatrix',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'awardmatrix.store',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\AwardmatrixController@store',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\AwardmatrixController@store',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'awardmatrix.show' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/awardmatrix/{awardmatrix}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'awardmatrix.show',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\AwardmatrixController@show',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\AwardmatrixController@show',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'awardmatrix.edit' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/awardmatrix/{awardmatrix}/edit',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'awardmatrix.edit',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\AwardmatrixController@edit',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\AwardmatrixController@edit',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'awardmatrix.update' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
        1 => 'PATCH',
      ),
      'uri' => 'admin/awardmatrix/{awardmatrix}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'awardmatrix.update',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\AwardmatrixController@update',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\AwardmatrixController@update',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'awardmatrix.destroy' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'admin/awardmatrix/{awardmatrix}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'awardmatrix.destroy',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\AwardmatrixController@destroy',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\AwardmatrixController@destroy',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'admins.index' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/admins',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'admins.index',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\AdminsController@index',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\AdminsController@index',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'admins.create' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/admins/create',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'admins.create',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\AdminsController@create',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\AdminsController@create',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'admins.store' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'admin/admins',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'admins.store',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\AdminsController@store',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\AdminsController@store',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'admins.show' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/admins/{admin}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'admins.show',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\AdminsController@show',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\AdminsController@show',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'admins.edit' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/admins/{admin}/edit',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'admins.edit',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\AdminsController@edit',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\AdminsController@edit',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'admins.update' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
        1 => 'PATCH',
      ),
      'uri' => 'admin/admins/{admin}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'admins.update',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\AdminsController@update',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\AdminsController@update',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'admins.destroy' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'admin/admins/{admin}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'admins.destroy',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\AdminsController@destroy',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\AdminsController@destroy',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'articles.index' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/articles',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'articles.index',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ArticlesController@index',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ArticlesController@index',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'articles.create' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/articles/create',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'articles.create',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ArticlesController@create',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ArticlesController@create',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'articles.store' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'admin/articles',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'articles.store',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ArticlesController@store',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ArticlesController@store',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'articles.show' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/articles/{article}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'articles.show',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ArticlesController@show',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ArticlesController@show',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'articles.edit' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/articles/{article}/edit',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'articles.edit',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ArticlesController@edit',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ArticlesController@edit',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'articles.update' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
        1 => 'PATCH',
      ),
      'uri' => 'admin/articles/{article}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'articles.update',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ArticlesController@update',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ArticlesController@update',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'articles.destroy' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'admin/articles/{article}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'articles.destroy',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ArticlesController@destroy',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ArticlesController@destroy',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'cmsseourls.index' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/cmsseourls',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'cmsseourls.index',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\CmsseourlsController@index',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\CmsseourlsController@index',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'cmsseourls.create' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/cmsseourls/create',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'cmsseourls.create',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\CmsseourlsController@create',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\CmsseourlsController@create',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'cmsseourls.store' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'admin/cmsseourls',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'cmsseourls.store',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\CmsseourlsController@store',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\CmsseourlsController@store',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'cmsseourls.show' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/cmsseourls/{cmsseourl}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'cmsseourls.show',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\CmsseourlsController@show',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\CmsseourlsController@show',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'cmsseourls.edit' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/cmsseourls/{cmsseourl}/edit',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'cmsseourls.edit',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\CmsseourlsController@edit',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\CmsseourlsController@edit',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'cmsseourls.update' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
        1 => 'PATCH',
      ),
      'uri' => 'admin/cmsseourls/{cmsseourl}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'cmsseourls.update',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\CmsseourlsController@update',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\CmsseourlsController@update',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'cmsseourls.destroy' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'admin/cmsseourls/{cmsseourl}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'cmsseourls.destroy',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\CmsseourlsController@destroy',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\CmsseourlsController@destroy',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'contestawardmatrix.index' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/contestawardmatrix',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'contestawardmatrix.index',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ContestawardmatrixController@index',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ContestawardmatrixController@index',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'contestawardmatrix.create' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/contestawardmatrix/create',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'contestawardmatrix.create',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ContestawardmatrixController@create',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ContestawardmatrixController@create',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'contestawardmatrix.store' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'admin/contestawardmatrix',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'contestawardmatrix.store',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ContestawardmatrixController@store',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ContestawardmatrixController@store',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'contestawardmatrix.show' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/contestawardmatrix/{contestawardmatrix}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'contestawardmatrix.show',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ContestawardmatrixController@show',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ContestawardmatrixController@show',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'contestawardmatrix.edit' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/contestawardmatrix/{contestawardmatrix}/edit',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'contestawardmatrix.edit',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ContestawardmatrixController@edit',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ContestawardmatrixController@edit',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'contestawardmatrix.update' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
        1 => 'PATCH',
      ),
      'uri' => 'admin/contestawardmatrix/{contestawardmatrix}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'contestawardmatrix.update',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ContestawardmatrixController@update',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ContestawardmatrixController@update',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'contestawardmatrix.destroy' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'admin/contestawardmatrix/{contestawardmatrix}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'contestawardmatrix.destroy',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ContestawardmatrixController@destroy',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ContestawardmatrixController@destroy',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'contestparticipanthistory.index' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/contestparticipanthistory',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'contestparticipanthistory.index',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ContestparticipanthistoryController@index',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ContestparticipanthistoryController@index',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'contestparticipanthistory.create' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/contestparticipanthistory/create',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'contestparticipanthistory.create',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ContestparticipanthistoryController@create',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ContestparticipanthistoryController@create',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'contestparticipanthistory.store' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'admin/contestparticipanthistory',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'contestparticipanthistory.store',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ContestparticipanthistoryController@store',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ContestparticipanthistoryController@store',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'contestparticipanthistory.show' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/contestparticipanthistory/{contestparticipanthistory}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'contestparticipanthistory.show',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ContestparticipanthistoryController@show',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ContestparticipanthistoryController@show',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'contestparticipanthistory.edit' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/contestparticipanthistory/{contestparticipanthistory}/edit',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'contestparticipanthistory.edit',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ContestparticipanthistoryController@edit',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ContestparticipanthistoryController@edit',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'contestparticipanthistory.update' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
        1 => 'PATCH',
      ),
      'uri' => 'admin/contestparticipanthistory/{contestparticipanthistory}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'contestparticipanthistory.update',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ContestparticipanthistoryController@update',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ContestparticipanthistoryController@update',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'contestparticipanthistory.destroy' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'admin/contestparticipanthistory/{contestparticipanthistory}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'contestparticipanthistory.destroy',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ContestparticipanthistoryController@destroy',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ContestparticipanthistoryController@destroy',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'contestwinners.index' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/contestwinners',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'contestwinners.index',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ContestwinnersController@index',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ContestwinnersController@index',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'contestwinners.create' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/contestwinners/create',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'contestwinners.create',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ContestwinnersController@create',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ContestwinnersController@create',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'contestwinners.store' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'admin/contestwinners',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'contestwinners.store',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ContestwinnersController@store',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ContestwinnersController@store',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'contestwinners.show' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/contestwinners/{contestwinner}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'contestwinners.show',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ContestwinnersController@show',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ContestwinnersController@show',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'contestwinners.edit' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/contestwinners/{contestwinner}/edit',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'contestwinners.edit',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ContestwinnersController@edit',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ContestwinnersController@edit',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'contestwinners.update' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
        1 => 'PATCH',
      ),
      'uri' => 'admin/contestwinners/{contestwinner}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'contestwinners.update',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ContestwinnersController@update',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ContestwinnersController@update',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'contestwinners.destroy' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'admin/contestwinners/{contestwinner}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'contestwinners.destroy',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ContestwinnersController@destroy',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ContestwinnersController@destroy',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'users.index' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/users',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'users.index',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\UsersController@index',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\UsersController@index',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'users.create' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/users/create',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'users.create',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\UsersController@create',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\UsersController@create',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'users.store' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'admin/users',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'users.store',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\UsersController@store',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\UsersController@store',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'users.show' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/users/{user}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'users.show',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\UsersController@show',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\UsersController@show',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'users.edit' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/users/{user}/edit',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'users.edit',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\UsersController@edit',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\UsersController@edit',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'users.update' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
        1 => 'PATCH',
      ),
      'uri' => 'admin/users/{user}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'users.update',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\UsersController@update',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\UsersController@update',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'users.destroy' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'admin/users/{user}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'users.destroy',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\UsersController@destroy',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\UsersController@destroy',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::mOHqiPL3W0t3KBKm' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/users/{id}/history',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\UsersController@getHistory',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\UsersController@getHistory',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
        'as' => 'generated::mOHqiPL3W0t3KBKm',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'userwalletcash.index' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/userwalletcash',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'userwalletcash.index',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\UserwalletcashController@index',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\UserwalletcashController@index',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'userwalletcash.create' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/userwalletcash/create',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'userwalletcash.create',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\UserwalletcashController@create',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\UserwalletcashController@create',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'userwalletcash.store' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'admin/userwalletcash',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'userwalletcash.store',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\UserwalletcashController@store',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\UserwalletcashController@store',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'userwalletcash.show' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/userwalletcash/{userwalletcash}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'userwalletcash.show',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\UserwalletcashController@show',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\UserwalletcashController@show',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'userwalletcash.edit' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/userwalletcash/{userwalletcash}/edit',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'userwalletcash.edit',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\UserwalletcashController@edit',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\UserwalletcashController@edit',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'userwalletcash.update' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
        1 => 'PATCH',
      ),
      'uri' => 'admin/userwalletcash/{userwalletcash}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'userwalletcash.update',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\UserwalletcashController@update',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\UserwalletcashController@update',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'userwalletcash.destroy' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'admin/userwalletcash/{userwalletcash}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'userwalletcash.destroy',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\UserwalletcashController@destroy',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\UserwalletcashController@destroy',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'userwalletcoins.index' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/userwalletcoins',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'userwalletcoins.index',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\UserwalletcoinsController@index',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\UserwalletcoinsController@index',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'userwalletcoins.create' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/userwalletcoins/create',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'userwalletcoins.create',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\UserwalletcoinsController@create',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\UserwalletcoinsController@create',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'userwalletcoins.store' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'admin/userwalletcoins',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'userwalletcoins.store',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\UserwalletcoinsController@store',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\UserwalletcoinsController@store',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'userwalletcoins.show' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/userwalletcoins/{userwalletcoin}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'userwalletcoins.show',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\UserwalletcoinsController@show',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\UserwalletcoinsController@show',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'userwalletcoins.edit' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/userwalletcoins/{userwalletcoin}/edit',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'userwalletcoins.edit',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\UserwalletcoinsController@edit',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\UserwalletcoinsController@edit',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'userwalletcoins.update' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
        1 => 'PATCH',
      ),
      'uri' => 'admin/userwalletcoins/{userwalletcoin}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'userwalletcoins.update',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\UserwalletcoinsController@update',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\UserwalletcoinsController@update',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'userwalletcoins.destroy' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'admin/userwalletcoins/{userwalletcoin}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'userwalletcoins.destroy',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\UserwalletcoinsController@destroy',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\UserwalletcoinsController@destroy',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'userwallettranshistory.index' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/userwallettranshistory',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'userwallettranshistory.index',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\UserwallettranshistoryController@index',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\UserwallettranshistoryController@index',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'userwallettranshistory.create' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/userwallettranshistory/create',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'userwallettranshistory.create',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\UserwallettranshistoryController@create',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\UserwallettranshistoryController@create',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'userwallettranshistory.store' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'admin/userwallettranshistory',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'userwallettranshistory.store',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\UserwallettranshistoryController@store',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\UserwallettranshistoryController@store',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'userwallettranshistory.show' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/userwallettranshistory/{userwallettranshistory}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'userwallettranshistory.show',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\UserwallettranshistoryController@show',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\UserwallettranshistoryController@show',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'userwallettranshistory.edit' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/userwallettranshistory/{userwallettranshistory}/edit',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'userwallettranshistory.edit',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\UserwallettranshistoryController@edit',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\UserwallettranshistoryController@edit',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'userwallettranshistory.update' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
        1 => 'PATCH',
      ),
      'uri' => 'admin/userwallettranshistory/{userwallettranshistory}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'userwallettranshistory.update',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\UserwallettranshistoryController@update',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\UserwallettranshistoryController@update',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'userwallettranshistory.destroy' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'admin/userwallettranshistory/{userwallettranshistory}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'userwallettranshistory.destroy',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\UserwallettranshistoryController@destroy',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\UserwallettranshistoryController@destroy',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::qLHu6PbkVzQcDd3j' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/quizuniqueuser',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ReportsController@quizUniqueUser',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ReportsController@quizUniqueUser',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
        'as' => 'generated::qLHu6PbkVzQcDd3j',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::oVTPzOz8dblelu3g' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/crickuniqueuser',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ReportsController@crickuniqueuser',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ReportsController@crickuniqueuser',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
        'as' => 'generated::oVTPzOz8dblelu3g',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::Em9yVZevdExfJNoo' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/quizcontestplayed',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ReportsController@quizContestPlayed',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ReportsController@quizContestPlayed',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
        'as' => 'generated::Em9yVZevdExfJNoo',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::tgYkkE1OvtxJFxsl' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/crickcontestplayed',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ReportsController@crickContestPlayed',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ReportsController@crickContestPlayed',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
        'as' => 'generated::tgYkkE1OvtxJFxsl',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::1RIERzZ86WGoQ9Cx' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/loginusers',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ReportsController@loginUsers',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ReportsController@loginUsers',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
        'as' => 'generated::1RIERzZ86WGoQ9Cx',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::ZLe8EkUOtpNRCqV0' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/newloginusers',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ReportsController@newLoginUsers',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ReportsController@newLoginUsers',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
        'as' => 'generated::ZLe8EkUOtpNRCqV0',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::OKzpkWoY4mb3Gv2V' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/goolelogins',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ReportsController@gooleLogins',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ReportsController@gooleLogins',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
        'as' => 'generated::OKzpkWoY4mb3Gv2V',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::gbmynxeKBTapDoUD' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/facebooklogins',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ReportsController@facebookLogins',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ReportsController@facebookLogins',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
        'as' => 'generated::gbmynxeKBTapDoUD',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'serverinfo.index' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/serverinfo',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'serverinfo.index',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ServerinfoController@index',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ServerinfoController@index',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'serverinfo.create' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/serverinfo/create',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'serverinfo.create',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ServerinfoController@create',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ServerinfoController@create',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'serverinfo.store' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'admin/serverinfo',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'serverinfo.store',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ServerinfoController@store',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ServerinfoController@store',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'serverinfo.show' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/serverinfo/{serverinfo}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'serverinfo.show',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ServerinfoController@show',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ServerinfoController@show',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'serverinfo.edit' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/serverinfo/{serverinfo}/edit',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'serverinfo.edit',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ServerinfoController@edit',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ServerinfoController@edit',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'serverinfo.update' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
        1 => 'PATCH',
      ),
      'uri' => 'admin/serverinfo/{serverinfo}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'serverinfo.update',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ServerinfoController@update',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ServerinfoController@update',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'serverinfo.destroy' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'admin/serverinfo/{serverinfo}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'serverinfo.destroy',
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\ServerinfoController@destroy',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\ServerinfoController@destroy',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::FbeqFMrkGsZtAyX7' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/unauthorized',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\CmsController@unauthorized',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\CmsController@unauthorized',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
        'as' => 'generated::FbeqFMrkGsZtAyX7',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::ohDTTCg2d5neoPfH' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/testemail',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'uses' => 'App\\modules\\backend\\cms\\Controllers\\CmsController@testEmail',
        'controller' => 'App\\modules\\backend\\cms\\Controllers\\CmsController@testEmail',
        'namespace' => 'App\\modules\\backend\\cms\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
        'as' => 'generated::ohDTTCg2d5neoPfH',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'adminuser.index' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/adminuser',
      'action' => 
      array (
        'module' => 'rbac',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'adminuser.index',
        'uses' => 'App\\modules\\backend\\rbac\\Controllers\\AdminsController@index',
        'controller' => 'App\\modules\\backend\\rbac\\Controllers\\AdminsController@index',
        'namespace' => 'App\\modules\\backend\\rbac\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'adminuser.create' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/adminuser/create',
      'action' => 
      array (
        'module' => 'rbac',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'adminuser.create',
        'uses' => 'App\\modules\\backend\\rbac\\Controllers\\AdminsController@create',
        'controller' => 'App\\modules\\backend\\rbac\\Controllers\\AdminsController@create',
        'namespace' => 'App\\modules\\backend\\rbac\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'adminuser.store' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'admin/adminuser',
      'action' => 
      array (
        'module' => 'rbac',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'adminuser.store',
        'uses' => 'App\\modules\\backend\\rbac\\Controllers\\AdminsController@store',
        'controller' => 'App\\modules\\backend\\rbac\\Controllers\\AdminsController@store',
        'namespace' => 'App\\modules\\backend\\rbac\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'adminuser.show' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/adminuser/{adminuser}',
      'action' => 
      array (
        'module' => 'rbac',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'adminuser.show',
        'uses' => 'App\\modules\\backend\\rbac\\Controllers\\AdminsController@show',
        'controller' => 'App\\modules\\backend\\rbac\\Controllers\\AdminsController@show',
        'namespace' => 'App\\modules\\backend\\rbac\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'adminuser.edit' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/adminuser/{adminuser}/edit',
      'action' => 
      array (
        'module' => 'rbac',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'adminuser.edit',
        'uses' => 'App\\modules\\backend\\rbac\\Controllers\\AdminsController@edit',
        'controller' => 'App\\modules\\backend\\rbac\\Controllers\\AdminsController@edit',
        'namespace' => 'App\\modules\\backend\\rbac\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'adminuser.update' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
        1 => 'PATCH',
      ),
      'uri' => 'admin/adminuser/{adminuser}',
      'action' => 
      array (
        'module' => 'rbac',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'adminuser.update',
        'uses' => 'App\\modules\\backend\\rbac\\Controllers\\AdminsController@update',
        'controller' => 'App\\modules\\backend\\rbac\\Controllers\\AdminsController@update',
        'namespace' => 'App\\modules\\backend\\rbac\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'adminuser.destroy' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'admin/adminuser/{adminuser}',
      'action' => 
      array (
        'module' => 'rbac',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'adminuser.destroy',
        'uses' => 'App\\modules\\backend\\rbac\\Controllers\\AdminsController@destroy',
        'controller' => 'App\\modules\\backend\\rbac\\Controllers\\AdminsController@destroy',
        'namespace' => 'App\\modules\\backend\\rbac\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'menucategory.index' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/menucategory',
      'action' => 
      array (
        'module' => 'rbac',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'menucategory.index',
        'uses' => 'App\\modules\\backend\\rbac\\Controllers\\MenucategoriesController@index',
        'controller' => 'App\\modules\\backend\\rbac\\Controllers\\MenucategoriesController@index',
        'namespace' => 'App\\modules\\backend\\rbac\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'menucategory.create' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/menucategory/create',
      'action' => 
      array (
        'module' => 'rbac',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'menucategory.create',
        'uses' => 'App\\modules\\backend\\rbac\\Controllers\\MenucategoriesController@create',
        'controller' => 'App\\modules\\backend\\rbac\\Controllers\\MenucategoriesController@create',
        'namespace' => 'App\\modules\\backend\\rbac\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'menucategory.store' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'admin/menucategory',
      'action' => 
      array (
        'module' => 'rbac',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'menucategory.store',
        'uses' => 'App\\modules\\backend\\rbac\\Controllers\\MenucategoriesController@store',
        'controller' => 'App\\modules\\backend\\rbac\\Controllers\\MenucategoriesController@store',
        'namespace' => 'App\\modules\\backend\\rbac\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'menucategory.show' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/menucategory/{menucategory}',
      'action' => 
      array (
        'module' => 'rbac',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'menucategory.show',
        'uses' => 'App\\modules\\backend\\rbac\\Controllers\\MenucategoriesController@show',
        'controller' => 'App\\modules\\backend\\rbac\\Controllers\\MenucategoriesController@show',
        'namespace' => 'App\\modules\\backend\\rbac\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'menucategory.edit' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/menucategory/{menucategory}/edit',
      'action' => 
      array (
        'module' => 'rbac',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'menucategory.edit',
        'uses' => 'App\\modules\\backend\\rbac\\Controllers\\MenucategoriesController@edit',
        'controller' => 'App\\modules\\backend\\rbac\\Controllers\\MenucategoriesController@edit',
        'namespace' => 'App\\modules\\backend\\rbac\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'menucategory.update' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
        1 => 'PATCH',
      ),
      'uri' => 'admin/menucategory/{menucategory}',
      'action' => 
      array (
        'module' => 'rbac',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'menucategory.update',
        'uses' => 'App\\modules\\backend\\rbac\\Controllers\\MenucategoriesController@update',
        'controller' => 'App\\modules\\backend\\rbac\\Controllers\\MenucategoriesController@update',
        'namespace' => 'App\\modules\\backend\\rbac\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'menucategory.destroy' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'admin/menucategory/{menucategory}',
      'action' => 
      array (
        'module' => 'rbac',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'menucategory.destroy',
        'uses' => 'App\\modules\\backend\\rbac\\Controllers\\MenucategoriesController@destroy',
        'controller' => 'App\\modules\\backend\\rbac\\Controllers\\MenucategoriesController@destroy',
        'namespace' => 'App\\modules\\backend\\rbac\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'modulelink.index' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/modulelink',
      'action' => 
      array (
        'module' => 'rbac',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'modulelink.index',
        'uses' => 'App\\modules\\backend\\rbac\\Controllers\\ModulelinksController@index',
        'controller' => 'App\\modules\\backend\\rbac\\Controllers\\ModulelinksController@index',
        'namespace' => 'App\\modules\\backend\\rbac\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'modulelink.create' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/modulelink/create',
      'action' => 
      array (
        'module' => 'rbac',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'modulelink.create',
        'uses' => 'App\\modules\\backend\\rbac\\Controllers\\ModulelinksController@create',
        'controller' => 'App\\modules\\backend\\rbac\\Controllers\\ModulelinksController@create',
        'namespace' => 'App\\modules\\backend\\rbac\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'modulelink.store' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'admin/modulelink',
      'action' => 
      array (
        'module' => 'rbac',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'modulelink.store',
        'uses' => 'App\\modules\\backend\\rbac\\Controllers\\ModulelinksController@store',
        'controller' => 'App\\modules\\backend\\rbac\\Controllers\\ModulelinksController@store',
        'namespace' => 'App\\modules\\backend\\rbac\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'modulelink.show' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/modulelink/{modulelink}',
      'action' => 
      array (
        'module' => 'rbac',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'modulelink.show',
        'uses' => 'App\\modules\\backend\\rbac\\Controllers\\ModulelinksController@show',
        'controller' => 'App\\modules\\backend\\rbac\\Controllers\\ModulelinksController@show',
        'namespace' => 'App\\modules\\backend\\rbac\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'modulelink.edit' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/modulelink/{modulelink}/edit',
      'action' => 
      array (
        'module' => 'rbac',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'modulelink.edit',
        'uses' => 'App\\modules\\backend\\rbac\\Controllers\\ModulelinksController@edit',
        'controller' => 'App\\modules\\backend\\rbac\\Controllers\\ModulelinksController@edit',
        'namespace' => 'App\\modules\\backend\\rbac\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'modulelink.update' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
        1 => 'PATCH',
      ),
      'uri' => 'admin/modulelink/{modulelink}',
      'action' => 
      array (
        'module' => 'rbac',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'modulelink.update',
        'uses' => 'App\\modules\\backend\\rbac\\Controllers\\ModulelinksController@update',
        'controller' => 'App\\modules\\backend\\rbac\\Controllers\\ModulelinksController@update',
        'namespace' => 'App\\modules\\backend\\rbac\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'modulelink.destroy' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'admin/modulelink/{modulelink}',
      'action' => 
      array (
        'module' => 'rbac',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'modulelink.destroy',
        'uses' => 'App\\modules\\backend\\rbac\\Controllers\\ModulelinksController@destroy',
        'controller' => 'App\\modules\\backend\\rbac\\Controllers\\ModulelinksController@destroy',
        'namespace' => 'App\\modules\\backend\\rbac\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'module.index' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/module',
      'action' => 
      array (
        'module' => 'rbac',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'module.index',
        'uses' => 'App\\modules\\backend\\rbac\\Controllers\\ModulesController@index',
        'controller' => 'App\\modules\\backend\\rbac\\Controllers\\ModulesController@index',
        'namespace' => 'App\\modules\\backend\\rbac\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'module.create' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/module/create',
      'action' => 
      array (
        'module' => 'rbac',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'module.create',
        'uses' => 'App\\modules\\backend\\rbac\\Controllers\\ModulesController@create',
        'controller' => 'App\\modules\\backend\\rbac\\Controllers\\ModulesController@create',
        'namespace' => 'App\\modules\\backend\\rbac\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'module.store' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'admin/module',
      'action' => 
      array (
        'module' => 'rbac',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'module.store',
        'uses' => 'App\\modules\\backend\\rbac\\Controllers\\ModulesController@store',
        'controller' => 'App\\modules\\backend\\rbac\\Controllers\\ModulesController@store',
        'namespace' => 'App\\modules\\backend\\rbac\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'module.show' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/module/{module}',
      'action' => 
      array (
        'module' => 'rbac',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'module.show',
        'uses' => 'App\\modules\\backend\\rbac\\Controllers\\ModulesController@show',
        'controller' => 'App\\modules\\backend\\rbac\\Controllers\\ModulesController@show',
        'namespace' => 'App\\modules\\backend\\rbac\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'module.edit' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/module/{module}/edit',
      'action' => 
      array (
        'module' => 'rbac',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'module.edit',
        'uses' => 'App\\modules\\backend\\rbac\\Controllers\\ModulesController@edit',
        'controller' => 'App\\modules\\backend\\rbac\\Controllers\\ModulesController@edit',
        'namespace' => 'App\\modules\\backend\\rbac\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'module.update' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
        1 => 'PATCH',
      ),
      'uri' => 'admin/module/{module}',
      'action' => 
      array (
        'module' => 'rbac',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'module.update',
        'uses' => 'App\\modules\\backend\\rbac\\Controllers\\ModulesController@update',
        'controller' => 'App\\modules\\backend\\rbac\\Controllers\\ModulesController@update',
        'namespace' => 'App\\modules\\backend\\rbac\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'module.destroy' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'admin/module/{module}',
      'action' => 
      array (
        'module' => 'rbac',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'module.destroy',
        'uses' => 'App\\modules\\backend\\rbac\\Controllers\\ModulesController@destroy',
        'controller' => 'App\\modules\\backend\\rbac\\Controllers\\ModulesController@destroy',
        'namespace' => 'App\\modules\\backend\\rbac\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'usergroup.index' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/usergroup',
      'action' => 
      array (
        'module' => 'rbac',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'usergroup.index',
        'uses' => 'App\\modules\\backend\\rbac\\Controllers\\UsergroupsController@index',
        'controller' => 'App\\modules\\backend\\rbac\\Controllers\\UsergroupsController@index',
        'namespace' => 'App\\modules\\backend\\rbac\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'usergroup.create' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/usergroup/create',
      'action' => 
      array (
        'module' => 'rbac',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'usergroup.create',
        'uses' => 'App\\modules\\backend\\rbac\\Controllers\\UsergroupsController@create',
        'controller' => 'App\\modules\\backend\\rbac\\Controllers\\UsergroupsController@create',
        'namespace' => 'App\\modules\\backend\\rbac\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'usergroup.store' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'admin/usergroup',
      'action' => 
      array (
        'module' => 'rbac',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'usergroup.store',
        'uses' => 'App\\modules\\backend\\rbac\\Controllers\\UsergroupsController@store',
        'controller' => 'App\\modules\\backend\\rbac\\Controllers\\UsergroupsController@store',
        'namespace' => 'App\\modules\\backend\\rbac\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'usergroup.show' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/usergroup/{usergroup}',
      'action' => 
      array (
        'module' => 'rbac',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'usergroup.show',
        'uses' => 'App\\modules\\backend\\rbac\\Controllers\\UsergroupsController@show',
        'controller' => 'App\\modules\\backend\\rbac\\Controllers\\UsergroupsController@show',
        'namespace' => 'App\\modules\\backend\\rbac\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'usergroup.edit' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/usergroup/{usergroup}/edit',
      'action' => 
      array (
        'module' => 'rbac',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'usergroup.edit',
        'uses' => 'App\\modules\\backend\\rbac\\Controllers\\UsergroupsController@edit',
        'controller' => 'App\\modules\\backend\\rbac\\Controllers\\UsergroupsController@edit',
        'namespace' => 'App\\modules\\backend\\rbac\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'usergroup.update' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
        1 => 'PATCH',
      ),
      'uri' => 'admin/usergroup/{usergroup}',
      'action' => 
      array (
        'module' => 'rbac',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'usergroup.update',
        'uses' => 'App\\modules\\backend\\rbac\\Controllers\\UsergroupsController@update',
        'controller' => 'App\\modules\\backend\\rbac\\Controllers\\UsergroupsController@update',
        'namespace' => 'App\\modules\\backend\\rbac\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'usergroup.destroy' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'admin/usergroup/{usergroup}',
      'action' => 
      array (
        'module' => 'rbac',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'as' => 'usergroup.destroy',
        'uses' => 'App\\modules\\backend\\rbac\\Controllers\\UsergroupsController@destroy',
        'controller' => 'App\\modules\\backend\\rbac\\Controllers\\UsergroupsController@destroy',
        'namespace' => 'App\\modules\\backend\\rbac\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::YgIdmUsslgwkmE76' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/settings',
      'action' => 
      array (
        'module' => 'rbac',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'uses' => 'App\\modules\\backend\\rbac\\Controllers\\AdminsController@getSettings',
        'controller' => 'App\\modules\\backend\\rbac\\Controllers\\AdminsController@getSettings',
        'namespace' => 'App\\modules\\backend\\rbac\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
        'as' => 'generated::YgIdmUsslgwkmE76',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::8k5gjV9Kxi9UvdWo' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'admin/settings',
      'action' => 
      array (
        'module' => 'rbac',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'uses' => 'App\\modules\\backend\\rbac\\Controllers\\AdminsController@applySettings',
        'controller' => 'App\\modules\\backend\\rbac\\Controllers\\AdminsController@applySettings',
        'namespace' => 'App\\modules\\backend\\rbac\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
        'as' => 'generated::8k5gjV9Kxi9UvdWo',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::bbyi6fMIAxJx2lCw' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'admin/usergroup/{id}/assign',
      'action' => 
      array (
        'module' => 'rbac',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'uses' => 'App\\modules\\backend\\rbac\\Controllers\\UsergroupsController@getModules',
        'controller' => 'App\\modules\\backend\\rbac\\Controllers\\UsergroupsController@getModules',
        'namespace' => 'App\\modules\\backend\\rbac\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
        'as' => 'generated::bbyi6fMIAxJx2lCw',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::igiLawrPattKDQsZ' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'admin/usergroup/{id}/assign',
      'action' => 
      array (
        'module' => 'rbac',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'admin',
          2 => 'adminroleauthenticate',
        ),
        'uses' => 'App\\modules\\backend\\rbac\\Controllers\\UsergroupsController@assignModules',
        'controller' => 'App\\modules\\backend\\rbac\\Controllers\\UsergroupsController@assignModules',
        'namespace' => 'App\\modules\\backend\\rbac\\Controllers',
        'prefix' => 'admin',
        'where' => 
        array (
        ),
        'as' => 'generated::igiLawrPattKDQsZ',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::zJEdP8H9oGjOYJkR' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/sample-question/{origin}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'api',
          1 => 'verifyapitoken',
        ),
        'uses' => 'App\\modules\\api\\cms\\Controllers\\CmsController@getSampleQuestion',
        'controller' => 'App\\modules\\api\\cms\\Controllers\\CmsController@getSampleQuestion',
        'namespace' => 'App\\modules\\api\\cms\\Controllers',
        'prefix' => 'api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::zJEdP8H9oGjOYJkR',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::FNUIvHmlprpMK545' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/validateapp',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'api',
          1 => 'verifyapitoken',
        ),
        'uses' => 'App\\modules\\api\\cms\\Controllers\\CmsController@validateApp',
        'controller' => 'App\\modules\\api\\cms\\Controllers\\CmsController@validateApp',
        'namespace' => 'App\\modules\\api\\cms\\Controllers',
        'prefix' => 'api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::FNUIvHmlprpMK545',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::BZ9cQRAM2JqU737u' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/fireevent',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'api',
          1 => 'verifyapitoken',
        ),
        'uses' => 'App\\modules\\api\\cms\\Controllers\\CmsController@quizbyteEventFire',
        'controller' => 'App\\modules\\api\\cms\\Controllers\\CmsController@quizbyteEventFire',
        'namespace' => 'App\\modules\\api\\cms\\Controllers',
        'prefix' => 'api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::BZ9cQRAM2JqU737u',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::lwRTi1wc8UTayimN' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/validateevent',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'api',
          1 => 'verifyapitoken',
        ),
        'uses' => 'App\\modules\\api\\cms\\Controllers\\CmsController@validateEvent',
        'controller' => 'App\\modules\\api\\cms\\Controllers\\CmsController@validateEvent',
        'namespace' => 'App\\modules\\api\\cms\\Controllers',
        'prefix' => 'api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::lwRTi1wc8UTayimN',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::qypCiIEjgsLrVrAm' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => '/',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
        ),
        'uses' => 'App\\modules\\frontend\\cms\\Controllers\\QurekaController@getBase',
        'controller' => 'App\\modules\\frontend\\cms\\Controllers\\QurekaController@getBase',
        'namespace' => 'App\\modules\\frontend\\cms\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'generated::qypCiIEjgsLrVrAm',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::VmUfnMCnk8wixdSW' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'intro',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
        ),
        'uses' => 'App\\modules\\frontend\\cms\\Controllers\\QurekaController@getIntro',
        'controller' => 'App\\modules\\frontend\\cms\\Controllers\\QurekaController@getIntro',
        'namespace' => 'App\\modules\\frontend\\cms\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'generated::VmUfnMCnk8wixdSW',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::VZf2pzGsjq0jvCcs' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'intro/question',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
        ),
        'uses' => 'App\\modules\\frontend\\cms\\Controllers\\QurekaController@getSampleQuestions',
        'controller' => 'App\\modules\\frontend\\cms\\Controllers\\QurekaController@getSampleQuestions',
        'namespace' => 'App\\modules\\frontend\\cms\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'generated::VZf2pzGsjq0jvCcs',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::w3KtBBUzcpToQx0U' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'intro/success',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
        ),
        'uses' => 'App\\modules\\frontend\\cms\\Controllers\\QurekaController@getScore',
        'controller' => 'App\\modules\\frontend\\cms\\Controllers\\QurekaController@getScore',
        'namespace' => 'App\\modules\\frontend\\cms\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'generated::w3KtBBUzcpToQx0U',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::VfiGbnJNhcI85cyQ' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => '{any}',
      'action' => 
      array (
        'module' => 'cms',
        'middleware' => 
        array (
          0 => 'web',
        ),
        'uses' => 'App\\modules\\frontend\\cms\\Controllers\\QurekaController@PageNotFound',
        'controller' => 'App\\modules\\frontend\\cms\\Controllers\\QurekaController@PageNotFound',
        'namespace' => 'App\\modules\\frontend\\cms\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'generated::VfiGbnJNhcI85cyQ',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
        'any' => '^(?!api).*$',
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
  ),
)
);
